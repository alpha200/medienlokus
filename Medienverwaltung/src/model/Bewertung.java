package model;

import java.io.Serializable;

import kontrolle.EmptyStringException;

public class Bewertung implements Serializable {

    private String bewerter;

    private int bewertung;

    private String beschreibung;
    
    /**
     * @param bewerter Bewerter darf nicht null oder leer sein.
     * @param bewertung Bewertung muss zwischen 0 und 5 sein
     * @param beschreibung Beschreibung darf nicht null sein
     * @throws NullPointerException
     * @throws EmptyStringException
     * @throws IllegalArgumentException
     */
    public Bewertung(String bewerter, int bewertung, String beschreibung) {
        super();
        if (bewerter == null)
            throw new NullPointerException("Kein Bewerter vorhanden!");
        if (bewerter.isEmpty())
            throw new EmptyStringException("Bewerter-Feld ist leer!");
        if (bewertung > (0+5))
            throw new IllegalArgumentException("Bewertung zu hoch. Nur Bewertungen zwischen 0 und 5 erlaubt!");
        if (bewertung < 0)
            throw new IllegalArgumentException("Bewertung zu niedrig. Nur Bewertungen zwischen 0 und 5 erlaubt!");
        if (beschreibung == null)
            throw new NullPointerException("Keine Beschreibung vorhanden!");
        this.bewerter = bewerter;
        this.bewertung = bewertung;
        this.beschreibung = beschreibung;
    }

    /**
     * @return Bewerter : String
     */
    public String getBewerter() {
        return bewerter;
    }

    /**
     * @param bewerter : String
     * @throws NullPointerException
     * @throws EmptyStringException
     */
    public void setBewerter(String bewerter) {
    	if (bewerter == null)
            throw new NullPointerException("Kein Bewerter vorhanden!");
        if (bewerter.isEmpty())
            throw new EmptyStringException("Bewerter-Feld ist leer!");    
        this.bewerter = bewerter;
    }

    /**
     * @return Bewertung : int
     */
    public int getBewertung() {
        return bewertung;
    }

    /**
     * @param bewertung : int
     * @throws IllegalArgumentException
     */
    public void setBewertung(int bewertung) {
        if (bewertung > (0+5))
            throw new IllegalArgumentException("Bewertung zu hoch. Nur Bewertungen zwischen 0 und 5 erlaubt!");
        if (bewertung < 0)
            throw new IllegalArgumentException("Bewertung zu niedrig. Nur Bewertungen zwischen 0 und 5 erlaubt!");
        this.bewertung = bewertung;
    }

    /**
     * @return Beschreibung : String
     */
    public String getBeschreibung() {
    	return beschreibung;
    }

    /**
     * @param beschreibung : String
     * @throws NullPointerException
     */
    public void setBeschreibung(String beschreibung) {
        if (beschreibung == null)
            throw new NullPointerException("Keine Beschreibung vorhanden!");
        this.beschreibung = beschreibung;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Bewertung [bewerter=" + bewerter + ", bewertung=" + bewertung + ", beschreibung=" + beschreibung + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Bewertung) {
            Bewertung tmpBewertung = (Bewertung) obj;
            return tmpBewertung.beschreibung.equals(this.beschreibung) && tmpBewertung.bewerter.equals(this.bewerter) && tmpBewertung.bewertung == this.bewertung;
        }
        else {
            return false;
        }
    }
}
