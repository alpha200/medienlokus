package model;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import kontrolle.MissingValueException;

public class Medium implements Serializable {

	private static final long serialVersionUID = -4964170935179018109L;
	private Map<String, String> paramListe;
    private List<Bewertung> bewertungList;
    private List<Verknuepfung> verknuepfungList;
    private MedienArt medienArt;

    static List<String> buchattribute = Arrays.asList("Titel", "Autor", "Genre", "Verliehen an", "Verleihdatum",
            "Kaufpreis", "Jahr", "Speicherort", "ISBN");
    static List<String> filmattribute = Arrays.asList("Titel", "Autor", "Genre", "Verliehen an", "Verleihdatum",
            "Kaufpreis", "Jahr", "Speicherort", "ISAN", "Abspieldauer", "Herkunftsland", "Top-Schauspieler");
    static List<String> musikattribute = Arrays.asList("Titel", "Autor", "Genre", "Verliehen an", "Verleihdatum",
            "Kaufpreis", "Jahr", "Speicherort", "ISAN", "Album", "Titelnummer");
    static List<String> hoerbuchattribute = Arrays.asList("Titel", "Autor", "Genre", "Verliehen an", "Verleihdatum",
            "Kaufpreis", "Jahr", "Speicherort", "Sprecher", "Abspieldauer", "CD-Anzahl");
    static List<String> tvserieattribute = Arrays.asList("Titel", "Autor", "Genre", "Verliehen an", "Verleihdatum",
            "Kaufpreis", "Jahr", "Speicherort", "ISAN", "Sender", "Episodenanzahl", "Abspieldauer", "Herkunftsland", "Top-Schauspieler");
    /**
     * Die ParamListe wird fuer jede Medienart festgelegt.
     * @param paramListe
     * @param medienArt
     */
    public Medium(Map<String, String> paramListe, MedienArt medienArt) {
        this.paramListe = new HashMap<>();
        this.paramListe.put("Titel", paramListe.get("Titel"));
        this.paramListe.put("Autor", paramListe.get("Autor"));
        this.paramListe.put("Genre", paramListe.get("Genre"));
        this.paramListe.put("Verliehen an", paramListe.get("Verliehen an"));
        this.paramListe.put("Verleihdatum", paramListe.get("Verleihdatum"));
        this.paramListe.put("Kaufpreis", paramListe.get("Kaufpreis"));
        this.paramListe.put("Jahr", paramListe.get("Jahr"));
        this.paramListe.put("Speicherort", paramListe.get("Speicherort"));

        switch (medienArt) {
            case FILM:
                this.paramListe.put("ISAN", paramListe.get("ISAN"));
                this.paramListe.put("Abspieldauer", paramListe.get("Abspieldauer"));
                this.paramListe.put("Herkunftsland", paramListe.get("Herkunftsland"));
                this.paramListe.put("Top-Schauspieler", paramListe.get("Top-Schauspieler"));
                break;
            case TVSERIE:
                this.paramListe.put("ISAN", paramListe.get("ISAN"));
                this.paramListe.put("Abspieldauer", paramListe.get("Abspieldauer"));
                this.paramListe.put("Herkunftsland", paramListe.get("Herkunftsland"));
                this.paramListe.put("Top-Schauspieler", paramListe.get("Top-Schauspieler"));
                this.paramListe.put("Sender", paramListe.get("Sender"));
                this.paramListe.put("Episodenanzahl", paramListe.get("Episodenanzahl"));
                break;
            case BUCH: case EBOOK:
                this.paramListe.put("ISBN", paramListe.get("ISBN"));
                break;
            case HOERBUCH:
                this.paramListe.put("Sprecher", paramListe.get("Sprecher"));
                this.paramListe.put("Abspieldauer", paramListe.get("Abspieldauer"));
                this.paramListe.put("CD-Anzahl", paramListe.get("CD-Anzahl"));
                break;
            case MUSIK:
                this.paramListe.put("ISAN", paramListe.get("ISAN"));
                this.paramListe.put("Album", paramListe.get("Album"));
                this.paramListe.put("Titelnummer", paramListe.get("Titelnummer"));
                break;
        }
        
        for(String key : this.paramListe.keySet())
        {
            if(this.paramListe.get(key) == null)
            {
                this.paramListe.put(key, "");
            }
        }

        this.medienArt = medienArt;
        bewertungList = new LinkedList<Bewertung>();
        verknuepfungList = new LinkedList<Verknuepfung>();
    }

    /**
     *
     * @return paramListe : HashMap<>
     */
    public Map<String, String> getParamListe() {
        return new HashMap<>(paramListe);
    }

    /**
     * Weder die Liste noch die Medienart darf null sein.
     * @param paramListe
     * @param art
     * @throws MissingValueException
     */
    public void setParamListe(Map<String, String> paramListe, MedienArt art) throws MissingValueException {
    	if (paramListe == null)
    		throw new NullPointerException("Keine Parameterliste uebergeben!");
    	if (art == null)
    		throw new NullPointerException("Keine Medienart uebergeben!");
        pruefeParamListe(art, paramListe);
        this.paramListe = paramListe;
        medienArt = art;
    }

    /**
     * @return bewertungList : LinkedList<>
     */
    public List<Bewertung> getBewertungList() {
        return new LinkedList<>(bewertungList);
    }

    /**
     * Die Bewertungsliste darf nicht leer und nicht null sein.
     * Außerdem darf die Liste keine null-Werte enthalten.
     * @param bewertungList : List<Bewertung>
     * @throws NullPointerException
     * @throws IllegalArgumentException
     */
    public void setBewertungList(List<Bewertung> bewertungList) {
        if (bewertungList == null)
            throw new NullPointerException("Leere Liste fuer Bewertungen uebergeben!");
        if (bewertungList.contains(null))
            throw new NullPointerException("Leere Bewertung(en) in Liste uebergeben!");
        if (bewertungList.size() == 0)
        	throw new IllegalArgumentException("Keine Bewertung in uebergebener Liste!");
        this.bewertungList = bewertungList;
    }

    /**
     * @return verknuepfungList : LinkedList<>
     */
    public List<Verknuepfung> getVerknuepfungList() {
        return new LinkedList<>(verknuepfungList);
    }

    /**
     * Verknuepfungsliste darf nicht null sein oder null-Werte enthalten.
     * @param verknuepfungList : List<Verknuepfung>
     * @throws NullPointerException
     */
    public void setVerknuepfungList(List<Verknuepfung> verknuepfungList) {
    	if (verknuepfungList == null)
            throw new NullPointerException("Leere Liste fuer Verknuepfungen uebergeben!");
        if (verknuepfungList.contains(null))
            throw new NullPointerException("Leere Verknuepfung(en) in Liste uebergeben!");
        this.verknuepfungList = verknuepfungList;
    }

    /**
     *
     * @return medienart : Medienart
     */
    public MedienArt getMedienArt() {
        return medienArt;
    }

    /**
     * Überprüft, ob die Liste der Parameter eines Mediums alle erforderlichen Angaben enthält,
     * und ob nur zu dem Medium (Buch, Musik, etc.) passende Attribute in der Parameterliste enthalten sind.
     *
     * @param art        Der Typ des Mediums, zu dem die paramListe gehört (Buch, Musik, etc.)
     * @param paramListe Die Parameterliste die auf Konformität überprüft werden soll.
     * @throws MissingValueException
     */
    public static void pruefeParamListe(MedienArt art, Map<String, String> paramListe) throws MissingValueException {
        pruefeEssentiell(paramListe);
        Set<String> keySet = paramListe.keySet();
        Map<String, List<String>> mapTemp = new HashMap<String, List<String>>();
        mapTemp.put("buch", buchattribute);
        mapTemp.put("ebook", buchattribute);
        mapTemp.put("film", filmattribute);
        mapTemp.put("musik", musikattribute);
        mapTemp.put("hoerbuch", hoerbuchattribute);
        mapTemp.put("tvserie", tvserieattribute);

        for(String key : keySet)	{

        	if(!mapTemp.get(art.toString().toLowerCase()).contains(key))		{
        		throw new InvalidParameterException(key + " passt nicht zum Medium " + art.name.toUpperCase() + ".");
        	}
        }
    }

    /**
     * Die Liste muss Titel und Autor enthalten
     * @param paramListe
     * @throws MissingValueException
     */
	private static void pruefeEssentiell(Map<String, String> paramListe)
			throws MissingValueException {
		if (!paramListe.containsKey("Titel") || paramListe.get("Titel").equals("")) {
            throw new MissingValueException("Es muss ein Titel angegeben werden!");
        }
        if (!paramListe.containsKey("Autor") || paramListe.get("Autor").equals("")) {
            throw new MissingValueException("Es muss ein Autor angegeben werden!");
        }
	}

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Medium medium = (Medium) object;

        if (medienArt != medium.medienArt) return false;
        if (!paramListe.get("Autor").equals(medium.getParamListe().get("Autor"))) return false;
        if (!paramListe.get("Titel").equals(medium.getParamListe().get("Titel"))) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = medienArt.hashCode();
        result = 31 * result + paramListe.get("Autor").hashCode();
        result = 31 * result + paramListe.get("Titel").hashCode();
        return result;
    }
}
