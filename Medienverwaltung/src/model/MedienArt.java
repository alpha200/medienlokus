package model;

public enum MedienArt {
    BUCH("Buch"), MUSIK("Musik"), FILM("Film"), HOERBUCH("Hörbuch"), EBOOK("eBook"), TVSERIE("TV-Serie");
    
    public final String name;
	
	private MedienArt(String name) {
		this.name = name;
	}
}

