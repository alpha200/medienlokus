package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import kontrolle.EmptyStringException;

public class Medienverwaltung implements Serializable {

   
	private String benutzer;

    private List<Medium> wunschliste;

    private List<Medium> medienarchiv;


    /**
     * @param benutzer
     * @param wunschliste
     * @param medienarchiv
     */
    public Medienverwaltung(String benutzer, List<Medium> wunschliste, List<Medium> medienarchiv) {
        pruefeBenutzer(benutzer);
        pruefeListe(wunschliste);
        pruefeListe(medienarchiv);
        this.benutzer = benutzer;
        this.wunschliste = wunschliste;
        this.medienarchiv = medienarchiv;
    }

    /**
     * @return Benutzer : String
     */
    public String getBenutzer() {
        return benutzer;
    }

    /**
     * @param benutzer : String
     */
    public void setBenutzer(String benutzer) {
        pruefeBenutzer(benutzer);
        this.benutzer = benutzer;
    }

    /**
     * @return Wunschliste : List<Medium>
     */
    public List<Medium> getWunschliste() {
        return wunschliste;
    }

    /**
     * @param wunschliste : List<Medium>
     */
    public void setWunschliste(List<Medium> wunschliste) {
        pruefeListe(wunschliste);
        this.wunschliste = wunschliste;
    }

    /**
     * @return Medienarchiv : List<Medium>
     */
    public List<Medium> getMedienarchiv() {
        return medienarchiv;
    }

    /**
     * @param medienarchiv : List<Medium>
     */
    public void setMedienarchiv(List<Medium> medienarchiv) {
        pruefeListe(medienarchiv);
        this.medienarchiv = medienarchiv;
    }

    /**
     * Überprüft ob ein Benutzer vorhanden ist und 
     * wirft bei fehlendem Nutzer oder leerem Feld eine Exception.
     * @param benutzer Der zu überprüfende Nutzer
     */
    private void pruefeBenutzer(String benutzer) {
        if (benutzer == null)
            throw new NullPointerException("Kein Benutzer vorhanden!");
        if (benutzer.isEmpty())
            throw new EmptyStringException("Benutzer-Feld ist leer!");
    }

    /**
     * Überprüft Medienliste auf Leerheit und Duplikate
     * @param liste Medienliste
     * @throws NullPointerException Medium ist leer
     * @throws IllegalArgumentException Medium ist bereits in Liste vorhanden
     */
    private void pruefeListe(List<Medium> liste) {
        if (liste == null)
            throw new NullPointerException("Keine Liste vorhanden!");
        List<Medium> hilfsliste = new LinkedList<Medium>();
        for (Medium medium : liste) {
            if (medium == null)
                throw new NullPointerException("Leeres Medium in Liste uebergeben!");
            if (hilfsliste.contains(medium))
                throw new IllegalArgumentException("Medienliste enthaelt Duplikate!");
            hilfsliste.add(medium);
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Medienverwaltung [benutzer=" + benutzer + ", wunschliste=" + wunschliste.toString() + ", medienarchiv="
                + medienarchiv.toString() + "]";
    }
}