package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import kontrolle.EmptyStringException;

public class Verknuepfung implements Serializable {

    private String beschreibung;

    private List<Medium> liste;

    public Verknuepfung(String beschreibung, List<Medium> liste) {
        pruefeBeschreibung(beschreibung);
        pruefeListe(liste);

        this.beschreibung = beschreibung;
        this.liste = liste;
    }

    /**
     * @return Beschreibung : String
     */
    public String gebeBeschreibung() {
        return beschreibung;
    }

    /**
     * @param beschreibung
     */
    public void setzeBeschreibung(String beschreibung) {
        pruefeBeschreibung(beschreibung);
        this.beschreibung = beschreibung;
    }

    /**
     * @return Medienliste : List<Medium>
     */
    public List<Medium> gebeListe() {
        return new LinkedList<>(liste);
    }

    /**
     * @param liste
     */
    public void setzeListe(List<Medium> liste) {
        pruefeListe(liste);
        this.liste = liste;
    }
    
    /**
     * Prueft, ob die Beschreibung leer oder null ist.
     * @param beschreibung
     * @throws NullPointerException
     * @throws EmptyStringException
     */
    private void pruefeBeschreibung(String beschreibung) {
        if (beschreibung == null)
            throw new NullPointerException("Keine Beschreibung vorhanden!");
        if (beschreibung.isEmpty())
            throw new EmptyStringException("Beschreibung ist leer!");
    }
    
    /**
     * Prueft, ob die Liste null ist, Duplikate oder null-Elemente enthaelt.
     * @param liste
     * @throws NullPointerException
     * @throws EmptyStringException
     */
    private void pruefeListe(List<Medium> liste) {
        if (liste == null)
            throw new NullPointerException("Keine Liste vorhanden!");
        List<Medium> hilfsliste = new LinkedList<Medium>();
        for (Medium medium : liste) {
            if (medium == null)
                throw new NullPointerException("Leeres Medium in Liste uebergeben!");
            if (hilfsliste.contains(medium))
                throw new IllegalArgumentException("Medienliste enthaelt Duplikate!");
            hilfsliste.add(medium);
        }
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Verknuepfung [beschreibung=" + beschreibung + ", liste=" + liste.toString() + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((beschreibung == null) ? 0 : beschreibung.hashCode());
        result = prime * result + ((liste == null) ? 0 : liste.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Verknuepfung other = (Verknuepfung) obj;
        if (beschreibung == null && other.beschreibung != null)
            return false;
        if (!(beschreibung != null && beschreibung.equals(other.beschreibung)))
            return false;
        return liste.equals(other.liste);
    }
}
