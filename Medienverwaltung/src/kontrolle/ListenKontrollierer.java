package kontrolle;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.Sides;

import org.apache.commons.io.FileUtils;

import model.Bewertung;
import model.MedienArt;
import model.Medium;
import model.Verknuepfung;

public class ListenKontrollierer {

	protected VerwaltungsKontrollierer verwaltungsKontrollierer;
	//private String dataPath = "output";

	public ListenKontrollierer(VerwaltungsKontrollierer kontrollierer) {
		verwaltungsKontrollierer = kontrollierer;
	}

	/**
	 * Druckt die übergebenen Medien aus
	 *
	 * @param selektierteMedien - Die Liste der Medien, die gedruckt werden sollen
	 * @param pfad - Der pfad der Textdatei. ist dieser null, wird tatsächlich gedruckt
	 * @param doPrint - Ob auf Papier gedruckt werden soll oder nicht (nur für Test)
	 * @throws IOException 
	 * @throws PrinterException 
	 */
	public void medienDrucken(List<Medium> selektierteMedien, String pfad, boolean doPrint) throws IOException {
	    System.out.println("Drucke " + selektierteMedien.size() + " Medien");
		
		ArrayList<String> lines = new ArrayList<>();
	
	    //create each line
	    for (Medium medium : selektierteMedien) {
	        Map<String, String> params = medium.getParamListe();
	        List<Bewertung> bewertungen = medium.getBewertungList();
	        MedienArt typ = medium.getMedienArt();
	        lines.add(params.get("Titel"));
	        lines.add("\tTyp: " + typ.name + "; Autor: " + params.get("Autor") + "; Genre: " + params.get("Genre"));
	        lines.add("\tErscheinungsJahr: " + params.get("Jahr") + "; Speicher-/Lagerort: " + params.get("Speicherort"));
	        //Parameter
	        StringBuilder stringBuilder = new StringBuilder("\t");
	        Set<String> keys = params.keySet();
	        Object[] temparray = {"Titel", "Autor", "Genre", "Jahr", "SpeicherOrt"};
	        for(String key: keys) {
	        	if(!this.oneOf(key, temparray)) {
	        		String buf = params.get(key);
	        		if(buf != null)
	        			stringBuilder.append(key + ": " + buf + "; ");
	        	}
	        }
	        lines.add(stringBuilder.toString());            
	        //Bewertung
	        for (int i = 0; i < bewertungen.size(); i++) {
	            Bewertung bew = bewertungen.get(i);
	            lines.add("\tBewerter: " + bew.getBewerter() + "; Bewertung: " + bew.getBewertung() + "; Beschreibung: " + bew.getBeschreibung());
	        }
	        //2 empty lines as a separation
	        lines.add("");
	        lines.add("");
	    }
	
		File file = new File(pfad);
		FileUtils.writeLines(file, lines);
	    System.out.println("Ausgabe in Datei beendet");
	    if(doPrint) {
			this.print();
		    System.out.println("Druck beendet");
	    }
	}

	/**
	 * Checks if the given object is at least one of the given objects. Comparison is done via 
	 * @param object
	 * @param objectsToCompare
	 * @return true, if object is equal to aat least one of the objects in objectsToCompare
	 */
	private boolean oneOf(Object objectParam, Object... objectToCompare) {
		for(Object element : objectToCompare)	{
			if(element.equals(objectParam))	{
				return true;
			}
		}
		return false;    	
	}

	private void print(){
		PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
	    pras.add(new Copies(1));
	    PrinterJob pJob = PrinterJob.getPrinterJob();
	    String printerName = "";
	    if(pJob.printDialog())	{
	    	printerName = pJob.getPrintService().getName();
	    }
	    PrintService pss[] = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.GIF, pras);
	    PrintService ps = null;
	    for(int z = 0; z<pss.length; z++){
	    	if(printerName.equals(pss[z].getName()))	{
	    		ps = pss[z];
	    		break;
	    	}
	    }
	    if (ps == null || "".equals(printerName))	{
	    	return;
	    }
	    System.out.println("Printing to " + ps);
	    DocPrintJob job = ps.createPrintJob();
	    try{
	    	
		    FileInputStream fin = new FileInputStream("output/Druck.txt");
		    HashDocAttributeSet aset = new HashDocAttributeSet();
		    aset.add(OrientationRequested.PORTRAIT);
            aset.add(Sides.ONE_SIDED); 
            aset.add(MediaSizeName.ISO_A4);
		    Doc doc = new SimpleDoc(fin, DocFlavor.INPUT_STREAM.AUTOSENSE, aset);
		    
		    job.print(doc, pras);
		    fin.close();
	    } catch(Exception e) {
	    	e.printStackTrace();
	    }
	}

	/**
	 * Erstellt ein neues {@link model.Medium Medium} mit dem gegebenen Typ und den gegebenen Parametern
	 * und fügt es der Wunschliste hinzu.
	 *
	 * @param art        - Die Art des Mediums
	 * @param paramListe - Die Parameterliste für das Medium
	 * @throws MissingValueException
	 * @throws DuplicateMediumException
	 */
	public void inWunschlisteHinzufuegen(MedienArt art, Map<String, String> paramListe)
			throws MissingValueException, DuplicateMediumException {
			    List<Medium> wunschListe = verwaltungsKontrollierer.getMedienVerwaltung().getWunschliste();
			
			    Medium medium = verwaltungsKontrollierer.getMedienKontrollierer().erstelleMedium(art, paramListe);
			
			    if (wunschListe.contains(medium)) {
			        throw new DuplicateMediumException("Medium existiert bereits in Wunschliste.");
			    }
			
			    wunschListe.add(medium);
			}

	/**
	 * Erstellt ein neues {@link model.Medium Medium} mit dem gegebenen Typ und den gegebenen Parametern
	 * und fügt es dem Medienarchiv hinzu.
	 *
	 * @param art        - Die Art des Mediums
	 * @param paramListe - Die Parameterliste für das Medium
	 * @throws MissingValueException
	 * @throws DuplicateMediumException
	 */
	public void inMedienarchivHinzufuegen(MedienArt art, Map<String, String> paramListe)
			throws MissingValueException, DuplicateMediumException {
			    List<Medium> medienArchiv = verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv();
			
			    Medium medium = verwaltungsKontrollierer.getMedienKontrollierer().erstelleMedium(art, paramListe);
			
			    if (medienArchiv.contains(medium)) {
			        throw new DuplicateMediumException("Medium existiert bereits in Medienarchiv.");
			    }
			
			    medienArchiv.add(medium);
			}

	/**
	 * Löscht das übergebene Medium aus der angegebenen Liste.
	 *
	 * @param medium    Das übergebene Medium.
	 * @param listenArt Listenidentifizierer.
	 */
	public void loescheMedium(Medium medium, ListenArt listenArt) {
	
	    if (medium == null || listenArt == null) {
	        throw new IllegalArgumentException("none of the arguments must be null");
	    }
	
	    try {
			for (Verknuepfung verknuepfung : medium.getVerknuepfungList()) {
				if(verknuepfung.gebeListe().contains(medium))
			    	verwaltungsKontrollierer.getVerknuepfungsKontrollierer().entferneMediumAusVerknuepfung(medium, verknuepfung);
			}
		} catch (InformationException e) {
			if (listenArt == ListenArt.MEDIENARCHIV) {
		        verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv().remove(medium);
		    } else if (listenArt == ListenArt.WUNSCHLISTE) {
		        verwaltungsKontrollierer.getMedienVerwaltung().getWunschliste().remove(medium);
		    }
		}
	
	    if (listenArt == ListenArt.MEDIENARCHIV) {
	        verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv().remove(medium);
	    } else if (listenArt == ListenArt.WUNSCHLISTE) {
	        verwaltungsKontrollierer.getMedienVerwaltung().getWunschliste().remove(medium);
	    }
	
	}

}