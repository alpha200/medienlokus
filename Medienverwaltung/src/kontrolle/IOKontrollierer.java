package kontrolle; 


import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Bewertung;
import model.MedienArt;
import model.Medium;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


/**
 * Kontroller zur Verwaltung von Medienlisten
 */
public class IOKontrollierer extends ListenKontrollierer {

	///**
	// * @deprecated 
	// * use another constructor
	// */
	//public IOKontrollierer(){
		// Leerer Konstruktor fuer Tests.
	//}
	
	public IOKontrollierer(VerwaltungsKontrollierer kontrollierer) {
		super(kontrollierer);
	}
	
	/**
	 * Importiert/aktualisiert die Medieninformationen von Medien anderer Benutzer aus einer XML-Datei.
	 * @param dateiname 
	 * 					Der Dateipfad, der auf ein XML-Dokument verweist. 
	 * 					In diesem Dokument befinden sich die Parameter der zu importierenden Medien.
	 * @throws MissingValueException 
	 */
	public int[] importiereMedien(String dateiname) throws MissingValueException,NumberFormatException,JDOMException,IOException {
		if(dateiname == null || dateiname.equals(""))
			throw new IllegalArgumentException("Dateiname ist leer oder nicht vorhanden.");
		Document doc;
		int duplo = 0 ,neue = 0,fehler = 0, skip = 0;
		try{
			doc = new SAXBuilder().build( dateiname );		//JDOMException sowie IOException
		}
		catch(JDOMException ex)
		{
			throw new JDOMException("Datei ist leer oder hat ein falsches Format.");
		}
		Element medienExport = doc.getRootElement();				//hole wurzelknoten der XML-Datei
		String bewerter = null;										//hole den bewerter aus dem ersten Kindknoten	
		if(medienExport.getChild("bewerter") != null && !medienExport.getChild("bewerter").getValue().equals(""))
			bewerter = medienExport.getChild("bewerter").getValue();
		else
			throw new MissingValueException("kein Bewerter vorhanden!");		
		for(int i = 1; i < medienExport.getChildren().size(); i++) {				//gehe durch die restlichen kindknoten/medien
            Element medium = medienExport.getChildren().get(i);
            Medium neuesMedium;
            String beschreibung;
            MedienArt typ;
            int bew;
            try{//hole medium an stelle i           
            beschreibung = holeBeschreibung(medium);
            bew = wandleBewertung(medium);
            typ = typUmwandlung(medium);
            neuesMedium = verwaltungsKontrollierer.getMedienKontrollierer().erstelleMedium(typ, erstelleParamList(medium));}	 //erstelle neues medium mit typ und parameterliste
            catch(Exception ex) {	fehler++;	continue;	}
            List<Medium> medienArchiv = verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv(); 	//hole Medienarchiv um auf duplikate zu ueberpruefen
            boolean contains = false;
			for (Medium aMedienArchiv : medienArchiv) {            //gehe archiv durch und...
				if (aMedienArchiv.equals(neuesMedium)) {        //...falls das neueMedium in liste vorhanden...
					contains = true;                                //hänge importierte bewertung an die vorhandene berwertungsliste
					List<Bewertung> bewlist = aMedienArchiv.getBewertungList();
					Bewertung neueBewertung = new Bewertung(bewerter, bew, beschreibung);
					if(bewlist.contains(neueBewertung)){
						skip++;
						break;
					}
					else{
						bewlist.add(new Bewertung(bewerter, bew, beschreibung));
						aMedienArchiv.setBewertungList(bewlist);
						duplo++;
						break;
					}		
				}
			}
            if(!contains){		//falls neuesMedium nicht in medienarchiv vorhanden hänge die Bewertung an die bewertungsliste und fuege neuesMedium hinzu
            	List<Bewertung> bewlist = neuesMedium.getBewertungList();
            	bewlist.add(new Bewertung(bewerter,bew,beschreibung));
            	neuesMedium.setBewertungList(bewlist);
            	Map<String, String> paramMedium = neuesMedium.getParamListe();
            	paramMedium.put("Speicherort", "bei " + bewerter);
            	neuesMedium.setParamListe(paramMedium, typ);
            	medienArchiv.add(neuesMedium);
            	neue++;}
        }
		return new int[]{duplo,neue,fehler,skip};
	}
	
	/**
	 * Erstellt eine ParameterListe aus dem XML Element
	 * @param medium	Das XML Element
	 * @return param	Erstellte ParameterListe
	 * @throws MissingValueException
	 */
	private Map<String,String> erstelleParamList(Element medium) throws MissingValueException{
		//hole attributwerte aus medium
        String titel = medium.getChild("titel").getValue();
        String autor = medium.getChild("autor").getValue();
        if(	titel == null || titel.equals("") || autor == null || autor.equals(""))
        	throw new MissingValueException("Essentielles Attribut fehlt oder ist leer!");
        //erstelle parameterliste
        Map<String,String> param = new HashMap<>();
        //setze werte der parameterliste
        param.put("Titel",titel);
        param.put("Autor",autor);		
		return param;		
	}
	
	/**
	 * holt die (optionale) Beschreibung aus dem XML Element
	 * @param medium		Das XML Element
	 * @return beschreibung	Die Beschreibung oder ""
	 */
	private String holeBeschreibung(Element medium){
		String beschreibung = "";			//hole evtl die beschreibung sonst lasse leer
        if(medium.getChild("beschreibung") != null)         
        	beschreibung = medium.getChild("beschreibung").getValue();
        return beschreibung;		
	}
	
	/**
	 * wandelt die String Bewertung aus dem XML Element in int
	 * @param medium		Das XML Element
	 * @return bewertung	die umgewandelte Bewertung
	 * @throws MissingValueException
	 */
	private int wandleBewertung(Element medium) throws MissingValueException{
		String bewertung = medium.getChild("bewertung").getValue();           	//hole attributwerte aus medium 
        if(bewertung == null || bewertung.equals(""))
        		throw new MissingValueException("Essentielles Attribut fehlt oder ist leer!");
        int bew;
        try{	
        	//wandle bewertung string in int
            bew = Integer.parseInt(bewertung);	//NumberFormatException evtl
        }
        catch(NumberFormatException ex)
        {
        	throw new MissingValueException("die Bewertung war Ungültig!");
        }
        return bew;
	}
	 
	/**
	 * wandelt den String Medientyp des XML Elements in MedienArt um
	 * @param medium	Das XML Element
	 * @return art		Umgewandelter Typ
	 * @throws MissingValueException
	 */
	private MedienArt typUmwandlung(Element medium) throws MissingValueException{
		String typ = medium.getChild("typ").getValue();
		if(typ == null || typ.equals(""))
			throw new MissingValueException("Essentielles Attribut fehlt oder ist leer!");
		//hole typ als String und wandle in MedienArt um
        MedienArt art;
        switch(typ.toLowerCase())
        {
        case "buch":
        	art = MedienArt.BUCH;
        	break;
        case "ebook":
        	art = MedienArt.EBOOK;
        	break;
        case "film":
        	art = MedienArt.FILM;
        	break;
        case "musik":
        	art = MedienArt.MUSIK;
        	break;
        case "hoerbuch":
        	art = MedienArt.HOERBUCH;
        	break;
        case "tv-serie":
        	art = MedienArt.TVSERIE;
        	break;
        default:
        	throw new IllegalArgumentException("unbekannter Typ angegeben");
        }
        return art;
	}
	
	/**
	 * Exportiert die Medieninformationen der übergebenen Medien in eine XML-Datei.
	 * @param selektierteMedien 
	 * 							Die Liste der Medien, die exportiert werden sollen
	 * @throws MissingValueException
	 * @throws RatingFormatException
	 */
	public void exportiereMedien(List<Medium> selektierteMedien,String dateipfad) throws MissingValueException,RatingFormatException,IOException {
		if(selektierteMedien.size() == 0)
			throw new IllegalArgumentException("Liste ist Leer");
		String benutzer = verwaltungsKontrollierer.getMedienVerwaltung().getBenutzer(); //benutzer des systems zwischenspeichern und xml element benutzer erstellen
		Element bewerter = new Element("bewerter"); //benutzer element den benutzer in form eines strings übergeben		
		bewerter.addContent(benutzer);	
		Element medienExport = new Element("medienExport"); //neuen Wurzelknoten erstellen
		medienExport.addContent(0, bewerter); //dem wurzelknoten den benutzer an stelle 0 hinzufügen
		for(int i = 0;i < selektierteMedien.size();i++) { //liste der selektierten medien durchgehen
			Medium selMed = selektierteMedien.get(i); //medium an stelle i zwischenspeichern
			Element medium = erstelleElement(selMed);
			medienExport.addContent(i+1, medium); //fuege dem wurzelknoten das medium an geeigneter stelle hinzu(stelle 0 bereits belegt durch bewerter)
		}
		Document doc = new Document(medienExport); //erstelle XML-Document mit dem wurzelknoten
		XMLOutputter out = new XMLOutputter(Format.getPrettyFormat());
		FileOutputStream fileStream = new FileOutputStream(dateipfad);
		out.output( doc, fileStream );
		fileStream.close();
	}
	
	/**
	 * erstellt aus dem übergebenen Medium ein XML Element
	 * @param selMed	Medium aus dem das Element erstellt werden soll
	 * @return medium	Das XML Element
	 * @throws MissingValueException
	 * @throws RatingFormatException
	 */
	private Element erstelleElement(Medium selMed) throws MissingValueException,
			RatingFormatException {
		Map<String, String> paramList = pruefeMedium(selMed); //parameterliste abrufen
		Element medium = new Element("medium");
		Element titel = new	Element("titel");
		Element autor = new	Element("autor");
		Element bewertung = new	Element("bewertung");
		Element typ = typUmwandlung(selMed.getMedienArt()); 		//je nach medienart fuege passenden typ dem XML element hinzu
		Element beschreibung = new Element("beschreibung");
		if(!selMed.getBewertungList().get(0).getBeschreibung().equals("")){ //falls beschreibung vorhanden fuege sie dem xml element hinzu
			beschreibung.addContent(selMed.getBewertungList().get(0).getBeschreibung());
		}			
		if(selMed.getBewertungList().get(0).getBewertung() >= 0 && selMed.getBewertungList().get(0).getBewertung() < 6){
			bewertung.addContent(selMed.getBewertungList().get(0).getBewertung()+""); //bewertung als string dem XML element hinzufuegen
		}
		else{
			throw new RatingFormatException("die Bewertung hat ein ungültiges Format");
		}
		titel.addContent(paramList.get("Titel")); //titel und autor den XML elementen hinzufuegen
		autor.addContent(paramList.get("Autor"));		
		medium.addContent(titel);	//fuege dem elternknoten medium die kindknoten in passender reihenfolge hinzu
		medium.addContent(autor);
		medium.addContent(typ);
		if(!selMed.getBewertungList().get(0).getBeschreibung().equals("")){
			medium.addContent(beschreibung);
		}
		medium.addContent(bewertung);
		return medium;
	}
	/**
	 * pruefe Medium und gebe paramList zurueck
	 * @param selMed das Medium, dass geprüft wird
	 * @return paramList
	 * @throws MissingValueException
	 */
	private Map<String, String> pruefeMedium(Medium selMed) throws MissingValueException{
		Map<String, String> paramList = selMed.getParamListe();
		//XML elemente erstellen
		if(		paramList.get("Titel") == null || paramList.get("Titel").equals("") ||
				paramList.get("Autor") == null || paramList.get("Autor").equals("") ||
				selMed.getMedienArt() == null)
		{
			throw new MissingValueException("Ein essentielles Attribut ist leer oder fehlt");
		}
		return paramList;
	}
	private Element typUmwandlung(MedienArt paramTyp){
		Element typ = new Element("typ");
		switch(paramTyp)
		{
		case BUCH:
			typ.addContent("Buch");
			break;
		case FILM:
			typ.addContent("Film");
			break;
		case MUSIK:
			typ.addContent("Musik");
			break;
		case HOERBUCH:
			typ.addContent("Hoerbuch");
			break;
		case TVSERIE:
			typ.addContent("TV-Serie");
			break;
		case EBOOK:
			typ.addContent("eBook");
			break;
		}
		return typ;
	}

}
