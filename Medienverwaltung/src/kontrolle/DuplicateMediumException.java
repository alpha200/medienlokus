package kontrolle;

public class DuplicateMediumException extends Exception {

    public DuplicateMediumException(String string) {
        super(string);
    }
}
