package kontrolle;

public class MissingValueException extends Exception {

	public MissingValueException(String string) {
        super(string);
    }

}
