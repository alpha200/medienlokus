package kontrolle;

import javax.swing.JOptionPane;

import model.Medienverwaltung;
import model.Medium;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.FileInputStream;
import java.io.File;
import java.net.URI;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VerwaltungsKontrollierer {

	//Speichert die Medienverwaltung
	private Medienverwaltung medienVerwaltung;
	//Speichert den Listenkontrollierer
	private ListenKontrollierer listenKon;
	//Speichert den Listenkontrollierer
	private IOKontrollierer ioKon;
	//Speichert den Medienkontrollierer
	private MedienKontrollierer medienKon;
	//Speichert den Suchkontrollierer
	private SuchKontrollierer suchKon;
	//Speichert den VerknuepfungsKontrollierer
	private VerknuepfungsKontrollierer verknuepfungsKon;
	
	/**
	 * alternative constructor with gui interface for asking user
	 * after his name and setting it as username further
	 * @deprecated please use hauptfenster.java to start or 
	 * alternative VerwaltungsKontrollierer(String) constructor
	 */
	public VerwaltungsKontrollierer() {
		super();
		this.listenKon = new ListenKontrollierer(this);
		this.medienKon = new MedienKontrollierer(this);
		this.suchKon = new SuchKontrollierer(this);
		this.verknuepfungsKon = new VerknuepfungsKontrollierer();		
		//Suche nach vorhandenen Config
		boolean isExceptionOccured = false;
		try	{
			this.medienVerwaltung = this.ladeSitzung();
			Object[] options = {"Alles cool"};
			JOptionPane.showOptionDialog(null,"Es liegt ein altes Archiv vor. Das Archiv wird geladen." , "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		}
		catch (NullPointerException | IOException | ClassNotFoundException ex)	{
			isExceptionOccured = true;
		}
        if(isExceptionOccured)	{
			String eingabeWert = "";
			Object[] options = {"Mach das"};
			JOptionPane.showOptionDialog(null,"Es liegt kein altes Archiv vor. Neues Archiv wird angelegt" , "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			while("".equals(eingabeWert)){
				eingabeWert = JOptionPane.showInputDialog("Gebe den Benutzernamen ein.");
			}
			if(eingabeWert == null)	{
				JOptionPane.showOptionDialog(null,"Das Programm wird beendet. Tschüss!" , "Beenden", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				System.exit(0);
			}
			else
			{
				//Erstelle eine neue Verwaltung
				final List<Medium> archivListe = new ArrayList<Medium>();
				final List<Medium> wunschListe = new ArrayList<Medium>();
				this.medienVerwaltung = new Medienverwaltung(eingabeWert, archivListe, wunschListe);
				//Akzeptiere die AGB
				boolean agbAccepted = false;
				Object[] opts = {"Nö", "Danke"};
				String optText = "Vielen Dank, " + eingabeWert + ", dass Sie sich für unseres \ninnovatives Produkt entschieden haben.\n\nSie benutzen zurzeit die Testversion des Programms.\nDas Design-Team wünscht Ihnen viel Erfolg und Geduld\nbei der Arbeit mit unserem Tool!";
				while(!agbAccepted)	{
					int result = JOptionPane.showOptionDialog(null,optText , "Willkommen", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, opts, opts[0]);
					switch(result)	{
					case 1:
						{
							agbAccepted = true;
							break;
						}
					}				
				}
			}
		}
	}
	
	
	/**
	 * default constructor, declares intern attributes and sets username
	 * @param benutzer
	 * username value for creating archive
	 */
	public VerwaltungsKontrollierer(final String benutzer) {
		super();
		this.listenKon = new ListenKontrollierer(this);
		this.ioKon = new IOKontrollierer(this);
		this.medienKon = new MedienKontrollierer(this);
		this.suchKon = new SuchKontrollierer(this);
		this.verknuepfungsKon = new VerknuepfungsKontrollierer();		
		final List<Medium> archivListe = new ArrayList<Medium>();
		final List<Medium> wunschListe = new ArrayList<Medium>();
		this.medienVerwaltung = new Medienverwaltung(benutzer, archivListe, wunschListe);
	}
	
	/**
	 * @return the medienVerwaltung
	 */
	public Medienverwaltung getMedienVerwaltung() {
		return medienVerwaltung;
	}
	
	/**
	 * @param medienVerwaltung the medienVerwaltung to set
	 */
	public void setMedienVerwaltung(final Medienverwaltung medienVerwaltung) {
		this.medienVerwaltung = medienVerwaltung;
	}

   
    /**
     * @return the listenKontrollierer
     */
    public ListenKontrollierer getListenKontrollierer() {
        return listenKon;
    }

    /**
     * @param listenK the listenKontrollierer to set
     */
    public void setListenKontrollierer(final ListenKontrollierer listenK) {
        this.listenKon = listenK;
    }
    /**
     *
     */
    public IOKontrollierer getIOKontrollierer() {
		return ioKon;
	}

    /**
     * 
     * @param ioK the new ioKontrollierer
     */
	public void setIOKontrollierer(IOKontrollierer ioK) {
		this.ioKon = ioK;
	}

    /**
     * @return the medienKontrollierer
     */
    public MedienKontrollierer getMedienKontrollierer() {
        return medienKon;
    }

    /**
     * @param medienK the medienKontrollierer to set
     */
    public void setMedienKontrollierer(final MedienKontrollierer medienK) {
        this.medienKon = medienK;
    }

    /**
     * @return the suchKontrollierer
     */
    public SuchKontrollierer getSuchKontrollierer() {
        return suchKon;
    }

    /**
     * @param suchK the suchKontrollierer to set
     */
    public void setSuchKontrollierer(final SuchKontrollierer suchK) {
        this.suchKon = suchK;
    }

    /**
     * @return the verknuepfungsKontrollierer
     */
    public VerknuepfungsKontrollierer getVerknuepfungsKontrollierer() {
        return verknuepfungsKon;
    }

    /**
     * @param verknuepfungK the verknuepfungsKontrollierer to set
     */
    public void setVerknuepfungsKontrollierer(final VerknuepfungsKontrollierer verknuepfungK) {
        this.verknuepfungsKon = verknuepfungK;
    }

    /**
     * @param aktuelleSitzung Übergebe als Parameter ein Medienverwaltung-Objekt, das zu serializieren ist
     * @return boolean
     * true wird im Erfolgsfall zurückgegeben, sonst false.
     * @throws IOException Exception wird geworfen, wenn die Speicherdatei nicht erstellt bzw. gefunden
     *                     oder geschlossen werden konnte.
     */
    public boolean speicherSitzung(final Medienverwaltung aktuelleSitzung) throws NullPointerException, IOException {
        if (aktuelleSitzung == null) {
            throw new NullPointerException("Fehler : Das Parameter ist nicht initialisiert.");
        }
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final ObjectOutput outputObj = new ObjectOutputStream(outputStream);
        byte[] exportArray;
        new File("output").mkdir();
        try {
            outputObj.writeObject(aktuelleSitzung);
            exportArray = outputStream.toByteArray();
            final FileOutputStream fileStream = new FileOutputStream("output/config.ser");
            fileStream.write(exportArray);
            fileStream.close();
        } catch (IOException exception) {
            throw new IOException("Fehler : Die Konfigurationsdatei konnte nicht erstellt/geladen werden.");
        } finally {
            try {
                outputObj.close();
                outputStream.close();
            } catch (IOException exception) {
                throw new IOException("Fehler : Die Konfigurationsdatei konnte nicht geschlossen werden.");
            }
        }

        return true;
    }


    /**
     * @return Medienverwaltung
     * @throws IOException            Exception wird geworfen, wenn die Konfigurationsdatei nicht gefunden,
     *                                geladen oder geschlossen werden konnte.
     * @throws ClassNotFoundException Exception wird geworfen, wenn die geladenen Objekte kein Serializeable
     *                                Interface implementieren.
     */
    public Medienverwaltung ladeSitzung() throws NullPointerException, IOException, ClassNotFoundException {
        final byte[] inputBuff;
        try {
            //noinspection ResultOfMethodCallIgnored
            inputBuff = Files.readAllBytes(FileSystems.getDefault().getPath("output/config.ser"));
        } catch (IOException exception) {
            throw new IOException("Fehler : Die Konfigurationsdatei konnte nicht geladen werden.");
        }
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputBuff);
        ObjectInput inputObj = null;
        Medienverwaltung obiVerwaltung = null;
        try {
            inputObj = new ObjectInputStream(inputStream);
            obiVerwaltung = (Medienverwaltung) inputObj.readObject();
        } catch (ClassNotFoundException exception) {
            throw new ClassNotFoundException("Fehler : Das Objekt implementiert kein Serializeable Interface.");
        } catch (IOException exception) {
            throw new IOException("Fehler : Der Inhalt der Konfigurationsdatei konnte nicht geladen werden.");
        } finally {
            try {
                inputStream.close();
                if (inputObj != null) {
                    inputObj.close();
                }
            } catch (IOException exception) {
                throw new IOException("Fehler : Die Konfigurationsdatei konnte nicht geschlossen werden.");
            }
        }
        this.setMedienVerwaltung(obiVerwaltung);
        return obiVerwaltung;
    }


	
}


