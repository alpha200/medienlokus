package kontrolle;

public class EmptyStringException extends RuntimeException {

	public EmptyStringException() {

    }

    public EmptyStringException(String string) {
        super(string);
    }

}
