package kontrolle;

public class InformationException extends RuntimeException {

	public InformationException(String message) {
		super(message);
	}
}
