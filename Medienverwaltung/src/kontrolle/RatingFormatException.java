package kontrolle;

public class RatingFormatException extends Exception {

    public RatingFormatException(String string) {
        super(string);
    }
}
