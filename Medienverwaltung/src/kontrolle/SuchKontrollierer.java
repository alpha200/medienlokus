package kontrolle;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import model.MedienArt;
import model.Medium;

public class SuchKontrollierer {

    private VerwaltungsKontrollierer verwaltungsKontrollierer;

    public SuchKontrollierer(VerwaltungsKontrollierer verwaltungsKontrollierer) {
        this.verwaltungsKontrollierer = verwaltungsKontrollierer;
    }

    /**
     * Die universelle Suche durchsucht alle Parameter der Medien
     *
     * @param suchbegriff Der Suchbegriff, nach dem gesucht werden soll
     * @param listenArt   - Gibt an, in welcher Liste gesucht werden soll
     * @return Die gefundenen Medien
     */
    public List<Medium> universaleSuche(String suchbegriff, ListenArt listenArt) {

        suchbegriff = suchbegriff.trim();
        suchbegriff = suchbegriff.toLowerCase();

        List<Medium> medienListe;
        List<Medium> ergebnisListe = new ArrayList<>();

        if (listenArt == ListenArt.MEDIENARCHIV)
            medienListe = verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv();
        else
            medienListe = verwaltungsKontrollierer.getMedienVerwaltung().getWunschliste();

        for (Medium medium : medienListe) {
            for (String value : medium.getParamListe().values()) {
                if (value != null && value.toLowerCase().contains(suchbegriff.trim())) {
                    ergebnisListe.add(medium);
                    break;
                }
            }
        }

        return ergebnisListe;
    }

    /**
     * erweiterteSuche Doc
     *
     * @param suchListe   - ein key-value Objekt mit ausgeführten Kriterien, enthält keine leere Values
     * @param medienArten - Gibt an, welche der jeweiligen Medientypen ausgewählt wurden.
     * @param listenArt   - Gibt an, in welcher Liste gesucht werden soll
     * @return medienliste : Medienliste - Liste aller Medien, die die gesetzten Kriterien erfüllen und vom vorgegebenen
     * Typ sind. Falls keine Medien gefunden werden, wird eine leere Medienliste zurückgegeben
     * <p/>
     * Methode liefert eine Liste aller Medien, deren Medieninformationen mit den Benutzereigaben übereinstimmen.
     * Dafür werden alle Medien iteriert und bei Typübereinstimmung durchsucht, d.h. die Werte der internen Attribute
     * werden mit den Werten der suchListe abgeglichen. Ist der neue Wert ein (Teil-)String des Attributs, so wird
     * das Medium weiter bearbeitet. Wenn dies für alle Typ-relevanten Keys der suchListe gilt, dann wird das Medium
     * in die Rückgabeliste hinzugefügt.
     */
    public List<Medium> erweiterteSuche(Map<String, String> suchListe, EnumSet<MedienArt> medienArten, ListenArt listenArt) throws NullPointerException {
        List<Medium> ergebnisListe = new ArrayList<>();

        List<Medium> medienListe;

        if (listenArt == ListenArt.MEDIENARCHIV)
            medienListe = verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv();
        else
            medienListe = verwaltungsKontrollierer.getMedienVerwaltung().getWunschliste();

        for (Medium medium : medienListe) {
            if (!medienArten.contains(medium.getMedienArt())) {
                continue;
            }

            boolean isMediumValid = true;

            for (String mediumParam : medium.getParamListe().keySet()) {
                if (suchListe.get(mediumParam) == null) {
                    continue;
                }

                if (medium.getParamListe().get(mediumParam) == null) {
                    isMediumValid = false;
                    break;
                }

                if (!medium.getParamListe().get(mediumParam).toLowerCase().contains(suchListe.get(mediumParam).toLowerCase().trim())) {
                    isMediumValid = false;
                    break;
                }
            }

            if (isMediumValid) {
                ergebnisListe.add(medium);
            }
        }

        return ergebnisListe;
    }

}
