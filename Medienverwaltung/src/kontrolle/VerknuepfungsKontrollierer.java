package kontrolle;

import java.util.LinkedList;
import java.util.List;

import model.Medium;
import model.Verknuepfung;

public class VerknuepfungsKontrollierer {

    public VerknuepfungsKontrollierer() {
        // Leerer Konstruktor fuer Tests.
    }

    /**
     * Verknuepft alle Medien aus der uebergebenen Liste miteinander.
     *
     * @param medienliste  Liste aller zu verknuepfenden Medien
     * @param beschreibung Kurze Beschreibung der Verknuepfung (z.B. Buch zum Film, Teile einer Serie,...)
     * @return void
     * @throws IllegalArgumentException falls Liste Duplikate enthaelt oder weniger als 2 Medien enthaelt
     * @throws NullPointerException     fuer Parameter beschreibung, oder falls ein Medium der Liste null ist
     * @throws EmptyStringException     beschreibung
     */
    public Verknuepfung verknuepfen(List<Medium> medienliste, String beschreibung) {
        if (beschreibung == null)
            throw new NullPointerException();
        if (medienliste == null)
            throw new NullPointerException();
        if (beschreibung.equals(""))
            throw new EmptyStringException();
        if (medienliste.size() < 2)
            throw new IllegalArgumentException("Medienliste zum Verknuepfen muss mindestens 2 Medien enthalten!");
        List<Medium> hilfsliste = new LinkedList<>();
        for (Medium medium : medienliste) {
            if (medium == null)
                throw new NullPointerException("Leeres Medium in Liste uebergeben!");
            if (hilfsliste.contains(medium))
                throw new IllegalArgumentException("Medienliste enthaelt Duplikate!");
            hilfsliste.add(medium);
        }
        Verknuepfung verknuepfung = new Verknuepfung(beschreibung, medienliste);
        
        for (Medium medium : medienliste) {
        	List<Verknuepfung> hilfsVerknuepfungsListe = medium.getVerknuepfungList();
        	hilfsVerknuepfungsListe.add(verknuepfung);
            medium.setVerknuepfungList(hilfsVerknuepfungsListe);
        }
        return verknuepfung;
    }

    /**
     * Die Methode bearbeitet die Verknuepfung eines Mediums, indem es das
     * uebergebende Medium aus der übergebenen Verknuepfung entfernt.
     * Wird kein Medium oder keine Verknuepfung angegeben, wird eine Exception geworfen.
     * Ist das uebergebene Medium nicht in der uebergebenen Verknuepfung enthalten,
     * soll nichts geschehen.
     *
     * @param medium       : Medium - Referenz auf ein Objekt der Klasse Medium, welches
     *                     in der Verknuepfung enthalten ist
     * @param verknuepfung : Verknuepfung - Referenz auf ein Objekt der Klasse
     *                     Verknuepfung, das eine nicht leere Liste verknuepfter Medien enhaelt
     * @return void
     * @throws NullPointerException     fuer Parameter medium, verknuepfung
     * @throws IllegalArgumentException falls Medium und Verknuepfung einander nicht kennen
     **/
    public void entferneMediumAusVerknuepfung(Medium medium, Verknuepfung verknuepfung) {
        if (verknuepfung == null || medium == null)
            throw new NullPointerException("Medium oder Verknüpfung fehlt!");
        //falls weniger als 3 Medien in Verknuepfung sind
        //und man eins von denen loeschen will, wird die ganze Verknuepfung geloescht
        boolean sizeSmallerThanThree = verknuepfung.gebeListe().size()<3;
        if (sizeSmallerThanThree)
        {
        	entferneVerknuepfung(verknuepfung);
        	throw new InformationException("Loeschen eines Elements einer Verknuepfung mit 2 Medien loescht auch die Verknuepfung!");
        }	
        else{
	        List<Verknuepfung> hilfsVerknuepfungsListe = medium.getVerknuepfungList();
	        List<Medium> hilfsMedienListe = verknuepfung.gebeListe();
	        if (!hilfsMedienListe.remove(medium))
	            throw new IllegalArgumentException("Medium nicht in Verknuepfung vorhanden!");
	        if (!hilfsVerknuepfungsListe.remove(verknuepfung))
	            throw new IllegalArgumentException("Verknuepfung nicht im Medium registriert!");
	        medium.setVerknuepfungList(hilfsVerknuepfungsListe);
	        verknuepfung.setzeListe(hilfsMedienListe);
        }
    }

    /**
     * Die Methode loescht die uebergebene Verknuepfung, indem es
     * in jedem in verknuepfung enthaltenen Medium die die Verknuepfung loescht und
     * alle in verknuepfung enthaltenen Medien aus der Liste entfernt.
     * Wird eine leere Liste uebergeben soll nichts geschehen.
     *
     * @param verknuepfung : Verknuepfung - Referenz auf ein Objekt der
     *                     Klasse Verknuepfung, das eine Liste verknuepfter Medien enthält
     * @return void
     * @throws NullPointerException verknuepfung
     **/

    public void entferneVerknuepfung(Verknuepfung verknuepfung) {
        if (verknuepfung == null)
            throw new NullPointerException();
        List<Medium> medien = verknuepfung.gebeListe();
        Medium aktuellesMedium;
        
        while (medien.size() > 0) {
            aktuellesMedium = medien.remove(0);
            List<Verknuepfung> hilfsVerknuepfungsListe = aktuellesMedium.getVerknuepfungList();
            hilfsVerknuepfungsListe.remove(verknuepfung);
            aktuellesMedium.setVerknuepfungList(hilfsVerknuepfungsListe);
            verknuepfung.setzeListe(medien);
        }
    }

    /**
     * Die Methode bearbeitet übergegebenes Objekt
     * verknuepfung und ersetzt dessen Beschreibung mit
     * dem neuen Wert. Ist die Verknüpfg nungültig bzw.
     * der Wert für Beschreibung ist leer, so wird nichts
     * getan.
     *
     * @param verknuepfung : Verknuepfung - Refererz auf ein Objekt der Klasse
     *                     Verknüpfung, das eine nicht leere Liste verknüpfter Medien
     *                     enthält
     * @param beschreibung : String - Enthält den textuellen Wert für eine
     *                     Beschreibung der ausgewählten Verknüpfung
     * @return void
     * @throws NullPointerException verknuepfung, beschreibung
     * @throws EmptyStringException beschreibung
     */
    public void bearbeiteVerknuepfungsBeschreibung(Verknuepfung verknuepfung, String beschreibung) {
        if (beschreibung == null)
            throw new NullPointerException();
        if (beschreibung.equals(""))
            throw new EmptyStringException();
        if (verknuepfung == null)
            throw new NullPointerException();
        verknuepfung.setzeBeschreibung(beschreibung);
    }

}
