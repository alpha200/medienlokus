package kontrolle;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.Bewertung;
import model.MedienArt;
import model.Medium;

/**
 * Der Medienkontrollierer enthält alle Methoden die nötig sind,
 * um einzelne Medien und ihe Attribute zu erstellen und zu verändern.
 *
 * @author Nils & Cihat(erstelleMedium())
 */
public class MedienKontrollierer {
    private VerwaltungsKontrollierer verwaltungsKontrollierer;

    /**
     * @param verwaltungsKontrollierer: Übergibt den VerwaltungsKontrollierer
     *                                  beim erstellen von VerwaltungsKontrollierer. Um den Benutzer auslesen zu können.
     */
    public MedienKontrollierer(VerwaltungsKontrollierer verwaltungsKontrollierer) {
        this.verwaltungsKontrollierer = verwaltungsKontrollierer;

    }

    /**
     * Erstellt das Medium vom Typ der übergebenen Klasse mit den Attributen aus paramList,
     * falls die paramList korrekt Attribute entsprechend der Art enthält.
     *
     * @param art        Die zu erstellende Art des Mediums.
     * @param paramListe Enthält alle Attribute mit denen das Medium erstellt werden soll.
     * @return Das erstellte Medium
     * @throws MissingValueException Titel oder Autor leer
     * @throws InvalidParameterException Falsches Attribut in paramListe enthalten
     * @throws NullPointerException Art oder paramListe leer
     */
    public Medium erstelleMedium(MedienArt art, Map<String, String> paramListe) throws MissingValueException {
        Medium.pruefeParamListe(art, paramListe);
        Medium medium = new Medium(paramListe, art);
        List<Bewertung> bewertungsListe = new ArrayList<Bewertung>();
        String bewerter = verwaltungsKontrollierer.getMedienVerwaltung().getBenutzer();
        Bewertung ersteBewertung = new Bewertung(bewerter, 0, "");
        bewertungsListe.add(ersteBewertung);
        medium.setBewertungList(bewertungsListe);
        return medium;
    }

    /**
     * Bearbeitet ein Medium, indem die in der paramListe angegebenen Attribute in das übergebene Medium überprüft,
     * und bei Korrektheit eingespeichert werden.
     *
     * @param selektiertesMedium Das zu bearbeitende Medium.
     * @param paramListe         Die Liste der Attribute, die im Medium geändert werden sollen.
     * @return Das bearbeitete Medium
     * @throws MissingValueException
     */
    public Medium bearbeiteMedium(Medium selektiertesMedium, Map<String, String> paramListe) throws MissingValueException {
        Medium.pruefeParamListe(selektiertesMedium.getMedienArt(), paramListe);
        //Wenn eins von den Verleihattributen leer ist, leere das andere auch
        if(paramListe.get("Verliehen an").equals(""))
        {
        	paramListe.put("Verleihdatum", "");
        }
        if(paramListe.get("Verleihdatum").equals(""))
        {
        	paramListe.put("Verliehen an", "");
        }
        selektiertesMedium.setParamListe(paramListe, selektiertesMedium.getMedienArt());
        return selektiertesMedium;
    }

    /**
     * Bearbeitet die Bewertung eines Mediums, indem die übergebene Beschreibung und der Bewerter
     * in die Liste der Bewertungen des Mediums hinzugefügt wird.
     *
     * @param medium       Das Medium zu dem die Bewertung hinzugefügt werden soll.
     * @param beschreibung Die Beschreibung der Bewertung.
     * @param bewertung     Der Name des Bewerters.
     */
    public void bearbeiteBewertung(Medium medium, String beschreibung, int bewertung) {
        Bewertung eigeneBewertung;
        if (bewertung < 0 || bewertung >= 6) {
            throw new InvalidParameterException("Bitte eine Bewertung zwischen 0 und 5 Sternen angeben!");
        }
        if (beschreibung == null) {
            throw new NullPointerException("Beschreibung darf nicht null sein!");
        } else {
            eigeneBewertung = medium.
                    getBewertungList().
                    get(0);
            eigeneBewertung.setBeschreibung(beschreibung);
            eigeneBewertung.setBewertung(bewertung);
        }
    }

    /**
     * Markiert ein Medium als Verliehen und trägt das Verleihdatum und die Person an,
     * an die das Medium verliehen wurde. Wenn das Medium bereits verliehen ist, wird ein Fehler angezeigt.
     *
     * @param selektiertesMedium Das Medium das ausgeliehen werden soll.
     * @param datum              Das Datum, an dem das Medium verliehen werden soll.
     * @param person             Die Person an die das Medium verliehen werden soll.
     * @return Wahrheitswert, der angibt ob das verleihen erfolgreich war oder nicht.
     * Es können Beispielsweise keine Medien verliehen werden, die bereits verliehen sind.
     * @throws MediumLendException
     * @throws MissingValueException
     */
    public void verleiheMedium(Medium selektiertesMedium, String datum, String person) throws MediumLendException, MissingValueException {
        if (datum == null || person == null) {
            throw new NullPointerException("Es muss ein Datum und eine person angegeben werden!");
        }
        if (datum.equals("") || person.equals("")) {
            throw new EmptyStringException("Es muss ein Datum und eine Person angegeben werden!");
        }
        if (selektiertesMedium == null) {
            throw new NullPointerException("Medium darf nicht leer sein!");
        }
        Map<String, String> paramListe = selektiertesMedium.getParamListe();

        if (!(paramListe.get("Verliehen an").equals(""))) {
            throw new MediumLendException("Dieses Medium ist bereits an " + paramListe.get("Verliehen an") + " verliehen!");
        } else {
            paramListe.put("Verliehen an", person);
            paramListe.put("Verleihdatum", datum);
            selektiertesMedium.setParamListe(paramListe, selektiertesMedium.getMedienArt());
        }
    }

    /**
     * @param selektierteMedien Liste von Medien die verliehen werden sollen.
     * @param datum             Das Datum, an dem das Medium verliehen werden soll.
     * @param person            Die Person an die das Medium verliehen werden soll.
     * @throws MediumLendException
     * @throws MissingValueException 
     */
    public void verleiheMedien(List<Medium> selektierteMedien, String datum, String person) throws MediumLendException, MissingValueException {
        for (Medium medium : selektierteMedien) {
            verleiheMedium(medium, datum, person);
        }
    }

}
