package gui;

import kontrolle.DuplicateMediumException;
import kontrolle.ListenArt;
import kontrolle.MissingValueException;
import kontrolle.VerwaltungsKontrollierer;
import model.MedienArt;
import model.Medium;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MediumHinzufuegen extends JDialog {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final JPanel pnlAttribute;

    JRadioButton rdbtnBuch;
    JRadioButton rdbtnMusik;
    JRadioButton rdbtnFilm;
    JRadioButton rdbtnHoerbuch;
    JRadioButton rdbtnTVSerie;
    JRadioButton rdbtnEbook;

    private Map<String, JTextField> textfelder;
    private Medium medium;

    /**
     * Create the frame.
     */
    public MediumHinzufuegen(final Hauptfenster elter, final VerwaltungsKontrollierer verwaltungsKontrollierer, final ListenArt listenArt) {
        super(elter, true);

        setTitle("Medium hinzufügen");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 610, 500);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[grow]", "[35px:35px:35px][grow][]"));

        pnlAttribute = new JPanel();
        contentPane.add(pnlAttribute, "cell 0 1,grow");
        pnlAttribute.setLayout(new MigLayout("", "[][grow,fill]", "[][][][]"));

        JButton btnAbbrechen = new JButton("Abbrechen");
        btnAbbrechen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        contentPane.add(btnAbbrechen, "flowx,cell 0 2,alignx right");

        JButton btnHinzufgen = new JButton("Hinzufügen");
        btnHinzufgen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Map<String, String> params = new HashMap<>();

                for (String aparam : medium.getParamListe().keySet()) {
                    params.put(aparam, textfelder.get(aparam).getText().trim());
                }

                try {
                    if (listenArt == ListenArt.MEDIENARCHIV) {
                        verwaltungsKontrollierer.getListenKontrollierer().inMedienarchivHinzufuegen(medium.getMedienArt(), params);
                    } else {
                        verwaltungsKontrollierer.getListenKontrollierer().inWunschlisteHinzufuegen(medium.getMedienArt(), params);
                    }

                    elter.ladeArchivListe();
                    dispose();
                } catch (MissingValueException | DuplicateMediumException ex) {
                    Object[] options = {"OK"};
                    JOptionPane.showOptionDialog(null, ex.getMessage(), "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                }
            }
        });
        contentPane.add(btnHinzufgen, "cell 0 2,alignx right");

        JPanel pnlTypwahl = new JPanel();
        FlowLayout fl_pnlTypwahl = (FlowLayout) pnlTypwahl.getLayout();
        fl_pnlTypwahl.setHgap(20);
        contentPane.add(pnlTypwahl, "cell 0 0,growx");

        rdbtnBuch = new JRadioButton("Buch");
        rdbtnMusik = new JRadioButton("Musik");
        rdbtnFilm = new JRadioButton("Film");
        rdbtnHoerbuch = new JRadioButton("Hörbuch");
        rdbtnEbook = new JRadioButton("E-Book");
        rdbtnTVSerie = new JRadioButton("TV-Serie");

        pnlTypwahl.add(rdbtnMusik);
        pnlTypwahl.add(rdbtnBuch);
        pnlTypwahl.add(rdbtnFilm);
        pnlTypwahl.add(rdbtnHoerbuch);
        pnlTypwahl.add(rdbtnEbook);
        pnlTypwahl.add(rdbtnTVSerie);

        RadioButtonListener rdbListener = new RadioButtonListener();

        ButtonGroup group = new ButtonGroup();
        group.add(rdbtnBuch);
        group.add(rdbtnFilm);
        group.add(rdbtnHoerbuch);
        group.add(rdbtnMusik);
        group.add(rdbtnTVSerie);
        group.add(rdbtnEbook);

        rdbtnMusik.setSelected(true);

        rdbtnBuch.addActionListener(rdbListener);
        rdbtnFilm.addActionListener(rdbListener);
        rdbtnHoerbuch.addActionListener(rdbListener);
        rdbtnMusik.addActionListener(rdbListener);
        rdbtnEbook.addActionListener(rdbListener);
        rdbtnTVSerie.addActionListener(rdbListener);

        selectionChanged();
    }

    private class RadioButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            selectionChanged();
        }
    }

    private void selectionChanged() {
        textfelder = new HashMap<>();

        pnlAttribute.removeAll();

        int x = getX();
        int y = getY();

        if (rdbtnBuch.isSelected()) {
            medium = new Medium(new HashMap<String, String>(), MedienArt.BUCH);
            setBounds(x, y, 610, 360);
        } else if (rdbtnFilm.isSelected()) {
            medium = new Medium(new HashMap<String, String>(), MedienArt.FILM);
            setBounds(x, y, 610, 440);
        } else if (rdbtnHoerbuch.isSelected()) {
            medium = new Medium(new HashMap<String, String>(), MedienArt.HOERBUCH);
            setBounds(x, y, 610, 410);
        } else if (rdbtnEbook.isSelected()) {
            medium = new Medium(new HashMap<String, String>(), MedienArt.EBOOK);
            setBounds(x, y, 610, 360);
        }else if (rdbtnTVSerie.isSelected()) {
            medium = new Medium(new HashMap<String, String>(), MedienArt.TVSERIE);
            setBounds(x, y, 610, 490);
        }else {
            medium = new Medium(new HashMap<String, String>(), MedienArt.MUSIK);
            setBounds(x, y, 610, 410);
        }

        String rows = "";

        for (int i = 0; i < medium.getParamListe().keySet().size(); i++) {
            rows += "[]";
        }

        pnlAttribute.setLayout(new MigLayout("", "[][grow,fill]", rows));

        int row = 0;

        String[] params = medium.getParamListe().keySet().toArray(new String[medium.getParamListe().size()]);
        Arrays.sort(params);

        for (String param : params) {
            JLabel label = new JLabel(param + ":");
            pnlAttribute.add(label, "cell 0 " + row + ",alignx trailing");

            JTextField textField = new JTextField();
            pnlAttribute.add(textField, "cell 1 " + row + ",growx");
            textField.setColumns(10);

            textfelder.put(param, textField);

            row++;
        }

        revalidate();
        repaint();
    }
}
