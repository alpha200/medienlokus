package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import kontrolle.EmptyStringException;
import kontrolle.MediumLendException;
import kontrolle.MissingValueException;
import kontrolle.VerwaltungsKontrollierer;
import model.Medium;

import com.toedter.calendar.JCalendar;

public class Verleihen extends JDialog {

    private JTextField txtBenutzer;

    /**
     * Create the frame.
     */
    public Verleihen(final Hauptfenster elter, final VerwaltungsKontrollierer verwaltungsKontrollierer, final List<Medium> selektierteMedien) {
        super(elter, true);

        setTitle("Verleihen");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 496, 422);

        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblAn = new JLabel("Verleih an:");
        lblAn.setBounds(15, 19, 100, 20);
        contentPane.add(lblAn);

        JLabel lblDatum = new JLabel("Verleihdatum:");
        lblDatum.setBounds(15, 75, 100, 20);
        contentPane.add(lblDatum);

        txtBenutzer = new JTextField();
        txtBenutzer.setBounds(133, 16, 146, 26);
        contentPane.add(txtBenutzer);
        txtBenutzer.setColumns(10);

        final JCalendar calendar = new JCalendar();
        calendar.setBounds(133, 75, 324, 241);
        contentPane.add(calendar);

        JButton btnOkay = new JButton("Okay");
        btnOkay.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    verwaltungsKontrollierer.getMedienKontrollierer().verleiheMedien(selektierteMedien, calendar.getDate().toString(), txtBenutzer.getText());
                    dispose();
                } catch (MediumLendException | MissingValueException | EmptyStringException ex) {
                    Object[] options = {"OK"};
                    JOptionPane.showOptionDialog(null, ex.getMessage(), "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                }
            }
        });
        btnOkay.setBounds(342, 332, 115, 29);
        contentPane.add(btnOkay);

        JButton btnAbbrechen = new JButton("Abbrechen");
        btnAbbrechen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnAbbrechen.setBounds(209, 332, 115, 29);
        contentPane.add(btnAbbrechen);
    }
}
