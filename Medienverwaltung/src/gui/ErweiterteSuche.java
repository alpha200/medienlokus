package gui;

import java.applet.Applet;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumSet;
import java.util.HashMap;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import kontrolle.ListenArt;
import kontrolle.SuchKontrollierer;
import kontrolle.VerwaltungsKontrollierer;
import model.MedienArt;

import org.eclipse.wb.swing.FocusTraversalOnArray;

public class ErweiterteSuche extends JDialog {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtTitel;
    private JTextField txtAutor;
    private JTextField txtGenre;
    private JTextField txtJahr;
    private JTextField txtKaufpreis;
    private JTextField txtSpeicherort;
    private JTextField txtIsanFilm;
    private JTextField txtAbspieldauerFilm;
    private JTextField txtHerkunftsland;
    private JTextField txtSchauspieler;
    private JTextField txtIsbn;
    private JTextField txtTracknr;
    private JTextField txtAlbum;
    private JTextField txtIsanMusik;
    private JTextField txtSprecher;
    private JTextField txtAbspieldauerHoerbuch;
    private JTextField txtAnzahlCds;
    private JTextField txtSender;
    private JTextField txtEpisodenzahl;
    private final JCheckBox chckbxTvSerie;
    private final JCheckBox chckbxEbook;

    /**
     * Create the frame.
     */
    public ErweiterteSuche(final Hauptfenster elter, final VerwaltungsKontrollierer verwaltungsKontrollierer, final ListenArt listenArt) {
        super(elter, true);

        setResizable(false);
        setTitle("Erweiterte Suche");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 573, 517);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblTitel = new JLabel("Titel");
        lblTitel.setBounds(8, 99, 70, 15);
        contentPane.add(lblTitel);

        JLabel lblAutor = new JLabel("Autor/Regisseur");
        lblAutor.setBounds(8, 126, 114, 15);
        contentPane.add(lblAutor);

        JLabel lblGenre = new JLabel("Genre");
        lblGenre.setBounds(8, 153, 70, 15);
        contentPane.add(lblGenre);

        JLabel lblJahr = new JLabel("Jahr");
        lblJahr.setBounds(8, 180, 70, 15);
        contentPane.add(lblJahr);

        JLabel lblKaufpreis = new JLabel("Kaufpreis");
        lblKaufpreis.setBounds(8, 207, 70, 15);
        contentPane.add(lblKaufpreis);

        JLabel lblSpeicherort = new JLabel("Speicherort");
        lblSpeicherort.setBounds(8, 234, 89, 15);
        contentPane.add(lblSpeicherort);

        JLabel lblIsanFilm = new JLabel("ISAN");
        lblIsanFilm.setBounds(310, 228, 70, 15);
        contentPane.add(lblIsanFilm);

        JLabel lblAbspieldauerFilm = new JLabel("Abspieldauer");
        lblAbspieldauerFilm.setBounds(310, 255, 101, 15);
        contentPane.add(lblAbspieldauerFilm);

        JLabel lblHerkunftsland = new JLabel("Herkunftsland");
        lblHerkunftsland.setBounds(310, 282, 101, 15);
        contentPane.add(lblHerkunftsland);

        JLabel lblSchauspieler = new JLabel("Schauspieler");
        lblSchauspieler.setBounds(310, 309, 101, 15);
        contentPane.add(lblSchauspieler);

        JLabel lblIsbn = new JLabel("ISBN");
        lblIsbn.setBounds(8, 298, 70, 15);
        contentPane.add(lblIsbn);

        JLabel lblIsanMusik = new JLabel("ISAN");
        lblIsanMusik.setBounds(8, 354, 70, 15);
        contentPane.add(lblIsanMusik);

        JLabel lblAlbum = new JLabel("Album");
        lblAlbum.setBounds(8, 379, 70, 15);
        contentPane.add(lblAlbum);

        JLabel lblTracknummer = new JLabel("Tracknummer");
        lblTracknummer.setBounds(8, 404, 101, 15);
        contentPane.add(lblTracknummer);

        JLabel lblSprecher = new JLabel("Sprecher");
        lblSprecher.setBounds(310, 101, 70, 15);
        contentPane.add(lblSprecher);

        JLabel lblAbpsieldauerHoerbuch = new JLabel("Abspieldauer");
        lblAbpsieldauerHoerbuch.setBounds(310, 128, 101, 15);
        contentPane.add(lblAbpsieldauerHoerbuch);

        JLabel lblAnzahlCds = new JLabel("Anzahl CDs");
        lblAnzahlCds.setBounds(310, 155, 89, 15);
        contentPane.add(lblAnzahlCds);

        JLabel lblAllgemein = new JLabel("Allgemein");
        lblAllgemein.setFont(new Font("Dialog", Font.ITALIC, 15));
        lblAllgemein.setBounds(8, 72, 70, 15);
        contentPane.add(lblAllgemein);

        JLabel lblFilm = new JLabel("Film / Serie");
        lblFilm.setFont(new Font("Dialog", Font.ITALIC, 15));
        lblFilm.setBounds(310, 201, 101, 15);
        contentPane.add(lblFilm);

        JLabel lblBuch = new JLabel("Buch / E-Book");
        lblBuch.setFont(new Font("Dialog", Font.ITALIC, 15));
        lblBuch.setBounds(8, 269, 114, 15);
        contentPane.add(lblBuch);

        JLabel lblMusik = new JLabel("Musik");
        lblMusik.setFont(new Font("Dialog", Font.ITALIC, 15));
        lblMusik.setBounds(8, 327, 70, 15);
        contentPane.add(lblMusik);

        JLabel lblHrbcher = new JLabel("Hörbücher");
        lblHrbcher.setFont(new Font("Dialog", Font.ITALIC, 15));
        lblHrbcher.setBounds(310, 72, 89, 15);
        contentPane.add(lblHrbcher);

        txtTitel = new JTextField();
        txtTitel.setBounds(124, 97, 114, 19);
        contentPane.add(txtTitel);
        txtTitel.setColumns(10);

        txtAutor = new JTextField();
        txtAutor.setBounds(124, 124, 114, 19);
        contentPane.add(txtAutor);
        txtAutor.setColumns(10);

        txtGenre = new JTextField();
        txtGenre.setBounds(124, 151, 114, 19);
        contentPane.add(txtGenre);
        txtGenre.setColumns(10);

        txtJahr = new JTextField();
        txtJahr.setBounds(124, 178, 114, 19);
        contentPane.add(txtJahr);
        txtJahr.setColumns(10);

        txtKaufpreis = new JTextField();
        txtKaufpreis.setBounds(124, 205, 114, 19);
        contentPane.add(txtKaufpreis);
        txtKaufpreis.setColumns(10);

        txtSpeicherort = new JTextField();
        txtSpeicherort.setBounds(124, 232, 114, 19);
        contentPane.add(txtSpeicherort);
        txtSpeicherort.setColumns(10);

        txtIsanFilm = new JTextField();
        txtIsanFilm.setEditable(false);
        txtIsanFilm.setBounds(429, 226, 114, 19);
        contentPane.add(txtIsanFilm);
        txtIsanFilm.setColumns(10);

        txtAbspieldauerFilm = new JTextField();
        txtAbspieldauerFilm.setEditable(false);
        txtAbspieldauerFilm.setBounds(429, 253, 114, 19);
        contentPane.add(txtAbspieldauerFilm);
        txtAbspieldauerFilm.setColumns(10);

        txtHerkunftsland = new JTextField();
        txtHerkunftsland.setEditable(false);
        txtHerkunftsland.setBounds(429, 280, 114, 19);
        contentPane.add(txtHerkunftsland);
        txtHerkunftsland.setColumns(10);

        txtSchauspieler = new JTextField();
        txtSchauspieler.setEditable(false);
        txtSchauspieler.setBounds(429, 307, 114, 19);
        contentPane.add(txtSchauspieler);
        txtSchauspieler.setColumns(10);

        txtIsbn = new JTextField();
        txtIsbn.setEditable(false);
        txtIsbn.setBounds(127, 296, 114, 19);
        contentPane.add(txtIsbn);
        txtIsbn.setColumns(10);

        txtTracknr = new JTextField();
        txtTracknr.setEditable(false);
        txtTracknr.setBounds(127, 402, 114, 19);
        contentPane.add(txtTracknr);
        txtTracknr.setColumns(10);

        txtAlbum = new JTextField();
        txtAlbum.setEditable(false);
        txtAlbum.setBounds(127, 377, 114, 19);
        contentPane.add(txtAlbum);
        txtAlbum.setColumns(10);

        txtIsanMusik = new JTextField();
        txtIsanMusik.setEditable(false);
        txtIsanMusik.setBounds(127, 352, 114, 19);
        contentPane.add(txtIsanMusik);
        txtIsanMusik.setColumns(10);

        txtSprecher = new JTextField();
        txtSprecher.setEditable(false);
        txtSprecher.setBounds(429, 97, 114, 19);
        contentPane.add(txtSprecher);
        txtSprecher.setColumns(10);

        txtAbspieldauerHoerbuch = new JTextField();
        txtAbspieldauerHoerbuch.setEditable(false);
        txtAbspieldauerHoerbuch.setBounds(429, 126, 114, 19);
        contentPane.add(txtAbspieldauerHoerbuch);
        txtAbspieldauerHoerbuch.setColumns(10);

        txtAnzahlCds = new JTextField();
        txtAnzahlCds.setEditable(false);
        txtAnzahlCds.setBounds(429, 153, 114, 19);
        contentPane.add(txtAnzahlCds);
        txtAnzahlCds.setColumns(10);

        final JCheckBox chckbxMusik = new JCheckBox("Musik");
        chckbxMusik.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = chckbxMusik.isSelected();
                txtAlbum.setEditable(checked);
                txtIsanMusik.setEditable(checked);
                txtTracknr.setEditable(checked);
            }
        });
        chckbxMusik.setBounds(8, 23, 89, 23);
        contentPane.add(chckbxMusik);

        final JCheckBox chckbxBuch = new JCheckBox("Buch");
        chckbxBuch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = chckbxBuch.isSelected() || chckbxEbook.isSelected();
                txtIsbn.setEditable(checked);
            }
        });
        chckbxBuch.setBounds(101, 23, 86, 23);
        contentPane.add(chckbxBuch);

        final JCheckBox chckbxHoerbuch = new JCheckBox("HÃ¶rbuch");
        chckbxHoerbuch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = chckbxHoerbuch.isSelected();
                txtSprecher.setEditable(checked);
                txtAbspieldauerHoerbuch.setEditable(checked);
                txtAnzahlCds.setEditable(checked);
            }
        });
        chckbxHoerbuch.setBounds(191, 23, 89, 23);
        contentPane.add(chckbxHoerbuch);

        final JCheckBox chckbxFilm = new JCheckBox("Film");
        chckbxFilm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = chckbxFilm.isSelected() || chckbxTvSerie.isSelected();;
                txtIsanFilm.setEditable(checked);
                txtAbspieldauerFilm.setEditable(checked);
                txtHerkunftsland.setEditable(checked);
                txtSchauspieler.setEditable(checked);
            }
        });
        chckbxFilm.setBounds(294, 23, 59, 23);
        contentPane.add(chckbxFilm);

        JButton btnSuchen = new JButton("Suchen");
        btnSuchen.setBounds(426, 447, 117, 25);
        contentPane.add(btnSuchen);

        JButton btnAbbrechen = new JButton("Abbrechen");
        btnAbbrechen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnAbbrechen.setBounds(291, 447, 117, 25);
        contentPane.add(btnAbbrechen);

        JLabel lblTypauswahl = new JLabel("Typauswahl");
        lblTypauswahl.setFont(new Font("Dialog", Font.ITALIC, 15));
        lblTypauswahl.setBounds(8, 0, 89, 25);
        contentPane.add(lblTypauswahl);

        JButton btnZurcksetzen = new JButton("Zurücksetzen");
        btnZurcksetzen.setBounds(132, 448, 138, 23);
        contentPane.add(btnZurcksetzen);
        
        chckbxTvSerie = new JCheckBox("TV-Serie");
        chckbxTvSerie.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean checkedTV = chckbxTvSerie.isSelected();
                boolean checkedFilm = chckbxFilm.isSelected();
                txtIsanFilm.setEditable(checkedTV || checkedFilm);
                txtAbspieldauerFilm.setEditable(checkedTV || checkedFilm);
                txtHerkunftsland.setEditable(checkedTV || checkedFilm);
                txtSchauspieler.setEditable(checkedTV || checkedFilm);
                txtEpisodenzahl.setEditable(checkedTV);
                txtSender.setEditable(checkedTV);
            }
        });
        chckbxTvSerie.setBounds(357, 23, 89, 23);
        contentPane.add(chckbxTvSerie);
        
        chckbxEbook = new JCheckBox("E-Book");
        chckbxEbook.setBounds(450, 23, 78, 23);
        chckbxEbook.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtIsbn.setEditable(chckbxBuch.isSelected() || chckbxEbook.isSelected());
            }
        });
        contentPane.add(chckbxEbook);
        
        JLabel lblTVSerie = new JLabel("TV-Serie");
        lblTVSerie.setFont(new Font("Dialog", Font.ITALIC, 15));
        lblTVSerie.setBounds(310, 350, 70, 15);
        contentPane.add(lblTVSerie);
        
        JLabel lblSender = new JLabel("Sender:");
        lblSender.setBounds(310, 375, 70, 15);
        contentPane.add(lblSender);
        
        txtSender = new JTextField();
        txtSender.setEditable(false);
        txtSender.setColumns(10);
        txtSender.setBounds(429, 373, 114, 19);
        contentPane.add(txtSender);
        
        JLabel lblEpisodenzahl = new JLabel("Episodenzahl");
        lblEpisodenzahl.setBounds(310, 402, 101, 15);
        contentPane.add(lblEpisodenzahl);
        
        txtEpisodenzahl = new JTextField();
        txtEpisodenzahl.setEditable(false);
        txtEpisodenzahl.setColumns(10);
        txtEpisodenzahl.setBounds(429, 400, 114, 19);
        contentPane.add(txtEpisodenzahl);
        contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblTitel, lblIsbn, lblAutor, lblGenre, lblJahr, chckbxBuch, lblKaufpreis, lblSpeicherort, chckbxMusik, lblIsanFilm, lblAbspieldauerFilm, lblHerkunftsland, lblSchauspieler, lblIsanMusik, lblAlbum, lblTracknummer, lblSprecher, lblAbpsieldauerHoerbuch, lblAnzahlCds, lblAllgemein, lblFilm, lblBuch, lblMusik, lblHrbcher, txtTitel, txtAutor, txtGenre, txtJahr, txtKaufpreis, txtSpeicherort, txtIsanFilm, txtAbspieldauerFilm, txtHerkunftsland, txtSchauspieler, txtIsbn, txtTracknr, txtAlbum, txtIsanMusik, txtSprecher, txtAbspieldauerHoerbuch, txtAnzahlCds, chckbxHoerbuch, chckbxFilm, btnSuchen, btnAbbrechen, lblTypauswahl}));

        btnZurcksetzen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (Component komponente : contentPane.getComponents()) {
                    if (komponente instanceof JTextField) {
                        ((JTextField) komponente).setText("");
                    }
                    if (komponente instanceof JCheckBox)
                        ((JCheckBox) komponente).setSelected(false);
                }
            }
        });

        btnSuchen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SuchKontrollierer sk = verwaltungsKontrollierer.getSuchKontrollierer();

                HashMap<String, String> suchListe = new HashMap<>();
                EnumSet<MedienArt> medienArten = EnumSet.noneOf(MedienArt.class);

                if (!txtTitel.getText().equals("")) {
                    suchListe.put("Titel", txtTitel.getText());
                }

                if (!txtAutor.getText().equals("")) {
                    suchListe.put("Autor", txtAutor.getText());
                }

                if (!txtGenre.getText().equals("")) {
                    suchListe.put("Genre", txtGenre.getText());
                }

                if (!txtJahr.getText().equals("")) {
                    suchListe.put("Jahr", txtJahr.getText());
                }

                if (!txtKaufpreis.getText().equals("")) {
                    suchListe.put("Kaufpreis", txtKaufpreis.getText());
                }

                if (!txtSpeicherort.getText().equals("")) {
                    suchListe.put("Speicherort", txtSpeicherort.getText());
                }


                if (chckbxMusik.isSelected()) {
                    medienArten.add(MedienArt.MUSIK);

                    if (!txtIsanMusik.getText().equals("")) {
                        suchListe.put("ISAN", txtIsanMusik.getText());
                    }
                    if (!txtAlbum.getText().equals("")) {
                        suchListe.put("Album", txtAlbum.getText());
                    }
                    if (!txtTracknr.getText().equals("")) {
                        suchListe.put("Titelnummer", txtTracknr.getText());
                    }
                }

                if (chckbxBuch.isSelected()) {
                    medienArten.add(MedienArt.BUCH);

                    if (!txtIsbn.getText().equals("")) {
                        suchListe.put("ISBN", txtIsbn.getText());
                    }
                }

                if (chckbxEbook.isSelected()) {
                    medienArten.add(MedienArt.EBOOK);

                    if(!txtIsbn.getText().equals("")) {
                        suchListe.put("ISBN", txtIsbn.getText());
                    }
                }

                if (chckbxHoerbuch.isSelected()) {
                    medienArten.add(MedienArt.HOERBUCH);

                    if (!txtSprecher.getText().equals("")) {
                        suchListe.put("Sprecher", txtSprecher.getText());
                    }
                    if (!txtAbspieldauerHoerbuch.getText().equals("")) {
                        suchListe.put("Abspieldauer", txtAbspieldauerHoerbuch.getText());
                    }
                    if (!txtAnzahlCds.getText().equals("")) {
                        suchListe.put("CD-Anzahl", txtAnzahlCds.getText());
                    }
                }

                if(!(chckbxBuch.isSelected() || chckbxFilm.isSelected() || chckbxHoerbuch.isSelected() || chckbxMusik.isSelected() || chckbxTvSerie.isSelected() || chckbxBuch.isSelected())) {
                    medienArten = EnumSet.allOf(MedienArt.class);
                }

                if (chckbxFilm.isSelected()) {
                    medienArten.add(MedienArt.FILM);

                    if (!txtIsanFilm.getText().equals("")) {
                        suchListe.put("ISAN", txtIsanFilm.getText());
                    }
                    if (!txtAbspieldauerFilm.getText().equals("")) {
                        suchListe.put("Abspieldauer", txtAbspieldauerFilm.getText());
                    }
                    if (!txtHerkunftsland.getText().equals("")) {
                        suchListe.put("Herkunftsland", txtHerkunftsland.getText());
                    }
                    if (!txtSchauspieler.getText().equals("")) {
                        suchListe.put("Top-Schauspieler", txtSchauspieler.getText());
                    }
                }

                if (chckbxTvSerie.isSelected()) {
                    medienArten.add(MedienArt.TVSERIE);

                    if (!txtIsanFilm.getText().equals("")) {
                        suchListe.put("ISAN", txtIsanFilm.getText());
                    }
                    if (!txtAbspieldauerFilm.getText().equals("")) {
                        suchListe.put("Abspieldauer", txtAbspieldauerFilm.getText());
                    }
                    if (!txtHerkunftsland.getText().equals("")) {
                        suchListe.put("Herkunftsland", txtHerkunftsland.getText());
                    }
                    if (!txtSchauspieler.getText().equals("")) {
                        suchListe.put("Top-Schauspieler", txtSchauspieler.getText());
                    }
                    if (!txtEpisodenzahl.getText().equals("")) {
                        suchListe.put("Episodenanzahl", txtSchauspieler.getText());
                    }
                    if (!txtSender.getText().equals("")) {
                        suchListe.put("Sender", txtSender.getText());
                    }
                }

                elter.ladeSuchListe(sk.erweiterteSuche(suchListe, medienArten, listenArt));
                dispose();
            }
        });
    }
}
