package gui;

import kontrolle.MissingValueException;
import kontrolle.VerwaltungsKontrollierer;
import model.Medium;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class WunschlistenMediumBearbeiten extends JDialog {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final JPanel pnlLinks;
    private Map<String, JTextField> textfelder;
    private Medium medium;

    /**
     * Create the frame.
     */
    public WunschlistenMediumBearbeiten(final Hauptfenster elter, final VerwaltungsKontrollierer verwaltungsKontrollierer, final Medium medium) {
        super(elter, true);

        this.medium = medium;

        setTitle("Medium bearbeiten");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 678, 448);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[grow,fill]", "[grow][]"));

        pnlLinks = new JPanel();
        contentPane.add(pnlLinks, "cell 0 0,grow");

        JPanel panelButtons = new JPanel();
        contentPane.add(panelButtons, "cell 0 1,alignx right,aligny bottom");
        panelButtons.setLayout(new MigLayout("", "[177px][grow][105px][110px]", "[25px]"));

        JButton btnSpeichern = new JButton("Speichern");
        btnSpeichern.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Map<String, String> params = new HashMap<>();

                for (String aparam : medium.getParamListe().keySet()) {
                    params.put(aparam, textfelder.get(aparam).getText().trim());
                }

                try {
                    verwaltungsKontrollierer.getMedienKontrollierer().bearbeiteMedium(medium, params);
                    elter.ladeArchivListe();
                    dispose();
                } catch (MissingValueException ex) {
                    Object[] options = {"OK"};
                    JOptionPane.showOptionDialog(null, ex.getMessage(), "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                }
            }
        });

        JButton btnAbbrechen = new JButton("Abbrechen");
        btnAbbrechen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                dispose();
            }
        });
        panelButtons.add(btnAbbrechen, "flowx,cell 3 0,alignx left,aligny top");
        panelButtons.add(btnSpeichern, "cell 3 0,alignx left,aligny top");

        generateTextfields();
    }

    private void generateTextfields() {
        textfelder = new HashMap<>();

        String rows = "";

        for(int i = 0; i < medium.getParamListe().keySet().size(); i++) {
            rows += "[]";
        }

        pnlLinks.setLayout(new MigLayout("", "[][grow,fill]", rows));

        int row = 0;

        for(String param : medium.getParamListe().keySet()) {
            JLabel label = new JLabel(param + ":");
            pnlLinks.add(label, "cell 0 " + row);

            JTextField textField = new JTextField();
            pnlLinks.add(textField, "cell 1 " + row + ",growx");
            textField.setColumns(10);
            textField.setText(medium.getParamListe().get(param));

            textfelder.put(param, textField);

            row++;
        }

        revalidate();
        repaint();
    }
}