package gui;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import kontrolle.InformationException;
import kontrolle.VerwaltungsKontrollierer;
import model.Medium;
import model.Verknuepfung;
import net.miginfocom.swing.MigLayout;

public class Verknuepfen extends JDialog {
	 
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JTable tblVerknuepfungen;
    private JTextField txtAktuelleBeschreibung;
    private List<Medium> verknuepfteMedienListe;

    /**
     * Create the frame.
     */
    public Verknuepfen(final MediumBearbeiten elter, final VerwaltungsKontrollierer verwaltungsKontrollierer, final Verknuepfung verknuepfung) {
        super(elter, true);
        verknuepfteMedienListe = verknuepfung.gebeListe();


        setTitle("Verknüpfung bearbeiten");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 655, 481);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[grow]", "[grow][]"));

        JScrollPane pnlTabelle = new JScrollPane();
        contentPane.add(pnlTabelle, "flowy,cell 0 0,grow");

        tblVerknuepfungen = new JTable();
        tblVerknuepfungen.setFillsViewportHeight(true);
        tblVerknuepfungen.setModel(new MediumTabellenModell());

        tblVerknuepfungen.getColumnModel().getColumn(0).setPreferredWidth(235);
        tblVerknuepfungen.getColumnModel().getColumn(0).setMinWidth(235);
        pnlTabelle.setViewportView(tblVerknuepfungen);
        

        JPanel pnlButtonleiste = new JPanel();
        contentPane.add(pnlButtonleiste, "cell 0 1,growx");
        pnlButtonleiste.setLayout(new MigLayout("", "[grow 80][][][20px,grow 20][]", "[25px]"));

        txtAktuelleBeschreibung = new JTextField();
        txtAktuelleBeschreibung.setText(verknuepfung.gebeBeschreibung());
        pnlButtonleiste.add(txtAktuelleBeschreibung, "cell 0 0,growx,aligny center");
        txtAktuelleBeschreibung.setColumns(20);

        JButton btnBeschreibungndern = new JButton("Beschreibung ändern");
        btnBeschreibungndern.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    verwaltungsKontrollierer.getVerknuepfungsKontrollierer().bearbeiteVerknuepfungsBeschreibung(verknuepfung, txtAktuelleBeschreibung.getText());
                } catch (Exception ex) {
                    Object[] options = {"OK"};
                    JOptionPane.showOptionDialog(null, ex.getMessage(), "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                }
            }
        });
        pnlButtonleiste.add(btnBeschreibungndern, "cell 1 0,alignx left,aligny top");

        final JButton btnEntfernen = new JButton("Entfernen");
        btnEntfernen.setEnabled(false);
        btnEntfernen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    int selectedRow = tblVerknuepfungen.getSelectedRow();
                    Medium selected = verknuepfung.gebeListe().get(selectedRow);
                    verwaltungsKontrollierer.getVerknuepfungsKontrollierer().entferneMediumAusVerknuepfung(selected, verknuepfung);
                    //Aktualisiere tblVerknuepfung
                    verknuepfteMedienListe = verknuepfung.gebeListe();
                   
                    //nur ein Medium aus Verknüpfung aktualisieren
                	((AbstractTableModel) tblVerknuepfungen.getModel()).fireTableRowsDeleted(selectedRow, selectedRow);
                    
                } catch (InformationException ex) {
                	Object[] options = {"OK"};
                    JOptionPane.showOptionDialog(null, ex.getMessage(), "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                    //Aktualisiere tblVerknuepfung
                    verknuepfteMedienListe = verknuepfung.gebeListe();
                    //Verknuepfung wird komplett gelöscht, also aktualisiere alles
                   	((AbstractTableModel) tblVerknuepfungen.getModel()).fireTableRowsDeleted(0, 1);
                   	elter.aktualisiereVerknuepfungen();
                    dispose();
                   	
                }
            }
        });
        pnlButtonleiste.add(btnEntfernen, "cell 2 0,alignx left,aligny top");

        JButton btnSchliessen = new JButton("Schließen");
        btnSchliessen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                elter.aktualisiereVerknuepfungen();
                dispose();
            }
        });
        pnlButtonleiste.add(btnSchliessen, "cell 4 0");


        tblVerknuepfungen.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tblVerknuepfungen.getSelectedRowCount() < 1) {
                    btnEntfernen.setEnabled(false);
                } else {
                    btnEntfernen.setEnabled(true);
                }
            }
        });
    }


		private class MediumTabellenModell extends AbstractTableModel {

        @Override
        public int getRowCount() {
            return verknuepfteMedienListe.size();
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return "Titel";
                case 1:
                    return "Autor";
                case 2:
                    return "Typ";
            }

            return null;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (getColumnName(columnIndex).equals("Typ")) {
                switch (verknuepfteMedienListe.get(rowIndex).getMedienArt()) {
                    case BUCH:
                        return "Buch";
                    case MUSIK:
                        return "Musik";
                    case FILM:
                        return "Film";
                    case HOERBUCH:
                        return "Hörbuch";
                    case EBOOK:
                        return "E-Book";
                    case TVSERIE:
                        return "TV-Serie";
                }
            }

            return verknuepfteMedienListe.get(rowIndex).getParamListe().get(getColumnName(columnIndex));
        }
    }
}
