package gui;

import kontrolle.*;
import model.Medium;
import net.miginfocom.swing.MigLayout;
import org.jdom2.JDOMException;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


public class Hauptfenster extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JTextField txtSuchen;
    private JTable table;
    private List<Medium> medienListe;
    private ListenArt listenArt;
    private boolean suchlisteWirdAngezeigt;
    private VerwaltungsKontrollierer verwaltungsKontrollierer;
    private JButton btnAbbrechen;

    /**
     * Create the frame.
     */
    public Hauptfenster(final VerwaltungsKontrollierer verwaltungsKontrollierer) {
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                if (JOptionPane.showConfirmDialog(null, "Möchten Sie wirklich das Programm beenden?", "Meldung",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    try {
                        verwaltungsKontrollierer.speicherSitzung(verwaltungsKontrollierer.getMedienVerwaltung());
                    } catch (IOException | NullPointerException ex) {
                        JOptionPane.showConfirmDialog(null, ex.getMessage(), "Meldung", JOptionPane.DEFAULT_OPTION);

                    }
                    System.exit(0);
                }
            }
        });

        this.verwaltungsKontrollierer = verwaltungsKontrollierer;
        listenArt = ListenArt.MEDIENARCHIV;

        suchlisteWirdAngezeigt = false;

        setTitle("Medienverwaltung");
        setBounds(100, 100, 656, 594);

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu mnDatei = new JMenu("Datei");
        menuBar.add(mnDatei);

        final JMenuItem mntmImport = new JMenuItem("Import");
        mntmImport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                //Frage nach dem Lagerort
                String dateiPfad = "";
                JFileChooser fileChooser = new JFileChooser();
	            if (fileChooser.showOpenDialog(null) == javax.swing.JFileChooser.APPROVE_OPTION) {
	                dateiPfad = fileChooser.getSelectedFile().getPath();
                }
                if(dateiPfad.isEmpty())	{	return; }
                try {
                    int[] result = verwaltungsKontrollierer.getIOKontrollierer().importiereMedien(dateiPfad);
                    ladeArchivListe();
                    Object[] options = {"Ok"};
                    if(result[2] > 0)	{
                    	if(result[3] > 0)
                    		JOptionPane.showOptionDialog(null,"Es wurden "+ (result[0] + result[1]) + " Bewertung(en) und " + result[1] + " Element(e) hinzugefügt. " + result[3] + " Element(e) wurden übersprungen, " + result[2] + " Element(e) waren fehlerhaft." , "Meldung", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                    	else
                    		JOptionPane.showOptionDialog(null,"Es wurden "+ (result[0] + result[1]) + " Bewertung(en) und " + result[1] + " Element(e) hinzugefügt. " + result[2] + " Element(e) waren fehlerhaft." , "Meldung", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);		
                    }
                    else
                    {
                    	if(result[3] > 0)
                    		JOptionPane.showOptionDialog(null,"Es wurden "+ (result[0] + result[1]) + " Bewertung(en) und " + result[1] + " Element(e) hinzugefügt. " + result[3] + " Element(e) wurden übersprungen." , "Meldung", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                    	else
                    		JOptionPane.showOptionDialog(null,"Es wurden "+ (result[0] + result[1]) + " Bewertung(en) und " + result[1] + " Element(e) hinzugefügt. " , "Meldung", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                    }
                } catch (MissingValueException | JDOMException | IOException ex) {
                    Object[] options = {"Ok"};
                    JOptionPane.showOptionDialog(null, "Fehler : " + ex.getMessage(), "Fehler", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);

                }
            }
        });
        mnDatei.add(mntmImport);

        /**
         * Rufe exportiere-Methode auf
         */
        final JMenuItem mntmExport = new JMenuItem("Export");

        mntmExport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                //Frage nach Zielspeicherort
                String dateiPfad = "";
                JFileChooser fileChooser = new JFileChooser();
                if (fileChooser.showSaveDialog(null) == javax.swing.JFileChooser.APPROVE_OPTION) {
                    dateiPfad = fileChooser.getSelectedFile().getPath();
                }
                if(dateiPfad.isEmpty())	{	return; }
                int[] selected = table.getSelectedRows();
                List<Medium> MedienZumExportieren = new ArrayList<Medium>();
                for (int i : selected) {
                	MedienZumExportieren.add(medienListe.get(table.getRowSorter().convertRowIndexToModel(i)));
                }
                try {
                    verwaltungsKontrollierer.getIOKontrollierer().exportiereMedien(MedienZumExportieren, dateiPfad);
                } catch (MissingValueException | IllegalArgumentException | IOException | RatingFormatException ex) {
                    Object[] options = {"Ok"};
                    JOptionPane.showOptionDialog(null, "Fehler : " + ex.getMessage(), "Fehler", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                }

            }
        });
        mnDatei.add(mntmExport);

        final JMenuItem mntmDrucken = new JMenuItem("Drucken");
        mntmDrucken.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
            	
            	int[] indizes = table.getSelectedRows();
            	LinkedList<Medium> selMedien = new LinkedList<>();
            	
            	for(int index: indizes) {
            		selMedien.add(medienListe.get(table.getRowSorter().convertRowIndexToModel(index)));
            	}
            	
            	try {
					verwaltungsKontrollierer.getListenKontrollierer().medienDrucken(selMedien/*verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv()*/, "output/Druck.txt", true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        mnDatei.add(mntmDrucken);

        JSeparator separator = new JSeparator();
        mnDatei.add(separator);

        JSeparator separator_1 = new JSeparator();
        mnDatei.add(separator_1);

        /**
         * Speichere Sitzung beim Beenden
         */
        JMenuItem mntmBeenden = new JMenuItem("Beenden");
        mntmBeenden.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (JOptionPane.showConfirmDialog(
                        null, "Möchten Sie wirklich das Programm beenden?",
                        "Meldung",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    try {
                        verwaltungsKontrollierer.speicherSitzung(verwaltungsKontrollierer.getMedienVerwaltung());
                    } catch (IOException | NullPointerException ex) {
                        JOptionPane.showConfirmDialog(null, ex.getMessage(), "Meldung", JOptionPane.DEFAULT_OPTION);

                    }
                    System.exit(0);
                }
            }
        });
        mnDatei.add(mntmBeenden);

        JMenu mnMedienarchiv = new JMenu("Medienarchiv");
        menuBar.add(mnMedienarchiv);

        final JRadioButtonMenuItem rdbtnmntmArchiv = new JRadioButtonMenuItem("Archiv");
        final JRadioButtonMenuItem rdbtnmntmWunschliste = new JRadioButtonMenuItem("Wunschliste");

        ButtonGroup btngrp = new ButtonGroup();
        btngrp.add(rdbtnmntmArchiv);
        btngrp.add(rdbtnmntmWunschliste);

        rdbtnmntmArchiv.setSelected(true);
        mnMedienarchiv.add(rdbtnmntmArchiv);

        mnMedienarchiv.add(rdbtnmntmWunschliste);

        JMenu mnMedium = new JMenu("Medium");
        menuBar.add(mnMedium);

        JMenuItem mntmHinzufgen = new JMenuItem("Hinzufügen");
        mntmHinzufgen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MediumHinzufuegen(Hauptfenster.this, verwaltungsKontrollierer, listenArt).setVisible(true);
            }
        });
        mnMedium.add(mntmHinzufgen);

        final JMenuItem mntmBearbeiten = new JMenuItem("Anzeigen / Bearbeiten");
        mntmBearbeiten.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(listenArt == ListenArt.MEDIENARCHIV) {
                    new MediumBearbeiten(Hauptfenster.this, verwaltungsKontrollierer, medienListe.get(table.getRowSorter().convertRowIndexToModel(table.getSelectedRow()))).setVisible(true);
                }
                else {
                    new WunschlistenMediumBearbeiten(Hauptfenster.this, verwaltungsKontrollierer, medienListe.get(table.getRowSorter().convertRowIndexToModel(table.getSelectedRow()))).setVisible(true);
                }
            }
        });
        mnMedium.add(mntmBearbeiten);

        final JMenuItem mntmVerknpfen = new JMenuItem("Verknüpfen");
        mntmVerknpfen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] selected = table.getSelectedRows();
                List<Medium> selectedMedien = new ArrayList<Medium>();
                for (int i : selected) {
                    selectedMedien.add(medienListe.get(table.getRowSorter().convertRowIndexToModel(i)));
                }
                String beschreibung = "";
                while ("".equals(beschreibung)) {
                    beschreibung = JOptionPane.showInputDialog("Gebe eine Beschreibung für die Verknüpfung an.");
                }
                verwaltungsKontrollierer.getVerknuepfungsKontrollierer().verknuepfen(selectedMedien, beschreibung);
            }
        });

        final JMenuItem mntmEntfernen = new JMenuItem("Entfernen");
        mntmEntfernen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

					HashSet<Medium> tmpListe = new HashSet<>();


                for (int i : table.getSelectedRows()) {
                    Medium tmp = medienListe.get(table.getRowSorter().convertRowIndexToModel(i));
                    tmpListe.add(tmp);
                }


					for(Medium tmp : tmpListe) {
					    verwaltungsKontrollierer.getListenKontrollierer().loescheMedium(tmp, listenArt);
					    if(suchlisteWirdAngezeigt) {
					        medienListe.remove(tmp);
					    }
					}

					if(suchlisteWirdAngezeigt) {
					    ((AbstractTableModel)table.getModel()).fireTableDataChanged();
					}
					else {
					    ladeArchivListe();
					}

            }
        });
        mnMedium.add(mntmEntfernen);
        mnMedium.add(mntmVerknpfen);

        final JMenuItem mntmVerleihen = new JMenuItem("Verleihen");
        mntmVerleihen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] selected = table.getSelectedRows();
                List<Medium> selectedMedien = new ArrayList<Medium>();
                for (int i : selected) {
                    selectedMedien.add(medienListe.get(table.getRowSorter().convertRowIndexToModel(i)));
                }
                new Verleihen(Hauptfenster.this, verwaltungsKontrollierer, selectedMedien).setVisible(true);
            }
        });
        mnMedium.add(mntmVerleihen);


        final JMenuItem mntmInArchivUeberfuehren = new JMenuItem("In Archiv überführen");
        mntmInArchivUeberfuehren.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HashSet<Medium> tmpListe = new HashSet<>();

                for (int i : table.getSelectedRows()) {
                    Medium tmp = medienListe.get(table.getRowSorter().convertRowIndexToModel(i));
                    tmpListe.add(tmp);
                }

                for(Medium tmp : tmpListe) {
                    try {
                        verwaltungsKontrollierer.getListenKontrollierer().inMedienarchivHinzufuegen(tmp.getMedienArt(), tmp.getParamListe());
                        verwaltungsKontrollierer.getListenKontrollierer().loescheMedium(tmp, ListenArt.WUNSCHLISTE);
                        ladeArchivListe();
                    } catch (DuplicateMediumException | MissingValueException ex) {
                        Object[] options = {"Ok"};
                        JOptionPane.showOptionDialog(null, "Fehler : " + ex.getMessage(), "Fehler", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                    }
                }
            }
        });


        mnMedium.add(mntmInArchivUeberfuehren);

        JPanel contentPane = new JPanel();

        contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        setContentPane(contentPane);
        Dimension d = new Dimension();
        d.setSize(600, 400);
        contentPane.setMinimumSize(new Dimension(600, 400));
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{650};
        gbl_contentPane.rowHeights = new int[]{0, 0};
        gbl_contentPane.columnWeights = new double[]{1.0};
        gbl_contentPane.rowWeights = new double[]{0.0, 1.0};
        contentPane.setLayout(gbl_contentPane);

        JPanel pnlSuchen = new JPanel();
        GridBagConstraints gbc_pnlSuchen = new GridBagConstraints();
        gbc_pnlSuchen.anchor = GridBagConstraints.BASELINE;
        gbc_pnlSuchen.fill = GridBagConstraints.HORIZONTAL;
        gbc_pnlSuchen.insets = new Insets(0, 0, 5, 0);
        gbc_pnlSuchen.gridx = 0;
        gbc_pnlSuchen.gridy = 0;
        contentPane.add(pnlSuchen, gbc_pnlSuchen);
        pnlSuchen.setLayout(new MigLayout("", "[57px:57px:57px][grow][][]", "[19px]"));

        JLabel lblSuchen = new JLabel("Suchen:");
        pnlSuchen.add(lblSuchen, "cell 0 0,alignx left,aligny center");

        txtSuchen = new JTextField();
        pnlSuchen.add(txtSuchen, "cell 1 0,growx,aligny center");
        txtSuchen.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                ladeSuchListe(verwaltungsKontrollierer.getSuchKontrollierer().universaleSuche(txtSuchen.getText() + e.getKeyChar(), listenArt));
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtSuchen.setColumns(10);

        JButton btnErwSuche = new JButton("erweiterte Suche");
        btnErwSuche.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new ErweiterteSuche(Hauptfenster.this, verwaltungsKontrollierer, listenArt).setVisible(true);
            }
        });

        pnlSuchen.add(btnErwSuche, "cell 3 0");

        JPanel pnlHauptbereich = new JPanel();
        GridBagConstraints gbc_pnlHauptbereich = new GridBagConstraints();
        gbc_pnlHauptbereich.fill = GridBagConstraints.BOTH;
        gbc_pnlHauptbereich.gridx = 0;
        gbc_pnlHauptbereich.gridy = 1;
        contentPane.add(pnlHauptbereich, gbc_pnlHauptbereich);
        pnlHauptbereich.setLayout(new MigLayout("", "[grow]", "[grow]"));

        JScrollPane pnlMedienListe = new JScrollPane();
        pnlHauptbereich.add(pnlMedienListe, "cell 0 0,grow");

        table = new JTable();
        table.setFillsViewportHeight(true);
        table.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{
                        "Titel", "Autor", "Typ"
                }
        ));
        table.setAutoCreateRowSorter(true);

        pnlMedienListe.setViewportView(table);

        JPanel pnlButtonLeiste = new JPanel();
        pnlHauptbereich.add(pnlButtonLeiste, "cell 0 0,alignx left,aligny top");
        pnlButtonLeiste.setLayout(new GridLayout(7, 1, 10, 5));

        JButton btnHinzufuegen = new JButton("Hinzufügen");
        btnHinzufuegen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MediumHinzufuegen(Hauptfenster.this, verwaltungsKontrollierer, listenArt).setVisible(true);
            }
        });
        pnlButtonLeiste.add(btnHinzufuegen);

        final JButton btnBearbeiten = new JButton("Anzeigen / Bearbeiten");
        btnBearbeiten.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(listenArt == ListenArt.MEDIENARCHIV) {
                    new MediumBearbeiten(Hauptfenster.this, verwaltungsKontrollierer, medienListe.get(table.getRowSorter().convertRowIndexToModel(table.getSelectedRow()))).setVisible(true);
                }
                else {
                    new WunschlistenMediumBearbeiten(Hauptfenster.this, verwaltungsKontrollierer, medienListe.get(table.getRowSorter().convertRowIndexToModel(table.getSelectedRow()))).setVisible(true);
                }
            }
        });
        pnlButtonLeiste.add(btnBearbeiten);

        final JButton btnVerknuepfen = new JButton("Verknüpfen");
        btnVerknuepfen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] selected = table.getSelectedRows();
                List<Medium> selectedMedien = new ArrayList<Medium>();
                for (int i : selected) {
                    selectedMedien.add(medienListe.get(table.getRowSorter().convertRowIndexToModel(i)));
                }
                String beschreibung = "";
                while ("".equals(beschreibung)) {
                    beschreibung = JOptionPane.showInputDialog("Gebe eine Beschreibung für die Verknüpfung an.");
                }
                try {
                    verwaltungsKontrollierer.getVerknuepfungsKontrollierer().verknuepfen(selectedMedien, beschreibung);
                } catch (EmptyStringException ex) {
                    Object[] options = {"OK"};
                    JOptionPane.showOptionDialog(null, ex.getMessage(), "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                } catch (NullPointerException ex1) {//Wenn man abbrechen drückt
                }

            }
        });

        final JButton btnEntfernen = new JButton("Entfernen");
        btnEntfernen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

					HashSet<Medium> tmpListe = new HashSet<>();

                for (int i : table.getSelectedRows()) {
                    Medium tmp = medienListe.get(table.getRowSorter().convertRowIndexToModel(i));
                    tmpListe.add(tmp);
                }


					for(Medium tmp : tmpListe) {
					    verwaltungsKontrollierer.getListenKontrollierer().loescheMedium(tmp, listenArt);
					    if(suchlisteWirdAngezeigt) {
					        medienListe.remove(tmp);
					    }
					}

					if(suchlisteWirdAngezeigt) {
					    ((AbstractTableModel)table.getModel()).fireTableDataChanged();
					}
					else {
					    ladeArchivListe();
					}
            }
        });
        pnlButtonLeiste.add(btnEntfernen);
        pnlButtonLeiste.add(btnVerknuepfen);

        final JButton btnVerleihen = new JButton("Verleihen");
        btnVerleihen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                int[] selected = table.getSelectedRows();
                List<Medium> selectedMedien = new ArrayList<Medium>();
                for (int i : selected) {
                    selectedMedien.add(medienListe.get(table.getRowSorter().convertRowIndexToModel(i)));
                }
                new Verleihen(Hauptfenster.this, verwaltungsKontrollierer, selectedMedien).setVisible(true);
            }

        });
        pnlButtonLeiste.add(btnVerleihen);

        final JButton btnDrucken = new JButton("Drucken");
        btnDrucken.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] indizes = table.getSelectedRows();
                LinkedList<Medium> selMedien = new LinkedList<>();

                for (int index : indizes) {
                    selMedien.add(medienListe.get(table.getRowSorter().convertRowIndexToModel(index)));
                }

                try {
                    verwaltungsKontrollierer.getListenKontrollierer().medienDrucken(selMedien/*verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv()*/, "output/Druck.txt", true);
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
        pnlButtonLeiste.add(btnDrucken);

        final JButton btnInArchivUeberfuehren = new JButton("In Archiv überführen");
        pnlButtonLeiste.add(btnInArchivUeberfuehren);
        btnInArchivUeberfuehren.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HashSet<Medium> tmpListe = new HashSet<>();

                for (int i : table.getSelectedRows()) {
                    Medium tmp = medienListe.get(table.getRowSorter().convertRowIndexToModel(i));
                    tmpListe.add(tmp);
                }

                for (Medium tmp : tmpListe) {
                    try {
                        verwaltungsKontrollierer.getListenKontrollierer().inMedienarchivHinzufuegen(tmp.getMedienArt(), tmp.getParamListe());
                        verwaltungsKontrollierer.getListenKontrollierer().loescheMedium(tmp, ListenArt.WUNSCHLISTE);
                        ladeArchivListe();
                    } catch (DuplicateMediumException | MissingValueException ex) {
                        Object[] options = {"Ok"};
                        JOptionPane.showOptionDialog(null, "Fehler : " + ex.getMessage(), "Fehler", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
                    }
                }
            }
        });


        rdbtnmntmArchiv.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                btnInArchivUeberfuehren.setEnabled(false);
                listenArt = ListenArt.MEDIENARCHIV;
                mntmImport.setEnabled(true);
                ladeArchivListe();
            }
        });
        rdbtnmntmWunschliste.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                listenArt = ListenArt.WUNSCHLISTE;
                mntmImport.setEnabled(false);
                ladeArchivListe();
            }
        });

        table.addMouseListener(new MouseListener() {
            @Override
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                Point p = me.getPoint();

                if (me.getButton() == 1 && me.getClickCount() == 2) {
                    int row = table.rowAtPoint(p);
                    if (row >= 0)
                        table.setRowSelectionInterval(row, row);
                    else
                        table.clearSelection();

                    btnBearbeiten.doClick();
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (table.getSelectedRowCount() >= 2) {
                    if (listenArt == ListenArt.MEDIENARCHIV) {
                        btnVerknuepfen.setEnabled(true);
                        mntmVerknpfen.setEnabled(true);
                    }
                } else {
                    btnVerknuepfen.setEnabled(false);
                    mntmVerknpfen.setEnabled(false);
                }
                if (table.getSelectedRowCount() >= 1) {
                    btnBearbeiten.setEnabled(true);
                    mntmBearbeiten.setEnabled(true);

                    btnDrucken.setEnabled(true);
                    mntmDrucken.setEnabled(true);

                    mntmExport.setEnabled(true);

                    if (listenArt == ListenArt.MEDIENARCHIV) {
                        btnVerleihen.setEnabled(true);
                        mntmVerleihen.setEnabled(true);
                    }

                    btnEntfernen.setEnabled(true);
                    mntmEntfernen.setEnabled(true);

                    if (listenArt == ListenArt.WUNSCHLISTE) {
                        btnInArchivUeberfuehren.setEnabled(true);
                        mntmInArchivUeberfuehren.setEnabled(true);
                    } else {
                        btnInArchivUeberfuehren.setEnabled(false);
                        mntmInArchivUeberfuehren.setEnabled(false);
                    }
                } else {
                    btnBearbeiten.setEnabled(false);
                    mntmBearbeiten.setEnabled(false);

                    btnDrucken.setEnabled(false);
                    mntmDrucken.setEnabled(false);

                    btnVerleihen.setEnabled(false);
                    mntmVerleihen.setEnabled(false);

                    btnInArchivUeberfuehren.setEnabled(false);
                    mntmInArchivUeberfuehren.setEnabled(false);

                    btnEntfernen.setEnabled(false);
                    mntmEntfernen.setEnabled(false);

                    mntmExport.setEnabled(false);
                }
            }
        });

        btnVerknuepfen.setEnabled(false);
        mntmVerknpfen.setEnabled(false);

        btnBearbeiten.setEnabled(false);
        mntmBearbeiten.setEnabled(false);

        btnDrucken.setEnabled(false);
        mntmDrucken.setEnabled(false);

        btnVerleihen.setEnabled(false);
        mntmVerleihen.setEnabled(false);

        btnInArchivUeberfuehren.setEnabled(false);
        mntmInArchivUeberfuehren.setEnabled(false);

        btnEntfernen.setEnabled(false);
        mntmEntfernen.setEnabled(false);

        mntmExport.setEnabled(false);

        medienListe = verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv();

        table.setModel(new MediumTabellenModell());

                btnAbbrechen = new JButton("Abbrechen");
                btnAbbrechen.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ladeArchivListe();
                        txtSuchen.setText("");
                    }
                });

                        pnlSuchen.add(btnAbbrechen, "cell 2 0");

                                btnAbbrechen.setEnabled(false);

        /*JPopupMenu popupmenu = new JPopupMenu();
        popupmenu.add(mntmHinzufgen);
        popupmenu.add(mntmBearbeiten);
        popupmenu.add(mntmVerknpfen);
        popupmenu.add(mntmVerleihen);
        popupmenu.add(mntmDrucken);*/
    }


    public void ladeArchivListe() {
        btnAbbrechen.setEnabled(false);
        txtSuchen.setText("");

        suchlisteWirdAngezeigt = false;

        if (listenArt == ListenArt.MEDIENARCHIV)
            medienListe = verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv();

        if (listenArt == ListenArt.WUNSCHLISTE)
            medienListe = verwaltungsKontrollierer.getMedienVerwaltung().getWunschliste();

        ((AbstractTableModel) table.getModel()).fireTableDataChanged();
    }

    public void ladeSuchListe(List<Medium> medienListe) {
    	suchlisteWirdAngezeigt = true;
    	btnAbbrechen.setEnabled(true);
    	this.medienListe = medienListe;
    	((AbstractTableModel)table.getModel()).fireTableDataChanged();
    	btnAbbrechen.setEnabled(true);
    }

    private class MediumTabellenModell extends AbstractTableModel {
        private static final long serialVersionUID = 1L;

        @Override
        public int getRowCount() {
            return medienListe.size();
        }

        @Override
        public int getColumnCount() {
            return 7;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return "Titel";
                case 1:
                    return "Autor";
                case 2:
                    return "Typ";
                case 3:
                    return "Genre";
                case 4:
                    return "Jahr";
                case 5:
                    return "Kaufpreis";
                case 6:
                    return "Speicherort";
            }

            return null;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (getColumnName(columnIndex).equals("Typ")) {
                switch (medienListe.get(rowIndex).getMedienArt()) {
                    case BUCH:
                        return "Buch";
                    case MUSIK:
                        return "Musik";
                    case FILM:
                        return "Film";
                    case HOERBUCH:
                        return "Hörbuch";
                    case EBOOK:
                        return "E-Book";
                    case TVSERIE:
                        return "TV-Serie";
                }
            }

            return medienListe.get(rowIndex).getParamListe().get(getColumnName(columnIndex));
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
            	VerwaltungsKontrollierer verwaltungKonAlt = new VerwaltungsKontrollierer("init");

            	//Suche nach vorhandenen Config
        		boolean isExceptionOccured = false;
        		try	{
        			verwaltungKonAlt.ladeSitzung();
        		}
        		catch (NullPointerException | IOException | ClassNotFoundException ex)	{
        			isExceptionOccured = true;
        		}
                if(isExceptionOccured)	{
        			String eingabeWert = "";
        			Object[] options = {"Ok"};
        			JOptionPane.showOptionDialog(null,"Es liegt kein altes Archiv vor. Neues Archiv wird angelegt" , "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        			while("".equals(eingabeWert)){
        				eingabeWert = JOptionPane.showInputDialog("Gebe den Benutzernamen ein.");
        			}
        			if(eingabeWert == null)	{
        				JOptionPane.showOptionDialog(null,"Das Programm wird beendet." , "Beenden", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
        				System.exit(0);
        			}
        			else
        			{
        				//Erstelle eine neue Verwaltung
        				verwaltungKonAlt = new VerwaltungsKontrollierer(eingabeWert);
        				//Akzeptiere die AGB
        				boolean agbAccepted = false;
        				Object[] opts = {"Danke"};
        				String optText = "Vielen Dank, " + eingabeWert + ", dass Sie sich für unseres \ninnovatives Produkt entschieden haben.\n\nSie benutzen zurzeit die Version 1.0 des Programms.\nDas Design-Team wünscht Ihnen viel Erfolg und Geduld\nbei der Arbeit mit unserem Tool!";
        				while(!agbAccepted)	{
        					int result = JOptionPane.showOptionDialog(null,optText , "Willkommen", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, opts, opts[0]);
        					switch(result)	{
        					case 0:
        						{
        							agbAccepted = true;
        							break;
        						}
        					}
        				}
        			}
        		}


                try {
                    Hauptfenster frame = new Hauptfenster(verwaltungKonAlt);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
