package gui;

import kontrolle.MissingValueException;
import kontrolle.VerwaltungsKontrollierer;
import model.Medium;
import model.Verknuepfung;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;

import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MediumBearbeiten extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private Map<String, JTextField> textfelder;
	JPanel pnlLinks;
    JTable tblVerknuepfungen;

	Medium medium;

    private int bewertung;
    private String bewertungsText;


	/**
	 * Create the frame.
	 */
	public MediumBearbeiten(final Hauptfenster elter, final VerwaltungsKontrollierer verwaltungsKontrollierer, final Medium medium) {

		this.medium = medium;

		setTitle("Medium anzeigen / bearbeiten");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 708, 498);
        JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[300px:n,grow 80,fill][grow]", "[grow][grow]"));
		
		pnlLinks = new JPanel();
		contentPane.add(pnlLinks, "cell 0 0,grow");
		
		final Object[] headers = {"Bewertung", "Bewerter", "Beschreibung"};

        JPanel pnlRechts = new JPanel();
		contentPane.add(pnlRechts, "cell 1 0,grow");
		pnlRechts.setLayout(new GridLayout(0, 1, 0, 0));

        JPanel pnlRechts2 = new JPanel();
		pnlRechts.add(pnlRechts2);
		pnlRechts2.setLayout(new MigLayout("", "[grow]", "[][grow][][grow]"));
        
        JLabel lblBewertungen = new JLabel("Bewertungen");
        pnlRechts2.add(lblBewertungen, "cell 0 0");

        JScrollPane pnlBewertungen = new JScrollPane();
		pnlRechts2.add(pnlBewertungen, "cell 0 1,grow");

        final JTable tblBewertungen = new JTable(new Object[][]{}, headers);
		tblBewertungen.setFillsViewportHeight(true);
		pnlBewertungen.setViewportView(tblBewertungen);
		tblBewertungen.setBorder(null);
		tblBewertungen.setModel(new BewertungTableModel());
		tblBewertungen.getColumnModel().getColumn(0).setMinWidth(75);
		tblBewertungen.getColumnModel().getColumn(0).setMaxWidth(75);
		tblBewertungen.getColumnModel().getColumn(1).setPreferredWidth(115);
		tblBewertungen.getColumnModel().getColumn(1).setMinWidth(115);
		tblBewertungen.getColumnModel().getColumn(2).setPreferredWidth(215);
		tblBewertungen.getColumnModel().getColumn(2).setMinWidth(215);
        
        JLabel lblVerknuepfunen = new JLabel("Verknüpfungen");
        pnlRechts2.add(lblVerknuepfunen, "cell 0 2");

        JScrollPane pnlVerknuepfungen = new JScrollPane();
		pnlRechts2.add(pnlVerknuepfungen, "cell 0 3,grow");

        tblVerknuepfungen = new JTable();
		tblVerknuepfungen.setFillsViewportHeight(true);
        tblVerknuepfungen.setModel(new VerknuepfungTableModel());
		tblVerknuepfungen.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = ((JTable) e.getSource()).rowAtPoint(e.getPoint());

                    if (index != -1) {
                        Verknuepfung verknuepfung = medium.getVerknuepfungList().get(index);
                        new Verknuepfen(MediumBearbeiten.this, verwaltungsKontrollierer, verknuepfung).setVisible(true);
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        pnlVerknuepfungen.setViewportView(tblVerknuepfungen);

        JPanel panel = new JPanel();
		contentPane.add(panel, "cell 1 1,alignx right,growy");
		panel.setLayout(new MigLayout("", "[][]", "[]"));

        JButton btnAbbrechen = new JButton("Abbrechen");
        btnAbbrechen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                medium.getBewertungList().get(0).setBewertung(bewertung);
                medium.getBewertungList().get(0).setBeschreibung(bewertungsText);

                dispose();
            }
        });
		panel.add(btnAbbrechen, "cell 0 0");

        final JButton btnSpeichern = new JButton("Speichern");
        btnSpeichern.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Map<String, String> params = new HashMap<>();

                for (String aparam : medium.getParamListe().keySet()) {
                    params.put(aparam, textfelder.get(aparam).getText().trim());
                }

                try {
                    verwaltungsKontrollierer.getMedienKontrollierer().bearbeiteMedium(medium, params);
                    elter.ladeArchivListe();
                    dispose();
                } catch (MissingValueException ex) {
                    Object[] options = {"OK"};
                    JOptionPane.showOptionDialog(null, ex.getMessage(), "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                }
            }
        });
		panel.add(btnSpeichern, "cell 1 0");

        generateTextfields();
	}

    public void aktualisiereVerknuepfungen() {
        ((AbstractTableModel)tblVerknuepfungen.getModel()).fireTableDataChanged();
    }

	private void generateTextfields() {
		textfelder = new HashMap<>();

		String rows = "";

		for(int i = 0; i < medium.getParamListe().keySet().size(); i++) {
			rows += "[]";
		}

		pnlLinks.setLayout(new MigLayout("", "[][grow,fill]", rows));

		int row = 0;

        String[] params = medium.getParamListe().keySet().toArray(new String[medium.getParamListe().size()]);
        Arrays.sort(params);

		for(String param : params) {
			JLabel label = new JLabel(param + ":");
			pnlLinks.add(label, "cell 0 " + row);

			JTextField textField = new JTextField();
			pnlLinks.add(textField, "cell 1 " + row + ",growx");
			textField.setColumns(10);
            textField.setText(medium.getParamListe().get(param));

			textfelder.put(param, textField);

			row++;
		}

        bewertung = medium.getBewertungList().get(0).getBewertung();
        bewertungsText = medium.getBewertungList().get(0).getBeschreibung();

		revalidate();
		repaint();
	}

    private class BewertungTableModel extends AbstractTableModel {

        @Override
        public int getRowCount() {
            return medium.getBewertungList().size();
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch(columnIndex) {
                case 0:
                    return medium.getBewertungList().get(rowIndex).getBewertung();
                case 1:
                    return medium.getBewertungList().get(rowIndex).getBewerter();
                case 2:
                    return medium.getBewertungList().get(rowIndex).getBeschreibung();
            }

            return null;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex)
            {
                case 0:
                    return "Bewertung";
                case 1:
                    return "Bewerter";
                case 2:
                    return "Beschreibung";
            }

            return null;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return rowIndex == 0 && columnIndex != 1;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            switch(columnIndex) {
                case 0:
                    try {
                        medium.getBewertungList().get(0).setBewertung(Integer.parseInt((String) aValue));
                    } catch (NumberFormatException ex ) {
                        Object[] options = {"OK"};
                        JOptionPane.showOptionDialog(null, "Sie dürfen nur eine Zahl zwischen 0 und 5 eingeben!", "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                    }
                    catch(IllegalArgumentException ex){
                    	Object[] options = {"OK"};
                        JOptionPane.showOptionDialog(null, ex.getMessage(), "Information", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                    }
                    break;
                case 1:
                    medium.getBewertungList().get(0).setBewerter((String) aValue);
                    break;
                case 2:
                    medium.getBewertungList().get(0).setBeschreibung((String) aValue);
                    break;
            }
        }
    }

    private class VerknuepfungTableModel extends AbstractTableModel {

        @Override
        public int getRowCount() {
            return medium.getVerknuepfungList().size();
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch(columnIndex) {
                case 0:
                    return medium.getVerknuepfungList().get(rowIndex).gebeListe().size();
                case 1:
                    return medium.getVerknuepfungList().get(rowIndex).gebeBeschreibung();
            }

            return null;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex)
            {
                case 0:
                    return "Anzahl Medien";
                case 1:
                    return "Beschreibung";
            }

            return null;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }
    }
}
