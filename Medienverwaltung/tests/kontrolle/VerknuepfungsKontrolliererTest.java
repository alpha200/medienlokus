package kontrolle;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import kontrolle.EmptyStringException;
import kontrolle.VerknuepfungsKontrollierer;
import model.MedienArt;
import model.Medium;
import model.Verknuepfung;

import org.junit.Before;
import org.junit.Test;


public class VerknuepfungsKontrolliererTest {

	VerknuepfungsKontrollierer kontrollierer = new VerknuepfungsKontrollierer();
	Medium med1, med2, med3, med4;
	Map<String, String> map1, map2, map3, map4;
	Verknuepfung testVerknuepfung;
	LinkedList<Medium> medien = new LinkedList<>();
	
	@Before
	public void setUp(){
		
		map1 = new HashMap<String, String>();
		map1.put("Autor", "Rowling");
		map1.put("Titel", "Harry Potter");
		map1.put("Genre", "Fantasy");
		map1.put("Herkunftsland", "England");
		med1 = new Medium(map1, MedienArt.FILM);
		medien.add(med1);
		
		map2 = new HashMap<String, String>();
		map2.put("Autor", "Rowling");
		map2.put("Titel", "Harry Potter");
		map2.put("Genre", "Fantasy");
		map2.put("ISBN", "2432396848");
		med2 = new Medium(map2, MedienArt.BUCH);
		medien.add(med2);
		
		map3 = new HashMap<String, String>();
		map3.put("Autor", "Green Day");
		map3.put("Titel", "American Idiot");
		map3.put("Genre", "Punk-Rock");
		map3.put("Herkunftsland", "Amerika");
		med3 = new Medium(map3, MedienArt.MUSIK);
		medien.add(med3);
		
		map4 = new HashMap<String, String>();
		map4.put("Autor", "Die Aerzte");
		map4.put("Titel", "Himmelblau");
		map4.put("Genre", "Punk-Rock");
		map4.put("Herkunftsland", "Deutschland");
		med4 = new Medium(map4, MedienArt.MUSIK);
		
		testVerknuepfung = new Verknuepfung("Gehoert zusammen", medien);
	}
	
	private void listeSetzen(){
		List<Verknuepfung> hilfsVerknuepfungsListe1 = med1.getVerknuepfungList();
		hilfsVerknuepfungsListe1.add(testVerknuepfung);
		med1.setVerknuepfungList(hilfsVerknuepfungsListe1);
		
		List<Verknuepfung> hilfsVerknuepfungsListe2 = med2.getVerknuepfungList();
		hilfsVerknuepfungsListe2.add(testVerknuepfung);
		med2.setVerknuepfungList(hilfsVerknuepfungsListe2);
		
		List<Verknuepfung> hilfsVerknuepfungsListe3 = med3.getVerknuepfungList();
		hilfsVerknuepfungsListe3.add(testVerknuepfung);
		med3.setVerknuepfungList(hilfsVerknuepfungsListe3);
	}
	/**
	 * Verknuepfung wird erstellt.
	 * Es wird geprueft, ob die Verknuepfung korrekt ist
	 * und die Medien jeweils in einer Verknuepfung sind.
	 */
	@Test
	public void testVerknuepfen() {
		Verknuepfung erstellteVerknuepfung = kontrollierer.verknuepfen(medien, "Gehoert zusammen");
		assertEquals(testVerknuepfung, erstellteVerknuepfung);
		assertEquals(1, med1.getVerknuepfungList().size());
		assertEquals(1, med2.getVerknuepfungList().size());
		assertEquals(1, med3.getVerknuepfungList().size());
	}
	
	/**
	 * Verknuepfung wird mit leerer Beschreibung erstellt.
	 */
	@Test(expected = EmptyStringException.class)
	public void testVerknuepfenLeererString() {
		kontrollierer.verknuepfen(medien, "");
	}
	
	/**
	 * Verknuepfung wird mit null-Liste erstellt.
	 */
	@Test(expected = NullPointerException.class)
	public void testVerknuepfenNullListe() {
		kontrollierer.verknuepfen(null, "Gehoert zusammen");
	}
	
	/**
	 * Verknuepfung wird mit einer Liste mit null-Werten erstellt.
	 */
	@Test(expected = NullPointerException.class)
	public void testVerknuepfenNullMedium() {
		medien.add(null);
		kontrollierer.verknuepfen(medien, "Gehoert zusammen");
	}
	
	/**
	 * Verknuepfung wird mit einer Liste mit nur einem Medium erstellt.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testVerknuepfenEinelementigeListe() {
		LinkedList<Medium> einMedium = new LinkedList<>();
		einMedium.add(med1);
		kontrollierer.verknuepfen(einMedium, "Gehoert zusammen");
	}
	
	/**
	 * Verknuepfung wird mit einer Liste mit Duplikaten erstellt.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testVerknuepfenDuplikate() {
		medien.add(med1);
		kontrollierer.verknuepfen(medien, "Gehoert zusammen");
	}
	
	/**
	 * 	Ein Medium wird aus Verknuepfung geloescht.
	 * 	Anfangs sind es 3 Medien.
	 * 	Nachdem eins geloescht wird, beinhaltet die Verknuepfung 2 Medien.
	 * 	Nachdem noch ein Medium geloescht wird, soll die ganze Verknuepfung
	 * 	geloescht werden.
	 */
	@Test
	public void testEntferneMediumAusVerknuepfung() {
		listeSetzen();
		kontrollierer.entferneMediumAusVerknuepfung(med1, testVerknuepfung); 
		int count = testVerknuepfung.gebeListe().size();
		assertEquals(2, count);
		assertTrue(!testVerknuepfung.gebeListe().contains(med1));
		assertEquals(0, med1.getVerknuepfungList().size());
		assertEquals(1, med2.getVerknuepfungList().size());
		assertEquals(1, med3.getVerknuepfungList().size());
		
		try {
			kontrollierer.entferneMediumAusVerknuepfung(med2, testVerknuepfung);
		} catch (InformationException infEx) {
			// Fehlermeldung ist erwartet, soll Ablauf aber nicht unterbrechen
		}
		count = testVerknuepfung.gebeListe().size();
		assertEquals(0, count);
		assertTrue(!testVerknuepfung.gebeListe().contains(med2));
		assertEquals(0, med2.getVerknuepfungList().size());
		assertEquals(0, med3.getVerknuepfungList().size());
	}
	
	/**
	 * Ein null-Medium wird aus Verknuepfung geloescht.
	 */
	@Test(expected = NullPointerException.class)
	public void testEntferneMediumAusVerknuepfungNullMedium(){
		listeSetzen();
		kontrollierer.entferneMediumAusVerknuepfung(null, testVerknuepfung);
	}
	
	/**
	 * Ein Medium wird einer null-Verknuepfung wird geloescht.
	 */
	@Test(expected = NullPointerException.class)
	public void testEntferneMediumAusVerknuepfungNullVerknuepfung(){
		listeSetzen();
		kontrollierer.entferneMediumAusVerknuepfung(med1, null);
	}
	
	/**
	 * Ein Medium aus einer anderen Veknuepfung wird geloescht.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testEntferneMediumAusVerknuepfungMediumNichtVerknuepft(){
		listeSetzen();
		kontrollierer.entferneMediumAusVerknuepfung(med4, testVerknuepfung);
	}
	
	/**
	 * Eine Verknuepfung wird geloescht.
	 */
	@Test
	public void testEntferneVerknuepfung() {
		listeSetzen();
		kontrollierer.entferneVerknuepfung(testVerknuepfung);
		assertEquals(0, testVerknuepfung.gebeListe().size());
		assertEquals(0, med1.getVerknuepfungList().size());
		assertEquals(0, med2.getVerknuepfungList().size());
		assertEquals(0, med3.getVerknuepfungList().size());
	}
	
	/**
	 * Eine null Verknuepfung wird geloescht.
	 */
	@Test(expected = NullPointerException.class)
	public void testEntferneVerknuepfungNullVerknuepfung(){
		kontrollierer.entferneVerknuepfung(null);
	}
	
	/**
	 * Eine Verknuepfungbeschreibung wird geaendert.
	 */
	@Test
	public void testbearbeiteVerknuepfungsbeschreibung() {
		listeSetzen();
		kontrollierer.bearbeiteVerknuepfungsBeschreibung(testVerknuepfung, "Neue Beschreibung");
		assertEquals("Neue Beschreibung", testVerknuepfung.gebeBeschreibung());
	}
	
	/**
	 * Beschreibung wird durch null ersetzt.
	 */
	@Test (expected = NullPointerException.class)
	public void testbearbeiteVerknuepfungsbeschreibungNull() {
		listeSetzen();
		kontrollierer.bearbeiteVerknuepfungsBeschreibung(testVerknuepfung, null);
	}
	
	/**
	 * Beschreibung wird durch leeren String ersetzt.
	 */
	@Test (expected = EmptyStringException.class)
	public void testbearbeiteVerknuepfungsbeschreibungLeer() {
		listeSetzen();
		kontrollierer.bearbeiteVerknuepfungsBeschreibung(testVerknuepfung, "");
	}
	
	/**
	 * Beschreibung einer null-Verknuepfung wird ersetzt.
	 */
	@Test (expected = NullPointerException.class)
	public void testbearbeiteVerknuepfungsbeschreibungNullVerknupefung() {
		listeSetzen();
		kontrollierer.bearbeiteVerknuepfungsBeschreibung(null, "Neue Beschreibung");
	}
	
}