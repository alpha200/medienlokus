package kontrolle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


import kontrolle.IOKontrollierer;
import kontrolle.MedienKontrollierer;
import kontrolle.MissingValueException;
import kontrolle.RatingFormatException;
import kontrolle.VerwaltungsKontrollierer;
import model.Bewertung;
import model.MedienArt;
import model.Medium;

import org.jdom2.JDOMException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class IOKontrolliererTest {

    @Rule
    public ExpectedException expected = ExpectedException.none();

    private VerwaltungsKontrollierer verwaltungsKontrollierer;
    private IOKontrollierer ioKontrollierer;
    private MedienKontrollierer medienKontrollierer;

    @Before
    public void setUp() throws Exception {
        verwaltungsKontrollierer = new VerwaltungsKontrollierer("TestNutzer");
        ioKontrollierer = new IOKontrollierer(verwaltungsKontrollierer);
        medienKontrollierer = new MedienKontrollierer(verwaltungsKontrollierer);

        verwaltungsKontrollierer.setListenKontrollierer(ioKontrollierer);
        verwaltungsKontrollierer.setMedienKontrollierer(medienKontrollierer);
    }
    
    // Erstellt ein Beispielmedium fuer einige Testfaelle
    private Medium beispielmedium(){
    	Hashtable<String, String> content = new Hashtable<>();
        content.put("Titel", "Eragon");
        content.put("Autor", "Christopher Paolini");
        content.put("Genre", "Fantasy");
        content.put("Jahr", "2004");
        content.put("Speicherort", "Bücherregal");
        content.put("ISBN", "432451");

        return new Medium(content, MedienArt.BUCH);
    }

    // Tests fuer importiereMedien() //
    
    /**
     * Testst, ob der Import von vorhandenen korrekten XML-Dateien fehlerfrei verlaeuft
     * @throws NumberFormatException
     * @throws MissingValueException
     * @throws JDOMException
     * @throws IOException
     */
    @Test
    public void testImportiereMedien() throws NumberFormatException, MissingValueException, JDOMException, IOException {
        ioKontrollierer.importiereMedien("input/bewertung2.xml");
    }

    /**
     * Erwartet eine Fehlermeldung, da in der XML-Datei keine Werte vorliegen
     * @throws NumberFormatException
     * @throws MissingValueException
     * @throws JDOMException
     * @throws IOException
     */
    //@Test
    //public void testImportiereMedienLeererString() throws NumberFormatException, MissingValueException, JDOMException, IOException {
    //
    //    expected.expect(MissingValueException.class);
    //    ioKontrollierer.importiereMedien("input/leererString.xml");
    //}

    /**
     * Erwartet eine Fehlermeldung, da ein Pflicht-Attribut fuer ein Medium in der XML-Datei fehlt
     * @throws NumberFormatException
     * @throws MissingValueException
     * @throws JDOMException
     * @throws IOException
     */
    //  @Test
    //  public void testImportiereMedienElementLeer() throws NumberFormatException, MissingValueException, JDOMException, IOException {
    //
    //     expected.expect(NullPointerException.class);
    //     ioKontrollierer.importiereMedien("input/fehlendesAttribut.xml");
    // }

    /**
     * Erwartet eine Fehlermeldung, da der Dateipfad nicht existiert
     * @throws NumberFormatException
     * @throws MissingValueException
     * @throws JDOMException
     * @throws IOException
     */
    @Test
    public void testImportiereMedienKeineDatei() throws NumberFormatException, MissingValueException, JDOMException, IOException {

        expected.expect(IOException.class);
        ioKontrollierer.importiereMedien("input/michGibtsGarnicht.xml");
    }

    /**
     * Erwartet eine Fehlermeldung, da die XML-Datei leer ist
     * @throws NumberFormatException
     * @throws MissingValueException
     * @throws JDOMException
     * @throws IOException
     */
    @Test
    public void testImportiereMedienLeereDatei() throws NumberFormatException, MissingValueException, JDOMException, IOException {

        expected.expect(JDOMException.class);
        ioKontrollierer.importiereMedien("input/leer.xml");
    }

    /**
     * Erwartet eine Fehlermeldung, da ein Medium eine fehlerhafte Bewertung enthaelt
     * @throws NumberFormatException
     * @throws MissingValueException
     * @throws JDOMException
     * @throws IOException
     */
//    @Test
//    public void testImportiereMedienFalscheBewertung() throws NumberFormatException, MissingValueException, JDOMException, IOException {
//
//        expected.expect(MissingValueException.class);
//        ioKontrollierer.importiereMedien("input/falscheBewertung.xml");
//    }
    
    // Tests fuer exportiereMedien() //

    /**
     * Erwartet eine Fehlermeldung, da der exportierten Datei die Pflichtangabe "Titel" fehlt
     * @throws MissingValueException
     * @throws RatingFormatException
     * @throws IOException
     */
    @Test
    public void testExportiereMedienStringLeer() throws MissingValueException, RatingFormatException, IOException {
        ArrayList<Medium> selektierteMedien = new ArrayList<>();

        Medium medium1 = beispielmedium();

        Hashtable<String, String> content = new Hashtable<>();
        //content.put("Titel", "He's a Pirate"); Medium ohne essentielles Attribut
        content.put("Autor", "Klaus Badelt");
        content.put("Genre", "Piratisch");
        content.put("Jahr", "2003");
        content.put("Speicherort", "C:/Users/Hans/hesapirate.ogg");
        content.put("ISAN", "42");
        content.put("Album", "Pirates of the Carribean: The Curse of the Black Pearl");
        content.put("Titelnummer", "15");

        Medium medium2 = new Medium(content, MedienArt.MUSIK);


        Bewertung bew1 = new Bewertung("Hans", 5, "Sehr gutes Buch");
        Bewertung bew2 = new Bewertung("Hans", 4, "");
        List<Bewertung> bewlist1 = medium1.getBewertungList();
        bewlist1.add(0, bew1);
        medium1.setBewertungList(bewlist1);
        
        List<Bewertung> bewlist2 = medium1.getBewertungList();
        bewlist2.add(0, bew2);
        medium2.setBewertungList(bewlist2);

        selektierteMedien.add(medium1);
        selektierteMedien.add(medium2);

        expected.expect(MissingValueException.class);
        ioKontrollierer.exportiereMedien(selektierteMedien, "output/Exporttest.xml");
    }

    /**
     * Erwartet eine Fehlermeldung, da das essentielles Attribut "Autor" einen Leerstring enthaelt
     * @throws MissingValueException
     * @throws RatingFormatException
     * @throws IOException
     */
    @Test
    public void testExportiereMedienElementLeer() throws MissingValueException, RatingFormatException, IOException {
        ArrayList<Medium> selektierteMedien = new ArrayList<>();

        Medium medium1 = beispielmedium();

        Hashtable<String, String> content = new Hashtable<>();
        content = new Hashtable<>();
        content.put("Titel", "Sharknado");
        content.put("Autor", ""); //Medium mit leerem essentiellen Attribut
        content.put("Genre", "Katastrophenfilm");
        content.put("Jahr", "2013");
        content.put("Speicherort", "C:/Users/Hans/pirated/Sharknado1080p.mkv");

        Medium medium3 = new Medium(content, MedienArt.FILM);


        Bewertung bew1 = new Bewertung("Hans", 5, "Sehr gutes Buch");
        Bewertung bew3 = new Bewertung("Hans", 2, "oberaffenscharf!");

        List<Bewertung> bewlist1 = medium1.getBewertungList();
        bewlist1.add(0, bew1);
        medium1.setBewertungList(bewlist1);
        
        List<Bewertung> bewlist3 = medium1.getBewertungList();
        bewlist3.add(0, bew3);
        medium3.setBewertungList(bewlist3);

        selektierteMedien.add(medium1);
        selektierteMedien.add(medium3);

        expected.expect(MissingValueException.class);
        ioKontrollierer.exportiereMedien(selektierteMedien, "output/Exporttest.xml");
    }

    /**
     * Testet, ob der Exportierprozess bei korrekten Daten fehlerfrei verlaeuft
     * @throws MissingValueException
     * @throws RatingFormatException
     * @throws IOException
     */
    @Test
    public void testExportiereMedien() throws MissingValueException, RatingFormatException, IOException {
        ArrayList<Medium> selektierteMedien = new ArrayList<>();

        Medium medium1 = beispielmedium();

        Hashtable<String, String> content = new Hashtable<>();
        content.put("Titel", "Firefly");
        content.put("Autor", "Joss Whedon");
        content.put("Genre", "Scifi/Western");
        content.put("Jahr", "2002");
        content.put("Speicherort", "C:/Users/Hans/Firefly/");
        content.put("Top-Schauspieler", "Nathan Fillion, Alan Tudyk");
        content.put("Episodenanzahl", "14");
        content.put("Herkunftsland", "USA");

        Medium medium4 = new Medium(content, MedienArt.TVSERIE);

        Bewertung bew1 = new Bewertung("Hans", 1, "Nicht Sehr gutes Buch");
        Bewertung bew4 = new Bewertung("Hans", 5, "viel zu kurz");
        
        List<Bewertung> bewlist1 = medium1.getBewertungList();
        bewlist1.add(0, bew1);
        medium1.setBewertungList(bewlist1);
        
        List<Bewertung> bewlist4 = medium1.getBewertungList();
        bewlist4.add(0, bew4);
        medium4.setBewertungList(bewlist4);

        selektierteMedien.add(medium1);
        selektierteMedien.add(medium4);

        ioKontrollierer.exportiereMedien(selektierteMedien, "output/Exporttest.xml");
    }

    /**
     * Erwartet eine Fehlermeldung, da keine Medien zum Exportieren uebergeben werden
     * @throws MissingValueException
     * @throws RatingFormatException
     * @throws IOException
     */
    @Test
    public void testExportiereMedienLeereListe() throws MissingValueException, RatingFormatException, IOException {
        ArrayList<Medium> selektierteMedien = new ArrayList<>();

        expected.expect(IllegalArgumentException.class);
        ioKontrollierer.exportiereMedien(selektierteMedien, "output/Exporttest.xml");
    }
}