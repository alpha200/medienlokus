package kontrolle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kontrolle.EmptyStringException;
import kontrolle.MedienKontrollierer;
import kontrolle.MediumLendException;
import kontrolle.MissingValueException;
import kontrolle.VerwaltungsKontrollierer;
import model.Bewertung;
import model.MedienArt;
import model.Medium;

import org.junit.Before;
import org.junit.Test;

public class MedienKontrolliererTest {

    private MedienKontrollierer medienKontrollierer;
    private Map<String, String> buchParam;
    private Medium buchausKonstruktor;
    private Map<String, String> buchParamISANFail;
    private Medium buchMitErstelleMethode;
    private Bewertung ersteBewertung;

    @Before
    public void setUp() throws Exception {
        //Standard Buch paramListe
        buchParam = new HashMap<>();
        buchParam.put("Autor", "Malte Isberner, Falk Howar, Bernhard Steffen");
        buchParam.put("Titel", "The TTT Algorithm: A Redundancy-Free Approach to Active Automata Learning");
        buchParam.put("Genre", "Education");
        buchParam.put("Verliehen an", "Cihat");
        buchParam.put("Verleihdatum", "");
        buchParam.put("Kaufpreis", "€47.59");
        buchParam.put("Jahr", "2014");
        buchParam.put("Speicherort", "");
        buchParam.put("ISBN", "978-3-319-11163-6");
        
        //paramListe mit ISAN
        buchParamISANFail = new HashMap<>(buchParam);
        buchParamISANFail.remove("ISBN");
        buchParamISANFail.put("ISAN", "978-3-319-11163-6");

        VerwaltungsKontrollierer verwaltungsKontrollierer = new VerwaltungsKontrollierer("Cihat");
        medienKontrollierer = new MedienKontrollierer(verwaltungsKontrollierer);

        //hat keine Berwertungen da aus Konstruktor erstellt
        buchausKonstruktor = new Medium(buchParam, MedienArt.BUCH);
        //hat Bewertungen da mit Methode erstellt
        buchMitErstelleMethode = medienKontrollierer.erstelleMedium(MedienArt.BUCH, buchParam);
        //Bewertung um BearbeiteBewertung zu testen
        List<Bewertung> bewertungsListe = new ArrayList<Bewertung>();
        String bewerter = verwaltungsKontrollierer.getMedienVerwaltung().getBenutzer();
        ersteBewertung = new Bewertung(bewerter, 4, "Sehr gut!");
        bewertungsListe.add(ersteBewertung);
        buchMitErstelleMethode.setBewertungList(bewertungsListe);
    }

/**
 * Tests für Methode erstelleMedium()
 */
    

    /**
     * Wenn die Art Null ist, dann wird eine NullPointerException geworfen.
     *
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testErstelleMediumArtNull() throws MissingValueException {
        medienKontrollierer.erstelleMedium(null, buchParam);
    }

    /**
     * Wenn die paramListe Null ist, dann wird eine NullPointerException geworfen.
     *
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testErstelleMediumParamListeNull() throws MissingValueException {
        medienKontrollierer.erstelleMedium(MedienArt.BUCH, null);
    }

    /**
     * Wenn die paramListe Attribute enthält die nicht zur Art passen,
     * dann wird eine InvalidParameterException geworfen.
     * @throws MissingValueException
     */
    @Test(expected = InvalidParameterException.class)
    public void testErstelleMediumFalscheParamListe() throws MissingValueException {
        medienKontrollierer.erstelleMedium(MedienArt.BUCH, buchParamISANFail);
    }
    
    /**
     * Wenn paramListe keinen Titel enthält, wird eine MissingValueException geworfen.
     * @throws MissingValueException
     */
    @Test(expected = MissingValueException.class)
    public void testErstelleMediumTitelMissing() throws MissingValueException {
    	HashMap<String, String> buchParamTitelMissing = new HashMap<>(buchParam);
        buchParamTitelMissing.put("Titel", "");
        medienKontrollierer.erstelleMedium(MedienArt.BUCH, buchParamTitelMissing);
    }

    /** Falls alle übergebenen Parameter korrekt sind, 
     * wird das Medium mit der paramListe erstellt.
     * @throws MissingValueException
     */
    @Test
    public void testErstelleMedium() throws MissingValueException {
        Medium mediumBuch = medienKontrollierer.erstelleMedium(MedienArt.BUCH, buchParam);
        assertNotNull("Buch wurde leer erstellt", mediumBuch);
        assertEquals("Stimmt nicht überein", buchausKonstruktor, mediumBuch);
    }
/**
 * Tests für bearbeiteMedium()
 */
    
    /**
     * Wenn die paramListe null und das Medium korrekt ist, 
     * wird eine NullPointerException geworfen.
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testBearbeiteMediumNullParamListe() throws MissingValueException {
        medienKontrollierer.bearbeiteMedium(buchausKonstruktor, null);
    }

    /**
     * Wenn die paramListe falsche Attribute entsprechend der Art des Mediums
     * enthält, wird eine InvalidParameterException geworfen.
     * @throws MissingValueException
     */
    @Test(expected = InvalidParameterException.class)
    public void testBearbeiteMediumFalscheParamListe() throws MissingValueException {
        medienKontrollierer.bearbeiteMedium(buchausKonstruktor, buchParamISANFail);
    }

    /**
     * Wenn das Medium null ist, wird eine NullPointerException geworfen.
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testBearbeiteMediumNull() throws MissingValueException {
        medienKontrollierer.bearbeiteMedium(null, buchParam);
    }

    /**
     * Wenn das Medium und die paramListe korrekt sind, überschreibt 
     * das Medium die paramListe mit der übergebenen.
     * @throws MissingValueException
     */
    @Test
    public void testBearbeiteMedium() throws MissingValueException {
        HashMap<String, String> buchParamBearbeitet = new HashMap<>(buchParam);
        buchParamBearbeitet.put("Kaufpreis", "€12.59");
        Medium buchBearbeitet = medienKontrollierer.bearbeiteMedium(buchausKonstruktor, buchParamBearbeitet);
        //Prüfe ob Autor gleichgeblieben ist.
        assertEquals(buchBearbeitet.getParamListe().get("Autor"), buchBearbeitet.getParamListe().get("Autor"));
        //Prüfe ob Kaufpreis sich verändert hat
        assertEquals("€12.59", buchBearbeitet.getParamListe().get("Kaufpreis"));
    }

    
    /*
     * Bearbeite Bewertung tests
     * 
     * 
     * 
     */
    
    /**
     * Das Medium und die Bewertung werden korrekt übergeben aber die Beschreibung ist null.
     * 
     * @throws NullPointerException
     */
    @Test(expected = NullPointerException.class)
    public void testBearbeiteBewertungNullBeschreibung() {
        medienKontrollierer.bearbeiteBewertung(buchMitErstelleMethode, null, 1);
    }

    /**
     * Das Medium und die Beschreibung ist korrekt aber die Bewertung außerhalb des gültigen Bereichs
     * 
     * @throws InvalidParameterException
     */
    @Test(expected = InvalidParameterException.class)
    public void testBearbeiteBewertungFalscheBewertung() {
        medienKontrollierer.bearbeiteBewertung(buchMitErstelleMethode, "Sehr gut!", 6);
    }

    /**
     * Das Medium ist null und Bewertung und Beschreibung sind korrekt
     * 
     * @throws NullPointerException
     */
    @Test(expected = NullPointerException.class)
    public void testBearbeiteBewertungNullMedium() {
        medienKontrollierer.bearbeiteBewertung(null, "Sehr gut!", 4);
    }

    /**
     * Alle Parameter sind korrekt und die Beschreibung ist ein Leerstring
     * 
     * 
     */
    @Test
    public void testBearbeiteBewertungLeereBeschreibung() {
        medienKontrollierer.bearbeiteBewertung(buchMitErstelleMethode, "", 4);
        assertEquals("Der Bewerter hat sich geändert, sollte aber gleich bleiben!", "Cihat", buchMitErstelleMethode.getBewertungList().get(0).getBewerter());
        assertEquals("Beschreibung ist nicht leer, sollte aber leer gesetzt werden!", "", buchMitErstelleMethode.getBewertungList().get(0).getBeschreibung());
        assertEquals("Bewertung hat sich verändert, sollte aber gleich bleiben!", 4, buchMitErstelleMethode.getBewertungList().get(0).getBewertung());
    }

    /**
     * Alle Parameter sind korrekt und die Bewertung ist nicht leer
     * 
     * 
     */
    @Test
    public void testBearbeiteBewertung() {
        medienKontrollierer.bearbeiteBewertung(buchMitErstelleMethode, "Ganz Okay!", 3);
        assertEquals("Der Bewerter hat sich geändert, sollte aber gleich bleiben!", "Cihat", buchMitErstelleMethode.getBewertungList().get(0).getBewerter());
        assertEquals("Beschreibung wurde nicht geändert, sollte aber verändert werden!", "Ganz Okay!", buchMitErstelleMethode.getBewertungList().get(0).getBeschreibung());
        assertEquals("Bewertung hat sich nicht, sollte aber auf 3 geändert werden!", 3, buchMitErstelleMethode.getBewertungList().get(0).getBewertung());
    }
    
    /*
     *  verleiheMedium tests
     */

    /**
     * Das Medium ist noch nicht verliehen und das Datum ist null
     * 
     * @throws NullPointerException
     * @throws MediumLendException
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testVerleiheMediumDateNull() throws MediumLendException, MissingValueException {
        medienKontrollierer.verleiheMedium(buchMitErstelleMethode, null, "Jan");
    }

    /**
     * Das Medium ist noch nicht verliehen und das Datum wird als Leerstring übergeben.
     * 
     * @throws EmptyStringException
     * @throws MediumLendException
     * @throws MissingValueException
     */
    @Test(expected = EmptyStringException.class)
    public void testVerleiheMediumDateLeer() throws MediumLendException, MissingValueException {
        medienKontrollierer.verleiheMedium(buchMitErstelleMethode, "", "Jan");
    }

    /**
     * Das Medium ist noch nicht verliehen und Person null
     * 
     * @throws NullPointerException
     * @throws MediumLendException
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testVerleiheMediumPersonNull() throws MediumLendException, MissingValueException {
        medienKontrollierer.verleiheMedium(buchMitErstelleMethode, "23.09.2015", null);
    }

    /**
     * Das Medium ist noch nicht verliehen, das Datum ist korrekt, und die Person ist leer
     * 
     * @throws EmptyStringException
     * @throws MediumLendException
     * @throws MissingValueException
     */
    @Test(expected = EmptyStringException.class)
    public void testVerleiheMediumPersonLeer() throws MediumLendException, MissingValueException {
        medienKontrollierer.verleiheMedium(buchMitErstelleMethode, "23.09.2015", "");
    }

    /**
     * Das Medium ist null und Datum und Person sind korrekt
     * 
     * @throws NullPointerException
     * @throws MediumLendException
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testVerleiheMediumNull() throws MediumLendException, MissingValueException {
        medienKontrollierer.verleiheMedium(null, "23.09.2015", "Jan");
    }

    /**
     * Übergebene Parameter korrekt aber das Medium ist bereits als verliehen Markiert.
     * 
     * @throws MediumLendException
     * @throws MissingValueException
     */
    @Test(expected = MediumLendException.class)
    public void testVerleiheMediumAlreadyLend() throws MediumLendException, MissingValueException {
        medienKontrollierer.verleiheMedium(buchMitErstelleMethode, "23.09.2015", "Jan");
    }

    /**
     * Übergebene Parameter korrekt und das Medium ist noch nicht verliehen.
     * 
     */
    @Test
    public void testVerleiheMedium() throws MissingValueException, MediumLendException {
        HashMap<String, String> buchParamNotLend = new HashMap<>(buchParam);
        buchParamNotLend.put("Verliehen an", "");
        buchParamNotLend.put("Verleihdatum", "");
        Medium buchNotLend = medienKontrollierer.erstelleMedium(MedienArt.BUCH, buchParamNotLend);
        medienKontrollierer.verleiheMedium(buchNotLend, "23.09.2015", "Jan");
        assertEquals("Jan", buchNotLend.getParamListe().get("Verliehen an"));
    }

    /**
     * Medienliste Null
     * @throws MediumLendException
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testVerleiheMedienListeNull() throws MediumLendException, MissingValueException {
        medienKontrollierer.verleiheMedien(null, "23.09.2015", "Jan");
    }

    /**
     * Medienliste Leer
     * @throws MediumLendException
     * @throws MissingValueException
     */
    @Test
    public void testVerleiheMedienListeLeer() throws MediumLendException, MissingValueException {
        medienKontrollierer.verleiheMedien(new ArrayList<Medium>(), "23.09.2015", "Jan");
    }

    /**
     * Art null, paramListe korrekt
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testPruefeParamListeArtNull() throws MissingValueException {
        Medium.pruefeParamListe(null, buchParam);
    }

    /**
     * Art korrekt, paramliste null
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testPruefeParamListeNull() throws MissingValueException {
        Medium.pruefeParamListe(MedienArt.BUCH, null);
    }

    /**
     * Art Buch, paramListe mit Attributen die der Art nicht entsprechen
     * @throws MissingValueException
     */
    @Test(expected = InvalidParameterException.class)
    public void testPruefeParamListeTypeFail() throws MissingValueException {
        Medium.pruefeParamListe(MedienArt.BUCH, buchParamISANFail);
    }

    /**
     * Art Buch, paramListe mit leerem Titel
     * @throws MissingValueException
     */
    @Test(expected = MissingValueException.class)
    public void testPruefeParamListeMissingTitel() throws MissingValueException {
    	HashMap<String, String> buchParamTitelMissing = new HashMap<>(buchParam);
        buchParamTitelMissing.put("Titel", "");
        Medium.pruefeParamListe(MedienArt.BUCH, buchParamTitelMissing);
    }

    /**
     * Art Buch, paramListe mit leerem Autor
     * @throws MissingValueException
     */
    @Test(expected = MissingValueException.class)
    public void testPruefeParamListeMissingAutor() throws MissingValueException {
    	HashMap<String, String> buchParamAutorMissing = new HashMap<>(buchParam);
        buchParamAutorMissing.put("Autor", "");
        Medium.pruefeParamListe(MedienArt.BUCH, buchParamAutorMissing);
    }

    /**
     * Erfolgreiches Prüfen der einzelnen Medien
     * @throws MissingValueException
     */
    @Test
    public void testPruefeParamListeEBook() throws MissingValueException {
    	HashMap<String, String> eBookParam = new HashMap<>(buchParam);
        Medium.pruefeParamListe(MedienArt.EBOOK, eBookParam);
    }

    @Test
    public void testPruefeParamListeFilm() throws MissingValueException {
        HashMap<String, String> filmParam = new HashMap<>(buchParam);    // Art Buch, paramListe mit leerem Autor
        filmParam.remove("ISBN");
        filmParam.put("Abspieldauer", "666");
        filmParam.put("Herkunftsland", "USA");
        filmParam.put("Top-Schauspieler", "Shark, Tornado");
        Medium.pruefeParamListe(MedienArt.FILM, filmParam);
    }

    @Test
    public void testPruefeParamListeMusik() throws MissingValueException {
    	HashMap<String, String> musikParam = new HashMap<>(buchParam);
        musikParam.remove("ISBN");
        musikParam.put("Album", "Dual");
        musikParam.put("Titelnummer", "5");
        Medium.pruefeParamListe(MedienArt.MUSIK, musikParam);
    }

    @Test
    public void testPruefeParamListeTVSerie() throws MissingValueException {
    	HashMap<String, String> tvSerieParam = new HashMap<>(buchParam);
        tvSerieParam.remove("ISBN");
        Medium.pruefeParamListe(MedienArt.TVSERIE, tvSerieParam);
    }

    @Test
    public void testPruefeParamListeBuch() throws MissingValueException {
        Medium.pruefeParamListe(MedienArt.BUCH, buchParam);
    }

    @Test
    public void testPruefeParamListeHoerBuch() throws MissingValueException {
    	HashMap<String, String> hoerBuchParam = new HashMap<>(buchParam);
        hoerBuchParam.remove("ISBN");
        hoerBuchParam.put("Sprecher", "Horst");
        hoerBuchParam.put("CD-Anzahl", "4");
        hoerBuchParam.put("Abspieldauer", "awe");
        Medium.pruefeParamListe(MedienArt.HOERBUCH, hoerBuchParam);
    }


}