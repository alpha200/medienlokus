package kontrolle;

import static org.junit.Assert.*;

import java.io.FileOutputStream;
import java.io.IOException;

import kontrolle.IOKontrollierer;
import kontrolle.MedienKontrollierer;
import kontrolle.SuchKontrollierer;
import kontrolle.VerknuepfungsKontrollierer;
import kontrolle.VerwaltungsKontrollierer;
import model.Medienverwaltung;

import org.junit.Before;
import org.junit.Test;

public class VerwaltungsKontrolliererTest {
    private VerwaltungsKontrollierer verwaltungsKontrollierer;
    private String besitzer = "Milo";

    @Before
    public void setUp() throws Exception {
        verwaltungsKontrollierer = new VerwaltungsKontrollierer(besitzer);
    }
    
    /**
     * Es wird geprueft, ob der VerwaltungsKontrollierer erfolgreich erstellt wurde
     * mit dem uebergebenen Benutzer, mit einer leeren Wunschliste
     * und einem leeren Medienarchiv
     */
    @Test
    public void testVerwaltungsKontrollierer() {
        assertEquals("Durch den Konstruktor soll ein Benutzer erstelt werden", besitzer, verwaltungsKontrollierer.getMedienVerwaltung().getBenutzer());
        assertTrue("Durch den Konstruktor soll eine leere Wunschliste erstellt werden", verwaltungsKontrollierer.getMedienVerwaltung().getWunschliste().isEmpty());
        assertTrue("Durch den Konstruktor soll ein leeres Archiv erstellt werden", verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv().isEmpty());
    }
    
    /**
     * Es wird geprueft, ob der Benutzer ersetzt wird.
     */
    @Test
    public void testSetMedienVerwaltung() {
        Medienverwaltung medienVerwaltung = verwaltungsKontrollierer.getMedienVerwaltung();
        medienVerwaltung.setBenutzer("testBenutzer");
        verwaltungsKontrollierer.setMedienVerwaltung(medienVerwaltung);
        assertEquals("SetMedienVerwaltung soll den Benutzer ersetzen koennen", "testBenutzer", verwaltungsKontrollierer.getMedienVerwaltung().getBenutzer());
    }
    
    /**
     * Es wird geprueft, ob der ListenKontrollierer neue Objekte uebernimmt.
     */
    @Test
    public void testSetListenKontrollierer() {
        ListenKontrollierer neueListe = new IOKontrollierer(verwaltungsKontrollierer);
        verwaltungsKontrollierer.setListenKontrollierer(neueListe);
        assertTrue("SetListenKontrollierer soll den neuen Objekt uebernehmen koennen", verwaltungsKontrollierer.getListenKontrollierer().equals(neueListe));
    }
    
    /**
     * Es wird geprueft, ob der MedienKontrollierer neue Objekte uebernimmt.
     */
    @Test
    public void testSetMedienKontrollierer() {
        MedienKontrollierer neueListe = new MedienKontrollierer(verwaltungsKontrollierer);
        verwaltungsKontrollierer.setMedienKontrollierer(neueListe);
        assertTrue("SetMedienKontrollierer soll den neuen Objekt uebernehmen koennen", verwaltungsKontrollierer.getMedienKontrollierer().equals(neueListe));
    }
    
    /**
     * Es wird geprueft, ob der SuchKontrollierer neue Objekte uebernimmt.
     */
    @Test
    public void testSetSuchKontrollierer() {
        SuchKontrollierer neueListe = new SuchKontrollierer(verwaltungsKontrollierer);
        verwaltungsKontrollierer.setSuchKontrollierer(neueListe);
        assertTrue("SetSuchKontrollierer soll den neuen Objekt uebernehmen koennen", verwaltungsKontrollierer.getSuchKontrollierer().equals(neueListe));
    }
    
    /**
     * Es wird geprueft, ob der VerknuepfungsKontrollierer neue Objekte uerbernimmt.
     */
    @Test
    public void testSetVerknuepfungsKontrollierer() {
        VerknuepfungsKontrollierer neueListe = new VerknuepfungsKontrollierer();
        verwaltungsKontrollierer.setVerknuepfungsKontrollierer(neueListe);
        assertTrue("SetVerknuepfungsKontrollierer soll den neuen Objekt uebernehmen koennen", verwaltungsKontrollierer.getVerknuepfungsKontrollierer().equals(neueListe));
    }
    
    /**
     * Es wird geprueft, ob Null-Parameter gespeichert werden koennen.
     */
    @Test
    public void testSpeicherSitzungNullPointerException() {
        boolean isExceptionOccured = false;
        try {
            verwaltungsKontrollierer.speicherSitzung(null);
        } catch (NullPointerException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertTrue("SpeichereSitzung soll keinen null-Parameter annehmen können.", isExceptionOccured);
    }
    
    /**
     * Es wird geprueft, ob Objekte, die das Interface implementieren, gespeichert werden.
     */
    @Test
    public void testSpeicherSitzungClassNotFound() {
        boolean exceptionThrown = false;
        try {
            verwaltungsKontrollierer.speicherSitzung(verwaltungsKontrollierer.getMedienVerwaltung());
        } catch (IOException ex) {
            exceptionThrown = true;
        }
        // TO DO  : Setze auf false
        assertFalse("SpeichereSitzung soll Objekte, die das Interface implementieren, annehmen können.", exceptionThrown);
    }
    
    /**
     * Es wird geprueft, ob null-Werte geladen werden koennen.
     */
    @Test
    public void testLadeSitzungNullPointerException() {
        boolean isExceptionOccured = false;
        try {
            verwaltungsKontrollierer.ladeSitzung();
        } catch (NullPointerException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertFalse("LadeSitzung soll keinen null-Attribut haben können.", isExceptionOccured);
    }
    
    /**
     * Es wird geprueft, ob Objekte, die das Interface implementieren, geladen werden.
     */
    @Test
    public void testLadeSitzungClassNotFountException() {
        boolean isExceptionOccured = false;
        try {
            verwaltungsKontrollierer.ladeSitzung();
        } catch (ClassNotFoundException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertFalse("SpeichereSitzung soll Objekte, die das Interface implementieren, annehmen können.", isExceptionOccured);
    }
    
    /**
     * Es wird geprueft, ob die letzte Sitzung geladen wird.
     */
    @Test
    public void testLadeSitzungIOException() {
        boolean isExceptionOccured = false;
        try {
            FileOutputStream fileStream = new FileOutputStream("output/config.ser");
            fileStream.write(55);
            fileStream.close();
        } catch (Exception e) {
            assertTrue("Test konnte keine Datei anlegen.", false);
        }
        try {
            verwaltungsKontrollierer.ladeSitzung();
        } catch (IOException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertTrue("SpeichereSitzung soll auf die Datei rangehen können. (Datei muss existieren)", isExceptionOccured);
    }

}
