package kontrolle;

import model.MedienArt;
import model.Medienverwaltung;
import model.Medium;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import kontrolle.ListenArt;
import kontrolle.SuchKontrollierer;
import kontrolle.VerwaltungsKontrollierer;
import static org.junit.Assert.assertEquals;

public class SuchKontrolliererTest {

    private Mockery context = new JUnitRuleMockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    private SuchKontrollierer suchKontrollierer;
    private VerwaltungsKontrollierer verwaltungsKontrollierer;

    @Before
    public void initializeTests() {
        verwaltungsKontrollierer = context.mock(VerwaltungsKontrollierer.class);
        suchKontrollierer = new SuchKontrollierer(verwaltungsKontrollierer);
    }

    /**
     * Generiert 2 Medien und setzt sie im Verwaltungskontrollierer-Mock
     */
    private void generiereStandardMedien() {
        MedienArt[] medienArten = {MedienArt.BUCH, MedienArt.FILM};
        String[][] keys = {{"Titel", "Autor", "ISBN"}, {"Titel", "Autor", "Abspieldauer"}};
        String[][] values = {{"Sharknado - das Buch", "Sharknado Inc.", "12341337"}, {"Sharknado - der Film", "Sharknado Filmstudios", "1387"}};

        //Testmedien erzeugen
        final Medienverwaltung medienverwaltung = new Medienverwaltung("Testnutzer", generiereMedienTestfaelle(medienArten, keys, values), generiereMedienTestfaelle(medienArten, keys, values));

        // Medienverwaltung für gebeMedienverwaltung zurückgeben
        context.checking(new Expectations() {{
            oneOf(verwaltungsKontrollierer).getMedienVerwaltung();
            will(returnValue(medienverwaltung));
        }});
    }

    /**
     * Generiert 1 Medium und setzt es im Verwaltungskontrollierer-Mock
     */
    private void generiereEinMedium() {
        MedienArt[] medienArten = {MedienArt.BUCH, MedienArt.FILM};
        String[][] keys = {{"Titel", "Autor", "ISBN"}};
        String[][] values = {{"Sharknado - das Buch", "Sharknado Inc.", "12341337"}};

        //Testmedien erzeugen
        final Medienverwaltung medienverwaltung = new Medienverwaltung("Testnutzer", generiereMedienTestfaelle(medienArten, keys, values), generiereMedienTestfaelle(medienArten, keys, values));

        // Medienverwaltung für gebeMedienverwaltung zurückgeben
        context.checking(new Expectations() {{
            oneOf(verwaltungsKontrollierer).getMedienVerwaltung();
            will(returnValue(medienverwaltung));
        }});
    }

    /**
     * @param medienArten gibt die Medienarten für die zu generierenden Medien an
     * @param keys gibt die Medienparameter für die zu generierenden Medien an
     * @param values die Werte für Parameter für die zu generierenden Medien an
     * @return die generierten Medien
     */
    private List<Medium> generiereMedienTestfaelle(MedienArt[] medienArten, String[][] keys, String[][] values) {
        List<Medium> mediumList = new ArrayList<>();

        HashMap<String, String> paramMap = new HashMap<>();
        for (int j = 0; j < keys.length; j++) {
            for (int i = 0; i < keys[0].length; i++) {
                paramMap.put(keys[j][i], values[j][i]);
            }
            Medium med = new Medium(paramMap, medienArten[j]);
            mediumList.add(med);
        }

        return mediumList;
    }

    /**
     * Generiert kein Medium und setzt es im Verwaltungskontrollierer-Mock
     */
    private void generiereKeinMedium() {
        //Testmedien erzeugen
        final Medienverwaltung medienverwaltung = new Medienverwaltung("Testnutzer", new ArrayList<Medium>(), new ArrayList<Medium>());

        // Medienverwaltung für gebeMedienverwaltung zurückgeben
        context.checking(new Expectations() {{
            oneOf(verwaltungsKontrollierer).getMedienVerwaltung();
            will(returnValue(medienverwaltung));
        }});
    }

    /**
     * Sucht nach Sharknado im Medienarchiv
     */
    @Test
    public void testUniversaleSucheWithMediumsMedienarchiv() {
        generiereStandardMedien();

        List<Medium> results = suchKontrollierer.universaleSuche("Sharknado", ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte beide Medien enthalten!", results.size(), 2);
    }

    /**
     * Sucht nach Sharknado in der Wunschliste
     */
    @Test
    public void testUniversaleSucheWithMediumsWunschliste() {
        generiereStandardMedien();

        List<Medium> results = suchKontrollierer.universaleSuche("Sharknado", ListenArt.WUNSCHLISTE);

        assertEquals("Ergebnis sollte beide Medien enthalten!", results.size(), 2);
    }

    /**
     * Sucht nach das Buch im Medienarchiv
     */
    @Test
    public void testUniversaleSucheWithMediumMedienarchiv() {
        generiereEinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("das Buch", ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte das Medium enthalten!", results.size(), 1);
    }

    /**
     * Sucht nach das Buch in der Wunschliste
     */
    @Test
    public void testUniversaleSucheWithMediumWunschliste() {
        generiereEinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("das Buch", ListenArt.WUNSCHLISTE);

        assertEquals("Ergebnis sollte das Medium enthalten!", results.size(), 1);
    }

    /**
     * Such nach das Buch in leerem Medienarchiv
     */
    @Test
    public void testUniversaleSucheWithoutMediaMedienarchiv() {
        generiereKeinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("das Buch", ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }

    /**
     * Such nach das Buch in leerer Wunschliste
     */
    @Test
    public void testUniversaleSucheWithoutMediaWunschliste() {
        generiereKeinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("das Buch", ListenArt.WUNSCHLISTE);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }

    /**
     * Sucht nach Begriff, der keine Ergebnisse erzeugen sollte
     */
    @Test
    public void testUniversaleSucheWithoutMediaNoResultsWunschliste() {
        generiereEinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("dasfindetgarantiertnichts", ListenArt.WUNSCHLISTE);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }

    /**
     * Sucht nach Begriff, der keine Ergebnisse erzeugen sollte
     */
    @Test
    public void testUniversaleSucheWithoutMediaNoResultsMedienarchiv() {
        generiereEinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("dasfindetgarantiertnichts", ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }

    /**
     * Sucht nach leerem String in Medienarchiv mit einem Medium
     */
    @Test
    public void testUniversaleSucheWithMediumsMedienarchivEmptyString() {
        generiereStandardMedien();

        List<Medium> results = suchKontrollierer.universaleSuche("", ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte beide Medien enthalten!", results.size(), 2);
    }

    /**
     * Sucht nach leerem String in Wunschliste mit einem Medium
     */
    @Test
    public void testUniversaleSucheWithMediumsWunschlisteEmptyString() {
        generiereStandardMedien();

        List<Medium> results = suchKontrollierer.universaleSuche("", ListenArt.WUNSCHLISTE);

        assertEquals("Ergebnis sollte beide Medien enthalten!", results.size(), 2);
    }

    /**
     * Sucht nach leerem String in Medienarchiv mit einem Medium
     */
    @Test
    public void testUniversaleSucheWithMediumMedienarchivEmptyString() {
        generiereEinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("", ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte das Medium enthalten!", results.size(), 1);
    }

    /**
     * Sucht nach leerem String in Wunschliste mit einem Medium
     */
    @Test
    public void testUniversaleSucheWithMediumWunschlisteEmptyString() {
        generiereEinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("", ListenArt.WUNSCHLISTE);

        assertEquals("Ergebnis sollte das Medium enthalten!", results.size(), 1);
    }

    /**
     * Sucht nach leerem String in Medienarchiv mit keinem Medium
     */
    @Test
    public void testUniversaleSucheWithoutMediaMedienarchivEmptyString() {
        generiereKeinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("", ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }

    /**
     * Sucht nach leerem String in Wunschliste mit keinem Medium
     */
    @Test
    public void testUniversaleSucheWithoutMediaWunschlisteEmptyString() {
        generiereKeinMedium();

        List<Medium> results = suchKontrollierer.universaleSuche("", ListenArt.WUNSCHLISTE);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }

    /**
     * Sucht nach Medien vom Typ Film mit dem Titel Sharknado
     */
    @Test
    public void testErweiterteSucheWithOneResult() {

        generiereStandardMedien();

        // Suchparameter festlegen
        Map<String, String> params = new HashMap<>();
        params.put("Titel", "Sharknado");

        List<Medium> results = suchKontrollierer.erweiterteSuche(params, EnumSet.of(MedienArt.FILM), ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte nur ein Medium enthalten!", results.size(), 1);
        assertEquals("Der Titel des Mediums sollte der Film sein!", results.get(0).getParamListe().get("Titel"), "Sharknado - der Film");
        assertEquals("Das Medium sollte vom Typ Film sein!", results.get(0).getMedienArt(), MedienArt.FILM);
    }

    /**
     * Sucht nach einem Titel, den es nicht gibt
     */
    @Test
    public void testErweiterteSucheWithNoResult() {

        generiereStandardMedien();

        // Suchparameter festlegen
        Map<String, String> params = new HashMap<>();
        params.put("Titel", "Titel der nicht drin ist!");

        List<Medium> results = suchKontrollierer.erweiterteSuche(params, EnumSet.of(MedienArt.FILM), ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }

    /**
     * Sucht nach Filmen und Medien mit dem Titel Sharknado
     */
    @Test
    public void testErweiterteWithMultipleMediatypes() {

        generiereStandardMedien();

        // Suchparameter festlegen
        Map<String, String> params = new HashMap<>();
        params.put("Titel", "Sharknado");

        List<Medium> results = suchKontrollierer.erweiterteSuche(params, EnumSet.of(MedienArt.FILM, MedienArt.BUCH), ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte beide Medien enthalten!", results.size(), 2);
    }

    /**
     * Sucht nach ISBN in Büchern
     */
    @Test
    public void testErweiterteWithExclusiveAttribute() {

        generiereStandardMedien();

        // Suchparameter festlegen
        Map<String, String> params = new HashMap<>();
        params.put("ISBN", "12341337");

        List<Medium> results = suchKontrollierer.erweiterteSuche(params, EnumSet.of(MedienArt.BUCH), ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte nur ein Medium enthalten!", results.size(), 1);
        assertEquals("Der Titel des Mediums sollte das Buch sein!", results.get(0).getParamListe().get("Titel"), "Sharknado - das Buch");
        assertEquals("Das Medium sollte vom Typ Buch sein!", results.get(0).getMedienArt(), MedienArt.BUCH);
    }

    /**
     * Sucht nach Titel in leerem Medienarchiv
     */
    @Test
    public void testErweiterteWithoutMedia() {

        generiereKeinMedium();

        // Suchparameter festlegen
        Map<String, String> params = new HashMap<>();
        params.put("Titel", "Sharknado");

        List<Medium> results = suchKontrollierer.erweiterteSuche(params, EnumSet.of(MedienArt.BUCH), ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }

    /**
     * Sucht nach ISAN in Medienarchiv, in dem der Film keine ISAN hat
     */
    @Test
    public void testErweiterteWithParameterNotSet() {

        generiereStandardMedien();

        // Suchparameter festlegen
        Map<String, String> params = new HashMap<>();
        params.put("ISAN", "blubb");

        List<Medium> results = suchKontrollierer.erweiterteSuche(params, EnumSet.of(MedienArt.FILM), ListenArt.MEDIENARCHIV);

        assertEquals("Ergebnis sollte kein Medium enthalten!", results.size(), 0);
    }
}