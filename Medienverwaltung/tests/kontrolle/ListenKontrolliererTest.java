package kontrolle;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import kontrolle.DuplicateMediumException;
import kontrolle.ListenArt;
import kontrolle.MedienKontrollierer;
import kontrolle.MissingValueException;
import kontrolle.VerwaltungsKontrollierer;
import model.MedienArt;
import model.Medium;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ListenKontrolliererTest {

    @Rule
    public ExpectedException expected = ExpectedException.none();

    private VerwaltungsKontrollierer verwaltungsKontrollierer;
    private ListenKontrollierer listenKontrollierer;
    private MedienKontrollierer medienKontrollierer;

    @Before
    public void setUp() throws Exception {
        verwaltungsKontrollierer = new VerwaltungsKontrollierer("TestNutzer");
        listenKontrollierer = new ListenKontrollierer(verwaltungsKontrollierer);
        medienKontrollierer = new MedienKontrollierer(verwaltungsKontrollierer);

        verwaltungsKontrollierer.setListenKontrollierer(listenKontrollierer);
        verwaltungsKontrollierer.setMedienKontrollierer(medienKontrollierer);
    }
    
    // Tests fuer medienDrucken() //
    
    /**
     * Testet, ob das Auswaehlen gueltiger Medien zum Drucken fehlerfrei verlaeuft 
     * @throws MissingValueException
     * @throws IOException
     */
    @Test
    public void testMedienDrucken() throws MissingValueException, IOException {

        ArrayList<Medium> selektierteMedien = new ArrayList<>();

        Hashtable<String, String> content = new Hashtable<>();
        content.put("Titel", "Eragon");
        content.put("Autor", "Christopher Paolini");
        content.put("Genre", "Fantasy");
        content.put("Jahr", "2004");
        content.put("Speicherort", "Bücherregal");
        content.put("ISBN", "432451");

        Medium medium1 = medienKontrollierer.erstelleMedium(MedienArt.BUCH, content);

        content = new Hashtable<>();
        content.put("Titel", "He's a Pirate");
        content.put("Autor", "Klaus Badelt");
        content.put("Genre", "Piratisch");
        content.put("Jahr", "2003");
        content.put("Speicherort", "C:/Users/Hans/hesapirate.ogg");
        content.put("ISAN", "42");
        content.put("Album", "Pirates of the Carribean: The Curse of the Black Pearl");
        content.put("Titelnummer", "15");

        Medium medium2 = medienKontrollierer.erstelleMedium(MedienArt.MUSIK, content);

        selektierteMedien.add(medium1);
        selektierteMedien.add(medium2);

        listenKontrollierer.medienDrucken(selektierteMedien, "output/Druck.txt", false);
    }

    // Tests fuer inWunschlisteHinzufuegen() //
    
    /**
     * 
     * @throws MissingValueException
     * @throws DuplicateMediumException
     */
    @Test
    public void testInWunschlisteHinzufuegenValid() throws MissingValueException, DuplicateMediumException {
        List<Medium> liste = verwaltungsKontrollierer.getMedienVerwaltung().getWunschliste();
        int sizeBefore = liste.size();

        Hashtable<String, String> content = new Hashtable<>();
        content.put("Titel", "Titel1");
        content.put("Autor", "Autor1");
        content.put("Genre", "Genre1");
        content.put("Jahr", "2012");
        content.put("Speicherort", "Ort1");
        content.put("ISBN", "432451");
        try {
            listenKontrollierer.inWunschlisteHinzufuegen(MedienArt.BUCH, content);
        } catch (MissingValueException e) {
            e.printStackTrace();
            fail("Unexpected Exception: " + e);
        } catch (DuplicateMediumException e) {
            e.printStackTrace();
            fail("Unexpected Exception: " + e);
        }

        if (liste.size() != sizeBefore + 1)
            fail("Medium has not been added");

    }

    @Test
    public void testInWunschlisteHinzufuegenMissingValueException() throws MissingValueException, DuplicateMediumException {
        Hashtable<String, String> content = new Hashtable<>();
        content = new Hashtable<>();
        content.put("Autor", "Autor2");
        content.put("Genre", "Genre2");
        content.put("Jahr", "2013");
        content.put("Speicherort", "Ort2");
        content.put("ISAN", "42");
        content.put("Album", "Album2");
        content.put("Titelnummer", "2");
        expected.expect(MissingValueException.class);
        try {
            listenKontrollierer.inWunschlisteHinzufuegen(MedienArt.MUSIK, content);
        } catch (DuplicateMediumException e) {
            e.printStackTrace();
            fail("Unexpected Exception: " + e);
        }
    }

    @Test
    public void testInMedienarchivHinzufuegenValid() throws MissingValueException, DuplicateMediumException {
        List<Medium> liste = verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv();
        int sizeBefore = liste.size();

        Hashtable<String, String> content = new Hashtable<>();
        content.put("Titel", "Titel1");
        content.put("Autor", "Autor1");
        content.put("Genre", "Genre1");
        content.put("Jahr", "2012");
        content.put("Speicherort", "Ort1");
        content.put("ISBN", "432451");
        try {
            listenKontrollierer.inMedienarchivHinzufuegen(MedienArt.BUCH, content);
        } catch (MissingValueException e) {
            e.printStackTrace();
            fail("Unexpected Exception: " + e);
        } catch (DuplicateMediumException e) {
            e.printStackTrace();
            fail("Unexpected Exception: " + e);
        }

        if (liste.size() != sizeBefore + 1)
            fail("Medium has not been added");

    }

    @Test
    public void testInMedienarchivHinzufuegenMissingValue() throws MissingValueException, DuplicateMediumException {
        Hashtable<String, String> content = new Hashtable<>();
        content = new Hashtable<>();
        content.put("Autor", "Autor2");
        content.put("Genre", "Genre2");
        content.put("Jahr", "2013");
        content.put("Speicherort", "Ort2");
        content.put("ISAN", "42");
        content.put("Album", "Album2");
        content.put("Titelnummer", "2");
        expected.expect(MissingValueException.class);
        try {
            listenKontrollierer.inMedienarchivHinzufuegen(MedienArt.MUSIK, content);
        } catch (DuplicateMediumException e) {
            e.printStackTrace();
            fail("Unexpected Exception: " + e);
        }
    }

    @Test
    public void testLoescheMediumValid() throws MissingValueException, DuplicateMediumException {
        Hashtable<String, String> content = new Hashtable<>();
        content.put("Titel", "Eragon");
        content.put("Autor", "Christopher Paolini");
        content.put("Genre", "Fantasy");
        content.put("Jahr", "2004");
        content.put("Speicherort", "Bücherregal");
        content.put("ISBN", "432451");

        try {
            listenKontrollierer.inMedienarchivHinzufuegen(MedienArt.BUCH, content);
        } catch (DuplicateMediumException e) {
            e.printStackTrace();
            fail("Unexpected Exception: " + e);
        }

        content = new Hashtable<>();
        content.put("Titel", "He's a Pirate");
        content.put("Autor", "Klaus Badelt");
        content.put("Genre", "Piratisch");
        content.put("Jahr", "2003");
        content.put("Speicherort", "C:/Users/Hans/hesapirate.ogg");
        content.put("ISAN", "42");
        content.put("Album", "Pirates of the Carribean: The Curse of the Black Pearl");
        content.put("Titelnummer", "15");

        try {
            listenKontrollierer.inMedienarchivHinzufuegen(MedienArt.MUSIK, content);
        } catch (DuplicateMediumException e) {
            e.printStackTrace();
            fail("Unexpected Exception: " + e);
        }

        Medium medium = verwaltungsKontrollierer.getMedienVerwaltung().getMedienarchiv().get(1);

        listenKontrollierer.loescheMedium(medium, ListenArt.MEDIENARCHIV);
    }

    @Test
    public void testLoescheMediumNull() {
        expected.expect(IllegalArgumentException.class);
        listenKontrollierer.loescheMedium(null, ListenArt.MEDIENARCHIV);
    }

}
