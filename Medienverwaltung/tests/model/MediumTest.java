package model;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import kontrolle.MissingValueException;
import model.Bewertung;
import model.MedienArt;
import model.Medium;
import model.Verknuepfung;

import org.junit.Before;
import org.junit.Test;

public class MediumTest {

    Medium med1, med2, med3, med4;
    Map<String, String> map1, map2, map3, map4;
    static Map<String, String> listeNormal;
	static Map<String, String> listeBuch;
	static Map<String, String> listeMusik;
	static Map<String, String> listeFilm;
	static Map<String, String> listeHoerbuch;
	static Map<String, String> listeTvSerie;

    @Before
    public void setUp() throws Exception {

    	initialisiereListen();
    
        map1 = new HashMap<>(listeFilm);
        map1.put("Autor", "Rowling");
        map1.put("Titel", "Harry Potter");
        map1.put("Genre", "Fantasy");
        map1.put("Herkunftsland", "England");
        med1 = new Medium(map1, MedienArt.FILM);

        map2 = new HashMap<>(listeBuch);
        map2.put("Autor", "Rowling");
        map2.put("Titel", "Harry Potter");
        map2.put("Genre", "Fantasy");
        map2.put("ISBN", "2432396848");
        med2 = new Medium(map2, MedienArt.EBOOK);

        map3 = new HashMap<>(listeMusik);
        map3.put("Autor", "Green Day");
        map3.put("Titel", "American Idiot");
        map3.put("Genre", "Punk-Rock");
        map3.put("Herkunftsland", "Amerika");    // gibt es nicht bei Musik
        med3 = new Medium(map3, MedienArt.MUSIK);

        map4 = new HashMap<>(listeTvSerie);
        map4.put("Autor", "Die Aerzte");
        map4.put("Titel", "Himmelblau");
        map4.put("Genre", "Punk-Rock");
        map4.put("Herkunftsland", "Deutschland");
        med4 = new Medium(map4, MedienArt.TVSERIE);
    }
    
    // Hilfsmethode zum Set-Up
    private void initialisiereListen(){

    	listeNormal = new HashMap<>();
        listeNormal.put("Titel", null);
        listeNormal.put("Autor", null);
        listeNormal.put("Genre", null);
        listeNormal.put("Verliehen an", null);
        listeNormal.put("Verleihdatum", null);
        listeNormal.put("Kaufpreis", null);
        listeNormal.put("Jahr", null);
        listeNormal.put("Speicherort", null);

        listeBuch = new HashMap<>(listeNormal);
        listeBuch.put("ISBN", null);

        listeMusik = new HashMap<>(listeNormal);
        listeMusik.put("ISAN", null);
        listeMusik.put("Album", null);
        listeMusik.put("Titelnummer", null);

        listeHoerbuch = new HashMap<>(listeNormal);
        listeHoerbuch.put("Sprecher", null);
        listeHoerbuch.put("Abspieldauer", null);
        listeHoerbuch.put("CD-Anzahl", null);

        listeFilm = new HashMap<>(listeNormal);
        listeFilm.put("ISAN", null);
        listeFilm.put("Abspieldauer", null);
        listeFilm.put("Herkunftsland", null);
        listeFilm.put("Top-Schauspieler", null);

        listeTvSerie = new HashMap<>(listeNormal);
        listeTvSerie.put("ISAN", null);
        listeTvSerie.put("Abspieldauer", null);
        listeTvSerie.put("Herkunftsland", null);
        listeTvSerie.put("Top-Schauspieler", null);
        listeTvSerie.put("Sender", null);
        listeTvSerie.put("Episodennummer", null);
    }

    // Konstruktor-Tests //
    
    /**
     * Test fuer die erfolgreiche Erstellung eines Mediums
     */
    @Test
    public void testMedium() {
        Medium testMedium = new Medium(map1, MedienArt.FILM);
        assertEquals(map1, testMedium.getParamListe());
        Medium testMedium2 = new Medium(map3, MedienArt.MUSIK);
        assertNotEquals(map3, testMedium2.getParamListe());
        map3.remove("Herkunftsland");
        assertEquals(map3, testMedium2.getParamListe());
        Medium testMedium3 = new Medium(map2, MedienArt.FILM);
        assertNotEquals(map2, testMedium3.getParamListe());
    }

    /**
     * Testet, ob eine passende Exception bei einer uebergebenen null-Referenz geworfen wird 
     */
    @Test(expected = NullPointerException.class)
    public void testMediumNullMedium() {
        new Medium(null, MedienArt.FILM);
    }

    // Tests fuer setParamList() //
    
    /**
     * Test fuer das Setzen einer neuen Datenliste fuer ein Medium im Normalfall
     */
    @Test
    public void testSetParamListe() throws MissingValueException {
        med4.setParamListe(map2, MedienArt.EBOOK);
        assertEquals(med2, med4);
        assertEquals("Fantasy", med2.getParamListe().get("Genre"));
        assertEquals("2432396848", med2.getParamListe().get("ISBN"));
    }
    
    /**
     * Erwartet eine Fehlermeldung, da eine null-Referenz uebergeben wird
     * @throws MissingValueException
     */
    @Test(expected = NullPointerException.class)
    public void testSetParamListeNullListe() throws MissingValueException {
        med4.setParamListe(null, MedienArt.EBOOK);
    }
    
    /**
     * Erwartet eine Fehlermeldung, da Medienart nicht zur paramList passt
     * @throws MissingValueException
     */
    @Test(expected = InvalidParameterException.class)
    public void testSetParamListeFalscheArt() throws MissingValueException {
        med4.setParamListe(med2.getParamListe(), MedienArt.FILM);
    }
    
    /**
     * Erwartet eine Fehlermeldung, da der paramList die Pflichtangabe "Autor" fehlt
     * @throws MissingValueException
     */
    @Test(expected = MissingValueException.class)
    public void testSetParamListeAutorFehlt() throws MissingValueException {
    	Map<String, String> map2OhneAutor = med2.getParamListe();
    	map2OhneAutor.remove("Autor");
        med4.setParamListe(map2OhneAutor, MedienArt.FILM);
    }
    
    /**
     * Erwartet eine Fehlermeldung, da der paramList die Pflichtangabe "Titel" fehlt
     * @throws MissingValueException
     */
    @Test(expected = MissingValueException.class)
    public void testSetParamListeTitelFehlt() throws MissingValueException {
    	Map<String, String> map2OhneTitel = med2.getParamListe();
    	map2OhneTitel.remove("Titel");
        med4.setParamListe(map2OhneTitel, MedienArt.FILM);
    }
    
    // Tests fuer setBewertungList() //

    /**
     * Testet, ob das Setzen der Bewertung im Normalfall die Bewertungsdaten korrekt manipuliert
     */
    @Test
    public void testSetBewertungList() {
    	Bewertung bewertung1 = new Bewertung("Lars", 3, "Ganz okay");
    	Bewertung bewertung2 = new Bewertung("Johannes", 5, "Beste Unterhaltung!!!");
    	Bewertung bewertung3 = new Bewertung("Tim", 0, "");
    	LinkedList<Bewertung> bewertungList = new LinkedList<>();
    	bewertungList.add(bewertung1);
    	bewertungList.add(bewertung2);
    	bewertungList.add(bewertung3);
    	
        med3.setBewertungList(bewertungList);
        assertEquals(3, med3.getBewertungList().size());
        assertTrue(med3.getBewertungList().contains(bewertung1));
        assertTrue(med3.getBewertungList().contains(bewertung2));
        assertTrue(med3.getBewertungList().contains(bewertung3));
    }
    
    /**
     * Erwartet eine Fehlermeldung, da eine null-Referenz uebergeben wird
     */
    @Test(expected = NullPointerException.class)
    public void testSetBewertungListNullListe() {
        med3.setBewertungList(null);
    }
    
    /**
     * Erwartet eine Fehlermeldung, da keine Bewertung in der uebergebenen Liste vorliegt, aber eine Bewertung (die eigene) notwendig ist
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetBewertungListLeereListe() {
        med3.setBewertungList(new LinkedList<Bewertung>());
    }
    
    /**
     * Erwartet eine Fehlermeldung, da ein Element der uebergebenen Liste eine null-Referenz ist
     */
    @Test(expected = NullPointerException.class)
    public void testSetBewertungListNullBewertung() {
    	Bewertung bewertung1 = new Bewertung("Lars", 3, "Ganz okay");
    	Bewertung bewertung2 = new Bewertung("Johannes", 5, "Beste Unterhaltung!!!");
    	LinkedList<Bewertung> bewertungList = new LinkedList<>();
    	bewertungList.add(bewertung1);
    	bewertungList.add(bewertung2);
    	bewertungList.add(null);
    	
        med3.setBewertungList(bewertungList);
    }
    
    // Tests fuer setVerknuepfungList() //

    /**
     * Testet, ob das Setzen der Verknuepfungsliste im Normalfall die Daten wie erwartet manipuliert
     */
    @Test
    public void testSetVerknuepfungList() {
    	LinkedList<Medium> medien = new LinkedList<>();
    	medien.add(med1);
    	medien.add(med2);
    	medien.add(med3);
        Verknuepfung verk1 = new Verknuepfung("Gehoert zusammen", medien);
        LinkedList<Medium> musiken = new LinkedList<>();
    	musiken.add(med1);
    	musiken.add(med2);
        Verknuepfung verk2 = new Verknuepfung("Tolle Lieder", musiken);
        LinkedList<Verknuepfung> verknuepfungList = new LinkedList<>();
        verknuepfungList.add(verk1);
        verknuepfungList.add(verk2);
        
        med3.setVerknuepfungList(verknuepfungList);
        assertEquals(2, med3.getVerknuepfungList().size());
        assertTrue(med3.getVerknuepfungList().contains(verk1));
        assertTrue(med3.getVerknuepfungList().contains(verk2));
        
        med3.setVerknuepfungList(new LinkedList<Verknuepfung>());
        assertEquals(0, med3.getVerknuepfungList().size());
    }
    
    /**
     * Erwartet eine Fehlermeldung, da in der Liste eine null-Referenz vorliegt
     */
    @Test(expected = NullPointerException.class)
    public void testSetVerknuepfungListNullVerknuepfung() {
    	LinkedList<Medium> medien = new LinkedList<>();
    	medien.add(med1);
    	medien.add(med2);
    	medien.add(med3);
        Verknuepfung verk1 = new Verknuepfung("Gehoert zusammen", medien);
        LinkedList<Verknuepfung> verknuepfungList = new LinkedList<>();
        verknuepfungList.add(verk1);
        verknuepfungList.add(null);
        
        med3.setVerknuepfungList(verknuepfungList);
    }
    
    
    /**
     * Erwartet eine Fehlermeldung, da eine null-Referenz uebergeben wird
     */
    @Test(expected = NullPointerException.class)
    public void testSetVerknuepfungListNullListe() {
        med3.setVerknuepfungList(null);
    }
}
