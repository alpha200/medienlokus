package model;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import kontrolle.EmptyStringException;
import kontrolle.VerknuepfungsKontrollierer;
import model.MedienArt;
import model.Medium;
import model.Verknuepfung;

import org.junit.Before;
import org.junit.Test;

public class VerknuepfungTest {

	 	VerknuepfungsKontrollierer kontrollierer = new VerknuepfungsKontrollierer();
	    Medium med1, med2, med3, med4;
	    Verknuepfung testVerknuepfung;
	    LinkedList<Medium> medien = new LinkedList<>();
	    LinkedList<Medium> medien2 = new LinkedList<>();
	    Map<String, String> map1, map2, map3, map4;
	    
	@Before
	public void setUp() throws Exception {
		
		map1 = new HashMap<String, String>();
		map1.put("Autor", "Rowling");
		map1.put("Titel", "Harry Potter");
		map1.put("Genre", "Fantasy");
		map1.put("Herkunftsland", "England");
		med1 = new Medium(map1, MedienArt.FILM);
		medien.add(med1);
		
		map2 = new HashMap<String, String>();
		map2.put("Autor", "Rowling");
		map2.put("Titel", "Harry Potter");
		map2.put("Genre", "Fantasy");
		map2.put("ISBN", "2432396848");
		med2 = new Medium(map2, MedienArt.BUCH);
		medien.add(med2);
		
		map3 = new HashMap<String, String>();
		map3.put("Autor", "Green Day");
		map3.put("Titel", "American Idiot");
		map3.put("Genre", "Punk-Rock");
		map3.put("Album", "American Idiot");
		med3 = new Medium(map3, MedienArt.MUSIK);
		medien.add(med3);
		
		map4 = new HashMap<String, String>();
		map4.put("Autor", "Die Aerzte");
		map4.put("Titel", "Himmelblau");
		map4.put("Genre", "Punk-Rock");
		map4.put("Album", "Jazz ist anders");
		med4 = new Medium(map4, MedienArt.MUSIK);
		medien2.add(med4);
		testVerknuepfung = new Verknuepfung("Gehoert zusammen", medien);
	}
	
	/**
	 * 3 Medien werden zu einer Verknuepfung hinzugefuegt.
	 * Es wird geprueft, ob alle 3 Medien in Verknuepfung sind
	 * und die Beschreibung passt.
	 */
	@Test
	public void testVerknuepfung() {
		assertTrue(testVerknuepfung.gebeListe().contains(med1));
		assertTrue(testVerknuepfung.gebeListe().contains(med2));
		assertTrue(testVerknuepfung.gebeListe().contains(med3));
		assertEquals(3, testVerknuepfung.gebeListe().size());
		assertEquals("Gehoert zusammen", testVerknuepfung.gebeBeschreibung());
	}
	
	/**
	 * 	Neue Verknuepfung wird ohne Medienliste erstellt.
	 */
	@Test(expected = NullPointerException.class)
	public void testVerknuepfungLeereListe() {
		new Verknuepfung("Gehoert zusammen", null);
	}
	
	/**
	 * 	Neue Verknuepfung wird mit Liste, die null-Werte enthaelt, erstellt.
	 */
	@Test(expected = NullPointerException.class)
	public void testVerknuepfungLeeresMedium() {
		medien.add(null);
		new Verknuepfung("Gehoert zusammen", medien);
	}
	
	/**
	 * 	Neue Verknuepfung wird mit Liste, die Duplikate enthaelt, erstellt.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testVerknuepfungDuplikatMedium() {
		medien.add(med1);
		new Verknuepfung("Gehoert zusammen", medien);
	}
	
	/**
	 * 	Neue Verknuepfung wird mit leerer Beschreibung erstellt.
	 */
	@Test(expected = EmptyStringException.class)
	public void testVerknuepfungLeerstring() {
		new Verknuepfung("", medien);
	}
	
	/**
	 * 	Neue Verknuepfung wird ohne Beschreibung erstellt.
	 */
	@Test(expected = NullPointerException.class)
	public void testVerknuepfungNullstring() {
		new Verknuepfung(null, medien);
	}
	
	/**
	 * 	Beschreibung der Verknuepfung wird geaendert.
	 * 	Neue Beschreibung muss gleich "Walking Dead" sein.
	 */
	@Test
	public void testSetzeBeschreibung() {
		testVerknuepfung.setzeBeschreibung("Walking Dead");
		assertEquals("Walking Dead", testVerknuepfung.gebeBeschreibung());
	}
	
	/**
	 * 	Beschreibung der Verknuepfung wird durch leeren String ersetzt.
	 */
	@Test(expected = EmptyStringException.class)
	public void testSetzeBeschreibungLeerstring() {
		testVerknuepfung.setzeBeschreibung("");
	}
	
	/**
	 * 	Beschreibung der Verknuepfung wird durch "null" ersetzt.
	 */
	@Test(expected = NullPointerException.class)
	public void testSetzeBeschreibungNullstring() {
		testVerknuepfung.setzeBeschreibung(null);
	}
	
	/**
	 * 	Medienliste der Verknuepfung wird ersetzt.
	 * 	Medium 4 muss in der neuen Verknuepfung sein.
	 * 	Medium 1 darf nicht in der neuen Verknuepfung sein.
	 */
	@Test
	public void testSetzeListe() {
		testVerknuepfung.setzeListe(medien2);
		assertTrue(testVerknuepfung.gebeListe().contains(med4));
		assertFalse(testVerknuepfung.gebeListe().contains(med1));
	}
	
	/**
	 * 	Liste der Verknuepfung wird durch Liste mit null-Werten ersetzt.
	 */
	@Test(expected = NullPointerException.class)
	public void testSetzeListeleereMedien() {
		medien.add(null);
		testVerknuepfung.setzeListe(medien);
	}
	
	/**
	 * 	Liste der Verknuepfung wird durch Liste mit Duplikaten ersetzt.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testSetzeListeduplikateMedien() {
		medien.add(med1);
		testVerknuepfung.setzeListe(medien);
	}
	
	/**
	 * 	Liste der Verknuepfung wird durch "null" ersetzt.
	 */
	@Test(expected = NullPointerException.class)
	public void testSetzeListeleereListe() {
		testVerknuepfung.setzeListe(null);
	}
}