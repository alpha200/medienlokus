package model;

import static org.junit.Assert.*;
import kontrolle.EmptyStringException;
import model.Bewertung;

import org.junit.Test;

public class BewertungTest {

	/**
	 * 	neue Bewertung wird erstellt und alle 3 Parameter werden ueberprueft.
	 */
    @Test
    public void testBewertung() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        assertEquals("Lars", testBewertung.getBewerter());
        assertEquals(3, testBewertung.getBewertung());
        assertEquals("Ganz okay", testBewertung.getBeschreibung());
    }
    
    /**
	 * Bewertung wird mit null-Benutzer erstellt.
	 */
    @Test(expected = NullPointerException.class)
    public void testBewertungNullBewerter() {
        new Bewertung(null, 3, "Ganz okay");
    }
    
    /**
	 * Bewertung wird mit null-Bewertung erstellt.
	 */
    @Test(expected = NullPointerException.class)
    public void testBewertungNullBeschreibung() {
        new Bewertung("Lars", 3, null);
    }
    
    /**
	 * Bewertung wird mit leerem Benutzer erstellt
	 */
    @Test(expected = EmptyStringException.class)
    public void testBewertungLeererBewerter() {
        new Bewertung("", 3, "Ganz okay");
    }
    
    /**
	 * Bewertung wird mit zu hoher Bewertung erstellt.
	 */
    @Test(expected = IllegalArgumentException.class)
    public void testBewertungHoheBewertung() {
        new Bewertung("Lars", 90, "Ganz okay");
    }
    
    /**
	 * Bewertung wird mit negativer Bewertung erstellt.
	 */
    @Test(expected = IllegalArgumentException.class)
    public void testBewertungNegativeBewertung() {
        new Bewertung("Lars", -1, "Ganz okay");
    }
    
    /**
     * Bewertung wird geandert.
     * Erneut werden alle 3 Paramater getestet
     */
    @Test
    public void testSetBewertung() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        testBewertung.setBewertung(5);
        assertEquals("Lars", testBewertung.getBewerter());
        assertEquals(5, testBewertung.getBewertung());
        assertEquals("Ganz okay", testBewertung.getBeschreibung());
    }
    
    /**
	 * Bewertung wird durch zu hohe Bewertung ersetzt.
	 */
    @Test(expected = IllegalArgumentException.class)
    public void testSetBewertungNullBewertung() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        testBewertung.setBewertung(87);
    }
    
    /**
	 * Bewertung wird durch negative Bewertung ersetzt.
	 */
    @Test(expected = IllegalArgumentException.class)
    public void testSetBewertungLeererBewertung() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        testBewertung.setBewertung(-1);
    }
    
    /**
	 * Bewerter wird geaendert.
	 * Alle 3 Parameter werden geprueft.
	 */
    @Test
    public void testSetBewerter() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        testBewertung.setBewerter("Johannes");
        assertEquals("Johannes", testBewertung.getBewerter());
        assertEquals(3, testBewertung.getBewertung());
        assertEquals("Ganz okay", testBewertung.getBeschreibung());
    }
    
    /**
	 * Bewerter wird durch null ersetzt.
	 */
    @Test(expected = NullPointerException.class)
    public void testSetBewerterNullBewerter() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        testBewertung.setBewerter(null);
    }
    
    /**
	 * Bewerter wird durch leeren String ersetzt.
	 */
    @Test(expected = EmptyStringException.class)
    public void testSetBewerterLeererBewerter() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        testBewertung.setBewerter("");
    }
    
    /**
     * 	Beschreibung wird geandert
     * 	Alle 3 Parameter werden geprueft.
     */
    @Test
    public void testSetBeschreibung() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        testBewertung.setBeschreibung("Voll im Durchschnitt");
        assertEquals("Lars", testBewertung.getBewerter());
        assertEquals(3, testBewertung.getBewertung());
        assertEquals("Voll im Durchschnitt", testBewertung.getBeschreibung());
    }
    
    /**
	 * Beschreibung wird durch null ersetzt.
	 */
    @Test(expected = NullPointerException.class)
    public void testSetBeschreibungNullBeschreibung() {
        Bewertung testBewertung = new Bewertung("Lars", 3, "Ganz okay");
        testBewertung.setBeschreibung(null);
    }

}