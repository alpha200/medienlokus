package model;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import kontrolle.EmptyStringException;
import model.MedienArt;
import model.Medienverwaltung;
import model.Medium;
import model.Verknuepfung;

import org.junit.Before;
import org.junit.Test;

public class MedienverwaltungTest {

    Medium med1, med2, med3, med4;
    Map<String, String> map1, map2, map3, map4;
    Verknuepfung testVerknuepfung;
    LinkedList<Medium> medienArchiv = new LinkedList<>();
    LinkedList<Medium> wunschListe = new LinkedList<>();

    @Before
    public void setUp() {

        map1 = new HashMap<String, String>();
        map1.put("Autor", "Rowling");
        map1.put("Titel", "Harry Potter");
        map1.put("Genre", "Fantasy");
        map1.put("Herkunftsland", "England");
        med1 = new Medium(map1, MedienArt.FILM);
        medienArchiv.add(med1);

        map2 = new HashMap<String, String>();
        map2.put("Autor", "Rowling");
        map2.put("Titel", "Harry Potter");
        map2.put("Genre", "Fantasy");
        map2.put("ISBN", "2432396848");
        med2 = new Medium(map2, MedienArt.BUCH);
        medienArchiv.add(med2);

        map3 = new HashMap<String, String>();
        map3.put("Autor", "Green Day");
        map3.put("Titel", "American Idiot");
        map3.put("Genre", "Punk-Rock");
        map3.put("Herkunftsland", "Amerika");
        med3 = new Medium(map3, MedienArt.MUSIK);
        medienArchiv.add(med3);

        map4 = new HashMap<String, String>();
        map4.put("Autor", "Die Aerzte");
        map4.put("Titel", "Himmelblau");
        map4.put("Genre", "Punk-Rock");
        map4.put("Herkunftsland", "Deutschland");
        med4 = new Medium(map4, MedienArt.MUSIK);
        wunschListe.add(med4);

    }

    // Konstruktor-Tests //
    
    /**
     * Testet, ob das Erstellen einer Verwaltung im Normalfall die uebergebenen Referenzen uebernimmt
     */
    @Test
    public void testMedienverwaltung() {
        Medienverwaltung testVerwaltung = new Medienverwaltung("Lars", wunschListe, medienArchiv);
        assertEquals("Lars", testVerwaltung.getBenutzer());
        assertEquals(1, testVerwaltung.getWunschliste().size());
        assertTrue(testVerwaltung.getWunschliste().contains(med4));
        assertEquals(3, testVerwaltung.getMedienarchiv().size());
        assertTrue(testVerwaltung.getMedienarchiv().contains(med1));
        assertTrue(testVerwaltung.getMedienarchiv().contains(med2));
        assertTrue(testVerwaltung.getMedienarchiv().contains(med3));
    }

    /**
     * Erwartet eine Fehlermeldung, da eine null-Referenz als Benutzer vorliegt
     */
    @Test(expected = NullPointerException.class)
    public void testMedienverwaltungNullBenutzer() {
        new Medienverwaltung(null, wunschListe, medienArchiv);
    }

    /**
     * Erwartet eine Fehlermeldung, da eine null-Referenz als Wunschliste vorliegt
     */
    @Test(expected = NullPointerException.class)
    public void testMedienverwaltungNullWunschliste() {
        new Medienverwaltung("Lars", null, medienArchiv);
    }

    /**
     * Erwartet eine Fehlermeldung, da eine null-Referenz als Medienarchiv vorliegt
     */
    @Test(expected = NullPointerException.class)
    public void testMedienverwaltungNullArchiv() {
        new Medienverwaltung("Lars", wunschListe, null);
    }

    /**
     * Erwartet eine Fehlermeldung, da ein leerer String als Benutzername unzulaessig ist
     */
    @Test(expected = EmptyStringException.class)
    public void testMedienverwaltungLeererBenutzer() {
        new Medienverwaltung("", wunschListe, medienArchiv);
    }

    // Tests fuer setBenutzer() //
    
    /**
     * Testet, ob im Normalfall nur der Benutzername manipuliert wird
     */
    @Test
    public void testSetBenutzer() {
        Medienverwaltung testVerwaltung = new Medienverwaltung("Lars", wunschListe, medienArchiv);
        testVerwaltung.setBenutzer("Johannes");
        assertEquals("Johannes", testVerwaltung.getBenutzer());
        assertEquals(1, testVerwaltung.getWunschliste().size());
        assertTrue(testVerwaltung.getWunschliste().contains(med4));
        assertEquals(3, testVerwaltung.getMedienarchiv().size());
        assertTrue(testVerwaltung.getMedienarchiv().contains(med1));
        assertTrue(testVerwaltung.getMedienarchiv().contains(med2));
        assertTrue(testVerwaltung.getMedienarchiv().contains(med3));
    }

    /**
     * Erwartet eine Fehlermeldung, da eine null-Referenz uebergeben wird
     */
    @Test(expected = NullPointerException.class)
    public void testSetBenutzerNullBenutzer() {
        Medienverwaltung testVerwaltung = new Medienverwaltung("Lars", wunschListe, medienArchiv);
        testVerwaltung.setBenutzer(null);
    }

    /**
     * Erwartet eine Fehlermeldung, da der Benutzername kein Leerstring sein darf
     */
    @Test(expected = EmptyStringException.class)
    public void testSetBenutzerLeererBenutzer() {
        Medienverwaltung testVerwaltung = new Medienverwaltung("Lars", wunschListe, medienArchiv);
        testVerwaltung.setBenutzer("");
    }
    
    // Tests fuer setztWunschliste() und setzeMedienarchiv() //

    /**
     * Testet, ob im Normalfall die uebergebenen Liste ins Medienarchiv und die Wunschliste uebernommen werden
     */
    @Test
    public void testSetzeWunschlisteUndArchiv() {
        Medienverwaltung testVerwaltung = new Medienverwaltung("Lars", wunschListe, medienArchiv);
        testVerwaltung.setWunschliste(medienArchiv);
        testVerwaltung.setMedienarchiv(wunschListe);
        assertEquals("Lars", testVerwaltung.getBenutzer());
        assertEquals(1, testVerwaltung.getMedienarchiv().size());
        assertTrue(testVerwaltung.getMedienarchiv().contains(med4));
        assertEquals(3, testVerwaltung.getWunschliste().size());
        assertTrue(testVerwaltung.getWunschliste().contains(med1));
        assertTrue(testVerwaltung.getWunschliste().contains(med2));
        assertTrue(testVerwaltung.getWunschliste().contains(med3));
    }

    /**
     * Erwartet eine Fehlermeldung, da eine null-Referenz vorliegt
     */
    @Test(expected = NullPointerException.class)
    public void testSetWunschlisteNullListe() {
        Medienverwaltung testVerwaltung = new Medienverwaltung("Lars", wunschListe, medienArchiv);
        testVerwaltung.setWunschliste(null);
    }

    /**
     * Erwartet eine Fehlermeldung, da eine null_referenz vorliegt
     */
    @Test(expected = NullPointerException.class)
    public void testSetMedienarchivNullListe() {
        Medienverwaltung testVerwaltung = new Medienverwaltung("Lars", wunschListe, medienArchiv);
        testVerwaltung.setMedienarchiv(null);
    }
}
