package control;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import model.Block;
import model.Game;
import model.GameState;
import model.Mode;
import model.Player;
import model.PlayerType;
import model.ShrinkedPlayer;
import model.ShrinkedTurn;
import model.Turn;

public class SiriKiControllerTest {
	public Player player1 = new Player(false, Block.BLUE, "Siri1", PlayerType.SIRI);
	public Player player2 = new Player(false, Block.YELLOW, "Siri2", PlayerType.SIRI);
	public Player player3 = new Player(false, Block.RED,"Siri3", PlayerType.SIRI);
	public Player player4 = new Player(false, Block.GREEN, "Siri4", PlayerType.SIRI);
	public ShrinkedPlayer splayer1 = new ShrinkedPlayer(player1);
	public ShrinkedPlayer splayer2 = new ShrinkedPlayer(player2);
	public ShrinkedPlayer splayer3 = new ShrinkedPlayer(player3);
	public ShrinkedPlayer splayer4 = new ShrinkedPlayer(player4);
	public Player[] players = {player1,player2,player3,player4};
	public ShrinkedPlayer[] splayers = {splayer1, splayer2, splayer3, splayer4};
	@Test
	public void testCalculateTurn() {
		List<Turn> turnList = new ArrayList<>();
		Game game = new Game(players, Mode.STANDARD);
		SiriKiController siri = new SiriKiController(game, player1);
		SiriKiController siri2 = new SiriKiController(game, player2);
		SiriKiController siri3 = new SiriKiController(game, player2);
		SiriKiController siri4 = new SiriKiController(game, player2);
		GameState state = new GameState(game.getField(), splayers);
		for(int i=0; i<7;i++){
			Turn turn1 = siri.calculateTurn();
			turnList.add(turn1);
			ShrinkedTurn sturn1 = new ShrinkedTurn(turn1);
			state.doTurn(sturn1);
			Turn turn2 = siri2.calculateTurn();
			turnList.add(turn2);
			ShrinkedTurn sturn2 = new ShrinkedTurn(turn2);
			state.doTurn(sturn2);
			player1.getBricks().remove(turn1.getBrick());
			player2.getBricks().remove(turn2.getBrick());
			Turn turn3 = siri3.calculateTurn();
			ShrinkedTurn sturn3 = new ShrinkedTurn(turn3);
			state.doTurn(sturn3);
			player3.getBricks().remove(turn3.getBrick());
			turnList.add(turn3);
			Turn turn4 = siri4.calculateTurn();
			ShrinkedTurn sturn4= new ShrinkedTurn(turn4);
			state.doTurn(sturn4);
			player4.getBricks().remove(turn4.getBrick());
			turnList.add(turn4);
			game.setTurns(turnList);
		}
	}

}
