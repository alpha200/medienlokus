package control;

import java.awt.Point;
import java.util.List;
import java.util.Set;

import model.Block;
import model.Brick;
import model.Game;
import model.Mode;
import model.Player;
import model.PlayerType;
import model.Turn;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class KIControllerTest {

    private void printField(Block[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if(field[j][i] == null) {
                    System.out.print("X");
                    continue;
                }

                switch (field[j][i]) {

                    case BLUE:
                        System.out.print('B');
                        break;
                    case YELLOW:
                        System.out.print('Y');
                        break;
                    case RED:
                        System.out.print('R');
                        break;
                    case GREEN:
                        System.out.print('G');
                        break;
                    case EMPTY:
                        System.out.print('X');
                        break;
                }
            }

            System.out.println();
        }
    }

    @Test
    public void testCalculateCorners() {
        KIController kiController = new KIController(new Game(new Player[] {new Player(false, Block.RED, "player1", PlayerType.PLAYER)}, Mode.STANDARD), new Player(false, Block.BLUE, "noname", PlayerType.SVOICE)) {
            @Override
            public Turn calculateTurn() {
                return null;
            }

            @Override
            public void transmitTurn(Turn turnDone) {

            }
        };

        Block[][] field = new Block[10][10];

        // Initialize field with Block.EMPTY
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = Block.EMPTY;
            }
        }

        // Generate testFields
        /*
            -B--------
            BB--------
            --B-------
            -?--------
            R---------
            R-?-------
            -R--------
            -R--------
            -R--------
            RR--------
         */
        field[1][0] = field[0][1] = field[1][1] = field[2][2] = Block.BLUE;
        field[0][4] = field[0][5] = field[0][9] = field[1][6] = field[1][7] = field[1][8] = field[1][9] = Block.RED;

        //printField(field);

        List<Point> corners = kiController.calculateCorners(field, Block.RED);

        Assert.assertEquals("Not the exact number of Points!", 2, corners.size());
        Assert.assertThat(corners, CoreMatchers.hasItem(new Point(1, 3)));
        Assert.assertThat(corners, CoreMatchers.hasItem(new Point(2, 5)));
    }
    @Test
	public void testGetBrickinAllForms(){
    	Player player1 = new Player(false, Block.RED, "player1", PlayerType.PLAYER);
    	Player player2 = new Player(false, Block.BLUE, "player2", PlayerType.PLAYER);
    	Player player3 = new Player(false, Block.GREEN, "player3", PlayerType.PLAYER);
    	Player player4 = new Player(false, Block.YELLOW, "player4", PlayerType.PLAYER);
    	Player[] player = {player1,player2,player3,player4};
    	Game game = new Game(player, Mode.STANDARD);
    	
    	KIController TestGetBrickInAllForms = new KIController(game, player1) {
			
			@Override
			public Turn calculateTurn() {
				// TODO Auto-generated method stub
				return null;
			}

            @Override
            public void transmitTurn(Turn turnDone) {

            }
        };
		Brick currentBrick = Brick.getStandardBrick(7);		
    	Set<Brick> testedBricks = TestGetBrickInAllForms.getBrickInAllForms(currentBrick);
    	Assert.assertEquals(testedBricks.size(), 4);

    	
    }

    @Test
    public void testCalculatePossibleTurns() {
        Player kiPlayer =  new Player(false, Block.RED, "noname", PlayerType.SVOICE);

        kiPlayer.getBricks().remove(10);
        kiPlayer.getBricks().remove(1);

        KIController kiController = new KIController(new Game(new Player[] {new Player(false, Block.RED, "player1", PlayerType.PLAYER)}, Mode.STANDARD), kiPlayer) {
            @Override
            public Turn calculateTurn() {
                return null;
            }

            @Override
            public void transmitTurn(Turn turnDone) {

            }
        };

        Block[][] field = new Block[10][10];

        // Initialize field with Block.EMPTY
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = Block.EMPTY;
            }
        }

        field[1][0] = field[0][1] = field[1][1] = field[2][2] = Block.BLUE;
        field[0][4] = field[0][5] = field[0][9] = field[1][6] = field[1][7] = field[1][8] = field[1][9] = Block.RED;

        Turn[] turns;

        long startTime = System.nanoTime();
        turns = kiController.calculatePossibleTurns(field, kiPlayer);
        long stopTime = System.nanoTime();

        System.out.println("Needed calculation time:" + (stopTime - startTime) / 1000000.0 + "ms");
        Assert.assertNotEquals("there should be at least one possible turn!", 0, turns.length);

        System.out.println("Number of possible turns: " + turns.length);

        /*for (Turn turn : turns) {
            printTurn(field, turn);
            System.out.println();
        }*/
    }

    private void printTurn(Block[][] field, Turn turn) {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(turn.getBrick().getCoord()[j][i] ? "#" : "-");
            }

            if(i==2) {
                System.out.print("   " + turn.getCoord().x + "," + turn.getCoord().y);
            }

            System.out.println();
        }
    }
}
