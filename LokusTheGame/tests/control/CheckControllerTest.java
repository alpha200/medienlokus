package control;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;

import model.Block;
import model.Game;
import model.Mode;
import model.Player;
import model.PlayerType;
import model.Turn;

import org.junit.Before;
import org.junit.Test;

public class CheckControllerTest {

	private CheckController checkController;
	private GameController gameController;
	
	
	private Player blue;
	private Block[][] testField;
	private Player yellow;
	private Player green;
	private Player red;
	private Game game;

	@Before
	public void setUp() throws Exception {
		gameController = new GameController();
		checkController = gameController.getCheckController();
		blue = new Player(false, Block.BLUE, "Blue", PlayerType.PLAYER);
		yellow = new Player(false, Block.YELLOW, "Yellow", PlayerType.PLAYER);
		red = new Player(false, Block.RED, "Red", PlayerType.PLAYER);
		green = new Player(false, Block.GREEN, "Green",
				PlayerType.PLAYER);
		Player[] players = { blue, yellow, red, green };
		gameController.startNewGame(players, Mode.STANDARD);
		game = gameController.getGame();

		testField = game.getField();
		testField[0][1] = Block.BLUE;
		testField[0][2] = Block.BLUE;
		testField[0][3] = Block.BLUE;
		testField[3][2] = Block.YELLOW;
		
	}

	/**
	 * Check Test
	 */
	@Test
	public void testCheckTurnNextToOwnColorBlue() {
		Point point = new Point(2, 1);
		Turn turn = new Turn(blue, blue.getBricks().get(1), point);
		assertFalse(
				"Zug sollte nicht möglich sein, da neben eigener Farbe, checkTurn sagt aber True!",
				checkController.checkTurn(turn, testField));
	}

	@Test
	public void testCheckTurnDiagonalBlue() {
		Point point = new Point(2, 0);
		Turn turn = new Turn(blue, blue.getBricks().get(2), point);
		assertTrue(checkController.checkTurn(turn, testField));
	}

	@Test
	public void testCheckTurnNoDiagonalYellow() {
		Point point = new Point(2, 0);
		Turn turn = new Turn(yellow, yellow.getBricks().get(2), point);
		assertFalse(checkController.checkTurn(turn, testField));
	}

	@Test
	public void testCheckTurnDiagonalYellow() {
		Point point = new Point(2, 1);
		Turn turn = new Turn(yellow, yellow.getBricks().get(1), point);
		assertTrue(checkController.checkTurn(turn, testField));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testCheckTurnBlockOutOfField() {
		Point point = new Point(0, 0);
		Turn turn = new Turn(yellow, yellow.getBricks().get(1), point);
		checkController.checkTurn(turn, testField);
	}

}
