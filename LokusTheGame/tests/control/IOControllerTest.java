package control;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class IOControllerTest {
	
	private IOController ioController;
	
	@Before
    public void setUp() throws Exception {
		ioController = new IOController(new GameController());
    }
	
	/**
     * Checks whether null-values want to be saved.
     */
    @Test
    public void testSaveGameNullPointerException() {
        boolean isExceptionOccured = false;
        try {
            ioController.saveGame(null);
            } catch (NullPointerException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertTrue("SaveGame should not accept null-values.", isExceptionOccured);
    }
    
    
    /**
     * Checks whether null-values can be loaded.
     */
    @Test
    public void testLoadGameNullPointerException() {
        boolean isExceptionOccured = false;
        try {
            ioController.loadGame();
        } catch (NullPointerException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertFalse("LoadGame should load no null-values.", isExceptionOccured);
    }
    
    /**
     * Checks whether objects, which implements interface, will be loaded.
     */
    @Test
    public void testLoadGameClassNotFountException() {
        boolean isExceptionOccured = false;
        try {
            ioController.loadGame();
        } catch (ClassNotFoundException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertFalse("LoadGame should load objects, which implements interface.", isExceptionOccured);
    }
    
    /**
     * Checks whether the session will be loaded
     */
    @Test
    public void testLoadGameIOException() {
        boolean isExceptionOccured = false;
        try {
            FileOutputStream fileStream = new FileOutputStream("output/config.ser");
            fileStream.write(55);
            fileStream.close();
        } catch (Exception e) {
            assertTrue("Test could not create a file.", false);
        }
        try {
            ioController.loadGame();
        } catch (IOException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertTrue("LoadGame should load the last session. (File must exist)", isExceptionOccured);
    }

}
