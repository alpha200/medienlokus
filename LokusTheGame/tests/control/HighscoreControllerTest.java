package control;

import static org.junit.Assert.*;

import java.io.FileOutputStream;
import java.io.IOException;
//import java.util.LinkedList;
//import java.util.List;

import model.Mode;
//import model.Score;

import org.junit.Before;
import org.junit.Test;

public class HighscoreControllerTest {
	
	private HighScoreController highscoreController;
//	private Score score1;
//	private Score score2;
//	private Score score3;
//	private Score score4;
//	
//	List<Score> scores = new LinkedList<Score>();
	
	@Before
    public void setUp() throws Exception {
		highscoreController = new HighScoreController();
		
//		score1 = new Score(200, "Lars", "2015.09.07 - 09:36:40 ");
//		score2 = new Score(250, "Cihat", "2015.08.07 - 19:36:40 ");
//		score3 = new Score(100, "Jan", "2015.09.06 - 08:36:40 ");
//		score4 = new Score(300, "Johannes", "2014.09.07 - 06:39:40 ");
//		
//		scores.add(score1);
//		scores.add(score2);
//		scores.add(score3);
//		scores.add(score4);		
    }
	
	/**
	 * checks whether a highscore is added
	 */
//	@Test
//	public void testAddHighscore(){
//		highscoreController.addHighScore(400, "Nils", Mode.CUSTOM);
//		assertEquals(1, highscoreController.getHighscore(Mode.CUSTOM).size());
//	}
	
	/**
	 * checks whether the highscore list is sorted
	 */
//	@Test
//	public void testAddHighscoreSorted(){
//		highscoreController.addHighScore(400, "Nils",Mode.CUSTOM);
//		highscoreController.addHighScore(200, "Lars",Mode.CUSTOM);
//		highscoreController.addHighScore(410, "Cihat",Mode.CUSTOM);
//		highscoreController.addHighScore(300, "Johannes",Mode.CUSTOM);
//		highscoreController.addHighScore(600, "Johannes",Mode.CUSTOM);
//		highscoreController.addHighScore(400, "Johannes",Mode.CUSTOM);
//		highscoreController.addHighScore(10, "Johannes",Mode.CUSTOM);
//		highscoreController.addHighScore(1000, "Johannes",Mode.CUSTOM);
//		assertEquals(8, highscoreController.getHighscore(Mode.CUSTOM).size());
//		assertEquals(1000, highscoreController.getHighscore(Mode.CUSTOM).get(0).getPoints());
//		assertEquals(600, highscoreController.getHighscore(Mode.CUSTOM).get(1).getPoints());
//		assertEquals(410, highscoreController.getHighscore(Mode.CUSTOM).get(2).getPoints());
//		assertEquals(400, highscoreController.getHighscore(Mode.CUSTOM).get(3).getPoints());
//		assertEquals(400, highscoreController.getHighscore(Mode.CUSTOM).get(4).getPoints());
//		assertEquals(300, highscoreController.getHighscore(Mode.CUSTOM).get(5).getPoints());
//		assertEquals(200, highscoreController.getHighscore(Mode.CUSTOM).get(6).getPoints());
//		assertEquals(10, highscoreController.getHighscore(Mode.CUSTOM).get(7).getPoints());
//	}
	
	/**
	 * checks whether null-values will be added
	 */
	@Test(expected = NullPointerException.class)
	public void testAddHighscoreNullPointerException(){
		highscoreController.addHighScore(400, null,Mode.CUSTOM);
	}
	
	/**
	 * checks whether empty-values will be added
	 */
//	@Test(expected = EmptyStringException.class)
//	public void testAddHighscoreEmptyStringException(){
//		highscoreController.addHighScore(400, "",Mode.CUSTOM);
//	}
//	
//	/**
//	 * checks whether negative points will be added
//	 */
//	@Test(expected = IllegalArgumentException.class)
//	public void testAddHighscoreIllegalArgumentException(){
//		highscoreController.addHighScore(-1, "Lars",Mode.CUSTOM);
//	}
	//TODO saveHighscore speichert das ganze Highscore element. testfall vlllt unnötig
//	/**
//     * Checks whether null-values want to be saved.
//     */
//    @Test
//    public void testSaveHighscoreNullPointerException() {
//        boolean isExceptionOccured = false;
//        try {
//        	highscoreController.saveHighScore();
//            } catch (NullPointerException e) {
//            isExceptionOccured = true;
//        } catch (Exception e) {
//        }
//        assertTrue("SaveHighScore should not accept null-values.", isExceptionOccured);
//    }
//    
//    /**
//     * Checks whether the objects implements the interface.
//     */
//    @Test
//    public void testSaveHighscoreClassNotFound() {
//        boolean exceptionThrown = false;
//        try {
//        	highscoreController.saveHighScore();
//        } catch (IOException ex) {
//            exceptionThrown = true;
//        }
//        // TO DO  : Setze auf false
//        assertTrue("SaveHighScore should accept objects which implements the interface.", exceptionThrown);
//    }
    
    /**
     * Checks whether null-values can be loaded.
     */
    @Test
    public void testloadHighscoreNullPointerException() {
        boolean isExceptionOccured = false;
        try {
        	highscoreController.loadHighscore();
        } catch (NullPointerException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertFalse("LoadHighscore should load no null-values.", isExceptionOccured);
    }
    
    /**
     * Checks whether objects, which implements interface, will be loaded.
     */
    @Test
    public void testloadHighscoreClassNotFoundException() {
        boolean isExceptionOccured = false;
        try {
        	highscoreController.loadHighscore();
        } catch (ClassNotFoundException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertFalse("LoadHighscore should load objects, which implements interface.", isExceptionOccured);
    }
    
    /**
     * Checks whether the session will be loaded
     */
    @Test
    public void testloadHighscoreIOException() {
        boolean isExceptionOccured = false;
        try {
            FileOutputStream fileStream = new FileOutputStream("output/score.ser");
            fileStream.write(55);
            fileStream.close();
        } catch (Exception e) {
            assertTrue("Test could not create a file.", false);
        }
        try {
        	highscoreController.loadHighscore();
        } catch (IOException e) {
            isExceptionOccured = true;
        } catch (Exception e) {
        }
        assertTrue("LoadHighscore should load the last session. (File must exist)", isExceptionOccured);
    }
}
