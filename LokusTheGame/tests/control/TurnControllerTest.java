package control;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;

import model.Block;
import model.Brick;
import model.Game;
import model.Mode;
import model.Player;
import model.PlayerType;
import model.Turn;

import org.junit.Before;
import org.junit.Test;

import view.LokusTheGame;

public class TurnControllerTest {

	private TurnController turnController;
	private GameController gameController;
	
	private Player blue;
	private Block[][] testField;
	private Player yellow;
	private Block[][] checkField;
	private GameController checkGameController;
	private Player checkBlue;
	private Player checkYellow;
	private Player green;
	private Player red;
	private Game game;

	@Before
	public void setUp() throws Exception {
		gameController = new GameController(new LokusTheGame());
		turnController = gameController.getTurnController();
		blue = new Player(false, Block.BLUE, "Blue", PlayerType.PLAYER);
		yellow = new Player(false, Block.YELLOW, "Yellow", PlayerType.PLAYER);
		red = new Player(false, Block.RED, "Red", PlayerType.PLAYER);
		green = new Player(false, Block.GREEN, "Green",
				PlayerType.PLAYER);
		Player[] players = { blue, yellow, red, green };
		gameController.startNewGame(players, Mode.STANDARD);
		game = gameController.getGame();

		testField = game.getField();
		testField[0][1] = Block.BLUE;
		testField[0][2] = Block.BLUE;
		testField[0][3] = Block.BLUE;
		testField[3][2] = Block.YELLOW;
		
		checkGameController = new GameController();
		checkBlue = new Player(false, Block.BLUE, "Blue", PlayerType.PLAYER);
		checkYellow = new Player(false, Block.YELLOW, "Yellow", PlayerType.PLAYER);
		Player checkRed = new Player(false, Block.RED, "Red", PlayerType.PLAYER);
		Player checkGreen = new Player(false, Block.GREEN, "Green",
				PlayerType.PLAYER);
		Player[] checkPlayers = { checkBlue, checkYellow, checkRed, checkGreen };
		checkGameController.startNewGame(checkPlayers, Mode.STANDARD);
		Game checkGame = checkGameController.getGame();
		
		checkField = checkGame.getField();
		checkField[0][1] = Block.BLUE;
		checkField[0][2] = Block.BLUE;
		checkField[0][3] = Block.BLUE;
		checkField[3][2] = Block.YELLOW;
	}

	
	
	/**
	 * Do Tests
	 */
	@Test
	public void testDoTurnNextToOwnColorBlue() {
		Point point = new Point(2, 1);
		Brick turnBrick = blue.getBricks().get(1);
		Turn turn = new Turn(blue, turnBrick, point);
		turnController.doTurn(turn, testField, false);
		assertArrayEquals(testField, checkField);
		assertTrue(blue.getBricks().contains(turnBrick));
	}

	@Test
	public void testDoTurnDiagonalBlue() {
		Point point = new Point(2, 0);
		Brick turnBrick = blue.getBricks().get(1);
		checkField[2][0] = Block.BLUE;
		checkField[1][0] = Block.BLUE;
		Turn turn = new Turn(blue, turnBrick, point);
		turnController.doTurn(turn, testField, false);
		assertArrayEquals(testField, checkField);
		assertFalse(blue.getBricks().contains(turnBrick));
	}

	@Test
	public void testDoTurnNoDiagonalYellow() {
		Point point = new Point(2, 0);
		Brick turnBrick = yellow.getBricks().get(1);
		Turn turn = new Turn(yellow, turnBrick, point);
		turnController.doTurn(turn, testField, false);
		assertArrayEquals(testField, checkField);
		assertTrue(yellow.getBricks().contains(turnBrick));
	}

	@Test
	public void testDoTurnDiagonalYellow() {
		Point point = new Point(2, 1);
		Brick turnBrick = yellow.getBricks().get(1);
		checkField[2][1] = Block.YELLOW;
		checkField[1][1] = Block.YELLOW;
		Turn turn = new Turn(yellow, turnBrick, point);
		turnController.doTurn(turn, testField, false);
		assertArrayEquals(testField, checkField);
		assertFalse(yellow.getBricks().contains(turnBrick));
	}

	@Test
	public void testDoTurnBlockOutOfField() {
		Point point = new Point(0, 0);
		Turn turn = new Turn(yellow, yellow.getBricks().get(1), point);
		assertFalse(turnController.doTurn(turn, testField, false));
	}

	@Test
	public void testRemoveBrickFromFieldDiagonalBlue() {
		Point point = new Point(2, 0);
		Turn turn = new Turn(blue, blue.getBricks().get(1), point);
		turnController.doTurn(turn, testField, false);
		turnController.removeBrickFromField(turn);
		assertArrayEquals(testField, checkField);
	}
	
	@Test
	public void testUndoTurn() throws FirstRoundException {
		Point point = new Point(2, 0);
		turnController.doTurn(new Turn(blue, blue.getBricks().get(1), point), testField, false);
		turnController.doTurn(new Turn(yellow, yellow.getBricks().get(0), new Point(0, 19)), testField, false);
		turnController.doTurn(new Turn(red,	red.getBricks().get(0), new Point(0, 0)), testField, false);
		turnController.doTurn(new Turn(green, green.getBricks().get(0), new Point(19, 0)), testField, false);
		//Alle Turns in der Liste?
		assertTrue(game.getTurns().size()==4);
		turnController.undoTurn();
		//Turns wieder aus der Liste gelöscht?
		assertTrue(game.getTurns().size()==0);
		//Spielzüge von Spielfeld entfernt?
		assertArrayEquals(testField, checkField);
		//Blöcke wieder auf der Hand der Spieler?
		assertTrue(blue.getBricks().contains(blue.getBricks().get(1)));
		assertTrue(blue.getBricks().contains(yellow.getBricks().get(0)));
		assertTrue(blue.getBricks().contains(red.getBricks().get(0)));
		assertTrue(blue.getBricks().contains(green.getBricks().get(0)));
	}
	
	@Test(expected = FirstRoundException.class)
	public void testUndoTurnNotEnoughTurns() throws FirstRoundException {
		Point point = new Point(2, 0);
		turnController.doTurn(new Turn(blue, blue.getBricks().get(1), point), testField, false);
		turnController.doTurn(new Turn(yellow, yellow.getBricks().get(0), new Point(19, 0)), testField, false);
		turnController.doTurn(new Turn(green, green.getBricks().get(0), new Point(0, 19)), testField, false);
		turnController.undoTurn();
		
	}

}
