package control;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import model.Block;
import model.Brick;
import model.Mode;
import model.Player;
import model.PlayerType;
import model.Turn;

import org.junit.Before;
import org.junit.Test;

import edu.udo.cs.sopra.tools.gameturnexchanger.client.GameTurnExchangerClient;
import edu.udo.cs.sopra.tools.gameturnexchanger.client.GteGameNotRunningException;

public class GameControllerTest {
	GameController gameController = new GameController();
	Player[] players = new Player[4];
	List<Turn> turns = new LinkedList<>();
	boolean[][] testBlock = new boolean[5][5];
	Brick testBrick = new Brick(testBlock);
	Point point = new Point(0,0);
	GameTurnExchangerClient exchanger;

	@Before
    public void setUp() throws Exception {
		
		testBlock[2][0] = testBlock[2][1] = testBlock[2][2] = testBlock[1][2] = testBlock[1][3] = true;
		
		// add Players to an array of Players
		players[0] = new Player(false, Block.BLUE, "Lars", PlayerType.PLAYER);
		players[1] = new Player(false, Block.YELLOW, "Johannes", PlayerType.PLAYER);
		players[2] = new Player(true, Block.RED, "Daniel", PlayerType.PLAYER);
		players[3] = new Player(false, Block.GREEN, "Andre", PlayerType.PLAYER);
		
		//start the game with a random game mode
		gameController.startNewGame(players, Mode.STANDARD);
		
		//add the turns to the game
		turns.add(new Turn(players[0], testBrick, point));
		turns.add(new Turn(players[1], testBrick, point));
		turns.add(new Turn(players[2], testBrick, point));
		turns.add(new Turn(players[3], testBrick, point));
		turns.add(new Turn(players[0], testBrick, point));
		turns.add(new Turn(players[1], testBrick, point));
		turns.add(new Turn(players[2], testBrick, point));
		
		gameController.getGame().setTurns(turns);
    }
	
	/**
	 * The current Player must be the player no. 3
	 */
	@Test
	public void testGetCurrentPlayer()
	{
		assertEquals(players[3], gameController.getCurrentPlayer());
	}
	
	@Test
	public void testParseTurnGteGameNotRunningException(){
        boolean isExceptionOccured = false;
        exchanger = null;
			try {
				gameController.parseTurn();
			} catch (GteGameNotRunningException e) {
				isExceptionOccured = true;
			} catch (ParseTurnException e) {
				
			}
			if(isExceptionOccured){
				assertTrue(isExceptionOccured);
			}
	}
	
	@Test
	public void testFormatTurnGteGameNotRunningException(){
        boolean isExceptionOccured = false;
        exchanger = null;
			try {
				gameController.formatTurn(turns.get(0));
			} catch (GteGameNotRunningException e) {
				isExceptionOccured = true;
			}
			if(isExceptionOccured){
				assertTrue(isExceptionOccured);
			}
	}
	
	@Test(expected = NullPointerException.class)
	public void testFormatTurnNullPointerException(){
        boolean isExceptionOccured = false;
        Turn turn = null;
			try {
				gameController.formatTurn(turn);
			} catch (GteGameNotRunningException e) {
			}
			if(isExceptionOccured){
				assertTrue(isExceptionOccured);
			}
	}
}
