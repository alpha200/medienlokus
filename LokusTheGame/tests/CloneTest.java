import model.Block;
import model.Brick;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CloneTest {

	public Block[][] field = new Block[20][20];
	@Test
	public void cloneTest() {
		field[2][4] = Block.RED;
		Block[][] clonedField = field.clone();
		
		for(int i=0; i< clonedField.length; i++) {
			clonedField[i] = Arrays.copyOf(field[i], field[i].length);
		}
		
		clonedField[2][4] = Block.BLUE;

        Assert.assertNotEquals("clone did not work!", clonedField[2][4], field[2][4]);
	}

	@Test
    public void ArrayListTest() {
        List<Brick> bricks = new ArrayList<>();

        bricks.add(new Brick(new boolean[5][5]));

        Assert.assertEquals("Length of List is not 1!", bricks.size(), 1);

        List<Brick> clonedBrickList = new ArrayList<>(bricks);

        clonedBrickList.remove(0);

        Assert.assertEquals("Cloned List is not empty!", clonedBrickList.size(), 0);
        Assert.assertEquals("Original list has changed!", bricks.size(), 1);
    }
}
