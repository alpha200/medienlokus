package model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {
	
	Player[] players = new Player[5];
	Player[] fivePlayers = new Player[5];
	Player[] fourPlayers = new Player[4];
	Player[] threePlayers = new Player[3];
	Player[] twoPlayers = new Player[2];
	Player[] onePlayer = new Player[1];
	Player[] noPlayer = new Player[0];

	@Before
	public void setUp() throws Exception {
		players[0] = new Player(false, Block.BLUE, "Lars", PlayerType.PLAYER);
		players[1] = new Player(false, Block.GREEN, "Johannes", PlayerType.PLAYER);
		players[2] = new Player(true, Block.RED, "Daniel", PlayerType.PLAYER);
		players[3] = new Player(false, Block.YELLOW, "Andre", PlayerType.PLAYER);
		players[4] = new Player(true, Block.GREEN, "Nils", PlayerType.PLAYER);
		System.arraycopy(players, 0, onePlayer, 0, 1);
		System.arraycopy(players, 0, twoPlayers, 0, 2);
		System.arraycopy(players, 0, threePlayers, 0, 3);
		System.arraycopy(players, 0, fourPlayers, 0, 4);
		System.arraycopy(players, 0, fivePlayers, 0, 5);
	}

	// Tests for Game(Player[], Mode) //
	
	@Test
	public void testGame() {
		Game testGame = new Game(fourPlayers, Mode.STANDARD);
		assertStartSetup(testGame);
		for(int i = 0; i < fourPlayers.length; i++) {
			assertEquals(fourPlayers[i], testGame.getPlayers()[i]);
		}
		assertEquals(20, testGame.getField().length);
		assertEquals(20, testGame.getField()[0].length);
		assertEquals(Mode.STANDARD, testGame.getMode());
		
		testGame = new Game(twoPlayers, Mode.BLOKUSDUO);
		assertStartSetup(testGame);
		for(int i = 0; i < twoPlayers.length; i++) {
			assertEquals(twoPlayers[i], testGame.getPlayers()[i]);
		}
		assertEquals(14, testGame.getField().length);
		assertEquals(14, testGame.getField()[0].length);
		assertEquals(Mode.BLOKUSDUO, testGame.getMode());
		
		testGame = new Game(onePlayer, Mode.SOLITAIR);
		assertStartSetup(testGame);
		assertEquals(onePlayer[0], testGame.getPlayers()[0]);
		assertEquals(8, testGame.getField().length);
		assertEquals(10, testGame.getField()[0].length);
		assertEquals(Mode.SOLITAIR, testGame.getMode());
		
		testGame = new Game(fourPlayers, Mode.CUSTOM);
		assertStartSetup(testGame);
		for(int i = 0; i < fourPlayers.length; i++) {
			assertEquals(fourPlayers[i], testGame.getPlayers()[i]);
		}
		assertEquals(Mode.CUSTOM, testGame.getMode());
	}
	
	@Test(expected = NullPointerException.class)
	public void testGameNullPlayers() {
		new Game(null, Mode.STANDARD);
	}
	
	@Test(expected = NullPointerException.class)
	public void testGameNullMode() {
		new Game(fourPlayers, null);
	}
	
	@Test(expected = NullPointerException.class)
	public void testGameNullPlayer() {
		fourPlayers[3] = null;
		new Game(fourPlayers, Mode.STANDARD);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGameThreePlayersInDuoMode() {
		new Game(threePlayers, Mode.BLOKUSDUO);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGameTwoPlayersInSoloMode() {
		new Game(twoPlayers, Mode.SOLITAIR);
	}

	private void assertStartSetup(Game game) {
		// Testmethode fuer jede Initialisierung eines Game
		for (Block[] line : game.getField()) {
			for (Block field : line) {
				assertEquals(Block.EMPTY, field);
			}
		}
		assertEquals(0, game.getTurns().size());
		assertTrue(game.getPlayers().length <= 4);
	}
}
