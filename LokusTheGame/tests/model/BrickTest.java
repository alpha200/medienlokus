package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.awt.Point;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BrickTest {

	boolean[][] emptyBlocks = new boolean[5][5];
	boolean[][] testBlocks = new boolean[5][5];
	boolean[][] testBlocksMirrored = new boolean[5][5];
	boolean[][] testBlocksRotated = new boolean[5][5];
	boolean[][] manyBlocks = new boolean[5][10];
	
	@Before
	public void setUp() throws Exception {
		testBlocks[2][0] = testBlocks[2][1] = testBlocks[2][2] = testBlocks[1][2] = testBlocks[1][3] = true;
		testBlocksMirrored[2][4] = testBlocksMirrored[2][3] = testBlocksMirrored[2][2] = testBlocksMirrored[1][2] = testBlocksMirrored[1][1] = true;
		testBlocksRotated[4][2] = testBlocksRotated[3][2] = testBlocksRotated[2][2] = testBlocksRotated[2][1] = testBlocksRotated[1][1] = true;
	}
	
	// Tests for Brick(boolean[][]) //

	@Test
	public void testBrick() {
		Brick testBrick = new Brick(testBlocks);
		for (int i = 0; i < testBlocks.length; i++) {
			for (int j = 0; j < testBlocks[0].length; j++) {
				assertEquals(testBlocks[i][j], testBrick.getCoord()[i][j]);
			}
		}
	}

	
	@Test(expected = NullPointerException.class)
	public void testBrickNull() {
		boolean[][] nullBlocks = null;
		new Brick(nullBlocks);
	}

	@Test(expected = NullPointerException.class)
	public void testBrickNullElements() {
		boolean[][] nullBlocks = new boolean[5][5];
		nullBlocks[4] = null;
		new Brick(nullBlocks);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testBrickWrongArrayDimensions() {
		new Brick(manyBlocks);
	}
	
	// Tests for setCoord(boolean[][]) //
	
	@Test
	public void testSetCoord() {
		Brick testBrick = new Brick(testBlocks);
		testBrick.setCoord(testBlocksRotated);
		for (int i = 0; i < testBlocksRotated.length; i++) {
			for (int j = 0; j < testBlocksRotated[0].length; j++) {
				assertEquals(testBlocksRotated[i][j], testBrick.getCoord()[i][j]);
			}
		}
		testBrick.setCoord(new boolean[5][5]);
		for (int i = 0; i < testBlocksRotated.length; i++) {
			for (int j = 0; j < testBlocksRotated[0].length; j++) {
				assertFalse(testBrick.getCoord()[i][j]);
			}
		}
	}
	
	@Test(expected = NullPointerException.class)
	public void testSetCoordNull() {
		Brick testBrick = new Brick(testBlocks);
		testBrick.setCoord(null);
	}
	
	@Test(expected = NullPointerException.class)
	public void testSetCoordNullElements() {
		Brick testBrick = new Brick(testBlocks);
		boolean[][] nullBlocks = new boolean[5][5];
		nullBlocks[4] = null;
		testBrick.setCoord(nullBlocks);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetCoordWrongArrayDimensions() {
		Brick testBrick = new Brick(testBlocks);
		testBrick.setCoord(manyBlocks);
	}
	
	// Tests for rotateBrick() //
	
	@Test
	public void testRotate() {
		Brick testBrick = new Brick(testBlocks);
		testBrick.rotateBrick();
		testBrick.setCoord(testBlocksRotated);
		for (int i = 0; i < testBlocksRotated.length; i++) {
			for (int j = 0; j < testBlocksRotated[0].length; j++) {
				assertEquals(testBlocksRotated[i][j], testBrick.getCoord()[i][j]);
			}
		}
		testBrick.rotateBrick();
		testBrick.rotateBrick();
		testBrick.rotateBrick();
		for (int i = 0; i < testBlocks.length; i++) {
			for (int j = 0; j < testBlocks[0].length; j++) {
				assertEquals(testBlocks[i][j], testBrick.getCoord()[i][j]);
			}
		}
	}
	
	// Tests for mirrorBrick() //
	
	@Test
	public void testMirrorBrick() {
		Brick testBrick = new Brick(testBlocks);
		testBrick.mirrorBrick();
		for (int i = 0; i < testBlocksMirrored.length; i++) {
			for (int j = 0; j < testBlocksMirrored[0].length; j++) {
				assertEquals(testBlocksMirrored[i][j], testBrick.getCoord()[i][j]);
			}
		}
		testBrick.mirrorBrick();
		for (int i = 0; i < testBlocks.length; i++) {
			for (int j = 0; j < testBlocks[0].length; j++) {
				assertEquals(testBlocks[i][j], testBrick.getCoord()[i][j]);
			}
		}
	}
	
	// Test for generateBricks() //
	
	@Test
	public void testGenerateBricks() {
		List<Brick> bricks = Brick.generateBricks();
		boolean[][][] expect = new boolean[22][5][5];
		expect[1][2][2] = true;
		expect[2][2][2] = expect[2][1][2] = true;
		expect[3][2][2] = expect[3][1][2] = expect[3][3][2] = true;
		expect[4][2][2] = expect[4][1][2] = expect[4][2][1] = true;
		expect[5][2][2] = expect[5][1][2] = expect[5][0][2] = expect[5][3][2] = true;
		expect[6][2][2] = expect[6][1][2] = expect[6][3][2] = expect[6][3][1] = true;
		expect[7][2][2] = expect[7][1][2] = expect[7][3][2] = expect[7][2][1] = true;
		expect[8][2][2] = expect[8][1][2] = expect[8][2][1] = expect[8][3][1] = true;
		expect[9][2][2] = expect[9][1][2] = expect[9][2][1] = expect[9][1][1] = true;
		expect[10][2][2] = expect[10][1][2] = expect[10][0][2] = expect[10][3][2] = expect[10][4][2] = true;
		expect[11][2][2] = expect[11][1][2] = expect[11][0][2] = expect[11][3][2] = expect[11][3][1] = true;
		expect[12][2][2] = expect[12][1][2] = expect[12][0][2] = expect[12][3][2] = expect[12][2][1] = true;
		expect[13][2][2] = expect[13][1][2] = expect[13][0][2] = expect[13][2][1] = expect[13][3][1] = true;
		expect[14][2][2] = expect[14][1][2] = expect[14][0][2] = expect[14][2][1] = expect[14][2][0] = true;
		expect[15][2][2] = expect[15][1][2] = expect[15][3][1] = expect[15][3][2] = expect[15][3][3] = true;
		expect[16][2][2] = expect[16][1][2] = expect[16][1][1] = expect[16][3][2] = expect[16][3][1] = true;
		expect[17][2][2] = expect[17][1][2] = expect[17][3][2] = expect[17][1][1] = expect[17][2][1] = true;
		expect[18][2][2] = expect[18][1][2] = expect[18][3][2] = expect[18][2][3] = expect[18][3][1] = true;
		expect[19][2][2] = expect[19][1][2] = expect[19][1][3] = expect[19][3][2] = expect[19][3][1] = true;
		expect[20][2][2] = expect[20][1][2] = expect[20][1][3] = expect[20][2][1] = expect[20][3][1] = true;
		expect[21][2][2] = expect[21][1][2] = expect[21][2][1] = expect[21][3][2] = expect[21][2][3] = true;
		for(int id = 1; id <= 21; id++) {
			for(int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					assertEquals(expect[id][i][j], bricks.get(id-1).getCoord()[i][j]);
				}
			}
		}
	}

	
	@Test
	public void testRotateCornerBricks(){
		Brick brick = Brick.getStandardBrick(10);
		//List<Point> cornersPreRotated= brick.getCornerBlocks(); 
		brick.rotateBrick();
		List<Point> cornersPostRotated=brick.getCornerBlocks();
		Assert.assertThat(cornersPostRotated, CoreMatchers.hasItem(new Point(2,0)));
		Assert.assertThat(cornersPostRotated, CoreMatchers.hasItem(new Point(2,4)));
	}
	@Test
	public void testMirrorCornerBricks(){
		Brick brick = Brick.getStandardBrick(11);
		brick.mirrorBrick();
		List<Point> cornersPostMirrored = brick.getCornerBlocks();
		Assert.assertThat(cornersPostMirrored,CoreMatchers.hasItem(new Point(3,3)));
		Assert.assertThat(cornersPostMirrored,CoreMatchers.hasItem(new Point(3,2)));
		Assert.assertThat(cornersPostMirrored,CoreMatchers.hasItem(new Point(0,2)));
		
	}

	@Test
	public void testGenerateCornerBlocks() {
		Brick brick = Brick.getStandardBrick(18);

        Assert.assertThat(brick.getCornerBlocks(), CoreMatchers.hasItem(new Point(3, 1)));
        Assert.assertThat(brick.getCornerBlocks(), CoreMatchers.hasItem(new Point(3, 2)));
        Assert.assertThat(brick.getCornerBlocks(), CoreMatchers.hasItem(new Point(1, 2)));
        Assert.assertThat(brick.getCornerBlocks(), CoreMatchers.hasItem(new Point(2, 3)));

        Assert.assertEquals("Contains not the exact number of cornerblocks", 4, brick.getCornerBlocks().size());
	}

}
