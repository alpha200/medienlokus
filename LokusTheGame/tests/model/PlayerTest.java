package model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.EmptyStringException;

public class PlayerTest {
	
	String name;

	@Before
	public void setUp() throws Exception {
		name = "Stephen";
	}

	// Tests for Player(boolean, Block, String, PlayerType) //
	
	@Test
	public void testPlayer() {
		Player testPlayer = new Player(false, Block.BLUE, name, PlayerType.PLAYER);
		assertEquals(Block.BLUE ,testPlayer.getColor());
		assertEquals("Stephen" ,testPlayer.getName());
		assertEquals(PlayerType.PLAYER ,testPlayer.getPlayerType());
		assertEquals(21, testPlayer.getBricks().size());
	}
	
	@Test(expected = NullPointerException.class)
	public void testPlayerNullFarbe() {
		new Player(false, null, name, PlayerType.PLAYER);
	}

	@Test(expected = NullPointerException.class)
	public void testPlayerNullName() {
		new Player(false, Block.BLUE, null, PlayerType.PLAYER);
	}
	
	@Test(expected = NullPointerException.class)
	public void testPlayerNullType() {
		new Player(false, Block.BLUE, name, null);
	}
	
	@Test(expected = InvalidBlockColorException.class)
	public void testPlayerBlockEmpty() {
		new Player(false, Block.EMPTY, name, PlayerType.PLAYER);
	}
	
	@Test(expected = EmptyStringException.class)
	public void testPlayerEmptyName() {
		new Player(false, Block.BLUE, "", PlayerType.PLAYER);
	}
}
