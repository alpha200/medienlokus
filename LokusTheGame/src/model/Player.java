package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Player implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private Block color;
	private int time;
	private List<Brick> bricks;
	private PlayerType playerType;
	private boolean cheaterFlag;
	private double totalTime = 0;

	/**
	 * Erstellt einen Spieler und sein jeweiliges BlockSet.
	 * @param isNetworkPlayer Gibt an ob es sich um einen
	 * Spieler handelt, der nicht am selben PC sitzt.
	 * @param color Gibt die Farber des Spielers und damit auch die Reihenfolge an.
	 * @param name Gibt des Names des Spielers an.
	 * @param playerType Gibt an ob es sich um einen Menschen oder
	 * eine KI(verschiedene Schwierigkeiten) handelt.
	 * @throws InvalidBlockColorException
	 */
    public Player(boolean isNetworkPlayer, Block color, String name, PlayerType playerType) throws InvalidBlockColorException {
        this(Brick.generateBricks(), isNetworkPlayer, color, name, playerType);
    }
    
    public Player(List<Brick> bricks, boolean isNetworkPlayer, Block color, String name, PlayerType playerType) throws InvalidBlockColorException {
    	if(color == Block.EMPTY)
            throw new InvalidBlockColorException();
    	if (color == null || name == null || playerType == null)
    		throw new NullPointerException();
    	if (name.trim().equals(""))
    		throw new EmptyStringException("A player name must not contain only whitespace!");
        this.color = color;
        this.name = name;
        this.playerType = playerType;

        time = 0;

        this.bricks = bricks;
    }
    
    /**
     * Umwandel-Konstruktor
     * WICHTIG: Es wird nur die Farbe gesetzt! Dies muss zur Identifizierung des Spielers reichen!
     * @param player the player
     */
    public Player(ShrinkedPlayer player) {
        this.color = player.getColor();
        this.bricks = new ArrayList<>();
        for(int i=0; i<player.getBricks().size();i++){
        	bricks.add(Brick.getStandardBrick(player.getBricks().get(i).getStandardBrickNr()));
        }
        
    }

    public String getName() {
        return name;
    }

	public Block getColor() {
        return color;
    }

    public int getTime() {
        return time;
    }

    public List<Brick> getBricks() {
        return bricks;
	}

    public PlayerType getPlayerType() {
        return playerType;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean getCheaterFlag() {
		return cheaterFlag;
	}

	public void setCheaterFlag(boolean cheaterFlag) {
		this.cheaterFlag = cheaterFlag;
	}

	public double getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(double totalTime) {
		this.totalTime = totalTime;
	}

	/**
     * Clone constructor
     * @param player old Player
     */
    public Player(Player player) {
        bricks = new ArrayList<>(player.getBricks());
        time = player.getTime();
        color = player.getColor();
        name = player.getName();
        cheaterFlag = false;
    }

    /**
     * Players are equal if name and color are equal
     * @param obj player to compare
     * @return true if equal
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Player player = (Player) obj;

        return color == player.color;

    }

    @Override
    public int hashCode() {
        return color.hashCode();
    }

}
