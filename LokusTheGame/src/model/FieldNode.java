package model;

import java.util.HashMap;
import java.util.Map;

/**
 * Treenode with current gamestate and possible generated substates if calculated
 * the substates can be reached by doing the turn saved as key in the children-map
 */
public final class FieldNode {
    private GameState data;
    private Map<ShrinkedTurn, FieldNode> children;
    private FieldNode parent;
    private int currentPlayer;
    private double bestAIValue;
    private ShrinkedTurn bestTurn;

    public GameState getData() {
        return data;
    }

    public void setData(GameState data) {
        this.data = data;
    }

    public Map<ShrinkedTurn, FieldNode> getChildren() {
        return children;
    }

    public void setChildren(Map<ShrinkedTurn, FieldNode> children) {
        this.children = children;
    }

    public FieldNode(GameState data, FieldNode parent, int currentPlayer) {
        this.data = data;
        this.currentPlayer = currentPlayer;
        this.children = new HashMap<>();
        this.parent = parent;
    }

    public FieldNode getParent() {
        return parent;
    }

    public void setParent(FieldNode parent) {
        this.parent = parent;
    }

    public int getHeight() {
        if(parent != null) {
            return parent.getHeight() + 1;
        }
        else {
            return 0;
        }
    }
    
    public synchronized void setMaxValue(){}

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public double getBestAIValue() {
        return bestAIValue;
    }

    public void setBestAIValue(double bestAIValue) {
        this.bestAIValue = bestAIValue;
    }

    public ShrinkedTurn getBestTurn() {
        return bestTurn;
    }

    public void setBestTurn(ShrinkedTurn bestTurn) {
        this.bestTurn = bestTurn;
    }
}
