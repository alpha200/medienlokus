package model;

import java.awt.*;
import java.io.Serializable;
import java.util.*;
import java.util.List;

public final class ShrinkedBrick implements Serializable {

    private int value;

    private boolean[][][] forms;
    private Point[][] cornerBlocks;

    private int standardBrickNr;

    public ShrinkedBrick(boolean[][] coord) {
        calculateForms(coord);
        calculateAllCornerBlocks();
        calculateValue();
    }

    public ShrinkedBrick(Brick brick) {
        calculateForms(brick.getCoord());
        calculateAllCornerBlocks();
        calculateValue();
        standardBrickNr = brick.getStandardbricknr();
    }

    /**
     * Clone constructor
     * @param brick the brick
     */
    public ShrinkedBrick(ShrinkedBrick brick) {
        value = brick.value;
        forms = brick.forms;
        cornerBlocks = brick.cornerBlocks;
        standardBrickNr = brick.standardBrickNr;
    }

    /**
     * Berechnet alle Formen des Blocks. Rotiert und Spiegelt .
     * @param coord - Der Block dessen rotierte und gespiegelte Formen berechnet erden sollen.
     */
    private void calculateForms(boolean[][] coord) {
        boolean[][] coordField = coord;

        List<boolean[][]> brickForms = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
        	if(!brickForms.contains(coordField)){
        		brickForms.add(coordField);
        		brickForms.add(mirrorField(coordField));
        		coordField = rotateField(coordField);
        	}
        }

        List<DeepField> shrinkedForms = new ArrayList<>();

        for(boolean[][] brickForm : brickForms) {
            DeepField df = new DeepField(shrinkBrick(brickForm));

        	if(!shrinkedForms.contains(df)){
        		shrinkedForms.add(df);
        	}
        }

        brickForms.clear();

        for (DeepField df : shrinkedForms) {
        	if(!brickForms.contains(df.getField())){
        		brickForms.add(df.getField());
        	}
        }

        forms = brickForms.toArray(new boolean[shrinkedForms.size()][][]);
    }

    /**
     * Rotiert den als field angegebenen Block
     * @param field
     * @return
     */
    private boolean[][] rotateField(boolean[][] field) {
        boolean[][] tempMatrix = new boolean[5][5];

        for(int i=0;i<5;i++)
        {
            for(int j=0;j<5;j++)
            {
                tempMatrix[i][j] = field[j][4-i];
            }
        }
        return tempMatrix;
    }

    /**
     * Spiegelt den als field angegebenen Block
     * @param field
     * @return
     */
    private boolean[][] mirrorField(boolean[][] field) {
        boolean[][] tempMatrix = new boolean[field.length][field[0].length];

        for(int i=0;i<5;i++)
        {
            for(int j=0;j<5;j++)
            {
                tempMatrix[i][j] = field[i][field[i].length-1-j];
            }
        }
        return tempMatrix;
    }

    /**
     * Calculates the value of the stone
     */
    private void calculateValue() {
        for (int i = 0; i < forms[0].length; i++) {
            for (int j = 0; j < forms[0][i].length; j++) {
                if(forms[0][i][j]) {
                    value++;
                }
            }
        }
    }

    /**
     * Berechnet Alle Eckblöcke des Steins
     */
    private void calculateAllCornerBlocks() {
        cornerBlocks = new Point[forms.length][];

        for (int i = 0; i < forms.length; i++) {
            cornerBlocks[i] = calculateCornerBlocks(forms[i]);
        }
    }

    /**
     * Calculates the blocks which can be put at a corner
     *
     * Example (? = cornerblock):
     *
     * -----
     * --?--
     * --#--
     * --#--
     * --?--
     */
    private Point[] calculateCornerBlocks(boolean[][] coord) {
        List<Point> cornerBlocks = new ArrayList<>();

        for (int i = 0; i < coord.length; i++) {
            for (int j = 0; j < coord[i].length; j++) {
                if(coord[i][j]) {
                    if(((i-1 < 0 || !coord[i - 1][j]) && (j-1 < 0 || !coord[i][j-1])) // Top left
                            || ((i-1 < 0 || !coord[i - 1][j]) && (j+1 >= coord[i].length || !coord[i][j+1])) // Bottom left
                            || ((i+1 >= coord.length || !coord[i + 1][j]) && (j-1 < 0 || !coord[i][j-1])) // Top right
                            || ((i+1 >= coord.length || !coord[i + 1][j]) && (j+1 >= coord[i].length || !coord[i][j+1]))) { // Bottom right
                        cornerBlocks.add(new Point(i,j));
                    }
                }
            }
        }

        return cornerBlocks.toArray(new Point[cornerBlocks.size()]);
    }

    /**
     * This method returns the matrix of a brick.
     */
    public boolean[][][] getForms(){
        return forms;
    }

    public Point[][] getCornerBlocks() {
        return cornerBlocks;
    }

    public int getValue() {
        return value;
    }

    /**
     * Shrinks the field of the brick to the needed array size and returns it
     *
     * Example:
     *
     * -----
     * --#--
     * --##-
     * -----
     * -----
     *
     * will be
     *
     * #-
     * ##
     *
     * @return the shrinked field
     *
     * TODO: Optimize
     */
    public boolean[][] shrinkBrick(boolean[][] brick) {
        int current = 0;
        boolean[][] newBrick = new boolean[5][5];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if(!brick[i][j])
                    continue;

                newBrick[current] = Arrays.copyOf(brick[i], 5);
                current++;
                break;
            }
        }

        newBrick = Arrays.copyOf(newBrick, current);

        int begin = -1, end = 0;

        for (int j = 0; j < 5; j++) {
            boolean nothing = true;

            for (int i = 0; i < newBrick.length; i++) {
                if(!newBrick[i][j])
                    continue;

                if(begin == -1) {
                    begin = j;
                }

                nothing = false;
                break;
            }

            if(begin != -1) {
                if(nothing) {
                    break;
                }
                else {
                    end = j;
                }
            }
        }

        boolean[][] reallynewBrick = new boolean[newBrick.length][end-begin+1];

        for (int i = 0; i < reallynewBrick.length; i++) {
            for (int j = begin; j <= end; j++) {
                reallynewBrick[i][j-begin] = newBrick[i][j];
            }
        }

        return reallynewBrick;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShrinkedBrick that = (ShrinkedBrick) o;

        if (value != that.value) return false;
        return Arrays.deepEquals(forms, that.forms);

    }

    @Override
    public int hashCode() {
        int result = value;

        int tresult = 0;

        for (boolean[][] form : forms) {
            tresult += Arrays.deepHashCode(form);
        }

        result = 31 * result + tresult;
        return result;
    }

    public int getStandardBrickNr() {
        return standardBrickNr;
    }

    public void setStandardBrickNr(int standardBrickNr) {
        this.standardBrickNr = standardBrickNr;
    }

    private class DeepField {
        boolean[][] field;

        public boolean[][] getField() {
            return field;
        }

        public void setField(boolean[][] field) {
            this.field = field;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DeepField deepField = (DeepField) o;

            return Arrays.deepEquals(field, deepField.field);

        }

        @Override
        public int hashCode() {
            return Arrays.deepHashCode(field);
        }

        public DeepField(boolean[][] field) {
            this.field = field;
        }
    }

    public ShrinkedBrick(int standardBrickID) {
        ShrinkedBrick sbneu = new ShrinkedBrick(Brick.getStandardBrick(standardBrickID));

        forms = sbneu.forms;
        cornerBlocks = sbneu.cornerBlocks;
        value = sbneu.value;
        standardBrickNr = standardBrickID;
    }
}