package model;

public class EmptyStringException extends RuntimeException {
    public EmptyStringException(String string) {
        super(string);
    }
}
