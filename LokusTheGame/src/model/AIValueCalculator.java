package model;

import model.FieldNode;
import model.ShrinkedTurn;

public interface AIValueCalculator {
    double ratePossibleTurn(ShrinkedTurn turnDone, FieldNode fnAfterTurn);
    double ratePossibleTurn(Turn turn, Block[][] field);
    double rateMidGame(Turn turn, Block[][] field);
}


