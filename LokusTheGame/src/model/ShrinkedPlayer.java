package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class ShrinkedPlayer implements Serializable {

    private final Block color;
    private final List<ShrinkedBrick> bricks;
    private int score;

    /**
     * Konstruktor 
     * @param color
     * @throws InvalidBlockColorException
     */
    public ShrinkedPlayer(Block color) throws InvalidBlockColorException {
        if(color == Block.EMPTY)
            throw new InvalidBlockColorException();
        if (color == null)
            throw new NullPointerException();
        this.color = color;

        List<Brick> garbageBricks = Brick.generateBricks();
        bricks = new ArrayList<>();

        score = 0;

        for (Brick brick : garbageBricks) {
            bricks.add(new ShrinkedBrick(brick));
            score -= brick.getValue();
        }
    }

    public Block getColor() {
        return color;
    }

    public List<ShrinkedBrick> getBricks() {
        return bricks;
    }

    /**
     * Clone constructor
     * @param player old Player
     */
    public ShrinkedPlayer(ShrinkedPlayer player) {
        bricks = new ArrayList<>();

        for(ShrinkedBrick sb : player.getBricks()) {
            bricks.add(new ShrinkedBrick(sb));
        }

        color = player.color;
        score = player.score;
    }

    /**
     * Umwandel-Konstruktor
     * @param player the player
     */
    public ShrinkedPlayer(Player player) {
        this.color = player.getColor();
        this.bricks = new ArrayList<>();

        for(Brick sb : player.getBricks()) {
            bricks.add(new ShrinkedBrick(sb));
        }

        // Für Anwendungsfälle des Konstruktors korrekter Wert unwichtig
        this.score = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShrinkedPlayer that = (ShrinkedPlayer) o;

        return color == that.color;

    }

    @Override
    public int hashCode() {
        return color.hashCode();
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
