package model;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Brick implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private boolean[][] coord;
	private List<Point> cornerBlocks;
	private int value;
	private int standardbricknr;

	public Brick(boolean[][] coord){
		checkCoord(coord);
		this.coord = coord;

        calculateCornerBlocks();
		calculateValue();
	}

	public Brick(boolean[][] coord, int standardbricknr) {
		checkCoord(coord);
		this.coord = coord;

		calculateCornerBlocks();
		calculateValue();

		this.standardbricknr = standardbricknr;
	}

	public int getStandardbricknr() {
		return standardbricknr;
	}

	/**
	 * Checks if the bricks are the same generated Standardbricks (regardless of rotation and mirroring)
	 * @param brick the brick to compare
	 * @return true if stones are the same
	 */
	public boolean isStandardBrick(Brick brick) {
		return brick != null && brick.standardbricknr == this.standardbricknr;
	}

	/**
	 * Calculates the value of the stone
	 */
	private void calculateValue() {
		for (int i = 0; i < coord.length; i++) {
			for (int j = 0; j < coord[i].length; j++) {
				if(coord[i][j]) {
					value++;
				}
			}
		}
	}

	/**
	 * Calculates the blocks which can be put at a corner
	 *
	 * Example (? = cornerblock):
	 *
	 * -----
	 * --?--
	 * --#--
	 * --#--
	 * --?--
	 */
	private void calculateCornerBlocks() {
        cornerBlocks = new ArrayList<>();

        for (int i = 0; i < coord.length; i++) {
            for (int j = 0; j < coord[i].length; j++) {
                if(coord[i][j]) {
                    boolean topLeft = (i-1 < 0 || !coord[i - 1][j]) && (j-1 < 0 || !coord[i][j-1]);
					boolean bottomLeft = (i-1 < 0 || !coord[i - 1][j]) && (j+1 >= coord.length || !coord[i][j+1]);
					boolean topRight = (i+1 >= coord.length || !coord[i + 1][j]) && (j-1 < 0 || !coord[i][j-1]);
					boolean bottomRight = (i+1 >= coord.length || !coord[i + 1][j]) && (j+1 >= coord[i].length || !coord[i][j+1]);
					if(topLeft || bottomLeft || topRight || bottomRight){
                        cornerBlocks.add(new Point(i,j));
                    }
                }
            }
        }
    }

	/**
	 * Clone constructor
	 * @param brick old brick
	 */
	public Brick(Brick brick) {
		coord = new boolean[brick.coord.length][brick.coord[0].length];
		for (int i = 0; i < coord.length; i++) {
			coord[i] = Arrays.copyOf(brick.coord[i], brick.coord[i].length);
		}
		
		cornerBlocks = new ArrayList<>();

        for (Point oldPoint : brick.cornerBlocks) {
            cornerBlocks.add(new Point(oldPoint.x, oldPoint.y));
        }

		value = brick.value;
        standardbricknr = brick.standardbricknr;
	}
	
	/**
	 * This method checks the array whether it has a lenght of 5 and is not null.
	 */
	private void checkCoord(boolean[][] toCheck){
		if (toCheck == null)
			throw new NullPointerException();
		for (boolean[] line : toCheck)
			if (line == null)
				throw new NullPointerException();
		if (toCheck.length != 5 || toCheck[0].length != 5)
			throw new IllegalArgumentException("Bricks have to be placed in a 5x5-Array!");
	}
	
	/**
	 * This method returns the matrix of a brick.
	 */
	public boolean[][] getCoord(){
		return coord;
	}
	
	/**
	 * This method sets a new brick
	 * @throws IllegalArgumentException
	 */
	public void setCoord(boolean[][] newCoord){
		checkCoord(newCoord);
		coord = newCoord;
	}
	
	
	/**
	 * This method rotates a brick clockwise by 90 degrees.
     * @return the brick itself (for chaining)
	 */
	public Brick rotateBrick() {
		boolean[][] tempMatrix = new boolean[5][5];
        List<Point> newCornerBlocks = new ArrayList<>();

		for(int i=0;i<5;i++)
		{
			for(int j=0;j<5;j++)
			{
				tempMatrix[i][j] = coord[j][4-i];

				// TODO: This could be done better
				if(cornerBlocks.contains(new Point(j, 4-i)))
                    newCornerBlocks.add(new Point(i,j));
			}
		}
		coord = tempMatrix;
        cornerBlocks = newCornerBlocks;
		
        return this;
	}
	
	/**
	 * This method mirrors the brick horizontally
     * @return the brick itself (for chaining)
	 */
	public Brick mirrorBrick() {
		boolean[][] tempMatrix = new boolean[5][5];
	       
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<5;j++)
			{
				tempMatrix[i][j] = coord[i][4-j];
			}
		}
		coord = tempMatrix;
		for(Point point : cornerBlocks){
			point.setLocation(point.x, 4-point.y);
		}
        return this;
	}
	
	/**
	 * Generates all 21 Bricks a player needs at the beginning of each game.
	 * 
	 * @return 21 Standard Bricks, contained in a List.
	 */
	public static List<Brick> generateBricks() {
		List<Brick> bricks = new ArrayList<>();
		for (int i = 1; i <= 21; i++){
			bricks.add(getStandardBrick(i));
		}
		return bricks;
	}

	/**
	 * Generates all 5 Bricks a player needs at the beginning of a SolitairMini game.
	 *
	 * @return 5 Standard Bricks, contained in a List.
	 */
	public static List<Brick> generateMiniSolitairBricks() {
		List<Brick> bricks = new ArrayList<>();
		for (int i = 5; i <= 9; i++){
			bricks.add(getStandardBrick(i));
		}
		return bricks;
	}


	/**
	 * Contains all standard bricks used for Blokus, hardcoded.
	 * They can be obtained by passing their ID to the method.
	 * 
	 * @param blockID - Number of the standard Brick (between 1 and 21)
	 * @return Standard Brick with number id. If id < 1 or id > 21, an empty block is returned.
	 */
	public static Brick getStandardBrick(int blockID){
		boolean[][] blocks = new boolean[5][5];
		blocks[2][0] = (blockID == 14);
		blocks[1][1] = (blockID == 9 || blockID == 16 || blockID == 17);
		blocks[2][1] = (blockID == 4 || blockID == 7 || blockID == 8 || blockID == 9 || blockID == 12 || blockID == 13 || blockID == 14 || blockID == 17 || blockID == 20 || blockID == 21);
		blocks[3][1] = (blockID == 6 || blockID == 8 || blockID == 11 || blockID == 13 || blockID == 15 || blockID == 16 || blockID == 18 || blockID == 19 || blockID == 20);
		blocks[0][2] = (blockID == 5 || (blockID >= 10 && blockID<= 14));
		blocks[1][2] = (blockID != 1);
		blocks[2][2] = true;
		blocks[3][2] = (blockID == 3 || blockID == 5 || blockID == 6 || blockID == 7 || blockID == 10 || blockID == 11 || blockID == 12 || blockID == 21 || (blockID >= 15 && blockID<= 19));
		blocks[4][2] = (blockID == 10);
		blocks[1][3] = (blockID == 19 || blockID == 20);
		blocks[2][3] = (blockID == 18 || blockID == 21);
		blocks[3][3] = (blockID == 15);
		blocks[2][4] = false;
		return new Brick(blocks, blockID);
		
		/* Created Bricks:
		 * 
		 * 1)  ----- 2)  ----- 3)  -----  4)  -----  5)  -----  6)  -----  7)  -----
		 *     -----     -----     -----      --#--      -----      ---#-      --#--
		 *     --#--     -##--     -###-      -##--      ####-      -###-      -###-
		 *     -----     -----     -----      -----      -----      -----      -----
		 *     -----     -----     -----      -----      -----      -----      -----
		 *     
		 * 8)  ----- 9)  ----- 10) -----  11) -----  12) -----  13) -----  14) --#--
		 *     --##-     -##--     -----      ---#-      --#--      --##-      --#--
		 *     -##--     -##--     #####      ####-      ####-      ###--      ###--
		 *     -----     -----     -----      -----      -----      -----      -----
		 *     -----     -----     -----      -----      -----      -----      -----
		 *     
		 * 15) ----- 16) ----- 17) -----  18) -----  19) -----  20) -----  21) -----
		 *     ---#-     -#-#-     -##--      ---#-      ---#-      --##-      --#--
		 *     -###-     -###-     -###-      -###-      -###-      -##--      -###-
		 *     ---#-     -----     -----      --#--      -#---      -#---      --#--
		 *     -----     -----     -----      -----      -----      -----      -----
		 *     
		 * All other values will result into an empty brick.
		 */
	}

    public List<Point> getCornerBlocks() {
        return cornerBlocks;
    }

	/**
	 * Checks if the bricks are equal (rotated and mirrored bricks are not detected (on purpose!))
	 * @param obj the brick to be compared
	 * @return
	 */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Brick brick = (Brick) obj;

        return Arrays.deepEquals(shrinkBrick(), brick.shrinkBrick());
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(shrinkBrick());
    }

	public int getValue() {
		return value;
	}

	/**
	 * Shrinks the field of the brick to the needed array size and returns it
	 *
	 * Example:
	 *
	 * -----
	 * --#--
	 * --##-
	 * -----
	 * -----
	 *
	 * will be
	 *
	 * #-
	 * ##
	 *
	 * @return the shrinked field
	 *
	 * TODO: Optimize
	 */
	public boolean[][] shrinkBrick() {
        int current = 0;
        boolean[][] newBrick = new boolean[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if(!coord[i][j])
                    continue;
                newBrick[current] = Arrays.copyOf(coord[i], 5);
                current++;
                break;
            }
        }
        newBrick = Arrays.copyOf(newBrick, current);
        int begin = -1, end = 0;
        for (int j = 0; j < 5; j++) {
            boolean nothing = true;
            for (int i = 0; i < newBrick.length; i++) {
                if(!newBrick[i][j])
                    continue;
                if(begin == -1)
                    begin = j;
                nothing = false;
                break;
            }
            if(begin != -1) {
                if(nothing)
                	break;
                else
                    end = j;
            }
        }
        boolean[][] reallynewBrick = new boolean[newBrick.length][end-begin+1];

        for (int i = 0; i < reallynewBrick.length; i++) {
            for (int j = begin; j <= end; j++) {
                reallynewBrick[i][j-begin] = newBrick[i][j];
            }
        }

        return reallynewBrick;
    }
}