package model;

import java.awt.*;
import java.io.Serializable;


public class Turn implements Serializable, Comparable<Turn> { // NOPMD by sopr043 on 07.09.15 10:25. name Turn is informative enough
	private static final long serialVersionUID = 1L;

	private Player player;
	private Brick brick;
	private Point coord;
	private double aiValue;


    

	public Turn(Player player, Brick brick, Point coord) {

        this.player = player;
        this.brick = brick;
        this.coord = coord;
        if(brick!=null){
        	this.aiValue = brick.getValue();
        }
    }
	
  
    public Player getPlayer() {
        return player;
    }

    public Brick getBrick() {
        return brick;
    }

    public Point getCoord() {
        return coord;
    }

    public double getAiValue() {
		return aiValue;
	}

	public void setAiValue(double aiValue) {
		this.aiValue = aiValue;
	}
    public Turn(ShrinkedTurn shrinkedTurn) {
        this.coord = new Point(shrinkedTurn.getCoord().x + 2, shrinkedTurn.getCoord().y +2);
        this.player = new Player(shrinkedTurn.getPlayer());

        boolean[][] transformedForm = shrinkedTurn.getBrick().getForms()[shrinkedTurn.getBrickFormNumber()];

        boolean[][] brickForm = new boolean[5][5];

        for (int i = 0; i < transformedForm.length; i++) {
            System.arraycopy(transformedForm[i], 0, brickForm[i], 0, transformedForm[i].length);
        }

        this.brick = new Brick(brickForm, shrinkedTurn.getBrick().getStandardBrickNr());
    }
    @Override
    public int compareTo(Turn toCompare){
    	return Double.compare(this.getAiValue(), toCompare.getAiValue());
    }
    public Turn (Turn turn){
    	this.aiValue = turn.getAiValue();
    	this.brick = turn.getBrick();
    	this.coord = turn.getCoord();
    	this.player = turn.getPlayer();
    }
}
