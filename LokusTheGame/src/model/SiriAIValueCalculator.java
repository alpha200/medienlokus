package model;

import java.awt.Point;
import java.util.List;

public class SiriAIValueCalculator implements AIValueCalculator{
	/**
	 * Dummy-Methode
	 */
	public double ratePossibleTurn(ShrinkedTurn turnDone, FieldNode fnAfterTurn){
        int value = turnDone.getBrick().getValue();
        int cTurns = 0;
        return value + cTurns;
    }
	
	/**
	 * Bewertet möglichen Zug
	 * @param turn - zu bewertender Zug
	 * @param field - Feld VOR dem Zug
	 */
	public double ratePossibleTurn(Turn turn, Block[][] field){
		double zoneValue = getBestZone(turn, field);
		return zoneValue;
	}
	
	public double rateMidGame(Turn turn, Block[][] field){
    	int cTurns = getBestZone(turn, field);

		double cornerValue=0;	
		double value = turn.getBrick().getValue();
		cornerValue = getNewTurnCount(turn, field);

		return 2*cTurns+cornerValue + value;
	}
    private int getBestZone(Turn turn, Block[][] field){
        int x;
        int y;
        Brick turnBrick = turn.getBrick();

        boolean[][] actualBrickCoords = turnBrick.getCoord();
        Point brickStartCoord = turn.getCoord();
        x= brickStartCoord.x-2;
        y= brickStartCoord.y-2;
        for(int i=x;i< x+actualBrickCoords.length;i++){
            for(int j=y; j< y+actualBrickCoords[0].length; j++){
            	if(turnBrick.getCoord()[i-x][j-y]){
	                if(i>= field.length/2-2 && i<=field.length/2+1 && j >= field[i].length/2 -2 && j<= field[i].length/2+1){//8<=i,j<=11
	                    return 10;
	                }
	                if(i>= field.length/2-4 && i<=field.length/2+3 && j >= field[i].length/2 -4 && j<= field[i].length/2+3){//6<=i,j <=13
	                    return 7;
	                }
	                if(i>= field.length/2-6 && i<=field.length/2+5 && j >= field[i].length/2 -6 && j<= field[i].length/2+5){//4<=i,j<=15
	                    return 4;
	                }
	                if(i>=field.length/2-8 && i<=field.length/2+7 && j>=field[i].length/2 -8 && j<=field[i].length/2+7){//2<=i,j<=17
	                	return 1;
	                }
            	}
            }
        }
        return 0;
    }
    
    
    private int getNewTurnCount(Turn turn, Block[][] field){
        int count = 0;
        List<Point> corners = turn.getBrick().getCornerBlocks();
        for(Point corner : corners){
            if(prooveCorner(corner, field)>0){
                count +=prooveCorner(corner, field);
            }
        }
        return count;
    }

    private int prooveCorner(Point actualPoint, Block[][] field){
        int count = 0;
        int x = actualPoint.x;
        int y = actualPoint.y;
        boolean top, bottom, left, right;
        top = x>0;
        bottom = x<field.length-1;
        left = y>0;
        right = y<field[x].length-1;

        if(top && left){
            if(field[x-1][y-1] == Block.EMPTY){
                count ++;
            }
        }
        if(top && right){
            if(field[x-1][y+1]== Block.EMPTY){
                count++;
            }
        }
        if(bottom && left){
            if(field[x+1][y-1]== Block.EMPTY){
                count++;
            }
        }
        if(bottom && right){
            if(field[x+1][y+1]== Block.EMPTY){
                count++;
            }
        }
        return count;
    }
}
