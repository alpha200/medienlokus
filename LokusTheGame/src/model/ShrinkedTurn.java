package model;

import java.awt.*;
import java.io.Serializable;


public final class ShrinkedTurn implements Serializable {

    private final ShrinkedPlayer player;
    private final ShrinkedBrick brick;
    private Point coord;
    private int brickFormNumber;
    private double aiValue;

    public double getAiValue() {
		return aiValue;
	}

	public void setAiValue(double aiValue) {
		this.aiValue = aiValue;
	}

	/**
	 * Konstruktor
	 * @param player - Spieler der den Zug ausführt
	 * @param brick - Stein der gelegt wird
	 * @param coord - Position an die der Stein gelegt werden soll
	 * @param brickFormNumber
	 */
	public ShrinkedTurn(ShrinkedPlayer player, ShrinkedBrick brick, Point coord, int brickFormNumber) {
        this.player = player;
        this.brick = brick;
        this.coord = coord;
        this.brickFormNumber = brickFormNumber;
        this.aiValue = brick.getValue();
    }

    /**
     * Turn in Shrinkturn Umwandel-Konstructor
     * @param turn the turn
     */

    public ShrinkedTurn(Turn turn) {
        this.player = new ShrinkedPlayer(turn.getPlayer());

        if(turn.getBrick() == null || turn.getCoord() == null) {
            this.brick = null;
            this.coord = null;
            this.brickFormNumber = -1;
        }
        else {
            this.brick = new ShrinkedBrick(turn.getBrick().getStandardbricknr());

            boolean[][] brickCoord = turn.getBrick().getCoord();

            boolean isCorrect = false;

            for (int i = 0; i < this.brick.getForms().length; i++) {
                boolean[][] brickForm = brick.getForms()[i];

                int curx, cury;

                for (int j = 0; j < 5 - brickForm.length + 1; j++) {
                    for (int k = 0; k < 5 - brickForm[0].length + 1; k++) {
                        curx = j;
                        cury = k;
                        isCorrect = true;

                        for (int l = 0; l < brickForm.length; l++) {
                            for (int m = 0; m < brickForm[l].length; m++) {
                                if(brickCoord[j+l][k+m] != brickForm[l][m]) {
                                    isCorrect = false;
                                    break;
                                }
                            }

                            if(!isCorrect) {
                                break;
                            }
                        }

                        if(isCorrect) {
                            this.coord = new Point(turn.getCoord().x + curx - 2, turn.getCoord().y + cury - 2);
                            this.brickFormNumber = i;
                            break;
                        }
                    }

                    if(isCorrect) {
                        break;
                    }
                }

                if(isCorrect) {
                    break;
                }
            }

            if(!isCorrect) {
                throw new BrickIsNotEqualWithStandardBrickNrException();
            }
        }
    }

    public ShrinkedPlayer getPlayer() {
        return player;
    }

    public ShrinkedBrick getBrick() {
        return brick;
    }

    public Point getCoord() {
        return coord;
    }

    public int getBrickFormNumber() {
        return brickFormNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShrinkedTurn that = (ShrinkedTurn) o;

        if (brickFormNumber != that.brickFormNumber) return false;
        if (!player.equals(that.player)) return false;
        if (!brick.equals(that.brick)) return false;
        return coord.equals(that.coord);

    }

    @Override
    public int hashCode() {
        int result = player.hashCode();
        result = 31 * result + brick.hashCode();
        result = 31 * result + coord.hashCode();
        result = 31 * result + brickFormNumber;
        return result;
    }
}
