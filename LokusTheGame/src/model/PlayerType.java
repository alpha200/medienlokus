package model;

public enum PlayerType {

	PLAYER, SVOICE, CORTANA, SIRI, GOOGLENOW, NETWORK_PLAYER, NONE;
	
	@Override
	public String toString() {
		switch (this) {
		case PLAYER: return "Player";
		case SVOICE: return "KI: S-Voice";
		case CORTANA: return "KI: Cortana";
		case SIRI: return "KI: Siri";
		case GOOGLENOW: return "KI: Google Now";
		case NETWORK_PLAYER: return "Network Player";
		case NONE: return "- None -";
		default: return "";
		}
	}

}
