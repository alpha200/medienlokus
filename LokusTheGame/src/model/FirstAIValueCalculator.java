package model;

import java.awt.Point;

public class FirstAIValueCalculator implements AIValueCalculator {
    /**
     * Rates the current Turn. Included values are the turns of enemy players that get contered, new generated own turns,
     * field zone for the new brick and the bricks own value.
     * @return rateValue of the current turn
     */

	/**
	 * @param turnDone
	 * @param fnAfterTurn
	 * @return Wertung des gemachten Zuges
	 */
    public double ratePossibleTurn(ShrinkedTurn turnDone, FieldNode fnAfterTurn){
        int value = turnDone.getBrick().getValue();
        Turn turn = new Turn(turnDone);
        int cTurns = getBestZone(turn, fnAfterTurn.getParent().getData().getField());
        int newTurns = getNewTurnCount(turnDone, fnAfterTurn.getParent().getData().getField());
        return value + 2*cTurns + newTurns;
    }
    
    /**
     * @param turn - Der zu bewertende Zug
     * @param field - Das aktuelle Feld
     * @return Wert des Zuges
     */
    public double ratePossibleTurn(Turn turn, Block[][] field){
    	int value = turn.getBrick().getValue();
    	int cTurns = getBestZone(turn, field);
    	return value+ cTurns;
    }
    
    /**
     * Dummy-Methode
     */
    public double rateMidGame(Turn turn, Block[][] field){
    	return 0.0;
    }
    /**
     * Counts all enemy turns that get blocked by the actual own turn
     * @param turn the turn
     * @param state the state
     * @return count of enemy turns contered
     */


  
    /**
     * Counts new possible turns
     * @param turn
     * @param field
     * @return count of new generated turns
     */
    private int getNewTurnCount(ShrinkedTurn turn, Block[][] field){
        int count = 0;
        Point[][] corners = turn.getBrick().getCornerBlocks();
        for(Point corner : corners[turn.getBrickFormNumber()]){
            if(prooveCorner(corner, field)>0){
                count +=prooveCorner(corner, field);
            }
        }
        return count;
    }

    /**
     * Prüft die Ecken der Eckblöcke auf mögliche Anlegeoptionen
     * @param actualPoint
     * @param field
     * @return
     */
    private int prooveCorner(Point actualPoint, Block[][] field){
        int count = 0;
        int x = actualPoint.x;
        int y = actualPoint.y;
        boolean top, bottom, left, right;
        top = x>0;
        bottom = x<field.length-1;
        left = y>0;
        right = y<field[x].length-1;

        if(top && left){
            if(field[x-1][y-1] == Block.EMPTY){
                count ++;
            }
        }
        if(top && right){
            if(field[x-1][y+1]== Block.EMPTY){
                count++;
            }
        }
        if(bottom && left){
            if(field[x+1][y-1]== Block.EMPTY){
                count++;
            }
        }
        if(bottom && right){
            if(field[x+1][y+1]== Block.EMPTY){
                count++;
            }
        }
        return count;
    }

    /**
     * Bewertet die Position des zu legenden Steins. Je weiter der Stein in der mitte liegt, desto besser
     * @param turn
     * @param field
     * @return
     */
    private int getBestZone(Turn turn, Block[][] field){
        int x;
        int y;
        Brick turnBrick = turn.getBrick();
        boolean[][] actualBrickCoords = turnBrick.getCoord();
        Point brickStartCoord = turn.getCoord();
        x= brickStartCoord.x;
        y= brickStartCoord.y;
        for(int i=x;i< x+actualBrickCoords.length;i++){
            for(int j=y; j< y+actualBrickCoords[0].length; j++){
                if(i>= field.length/2-2 && i<=field.length/2+2 && j >= field[i].length/2 -2 && j<= field[i].length/2+2){
                    return 3;
                }
                if(i>= field.length/2-4 && i<=field.length/2+4 && j >= field[i].length/2 -4 && j<= field[i].length/2+4){
                    return 2;
                }
                if(i>= field.length/2-6 && i<=field.length/2+6 && j >= field[i].length/2 -6 && j<= field[i].length/2+6){
                    return 1;
                }
            }
        }
        return 0;
    }
}
