package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class CalculationHelpers {
    /**
     * Calculates all corners blocks, where you could lay a brick
     * @param field the current field
     * @param blockColor the players color
     * @return list of all corners, where you could lay bricks
     */
    public static List<Point> calculateCorners(Block[][] field, Block blockColor) {
        List<Point> corners = new ArrayList<>();

        if(field[0][0] != blockColor
                && field[0][field[0].length-1] != blockColor
                && field[field.length-1][0] != blockColor
                && field[field.length-1][field[field.length-1].length-1] != blockColor) {

            if(field[field.length-1][field[field.length-1].length-1] == Block.EMPTY && blockColor == Block.BLUE)
                corners.add(new Point(field.length -1, field[field.length-1].length-1));
            else if (field[0][field[0].length-1] == Block.EMPTY && blockColor == Block.YELLOW) {
                corners.add(new Point(0, field[0].length-1));
            }
            else if (field[0][0] == Block.EMPTY && blockColor == Block.RED) {
                corners.add(new Point(0, 0));
            }
            else {
                corners.add(new Point(field.length -1, 0));
            }
        }
        else {
            for (int i = 0; i < field.length; i++) {
                for (int j = 0; j < field[i].length; j++) {
                    if (field[i][j] == Block.EMPTY) {
                        // Check if there is no collision with own color at top, bottom, left, right
                        if (    (i - 1 < 0 || field[i - 1][j] != blockColor)
                                && (i + 1 >= field.length || field[i + 1][j] != blockColor)
                                && (j - 1 < 0 || field[i][j - 1] != blockColor)
                                && (j + 1 >= field[i].length || field[i][j + 1] != blockColor)) {
                            // Check if there is at least one own color diagonal
                            if ((i - 1 >= 0 && j - 1 >= 0 && field[i - 1][j - 1] == blockColor)
                                    || (i - 1 >= 0 && j + 1 < field[i - 1].length && field[i - 1][j + 1] == blockColor)
                                    || (i + 1 < field.length && j - 1 >= 0 && field[i + 1][j - 1] == blockColor)
                                    || (i + 1 < field.length && j + 1 < field[i + 1].length && field[i + 1][j + 1] == blockColor)) {
                                corners.add(new Point(i, j));
                            }
                        }
                    }
                }
            }
        }

        return corners;
    }
    

    /**
     * Calculates wich turns are possible in players turn
     * @param field the current field
     * @param player the current player
     * @return possible turns
     */
    public static ShrinkedTurn[] calculatePossibleTurns(Block[][] field, ShrinkedPlayer player) {
        Block color = player.getColor();
        List<ShrinkedTurn> possibleTurns= new ArrayList<>();
        List<Point> corners = calculateCorners(field, color);

        for (ShrinkedBrick currentBrick : player.getBricks()) {
            for (int i=0; i < currentBrick.getForms().length; i++) {
                for (Point currentCorner : corners) {
                    for (Point currentCornerBlock : currentBrick.getCornerBlocks()[i]) {
                        Point currentPoint = new Point(currentCorner.x - currentCornerBlock.x, currentCorner.y - currentCornerBlock.y);
                        ShrinkedTurn turn = new ShrinkedTurn(new ShrinkedPlayer(player), currentBrick, currentPoint, i);
                        if(simpleCheckTurn(field, turn)) {
                            possibleTurns.add(turn);
                        }
                    }
                }
            }
        }

        return possibleTurns.toArray(new ShrinkedTurn[possibleTurns.size()]);
    }

    /**
     * Checks if the given turn is a valid turn
     * @param field the field
     * @param turn the turn
     * @return true if the turn is valid
     */
    private static synchronized boolean simpleCheckTurn(Block[][] field, ShrinkedTurn turn) {
        boolean[][] brickCoord = turn.getBrick().getForms()[turn.getBrickFormNumber()];
        Block playerColor = turn.getPlayer().getColor();

        for (int i = 0; i < brickCoord.length; i++) {
            for (int j = 0; j < brickCoord[i].length; j++) {
                // Position nur relevant, falls dort ein Block ist
                if(brickCoord[i][j]) {
                    int calculatedXCoord = turn.getCoord().x + i;
                    int calculatedYCoord = turn.getCoord().y + j;

                    // Falls Block des aktuellen Stein außerhalb des Spielfelds liegen wuerde false returnen
                    if(calculatedXCoord < 0 || calculatedYCoord < 0 || calculatedXCoord >= field.length || calculatedYCoord >= field[calculatedXCoord].length) {
                        return false;
                    }

                    // Falls auf dem Feld an der Stelle schon ein Block vorhanden ist abbrechen
                    if(field[calculatedXCoord][calculatedYCoord] != Block.EMPTY) {
                        return false;
                    }

                    // Überprüfen, ob nicht bereits ein eigener Stein direkt angrenzt
                    if((calculatedXCoord - 1 >= 0 && field[calculatedXCoord-1][calculatedYCoord] == playerColor)
                            || (calculatedXCoord + 1 < field.length && field[calculatedXCoord+1][calculatedYCoord] == playerColor)
                            || (calculatedYCoord -1 >= 0 && field[calculatedXCoord][calculatedYCoord-1] == playerColor)
                            || (calculatedYCoord +1 < field[calculatedXCoord].length && field[calculatedXCoord][calculatedYCoord+1] == playerColor)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Helper to visualize a brick
     * @param brick the brick
     */
    public static synchronized void printBrick(Brick brick) {
        for (int i = 0; i < brick.getCoord().length; i++) {
            for (int j = 0; j < brick.getCoord()[i].length; j++) {
                if(brick.getCoord()[j][i]) {
                    System.out.print("#");
                }
                else {
                    System.out.print("-");
                }
            }

            System.out.println();
        }

        System.out.println();
    }

    /**
     * Helper to visualize a brick
     * @param field the field of the brick
     */
    public static synchronized void printBrick(boolean[][] field) {
        int vlength = field[0].length;

        for (int i = 0; i < vlength; i++) {
            for (int j = 0; j < field.length; j++) {
                if(field[j][i]) {
                    System.out.print("#");
                }
                else {
                    System.out.print("-");
                }
            }

            System.out.println();
        }

        System.out.println();
    }

    /**
     * Helper to visualize a playfield
     * @param field the playfield
     */
    public static synchronized void printField(Block[][] field) {
        System.out.println("\n");

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if(field[j][i] == null) {
                    System.out.print("-");
                    continue;
                }

                switch (field[j][i]) {

                    case BLUE:
                        System.out.print('B');
                        break;
                    case YELLOW:
                        System.out.print('Y');
                        break;
                    case RED:
                        System.out.print('R');
                        break;
                    case GREEN:
                        System.out.print('G');
                        break;
                    case EMPTY:
                        System.out.print('-');
                        break;
                }
            }

            System.out.println();
        }

        System.out.println("==============================================");
    }

    public static synchronized void printFieldAndPlayerNumber(Block[][] field, int playernumber) {
        System.out.println("Player: " + playernumber);
        printField(field);
    }
}

