package model;

import java.awt.Point;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Class that holds players and current Field and is cloneable
 * Needed to transfer different gamestates via network
 */
public final class GameState implements Serializable {
    private Block[][] field;
    private ShrinkedPlayer[] players;

    public GameState(Block[][] field, ShrinkedPlayer[] players) {
        this.field = field;
        this.players = players;
    }

    public GameState cloneOnlyPlayerAndField(ShrinkedPlayer player) {
        Block[][] clonedField = cloneField(field);

        ShrinkedPlayer[] clonedPlayers = new ShrinkedPlayer[players.length];

        for(int i=0; i<players.length; i++) {
            if(players[i].equals(player)) {
                clonedPlayers[i] = new ShrinkedPlayer(players[i]);
            }
            else {
                clonedPlayers[i] = players[i];
            }
        }

        return new GameState(clonedField, clonedPlayers);
    }

    public Block[][] getField() {
        return field;
    }

    public ShrinkedPlayer[] getPlayers() {
        return players;
    }

    private Block[][] cloneField(Block[][] field) {
        Block[][] clonedField = new Block[field.length][field[0].length];

        for (int i = 0; i < clonedField.length; i++) {
            System.arraycopy(field[i], 0, clonedField[i], 0, field[i].length);
        }

        return clonedField;
    }

    private ShrinkedPlayer[] clonePlayers(ShrinkedPlayer[] players) {
        ShrinkedPlayer[] clonedPlayers = new ShrinkedPlayer[players.length];

        for (int i = 0; i < players.length; i++) {
            clonedPlayers[i] = new ShrinkedPlayer(players[i]);
        }

        return clonedPlayers;
    }

    /**
     * Clone constructor
     * @param gameState old gameState
     */
    public GameState(GameState gameState) {
        this.field = cloneField(gameState.getField());
        this.players = clonePlayers(gameState.getPlayers());
    }

    /**
     * Applies a Turn on the gamestate
     * @param turn the turn
     */
    public void doTurn(ShrinkedTurn turn) {
        Point coord = turn.getCoord();

        // Abbrechen, falls gepasst wurde
        if(coord == null) {
            return;
        }

        for(ShrinkedPlayer p : players) {
            if(p.equals(turn.getPlayer())) {
                p.getBricks().remove(turn.getBrick());
                p.setScore(p.getScore() + turn.getBrick().getValue());

                if(p.getScore() == 0) {
                    p.setScore(p.getScore() + 20);
                    if(turn.getBrick().getValue() == 1) {
                        p.setScore(p.getScore() + 5);
                    }
                }
                break;
            }
        }

        boolean[][] sbfield = turn.getBrick().getForms()[turn.getBrickFormNumber()];

        for (int i = 0; i < sbfield.length; i++) {
            for (int j = 0; j < sbfield[i].length; j++) {
                if (sbfield[i][j]) {
                    int currx = coord.x + i;
                    int curry = coord.y + j;
                    field[currx][curry] = turn.getPlayer().getColor();
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameState gameState = (GameState) o;

        if (!Arrays.deepEquals(field, gameState.field)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(players, gameState.players);

    }

    @Override
    public int hashCode() {
        int result = Arrays.deepHashCode(field);
        result = 31 * result + Arrays.hashCode(players);
        return result;
    }
}
