package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Game implements Serializable { // NOPMD by sopr043 on 07.09.15
											// 10:12. name Game is informative
											// enough

	private static final long serialVersionUID = 1L;

	private Player[] player;

	private List<Turn> turns;

	private Block field[][];

	private Mode mode;

	/**
	 * Initialisiert ein Spiel mit passender Spielfeldgröße zum GameMode und
	 * entsprechender Spieleranzahl.
	 * 
	 * @param players
	 *            Array von übergebenen Spielern
	 * @param gameMode
	 *            GameMode der erstellt werden soll.
	 */
	public Game(Player[] players, Mode gameMode) {
		if (players == null || gameMode == null)
			throw new NullPointerException();
		if (players.length == 0)
			throw new IllegalArgumentException(
					"Blokus needs at least one Player!");
		Block[][] field = createField(players, gameMode);

		for (Player current : players) {
			if (current == null)
				throw new NullPointerException();
		}
		setPlayers(players);
		this.mode = gameMode;

		// Initialize every field with EMPTY
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				field[i][j] = Block.EMPTY;
			}
		}

		turns = new ArrayList<>();
	}


	public Game(Game game){
		this.field = game.getField();
		this.mode = game.getMode();
		this.player = game.getPlayers();
		this.turns = game.getTurns();
	}

	public Block[][] createField(Player[] players, Mode gameMode) {
		switch (gameMode) {
		case BLOKUSDUO:
			field = new Block[14][14];
			if (players.length != 2)
				throw new IllegalArgumentException(
						"Blokus Duo can only be played by 2 Players!");
			break;
		case STANDARD:
			field = new Block[20][20];
			if (players.length > 4)
				throw new IllegalArgumentException(
						"Blokus Standard can only be played by 1-4 Players!");
			break;
		case SOLITAIR:
			field = new Block[8][10];
			if (players.length != 4)
				throw new IllegalArgumentException(
						"Blokus Solitair can only be played by 1 Player, but need 4 internally!");
			break;
		case SOLITAIRBIG:
			field = new Block[20][20];
			if (players.length != 4)
				throw new IllegalArgumentException(
						"Blokus Solitair can only be played by 1 Player!");
			break;
		case CUSTOM:
			field = new Block[20][20];
			break;
		default:
			field = new Block[20][20];
			break;
		}
		return field;
	}

	public Block[][] getField() {
		return field;
	}

	public Mode getMode() {
		return mode;
	}

	public Player[] getPlayers() {
		return player;
	}

	public void setPlayers(Player[] players) {
		player = players;
	}

	public void setTurns(List<Turn> turns) {
		this.turns = turns;
	}

	public List<Turn> getTurns() {
		return turns;

	}
}
