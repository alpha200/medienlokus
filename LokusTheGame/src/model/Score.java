package model;

import java.io.Serializable;

public class Score implements Serializable, Comparable<Score> {

	private int points;
	private String name;
	private String date;
	private double timeUsed;

	public Score(int points, String name, String date, double timeUsed) {
		this.points = points;
		this.name = name;
		this.date = date;
		this.timeUsed = timeUsed;
	}

	public double getTimeUsed() {
		return timeUsed;
	}

	public int getPoints() {
		return points;
	}

	public String getName() {
		return name;
	}

	public String getDate() {
		return date;
	}

	@Override
	public int compareTo(Score comp) {
		//0 bei gleich
		//-1 bei comp größer
		//1 bei comp kleiner
		if(this.points == comp.getPoints())
		{
			if(this.timeUsed > comp.timeUsed)
				return -1;
			else if(this.timeUsed < comp.timeUsed)
				return 1;
			else 
				return 0;
		}
		else if(this.points < comp.getPoints())
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
}
