package view;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import model.Mode;
import model.Score;

/**
 * ScreenHighscore shows a list of saved highscores and returns back to main screen
 * @author sopr049
 */

public class ScreenHighscore extends DefaultScreen{

	Sprite background, logo;
	SpriteBatch bgBatch;
	Skin buttonSkin;
	Label labelHighscore;
	Table table;
	Table root;
	ScrollPane scroller;
	Button backButton, forwardButton, deleteButton;
	Label labelStandard;
	LabelStyle elementStyle = new LabelStyle();
	LabelStyle style = new LabelStyle();
	
	private Mode[] gameModes = { Mode.STANDARD, Mode.BLOKUSDUO, Mode.SOLITAIR,
			Mode.SOLITAIRBIG, Mode.CUSTOM };
	public int choosedMode = 0;	
	
	public ScreenHighscore(LokusTheGame game,int mode) {
		super(game);
		
		choosedMode = mode;
	}
	
	public void resize(int width, int height) {
		viewport.update(width, height);
	}

	@Override
	public void show() {
		//choosedMode = lokusTheGame.gameController.getGame().getMode().ordinal();
		bgBatch = new SpriteBatch();
		bgBatch.getProjectionMatrix().setToOrtho2D(0, 0, Resources.getInstance().background.getWidth(), Resources.getInstance().background.getHeight());
		camera = new OrthographicCamera();
		viewport = new FitViewport(1920, 1200, camera);
		camera.translate(viewport.getWorldWidth() / 2, viewport.getWorldHeight() / 2, 0);
		camera.update();
		stage = new Stage(viewport);
		table = new Table();
		
		background = Resources.getInstance().background;
		logo = Resources.getInstance().logo;
		logo.setColor(1,1,1,1);
		logo.setX((Gdx.graphics.getWidth() / 2) - logo.getWidth() / 2);
		logo.setY(250);
		
		buttonSkin = new Skin();
		buttonSkin.addRegions(Resources.getInstance().atlas);
		
		elementStyle.background = buttonSkin.getDrawable("menuButtonBackground");
		elementStyle.font = Resources.getInstance().fontBigColor;
		TextButtonStyle styleBtn = new TextButtonStyle();
		styleBtn.up = buttonSkin.getDrawable("menuButtonBackground");
		styleBtn.font = Resources.getInstance().fontBigColor;
		
        labelHighscore = new Label("HIGHSCORE", elementStyle);
        labelHighscore.setBounds(viewport.getWorldWidth()/2 - (Resources.getInstance().backgroundButton.getWidth()/ 2)-70,  viewport.getWorldHeight() - 120, Resources.getInstance().backgroundButton.getWidth(), 30);
        
        
        TextButton btn = new TextButton("< back", styleBtn);
        btn.setPosition( 50, 150);
        
        btn.addListener(new InputListener() {
        	public boolean touchDown(InputEvent event, float posX, float posY, int pointer, int button) {
        		Gdx.app.log("ScreenHighscore", "Highscore closed.");
        		lokusTheGame.setScreen(lokusTheGame.mainMenu);
        		return true;
        	}        	
        });
       
        stage.addActor(labelHighscore);
        
        //create table
        elementStyle.font = Resources.getInstance().fontNormalColor;
        Table tableHead = new Table();
        tableHead.add(new Label("No.", elementStyle)).colspan(4).align(Align.left).width(120);
        tableHead.add(new Label("Name", elementStyle)).colspan(12).align(Align.left).width(450);
        tableHead.add(new Label("Date", elementStyle)).colspan(8).align(Align.left).width(160);
        tableHead.add(new Label("Score", elementStyle)).colspan(8).align(Align.left).width(150);
        tableHead.add(new Label("Time", elementStyle)).colspan(8).align(Align.left).width(200);
        tableHead.setBounds(420, viewport.getWorldHeight() - 290, 1080, 80);
        stage.addActor(tableHead);
        
        //Create the list dynamically
        updateTable();
        stage.setScrollFocus(scroller);
        
        stage.addActor(btn);
        
        // Buttons and labels for choosing game type
        style.background = buttonSkin.getDrawable("menuButtonBackground");
		style.font = Resources.getInstance().fontNormalColor;
        labelStandard = new Label(gameModes[choosedMode].toString(), style);

 		labelStandard.setWidth(225f);
 		Float choosePosX = btn.getX()+50;
 		Float choosePosY = btn.getY()+viewport.getWorldHeight()/2;
 		labelStandard.setPosition(choosePosX,choosePosY);
 		labelStandard.setAlignment(Align.center, Align.center);

 		ButtonStyle backStyle = new ButtonStyle();
 		backStyle.up = buttonSkin.getDrawable("menuArrowLeft");
 		backButton = new Button(backStyle);
 		backButton.setPosition(
 				labelStandard.getX()-backButton.getWidth(),labelStandard.getY()+20);
 		backButton.addListener(new InputListener() {
 			@Override
 			public boolean touchDown(InputEvent event, float x, float y,
 					int pointer, int button) {
 				choosedMode--;
 				if (choosedMode < 0)
 					choosedMode = gameModes.length - 1;
 				labelStandard.setText(gameModes[choosedMode].toString());
 				updateTable();
 				stage.setScrollFocus(scroller);
 				return true;
 			}
 		});

 		ButtonStyle forwardStyle = new ButtonStyle();
 		forwardStyle.up = buttonSkin.getDrawable("menuArrowRight");
 		forwardButton = new Button(forwardStyle);
 		forwardButton.setPosition(labelStandard.getX()+labelStandard.getWidth(),labelStandard.getY()+20);
 		forwardButton.addListener(new InputListener() {
 			@Override
 			public boolean touchDown(InputEvent event, float x, float y,
 					int pointer, int button) {
 				choosedMode++;
 				if (choosedMode >= gameModes.length)
 					choosedMode = 0;
 				labelStandard.setText(gameModes[choosedMode].toString());
 				updateTable();
 				stage.setScrollFocus(scroller);
 				return true;
 			}
 		});
 		
 		TextButton btnDelete = new TextButton("Delete", styleBtn);
 		btnDelete.setPosition(viewport.getWorldWidth() - 50f - btnDelete.getWidth(), 150);
        
 		btnDelete.addListener(new InputListener() {
        	public boolean touchDown(InputEvent event, float posX, float posY, int pointer, int button) {
        		switch(gameModes[choosedMode]){
        		case STANDARD : lokusTheGame.gameController.getHighscoreController().setStandardScores(new LinkedList<Score>());
        		break;
        		case BLOKUSDUO :lokusTheGame.gameController.getHighscoreController().setBlokusDuoScores(new LinkedList<Score>());
        		break;
        		case CUSTOM : lokusTheGame.gameController.getHighscoreController().setCustomScores(new LinkedList<Score>());
        		break;
        		case SOLITAIR : lokusTheGame.gameController.getHighscoreController().setSolitairScores(new LinkedList<Score>());
        		break;
        		case SOLITAIRBIG : lokusTheGame.gameController.getHighscoreController().setSolitairBigScores(new LinkedList<Score>());
        		break;
        		}
        		updateTable();
 				stage.setScrollFocus(scroller);
        		ScreenUIPopup popup = new ScreenUIPopup(
						"Are you sure?", 
						"Do you really want to delete\n the highscores of "+gameModes[choosedMode].toString()+"?",
						ScreenHighscore.this);
				popup.setListener(new HighscoreDeleteListener(lokusTheGame.mainMenu, popup, ScreenHighscore.this));
				lokusTheGame.setScreen(popup);
		   		updateTable();
    			stage.setScrollFocus(scroller);
        		return true;
        	}        	
        });
        
 		stage.addActor(btnDelete);
 		
 		stage.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if (keycode == Keys.LEFT) {
					choosedMode--;
	 				if (choosedMode < 0)
	 					choosedMode = gameModes.length - 1;
	 				labelStandard.setText(gameModes[choosedMode].toString());
	 				updateTable();
	 				stage.setScrollFocus(scroller);
				}
				if (keycode == Keys.RIGHT) {
					choosedMode++;
	 				if (choosedMode >= gameModes.length)
	 					choosedMode = 0;
	 				labelStandard.setText(gameModes[choosedMode].toString());
	 				updateTable();
	 				stage.setScrollFocus(scroller);		
								}
				return true;
			}
		});
 		
 		stage.addActor(labelStandard);
 		stage.addActor(backButton);
 		stage.addActor(forwardButton);
 		
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			Gdx.app.log("ScreenHighscore", "Highscore screen closed.");
			lokusTheGame.setScreen(lokusTheGame.mainMenu);
		}
		
		bgBatch.begin();
		background.draw(bgBatch);
		bgBatch.end();
		
		stage.act();
		stage.draw();
	}
	public void updateTable(){
		table = new Table();
		List<Score> highscore = lokusTheGame.gameController.getHighscoreController().getHighscore(gameModes[choosedMode]);
		Collections.sort(highscore);
		Collections.reverse(highscore);
		int time;
		int seconds,minutes;
		String timeUsed;
		switch(highscore.size()){
        case 0:
        	elementStyle.font = Resources.getInstance().fontNormalRed;
            table.add(new Label("", elementStyle)).colspan(4).align(Align.left).width(120);
            table.add(new Label("No Highscore available", elementStyle)).colspan(12).align(Align.left).width(450);
            table.add(new Label("", elementStyle)).colspan(8).align(Align.left).width(160);
            table.add(new Label("", elementStyle)).colspan(8).align(Align.left).width(150);
            table.add(new Label("", elementStyle)).colspan(8).align(Align.left).width(200);
            table.row();
            break;
        case 1:
        	elementStyle.font = Resources.getInstance().fontNormalRed;
        	table.add(new Label("1.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(0).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(0).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(0).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time = (int) highscore.get(0).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	break;
        case 2:
        	elementStyle.font = Resources.getInstance().fontNormalRed;
        	table.add(new Label("1.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(0).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(0).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(0).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time = (int)highscore.get(0).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	elementStyle.font = Resources.getInstance().fontNormalBlue;
        	table.add(new Label("2.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(1).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(1).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(1).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time = (int)highscore.get(1).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	break;
        case 3:
        	elementStyle.font = Resources.getInstance().fontNormalRed;
        	table.add(new Label("1.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(0).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(0).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(0).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time =(int) highscore.get(0).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	elementStyle.font = Resources.getInstance().fontNormalBlue;
        	table.add(new Label("2.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(1).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(1).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(1).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time =(int) highscore.get(1).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	elementStyle.font = Resources.getInstance().fontNormalGreen;
        	table.add(new Label("3.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(2).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(2).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(2).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time =(int) highscore.get(2).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	break;
        default:
        	elementStyle.font = Resources.getInstance().fontNormalRed;
        	table.add(new Label("1.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(0).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(0).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(0).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time = (int)highscore.get(0).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	elementStyle.font = Resources.getInstance().fontNormalBlue;
        	table.add(new Label("2.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(1).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(1).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(1).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time = (int)highscore.get(1).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	elementStyle.font = Resources.getInstance().fontNormalGreen;
        	table.add(new Label("3.", elementStyle)).colspan(4).align(Align.left).width(120);
        	table.add(new Label(highscore.get(2).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        	table.add(new Label(highscore.get(2).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        	table.add(new Label(String.valueOf(highscore.get(2).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        	time = (int)highscore.get(2).getTimeUsed();
        	minutes = time/60;
        	seconds = time - (minutes*60);
        	if(seconds < 10)
        		timeUsed = minutes +":0"+ seconds;
        	else
        		timeUsed = minutes +":"+ seconds;
        	table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        	table.row();
        	for(int i = 3;i<highscore.size();i++)
        	{
        		elementStyle.font = Resources.getInstance().fontNormalYellow;
        		table.add(new Label(String.valueOf(i+1)+".", elementStyle)).colspan(4).align(Align.left).width(120);
        		table.add(new Label(highscore.get(i).getName(), elementStyle)).colspan(12).align(Align.left).width(450);
        		table.add(new Label(highscore.get(i).getDate(), elementStyle)).colspan(8).align(Align.left).width(160);
        		table.add(new Label(String.valueOf(highscore.get(i).getPoints()), elementStyle)).colspan(8).align(Align.left).width(150);
        		time = (int)highscore.get(i).getTimeUsed();
        		minutes = time/60;
        		seconds = time - (minutes*60);
        		if(seconds < 10)
        			timeUsed = minutes +":0"+ seconds;
        		else
        			timeUsed = minutes +":"+ seconds;
        		table.add(new Label(timeUsed, elementStyle)).colspan(8).align(Align.left).width(200);
        		table.row();
        	}
        	break;
        }
		elementStyle.font = Resources.getInstance().fontNormalYellow;
        table.top();
        scroller = new ScrollPane(table);
        for(int i = 0; i< stage.getActors().size; i++)
        {
        	if(stage.getActors().get(i).equals(root))
        		stage.getActors().removeIndex(i);
        }
        root = new Table();
        root.setBounds(420, 110, 1080, viewport.getWorldHeight() - 400);
        root.add(scroller).fill().expand();
        stage.addActor(root);
	}
	
	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		try{
			lokusTheGame.gameController.getHighscoreController().saveHighScore();
		}
		catch(Exception ex){
			
		}
	}

}
