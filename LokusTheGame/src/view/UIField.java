package view;

import java.awt.Point;
import java.util.List;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import model.Block;
import model.Brick;
import model.Game;
import model.Mode;
import model.Player;
import model.PlayerType;
import model.Turn;
 
public class UIField extends Group {
	ScreenGame screenGame;
	UIFieldBlock[][] fieldBlocks;
		
	UIField(ScreenGame screenGame, Block[][] field, float xPos, float yPos, float width, float height) {
		this.screenGame = screenGame;
		
		fieldBlocks = new UIFieldBlock[field.length][field[0].length];
		for(int x = 0; x < field.length; x++) {
			for(int y = 0; y < field[0].length; y++) {
				fieldBlocks[x][y] = new UIFieldBlock(field[x][y], x * Resources.getInstance().blockTextures.get(field[x][y]).getWidth(), (field[0].length - 1 - y) * Resources.getInstance().blockTextures.get(field[x][y]).getHeight(), x, y);
				this.addActor(fieldBlocks[x][y]);
			}
		}
		
		float tmpWidth = width / field.length;
		float tmpHeight = height / field[0].length;
		
		this.setSize(Math.min(tmpWidth, tmpHeight) * field.length, Math.min(tmpWidth, tmpHeight) * field[0].length);
		this.setScale(Math.min(this.getWidth() / (fieldBlocks.length * Resources.getInstance().blockTextures.get(Block.EMPTY).getWidth()), this.getHeight() / (fieldBlocks[0].length * Resources.getInstance().blockTextures.get(Block.EMPTY).getHeight())));
	
		this.setPosition(xPos + ((width - this.getWidth()) / 2), yPos + ((height - this.getHeight()) / 2));
	}
	
	public void updateField() {
		Block[][] field = screenGame.lokusTheGame.gameController.getGame().getField();
		for(int x = 0; x < field.length; x++) {
			for(int y = 0; y < field[0].length; y++) {
				fieldBlocks[x][y].texture = Resources.getInstance().blockTextures.get(field[x][y]);
				fieldBlocks[x][y].color = field[x][y];
				fieldBlocks[x][y].fowLevel = 4;
			}
		}
		if(screenGame.lokusTheGame.gameController.getGame().getMode() == Mode.CUSTOM) {
			calcFOW(field);
		}
	}
	
	private void calcFOW(Block[][] field) {
		Player shownPlayer = screenGame.lokusTheGame.gameController.getCurrentPlayer();
		Player reprPlayer = shownPlayer;
		if(shownPlayer.getPlayerType() == PlayerType.NONE) {
			int repPlayerIndex = (screenGame.getGame().getTurns().size() / 4) % (screenGame.getGame().getPlayers().length - 1);
			if (screenGame.lokusTheGame.gameController.getCurrentPlayer().getColor().equals(Block.YELLOW) && repPlayerIndex > 0)
				repPlayerIndex++;	// The none player has to be omitted, so another player can do the turn
			reprPlayer = screenGame.getGame().getPlayers()[repPlayerIndex];
		}
		if(reprPlayer.getPlayerType() != PlayerType.PLAYER) return;
		for(int x = 0; x < field.length; x++) {
			for(int y = 0; y < field[0].length; y++) {
				if(screenGame.isGameOver()) {
					fieldBlocks[x][y].fowLevel = 0;
				} else if(field[x][y] == shownPlayer.getColor()) {
					light(x, y);
				}
			}
		}
	}
	
	private void light(int x, int y) {
		for(int i = -4; i < 5; i++) {
			for(int j = -4; j < 5; j++) {
				int posX = x + i;
				int posY = y + j;
				if(posX >= 0 && posX < fieldBlocks.length && posY >= 0 && posY < fieldBlocks[0].length) {
					int distance = Math.abs(i) + Math.abs(j) - 1; //distance from center
					fieldBlocks[posX][posY].fowLevel = Math.max(Math.min(fieldBlocks[posX][posY].fowLevel, distance), 0);
				}
			}
		}
	}
	
	@Override
	public void draw(Batch batch, float alpha) {
		updateField();
		super.draw(batch, alpha);
	}
	
	public class UIFieldBlock extends Actor {
		Texture texture;
		Block color;
		int fowLevel = 0;
		public UIFieldBlock(Block block, float x, float y, final int indexX, final int indexY)
		{			
			super();
			this.color = block;
			this.texture = Resources.getInstance().blockTextures.get(block);
			
			setBounds(x, y, texture.getWidth(), texture.getHeight());
			this.addListener(new InputListener() {
				public boolean touchDown(InputEvent event, float x, float y, int pointer, int buttons) {
					if(buttons == Input.Buttons.LEFT) {
						if(screenGame.hasBrickGrabbed()) {
													
							boolean turnSuccess = screenGame.lokusTheGame.gameController.getTurnController().doTurn(
									new Turn(screenGame.lokusTheGame.gameController.getCurrentPlayer(),
											screenGame.getGrabbedBrick(),
											new Point(indexX, indexY)),
									screenGame.lokusTheGame.gameController.getGame().getField()
									, true);
							
							if(turnSuccess) {
								screenGame.removeHintBrick();
								switch(screenGame.lokusTheGame.gameController.getGame().getMode()){
								case CUSTOM:
									Resources.getInstance().sounds.get("shoot").play(screenGame.lokusTheGame.soundVolume);
									break;
								default:
									Resources.getInstance().sounds.get("place_block").play(screenGame.lokusTheGame.soundVolume);
									break;
								}
								screenGame.ungrabBrick();
								screenGame.setLabelIncorrectTurnText("");
								
								Game game = screenGame.lokusTheGame.gameController.getGame();
								Brick turnBrick = game.getTurns().get(game.getTurns().size() - 1).getBrick();
								
								screenGame.removeBrickFromHand(turnBrick, (game.getTurns().size() - 1) % game.getPlayers().length);

								screenGame.checkIfGameIsOver();
							}
							else {
								screenGame.setLabelIncorrectTurnText("This turn is incorrect!");
								switch(screenGame.lokusTheGame.gameController.getGame().getMode()){
								case CUSTOM:
									Resources.getInstance().sounds.get("mainMenu_selectedExplosion").play(screenGame.lokusTheGame.soundVolume);
									break;
								default:
									Resources.getInstance().sounds.get("incorrect_turn").play(screenGame.lokusTheGame.soundVolume);
									break;
								}
							}
							System.out.println("Zug erfolgreich? " + turnSuccess);
						
							for(UIBrickGroup group: screenGame.getBrickGroups()) {
								group.setTouchable(Touchable.disabled);
							}							
						updateField();
							
						return true;
						}
					}
					return false;
				}
			});	
		}
		
		public void draw(Batch batch, float alpha) {
			if(!screenGame.isGameOver() && screenGame.lokusTheGame.gameController.getGame().getMode() == Mode.CUSTOM) {
				batch.draw(texture, getX(), getY(), texture.getWidth() * this.getScaleX(), texture.getHeight() * this.getScaleY());
				if(fowLevel > 0 && fowLevel < 5) {
					Texture tex = Resources.getInstance().stringTextures.get("fow_" + fowLevel);
					batch.draw(tex, getX(), getY(), tex.getWidth() * this.getScaleX(), tex.getHeight() * this.getScaleY());
				}
			} else {
				batch.draw(texture, getX(), getY(), texture.getWidth() * this.getScaleX(), texture.getHeight() * this.getScaleY());
			}
		}
	}
}
