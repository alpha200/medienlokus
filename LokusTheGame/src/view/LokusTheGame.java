package view;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALCdevice;

import model.Turn;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import control.GameController;
import control.NetworkUtils;
 
public class LokusTheGame extends Game {

	public GameController gameController = new GameController(this);
	public ScreenMainMenu mainMenu;
	public ScreenHighscore highscoreScreen;
	
	public final int DISPLAY_WIDTH = 1920, DISPLAY_HEIGHT = 1200;
	public ScreenGameMenu gameMenuScreen;
	public ScreenGame screenGame;
	
	public String[] playerNames = {"Empty", "Empty", "Empty", "Empty"};
	public boolean playerNamesUpdated = true;
	public int reconnectAttempts = 990;
	public boolean networkComponentEnabled = true;
	

	private Sound backgroundMusic;
	private boolean bgMusicOn = false;
	public boolean isMuted = false;
	public float bgVolume = 0.1f;
	public float soundVolume = 0.1f;
	

	
	public void setBgVolume(float bgVolume) {
		this.bgVolume = bgVolume*0.05f;
	}

	public void setSoundVolume(float soundVolume) {
		this.soundVolume = soundVolume*0.05f;
	}

	@Override
	public void create() {
		mainMenu = new ScreenMainMenu(this);
		highscoreScreen = new ScreenHighscore(this,0);
		gameMenuScreen = new ScreenGameMenu(this);
		backgroundMusic = Gdx.audio.newSound(Gdx.files.internal("res/sfx/ChibiNinjaMP3.mp3"));
		try{
			gameController.getHighscoreController().loadHighscore();
		}
		catch(Exception ex){
			
		}	
		ScreenStart startScreen = new ScreenStart(this);
		setScreen(startScreen);
	}
	
	public void pauseMusic(){
		backgroundMusic.pause();
		bgMusicOn = false;
	}
	
	public void startMusic(){
		if(!bgMusicOn && !isMuted){
			backgroundMusic.loop(bgVolume);
			soundVolume = bgVolume;
			bgMusicOn  = true;
		}
	}
	public void unMuteMusic(){
		backgroundMusic.loop(bgVolume);
		isMuted = false;
		bgMusicOn = true;
	}
	public void muteMusic(){
		backgroundMusic.pause();
		isMuted = true;
		bgMusicOn = false;
	}
	
	/**
	 * Dispose of all Screen except mainMenu because it will dispose itself
	 */
	@Override
	public void dispose() {
		gameController.exchanger.sendGameOver();
		
		gameController.dispose();
		backgroundMusic.dispose();
		highscoreScreen.dispose();
		gameMenuScreen.dispose();
		if(screenGame!=null){
			screenGame.dispose();
		}
		AL.destroy();
		super.dispose();
	}
	
	@Override
	public void render () {
		super.render();
	}
	
	public void switchNetworkEnabled(boolean pValue) {
		if(!pValue) {
			gameController.exchanger.close();
		}
		else {
			gameController.exchanger = new NetworkUtils();
			gameController.exchanger.open();
		}
	}

}