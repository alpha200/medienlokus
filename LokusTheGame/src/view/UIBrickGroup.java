package view;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Group;

public class UIBrickGroup extends Group {
	private ScreenGame screenGame;
	
	private List<UIBrick> bricks = new ArrayList<>();
	
	UIBrickGroup(ScreenGame screenGame, List<UIBrick> bricks, int xPos, int yPos, int width, int height) {
		this.screenGame = screenGame;
		this.setBounds(xPos, yPos, width, height);
		this.setScale(1);
		this.addBricks(bricks);
	}
	
	public List<UIBrick> getBricks() {
		return bricks;
	}
	
	public void removeBrick(UIBrick brick) {
		this.removeActor(brick);
		bricks.remove(brick);
		adjust();
	}
	
	public void addBrick(UIBrick brick) {
		bricks.add(brick);
		this.addActor(brick);
		brick.group = this;
		adjust();
	}
	
	public void addBricks(List<UIBrick> brickList) {
		for(UIBrick b : brickList) {
			bricks.add(b);
			b.group = this;
			this.addActor(b);
		}
		adjust();
	}
	
	public void adjust() {
		
		if(bricks.size() == 0) return;
		
		boolean fits = false;
		
		int bricksPerRow = 0, bricksPerColumn = 0;
		
		float currentScaling = 1;
		float space = 10;
		
		float brickWidth = bricks.get(0).getWidth() * currentScaling;
		float brickHeight = bricks.get(0).getHeight() * currentScaling;
		
		float scaleChange = 0.9f;
		
		while(!fits) {
			
			if(brickWidth > this.getWidth()) {
				currentScaling *= scaleChange;
				brickWidth = bricks.get(0).getWidth() * currentScaling;
				brickHeight = bricks.get(0).getHeight() * currentScaling;
				continue;
			}
			
			bricksPerRow = (int) (this.getWidth() / (brickWidth + space));
			bricksPerColumn = (int) Math.ceil((float) bricks.size() / (float) bricksPerRow);
			
			if((bricksPerColumn * brickHeight) + ((bricksPerColumn - 1) * space) <= this.getHeight()) {
				fits = true;
			} else {
				currentScaling *= scaleChange;
				brickWidth = bricks.get(0).getWidth() * currentScaling;
				brickHeight = bricks.get(0).getHeight() * currentScaling;
			}
		}
		
		int x = 0, y = 0;
		for(int i = 0; i < bricks.size(); i++) {
			bricks.get(i).setScale(currentScaling);
			float xPos = x * ((brickWidth) + space);
			float yPos = y * ((brickHeight) + space);
			bricks.get(i).setPosition(xPos, yPos);
			if(x == bricksPerRow - 1) {
				//ende dieser zeile erreicht
				y++;
				x = 0;
			} else {
				x++;
			}
		}
	}
}