package view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
 
public class UIButton extends TextButton {
	
	UIButton(String text, int xPos, int yPos, int width, int height) {
		super(text, createStyle());
		this.setStyle(createStyle());
		this.setBounds(xPos, yPos, width, height);		
	}
	
	private static TextButtonStyle createStyle() {
		Skin skin = new Skin();
		skin.addRegions(new TextureAtlas(Gdx.files.internal("res/buttons/btnMenu.pack")));
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable("btnIdle");
		style.down = skin.getDrawable("btnClick");
		style.over = skin.getDrawable("btnHover");
		style.font = new BitmapFont(Gdx.files.internal("res/buttons/font.fnt"));
		return style;
	}
}
