package view;

import model.Mode;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class HintListener extends InputListener{
	private ScreenGame screen ;
	private ScreenUIPopup popup;
	public HintListener(ScreenGame screen, ScreenUIPopup popup){
		super();
		this.popup = popup;
		this.screen = screen;		
	}
	public boolean touchDown(InputEvent event, float posX, float posY, int pointer, int button){
		LokusTheGame lokusTheGame = screen.lokusTheGame;
		if (lokusTheGame.gameController.getGame().getMode() == Mode.SOLITAIR
				|| lokusTheGame.gameController.getGame().getMode() == Mode.SOLITAIRBIG) {
			screen.lokusTheGame.gameController.getGame().getPlayers()[0].setCheaterFlag(true);
			screen.lokusTheGame.gameController.getGame().getPlayers()[1].setCheaterFlag(true);
			screen.lokusTheGame.gameController.getGame().getPlayers()[2].setCheaterFlag(true);
			screen.lokusTheGame.gameController.getGame().getPlayers()[3].setCheaterFlag(true);
		}
		else{
			screen.lokusTheGame.gameController.getCurrentPlayer().setCheaterFlag(true);
		}
		Gdx.app.log("ScreenUIPopup", "Notification closed.");
		
		screen.addHintBrick(screen.calculateHint());
		
		popup.dispose();
        screen.lokusTheGame.setScreen(screen);
        return true;
	}
}
