package view;

import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;
import control.CortanaKIController;
import control.GameController;
import control.GoogleNowKIController;
import control.KIController;
import control.KIException;
import control.ParseTurnException;
import control.SVoiceKIController;
import control.SiriKiController;
import edu.udo.cs.sopra.tools.gameturnexchanger.client.GteGameNotRunningException;
import model.Block;
import model.Brick;
import model.CalculationHelpers;
import model.Game;
import model.Mode;
import model.Player;
import model.PlayerType;
import model.ShrinkedPlayer;
import model.ShrinkedTurn;
import model.Turn;

public class ScreenGame extends DefaultScreen {
	
	private Stage fieldStage;

	private Sprite background;
	private Sprite fieldBG;
	private Skin buttonSkin;
	private TextButton btnUndo, btnHint;
	private TextButtonStyle styleBtnUndo, styleBtnHint;

	private AIThread kiThread;
	private NetworkThread networkThread;
	
	private Turn kiTurn;
	private Turn networkTurn;
	
	private InputMultiplexer multiplexer;

	private UIField field;
	private Game game;
	private ArrayList<UIBrickGroup> brickgroups = new ArrayList<>();
	private Label labelTurnPlayer;
	private Label labelIncorrectTurn;
	private Label labelTime;
	private UIBrick grabbedBrick = null;
	private boolean canUngrabBrick = true;
	
	UIBrick hintBrick;
	
	Label labelPlayerGreen;
	Label labelPlayerRed;
	Label labelPlayerBlue;
	Label labelPlayerYellow;
	
	Player oldPlayer;
	
	TweenManager manager;

	private ScreenGameInputHandler inputHandler;
	
	private boolean gameOver = false;
	private boolean displayGOScreen = false;

	private Sound backgroundMusic;
	
	protected boolean isMuted = false;
	private boolean bgMusicOn = false;

	private double startTime;

	private double turnTime;

	private float timeAnim = 0f;

	private float speedAnim = 1f;

	public ScreenGame(LokusTheGame lokusTheGame, Game game) {
		super(lokusTheGame);
		this.game = game;
		this.lokusTheGame.gameController.setGame(game);
		inputHandler = new ScreenGameInputHandler();

		multiplexer = new InputMultiplexer();

		field = new UIField(this, game.getField(), 560, 200, 800f, 800f);

		final boolean redEnabled = !game.getMode().equals(Mode.SOLITAIR)
				&& !game.getMode().equals(Mode.SOLITAIRBIG);
		final boolean greenYellowEnabled = !game.getMode().equals(
				Mode.BLOKUSDUO) && redEnabled;

		fieldStage = new Stage(viewport);
		
		createBrickGroups();
		guiSetup(redEnabled, greenYellowEnabled);
		buttonSetup();
		kiControllerSetup();
		
		stage.addListener(inputHandler);

		multiplexer.addProcessor(fieldStage);
		multiplexer.addProcessor(stage);

		lokusTheGame.screenGame = this;
		
		
		//TODO: Cleanup
		
		Tween.registerAccessor(UIBrick.class, new UIBrickTween());
		Tween.registerAccessor(Label.class, new LabelTween());
		manager = new TweenManager(); 
		
		
	}
	
	private void kiControllerSetup() {
		Game game = lokusTheGame.gameController.getGame();
		
		// KI-Controller nur fuer Zugriff auf calculatePossibleTurns
		lokusTheGame.gameController.getkIController()[0] = new SVoiceKIController(game, game.getPlayers()[0]);
		
		for (int i = 0; i < game.getPlayers().length; i++) {
			switch (game.getPlayers()[i].getPlayerType()) {
			case SVOICE: case NONE:	// NONE-case vorlaeufig, um fuer 3-Spieler-Modus KI-Zuege fuer fehlenden Spieler ausfuehren zu koennen
				lokusTheGame.gameController.getkIController()[i] = new SVoiceKIController(game, game.getPlayers()[i]);
				break;
			case SIRI:
				lokusTheGame.gameController.getkIController()[i] = new SiriKiController(game, game.getPlayers()[i]);
				break;
			case CORTANA:
				lokusTheGame.gameController.getkIController()[i] = new CortanaKIController(game, game.getPlayers()[i]);
				break;
			case GOOGLENOW:
				lokusTheGame.gameController.getkIController()[i] = new GoogleNowKIController(game, game.getPlayers()[i]);
				break;
			default:
			}
		}
	}

	private void guiSetup(boolean redEnabled, boolean greenYellowEnabled) {
		int border = 8;
		fieldBG = new Sprite(
				Resources.getInstance().stringTextures.get("field"));
		fieldBG.setPosition(field.getX() - border, field.getY() - border);
		fieldBG.setSize(field.getWidth() + (2 * border), field.getHeight() + (2 * border));
		
		background = new Sprite(
				Resources.getInstance().stringTextures.get("background"), 1920, 1200);
		
		Game game = lokusTheGame.gameController.getGame();
		
		LabelStyle blueStyle = new LabelStyle();
		blueStyle.font = Resources.getInstance().fontNormalBlue;
		labelPlayerBlue = new Label(game.getPlayers()[0].getName(),
				blueStyle);
		labelPlayerBlue.setPosition(1400f, 500f);

		LabelStyle yellowStyle = new LabelStyle();
		yellowStyle.font = Resources.getInstance().fontNormalYellow;
		labelPlayerYellow = null;
		if (greenYellowEnabled) {
			labelPlayerYellow = new Label(game.getPlayers()[1].getName(),
					yellowStyle);
			if (game.getPlayers()[1].getPlayerType().equals(PlayerType.NONE))
				labelPlayerYellow.setText("- None -");
			labelPlayerYellow.setPosition(20f, 500f);
		}
		LabelStyle redStyle = new LabelStyle();
		redStyle.font = Resources.getInstance().fontNormalRed;
		labelPlayerRed = null;
		if (redEnabled) {
			labelPlayerRed = new Label(game.getPlayers()[greenYellowEnabled ? 2
					: 1].getName(), redStyle);
			labelPlayerRed.setPosition(20f, 1100f);
		}
		LabelStyle greenStyle = new LabelStyle();
		greenStyle.font = Resources.getInstance().fontNormalGreen;
		labelPlayerGreen = null;
		if (greenYellowEnabled) {
			labelPlayerGreen = new Label(game.getPlayers()[3].getName(),
					greenStyle);
			if (game.getPlayers()[3].getPlayerType().equals(PlayerType.NONE))
				labelPlayerGreen.setText("- None -");
			labelPlayerGreen.setPosition(1400f, 1100f);
		}
		if (!redEnabled) {
			labelPlayerYellow = new Label("Yellow", yellowStyle);
			labelPlayerYellow.setPosition(20f, 500f);
			labelPlayerRed = new Label("Red", redStyle);
			labelPlayerRed.setPosition(20f, 1100f);
			labelPlayerGreen = new Label("Green", greenStyle);
			labelPlayerGreen.setPosition(1400f, 1100f);
		}

		LabelStyle turnStyle = new LabelStyle();
		turnStyle.font = Resources.getInstance().fontNormalColor;
		labelTurnPlayer = new Label("It's your turn, "
				+ lokusTheGame.gameController.getCurrentPlayer().getName(),
				turnStyle);
		labelTurnPlayer.setWidth(200);
		labelTurnPlayer.setPosition(860, 1100);
		labelTurnPlayer.setAlignment(Align.center, Align.center);
		
		stage.addActor(labelPlayerBlue);
		if (greenYellowEnabled || !redEnabled) {
			stage.addActor(labelPlayerYellow);
		}
		stage.addActor(labelPlayerRed);
		if (greenYellowEnabled || !redEnabled) {
			stage.addActor(labelPlayerGreen);
		}
		
		fieldStage.addActor(field);
		stage.addActor(labelTurnPlayer);
		
		LabelStyle labelIncorrectTurnStyle = new LabelStyle();
		labelIncorrectTurnStyle.font = Resources.getInstance().fontNormalColor;
		labelIncorrectTurn = new Label("",labelIncorrectTurnStyle);
		labelIncorrectTurn.setWidth(200);
		labelIncorrectTurn.setPosition(860, 1050);
		labelIncorrectTurn.setAlignment(Align.center, Align.center);
		
		stage.addActor(labelIncorrectTurn);
	}
	
	private void buttonSetup() {
		buttonSkin = new Skin();
		buttonSkin.addRegions(Resources.getInstance().atlas);
		LabelStyle style = new LabelStyle();
		style.background = buttonSkin.getDrawable("menuButtonBackground");
		style.font = Resources.getInstance().fontNormalColor;

		styleBtnUndo = new TextButtonStyle(); // ** Button properties **//
		styleBtnUndo.up = buttonSkin.getDrawable("btnUndo");
		styleBtnUndo.font = Resources.getInstance().fontNormalBlue;

		styleBtnHint = new TextButtonStyle(); // ** Button properties **//
		styleBtnHint.up = buttonSkin.getDrawable("btnHint");
		styleBtnHint.font = Resources.getInstance().fontNormalBlue;

		btnUndo = new TextButton("", styleBtnUndo);
		btnUndo.addListener(createUndoTurnListener());
		
		btnHint = new TextButton("", styleBtnHint);
		btnHint.addListener(createHintListener());
		
		startTime = System.currentTimeMillis();
		turnTime = (System.currentTimeMillis() - startTime)/ 1000;
		labelTime = new Label((Double.toString(turnTime)), style);
		labelTime.setAlignment(Align.left);
		
		
		Table tb = new Table();
		tb.setSize(field.getWidth(), 150);
		tb.add(btnUndo).width(150).height(150).padRight(field.getWidth() / 8);
		tb.add(labelTime).width(150).height(150);
		tb.add(btnHint).width(150).height(150).padLeft(field.getWidth() / 8);
		tb.setPosition(field.getX(), field.getY() - 160);
		stage.addActor(tb);
	}
	
	public void updateTimeForNextPlayer(boolean nextPlayer, Player currPlayer) {
		if(nextPlayer && currPlayer!=null){
			currPlayer.setTotalTime(currPlayer.getTotalTime()+(int)turnTime);
			startTime = System.currentTimeMillis();
			turnTime = (System.currentTimeMillis() - startTime)/ 1000;
			labelTime.setText(Double.toString(turnTime));
		}
		else{
			turnTime = (System.currentTimeMillis() - startTime)/ 1000;
			labelTime.setText((Double.toString(turnTime)));
		}
	}

	private void createBrickGroups() {
		for (Player p : lokusTheGame.gameController.getGame().getPlayers()) {
			ArrayList<UIBrick> bricks = new ArrayList<>();
			for (Brick b : p.getBricks()) {
				bricks.add(new UIBrick(this, b, p.getColor(), 0, 0, 1f));
			}
			brickgroups.add(new UIBrickGroup(this, bricks, 0, 0, 520, 520));
		}

		switch (lokusTheGame.gameController.getGame().getMode()) {
		case STANDARD:
		case CUSTOM:
		case SOLITAIR:
		case SOLITAIRBIG:
			brickgroups.get(0).setPosition(1400, 0);
			brickgroups.get(2).setPosition(0, 600);
			brickgroups.get(3).setPosition(1400, 600);
			stage.addActor(brickgroups.get(0));
			stage.addActor(brickgroups.get(1));
			stage.addActor(brickgroups.get(2));
			stage.addActor(brickgroups.get(3));
			brickgroups.get(0).setTouchable(Touchable.disabled);
			brickgroups.get(1).setTouchable(Touchable.disabled);
			brickgroups.get(2).setTouchable(Touchable.disabled);
			brickgroups.get(3).setTouchable(Touchable.disabled);
			break;
		case BLOKUSDUO:
			brickgroups.get(0).setPosition(1400, 0);
			brickgroups.get(1).setPosition(0, 600);
			stage.addActor(brickgroups.get(0));
			stage.addActor(brickgroups.get(1));
			brickgroups.get(0).setTouchable(Touchable.disabled);
			brickgroups.get(1).setTouchable(Touchable.disabled);
			break;
		}
		int playerIndex = lokusTheGame.gameController.getGame().getTurns().size() % lokusTheGame.gameController.getGame().getPlayers().length;
		brickgroups.get(playerIndex).setTouchable(Touchable.enabled);
	}

	@Override
	public void show() {
		renderPlayerName(lokusTheGame.gameController.getCurrentPlayer());
		
		Gdx.input.setInputProcessor(multiplexer);
		if(!bgMusicOn && !isMuted){
		playBgMusic();
		}
	}
	
	private ClickListener createUndoTurnListener() {
		return new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				Game currentGame = lokusTheGame.gameController.getGame();
				List<Turn> turnsList = currentGame.getTurns();
				int playerCount = 0;
				if(currentGame.getMode() == Mode.SOLITAIR || currentGame.getMode() == Mode.SOLITAIRBIG){
					playerCount = 1;
				}
				else{
					playerCount = currentGame.getPlayers().length;
				}
				if(lokusTheGame.gameController.getCurrentPlayer().getPlayerType().equals(PlayerType.PLAYER)) {
						if (turnsList.size() < playerCount) {
							ScreenUIPopup popup = new ScreenUIPopup(
									"Undo not possible", 
									"You can not undo your turn in the first round!", 
									lokusTheGame.screenGame);
							popup.setListener(new UndoNotPossibleListener(ScreenGame.this, popup));
							lokusTheGame.setScreen(popup);
							return true;
						}
						else{
							if(lokusTheGame.gameController.getCurrentPlayer().getCheaterFlag() == false)
							{
								ScreenUIPopup popup = new ScreenUIPopup("Are your sure?", 
										"If you click on OK, your score will not be saved \nand your turn will be undone!", 
										lokusTheGame.screenGame);
								popup.setListener(new UndoListener(ScreenGame.this, popup));
								lokusTheGame.setScreen(popup);
								return true;
							}
							else{
								ScreenUIPopup popup = new ScreenUIPopup("Are your sure?", 
										"Do you really want to undo your turn,\n"+lokusTheGame.gameController.getCurrentPlayer().getName()+"?", 
										lokusTheGame.screenGame);
								popup.setListener(new UndoListener(ScreenGame.this, popup));
								lokusTheGame.setScreen(popup);
								return true;
							}
						}
						
					}
					return false;
			}
		};
	}
	private ClickListener createHintListener() {
		return new ClickListener() {
			//TODO: Implementieren.
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {

			if(lokusTheGame.gameController.getCurrentPlayer().getPlayerType().equals(PlayerType.PLAYER)) {
					if(lokusTheGame.gameController.getCurrentPlayer().getCheaterFlag() == false)
					{
						ScreenUIPopup popup = new ScreenUIPopup(
								"Are you sure?", 
								"If you click on OK, your score will not be saved \nand you will get a hint!", 
								lokusTheGame.screenGame);
						popup.setListener(new HintListener(ScreenGame.this, popup));
						lokusTheGame.setScreen(popup);
						return true;
					}
					else{
						ScreenUIPopup popup = new ScreenUIPopup(
								"Are you sure?", 
								"Do you really want to see a hint,\n"+lokusTheGame.gameController.getCurrentPlayer().getName()+"?", 
								lokusTheGame.screenGame);
								popup.setListener(new HintListener(ScreenGame.this, popup));
								lokusTheGame.setScreen(popup);
								return true;
						}
				}
			
			return false;
		}	
		};
	}
	
	public Turn calculateHint() {
		Random ran = new Random();
		ShrinkedPlayer sPlayer = new ShrinkedPlayer(lokusTheGame.gameController.getCurrentPlayer());
		ShrinkedTurn[] sTurns = CalculationHelpers.calculatePossibleTurns(lokusTheGame.gameController.getGame().getField(), sPlayer);
		List<ShrinkedTurn> validTurns = new LinkedList<>();
		int maxValue = 0;
		if(lokusTheGame.gameController.getGame().getMode().equals(Mode.SOLITAIRBIG)) {
			for (Brick brick : lokusTheGame.gameController.getCurrentPlayer().getBricks()) {
				if (maxValue < brick.getValue())
					maxValue = brick.getValue();
			}
		}
		for (ShrinkedTurn sTurn : sTurns) {
			if (sTurn.getBrick().getValue() >= maxValue)
				validTurns.add(sTurn);
		}
		int randomInt = validTurns.size();
		return new Turn(validTurns.get(ran.nextInt(randomInt)));
	}
	
	public void addHintBrick(Turn turn) {
		hintBrick = new UIBrick(ScreenGame.this, turn.getBrick(), lokusTheGame.gameController.getCurrentPlayer().getColor(), 0, 0, field.getScaleX());
		float xPos = (float)(field.getX() + (Resources.getInstance().blockTextures.get(Block.EMPTY).getWidth() * (turn.getCoord().getX() - 2) * field.getScaleX()));
		float yPos = (float)(field.getY() + (Resources.getInstance().blockTextures.get(Block.EMPTY).getHeight() * (field.fieldBlocks[0].length - turn.getCoord().getY() - 3) * field.getScaleY()));
		hintBrick.setPosition(xPos, yPos);
		hintBrick.opacity = 0.8f;
		
		Timeline.createSequence().push(
				Tween.to(hintBrick, UIBrickTween.OPACITY, 0.5f)  //object <UIBrick>, Obj.Atrribut, duration
				.target(0.4f) //target Wert
				.ease(TweenEquations.easeInSine)		//Transitionsmodus
		).repeatYoyo(Tween.INFINITY, 0)
		.start(manager);
	}
	
	private void renderHintBrick(Batch batch) {
		if(hintBrick != null) {
			batch.begin();
			hintBrick.draw(batch, 1f);
			batch.end();
		}
	}
	
	public void removeHintBrick() {
		manager.killTarget(hintBrick);
		hintBrick = null;
	}
	
	public boolean isGameOver() {
		return gameOver;
	}

	@Override
	public void render(float delta) {
		manager.update(delta);
		updateTimeForNextPlayer(false, null);
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if(displayGOScreen) {
			field.updateField();
			ungrabBrick();
			System.out.println("gameover screen displayed");
			displayGameOverScreen();
			render(delta);
		}
		if(!gameOver){
			Game game = lokusTheGame.gameController.getGame();
			int playerIndex = game.getTurns().size() % game.getPlayers().length;
			brickgroups.get(playerIndex).setTouchable(Touchable.enabled);
			
			if (!labelTurnPlayer.getText().toString().equals("No Player has a turn ..."))
				if (lokusTheGame.gameController.getGame().getMode().equals(Mode.SOLITAIR)
						|| lokusTheGame.gameController.getGame().getMode().equals(Mode.SOLITAIRBIG)) {
					labelTurnPlayer.setText("It's your turn, "
							+ lokusTheGame.gameController.getCurrentPlayer().getColor().toString());
				}
				else {
					labelTurnPlayer.setText("It's your turn, "
							+ lokusTheGame.gameController.getCurrentPlayer().getName());
				}
			
			
			if(!lokusTheGame.gameController.getCurrentPlayer().getPlayerType().equals(PlayerType.NETWORK_PLAYER)) {
				
				Player currentPlayer = lokusTheGame.gameController.getCurrentPlayer();
				ShrinkedPlayer sPlayer = new ShrinkedPlayer(currentPlayer);
				ShrinkedTurn[] possibleTurns = CalculationHelpers.calculatePossibleTurns(lokusTheGame.gameController.getGame().getField(), sPlayer);
				
				if(possibleTurns.length == 0 || !maximalValueBrickTurn(possibleTurns, currentPlayer)) {
					lokusTheGame.gameController.getTurnController().doTurn(new Turn(currentPlayer, null, null), lokusTheGame.gameController.getGame().getField(), true);
				} else {
					if (lokusTheGame.gameController.getCurrentPlayer().getPlayerType().equals(PlayerType.NONE)) {
						int repPlayerIndex = (game.getTurns().size() / 4) % (game.getPlayers().length - 1);
						if (lokusTheGame.gameController.getCurrentPlayer().getColor().equals(Block.YELLOW) && repPlayerIndex > 0)
							repPlayerIndex++;	// The none player has to be omitted, so another player can do the turn
						Player representingPlayer = game.getPlayers()[repPlayerIndex];
						labelTurnPlayer.setText("Do a turn for " + lokusTheGame.gameController.getCurrentPlayer().getColor().toString() + ", " + representingPlayer.getName());
						if (!representingPlayer.getPlayerType().equals(PlayerType.PLAYER)) {
							brickgroups.get(playerIndex).setTouchable(Touchable.disabled);
							// do KI-Turn... Vorlaeufige Loesung mit eigener S-Voice-KI (s. kiControllerSetup()):
							if(kiThread == null/* || !kiThread.isAlive()*/) {
								kiThread= new AIThread(lokusTheGame.gameController.getkIController()[playerIndex]);
								kiThread.start();
							}
						}
					}
					else if (!lokusTheGame.gameController.getCurrentPlayer().getPlayerType().equals(PlayerType.PLAYER)) {
						// Do KI-Turn, if current player is a KI
						brickgroups.get(playerIndex).setTouchable(Touchable.disabled);
						if(kiThread == null/* || !kiThread.isAlive()*/) {
							kiThread = new AIThread(lokusTheGame.gameController.getkIController()[playerIndex]);
					  		kiThread.start();
						}
					}
				} 
				
			}
			else {
				brickgroups.get(playerIndex).setTouchable(Touchable.disabled);
				
				if(networkThread == null) {
					networkThread = new NetworkThread();
					networkThread.start();
				}
				else {
					if(networkTurn != null) {
						lokusTheGame.gameController.getTurnController().doTurn(networkTurn, lokusTheGame.gameController.getGame().getField(), false);
						removeBrickFromHand(networkTurn.getBrick(), playerIndex);
						checkIfGameIsOver();
						
						networkTurn = null;
						networkThread = null;
					}
				}
			}
			
			
			if(kiTurn != null) {
				boolean turnSuccess = lokusTheGame.gameController.getTurnController().doTurn(
						kiTurn,
						game.getField(), true);
				
				if (turnSuccess) {
					// After turn is done, remove corresponding UI-Brick from KI's Brick set	
					Brick turnBrick = game.getTurns().get(game.getTurns().size() - 1).getBrick();
					removeBrickFromHand(turnBrick, playerIndex);
					
					checkIfGameIsOver();
				}
				else {				
					for(boolean[] line : kiTurn.getBrick().getCoord()) {
						for(boolean isBlock : line) {
							if (isBlock)
								System.out.print("X ");
							else
								System.out.print("- ");
						}
						System.out.println();
					}
					System.out.println(kiTurn.getPlayer().getColor().toString() + ": " +
							kiTurn.getCoord().getX() + ", " +
							kiTurn.getCoord().getY());
					throw new KIException("KI has passed an invalid turn!");
				}
				
				kiTurn = null;
				kiThread = null;
			}
			
			renderPlayerName(lokusTheGame.gameController.getCurrentPlayer());
		}
		// rendering
		Batch batch = stage.getBatch();
		batch.begin();
		
		
		background.draw(batch);
		fieldBG.draw(batch);

		batch.end();

		fieldStage.act();
		
		stage.act();
		
		fieldStage.draw();
		renderHintBrick(batch);
		stage.draw();

		
		
		renderGrabbedBrick();
		
	}
	

	public void removeBrickFromHand(Brick turnBrick, int playerIndex) {
		if (turnBrick != null) {
			Brick[] turnBrickRotations = {
				new Brick(turnBrick),
				new Brick(turnBrick).rotateBrick(),
				new Brick(turnBrick).rotateBrick().rotateBrick(),
				new Brick(turnBrick).rotateBrick().rotateBrick().rotateBrick(),
				new Brick(turnBrick).mirrorBrick(),
				new Brick(turnBrick).rotateBrick().mirrorBrick(),
				new Brick(turnBrick).rotateBrick().rotateBrick().mirrorBrick(),
				new Brick(turnBrick).rotateBrick().rotateBrick().rotateBrick().mirrorBrick()
			};
			boolean brickRemoved = false;
			for (UIBrick uiBrick : brickgroups.get(playerIndex).getBricks()) {
				for (Brick brick : turnBrickRotations) {
					if (uiBrick.getBrick().equals(brick)) {
						brickgroups.get(playerIndex).removeBrick(uiBrick);
						game.getPlayers()[playerIndex].getBricks().remove(brick);
						brickRemoved = true;
						break;
					}
				}
				if (brickRemoved)
					break;
			}
		}
		field.updateField();
	}
	
	private void doTurnCallback(Turn turn) {
		kiTurn = turn;
	}
	
	private void resetPlayerLabels() {
		if(!lokusTheGame.gameController.getGame().getMode().equals(Mode.BLOKUSDUO))
		{
			manager.killTarget(labelPlayerGreen);
			manager.killTarget(labelPlayerYellow);

			labelPlayerGreen.setPosition(1400f, 1100f);
			labelPlayerYellow.setPosition(20f, 500f);
		}
		manager.killTarget(labelPlayerBlue);
		manager.killTarget(labelPlayerRed);
		
		labelPlayerBlue.setPosition(1400f, 500f);
		labelPlayerRed.setPosition(20f, 1100f);
	}
	
	private void renderPlayerName(Player p)
	{
		
		if(p.equals(oldPlayer))
			return;
		
		oldPlayer = p;
		
		resetPlayerLabels();
		if((float)p.getTotalTime()>150f){
			timeAnim = 150f;
		}
		else
		{
			timeAnim = (float)p.getTotalTime()+speedAnim;
		}
		speedAnim = 100/timeAnim;
		if(speedAnim>=1f){
			speedAnim=1f;
		}
		switch(p.getColor()) {
		case BLUE:
			Timeline.createSequence().push(
					Tween.to(labelPlayerBlue, LabelTween.POS_XY, 0.5f*speedAnim)  //object <UIBrick>, Obj.Atrribut, duration
					.target(labelPlayerBlue.getX() + 5 + timeAnim , labelPlayerBlue.getY()) //target Wert
					.ease(TweenEquations.easeInOutSine)		//Transitionsmodus
			).repeatYoyo(Tween.INFINITY, 0)
			.start(manager);
			break;
		case YELLOW:
			manager.killTarget(labelPlayerBlue);
			manager.killTarget(labelPlayerYellow);
			manager.killTarget(labelPlayerRed);
			manager.killTarget(labelPlayerGreen);
			Timeline.createSequence().push(
					Tween.to(labelPlayerYellow, LabelTween.POS_XY, 0.5f*speedAnim)  //object <UIBrick>, Obj.Atrribut, duration
					.target(labelPlayerYellow.getX() + 5 + timeAnim, labelPlayerYellow.getY()) //target Wert
					.ease(TweenEquations.easeInOutSine)		//Transitionsmodus
			).repeatYoyo(Tween.INFINITY, 0)
			.start(manager);
			break;
		case RED:
			manager.killTarget(labelPlayerBlue);
			manager.killTarget(labelPlayerYellow);
			manager.killTarget(labelPlayerRed);
			manager.killTarget(labelPlayerGreen);
			Timeline.createSequence().push(
					Tween.to(labelPlayerRed, LabelTween.POS_XY, 0.5f*speedAnim)  //object <UIBrick>, Obj.Atrribut, duration
					.target(labelPlayerRed.getX() + 5 + timeAnim, labelPlayerRed.getY()) //target Wert
					.ease(TweenEquations.easeInOutSine)		//Transitionsmodus
			).repeatYoyo(Tween.INFINITY, 0)
			.start(manager);
			break;
		case GREEN:			
			manager.killTarget(labelPlayerBlue);
			manager.killTarget(labelPlayerYellow);
			manager.killTarget(labelPlayerRed);
			manager.killTarget(labelPlayerGreen);
			Timeline.createSequence().push(
					Tween.to(labelPlayerGreen, LabelTween.POS_XY, 0.5f*speedAnim)  //object <UIBrick>, Obj.Atrribut, duration
					.target(labelPlayerGreen.getX() + 5 + timeAnim, labelPlayerGreen.getY()) //target Wert
					.ease(TweenEquations.easeInOutSine)		//Transitionsmodus
			).repeatYoyo(Tween.INFINITY, 0)
			.start(manager);
			
			break;
		case EMPTY:
			manager.killTarget(labelPlayerBlue);
			manager.killTarget(labelPlayerYellow);
			manager.killTarget(labelPlayerRed);
			manager.killTarget(labelPlayerGreen);
			break;
		}
	}
	
	private void renderGrabbedBrick() {		
		if (grabbedBrick != null) {
			Vector2 viewportvector = viewport.unproject(new Vector2(Gdx.input
					.getX(), Gdx.input.getY()));
			float x = viewportvector.x;
			float y = viewportvector.y;

			if (isInField(x, y)) {
				x -= (x - field.getX())
						% (field.fieldBlocks[0][0].getWidth() * field
								.getScaleX());
				y -= (y - field.getY())
						% (field.fieldBlocks[0][0].getHeight() * field
								.getScaleY());

				x -= grabbedBrick.getWidth() / 2 * grabbedBrick.getScaleX();
				y -= grabbedBrick.getWidth() / 2 * grabbedBrick.getScaleX();
				x += grabbedBrick.getChildren().get(0).getWidth() / 2
						* grabbedBrick.getScaleX();
				y += grabbedBrick.getChildren().get(0).getHeight() / 2
						* grabbedBrick.getScaleY();
			} else {
				y -= grabbedBrick.getHeight() / 2 * grabbedBrick.getScaleY();
				x -= grabbedBrick.getWidth() / 2 * grabbedBrick.getScaleX();
			}

			grabbedBrick.setPosition(x, y);
		}
	}
	
	public UIField getField() {
		return field;
	}
	
	public Game getGame() {
		return game;
	}
	
	public void checkIfGameIsOver() {
		GameController gameController = lokusTheGame.gameController;
		for (int i = 0; i < gameController.getGame().getPlayers().length; i++) {
			ShrinkedPlayer sPlayer = new ShrinkedPlayer(gameController.getGame().getPlayers()[i]);
			ShrinkedTurn[] possibleTurns = CalculationHelpers.calculatePossibleTurns(gameController.getGame().getField(), sPlayer);
			if (possibleTurns.length != 0 && maximalValueBrickTurn(possibleTurns, gameController.getGame().getPlayers()[i])) {
				return;
			}
		}
		gameOver = true;
		displayGOScreen = true;
		
		if(lokusTheGame.gameController.exchanger.isGameStarted())
		lokusTheGame.gameController.exchanger.sendGameOver();
	}
	
	private boolean maximalValueBrickTurn(ShrinkedTurn[] possibleTurns, Player currentPlayer) {
		if (!lokusTheGame.gameController.getGame().getMode().equals(Mode.SOLITAIRBIG))
			return true;
		int maxValue = 0;
		for (Brick brick : currentPlayer.getBricks()) {
			if (maxValue < brick.getValue())
				maxValue = brick.getValue();
		}
		for (ShrinkedTurn turn : possibleTurns) {
			int value = turn.getBrick().getValue();
			if (value >= maxValue)
				return true;
		}
		return false;
	}

	public void displayGameOverScreen() {
		System.out.println("game over");
		GameController gameController = lokusTheGame.gameController;
		
		int[] points = gameController.getTurnController().gameScore();
		Player[] players = gameController.getGame().getPlayers();
		String message = "No Player has a turn ... The game ends\nPlayer Scores:";
		
		if (players.length == 2) {
			message += "\n" + players[0].getName() + ":\t " + points[0];
			message += "\n" + players[1].getName() + ":\t " + points[2];
		}
		if (players.length == 4) {
			if (!players[1].getPlayerType().equals(PlayerType.NONE) && players[3].getPlayerType().equals(PlayerType.NONE)) {
				message += "\n" + players[0].getName() + ":\t " + points[0];
				message += "\n" + players[1].getName() + ":\t " + points[1];
				message += "\n" + players[2].getName() + ":\t " + points[2];
			}
			else if (!players[3].getPlayerType().equals(PlayerType.NONE)&&players[1].getPlayerType().equals(PlayerType.NONE)) {
				message += "\n" + players[0].getName() + ":\t " + points[0];
				message += "\n" + players[2].getName() + ":\t " + points[2];
				message += "\n" + players[3].getName() + ":\t " + points[3];
			}
			else if(players[1].getName().equals(players[3].getName())){
				message += "\n" + players[0].getName() + ":\t " + (points[0]+points[2]);
				message += "\n" + players[1].getName() + ":\t " + (points[1]+points[3]);
			}
			else if(!players[1].getPlayerType().equals(PlayerType.NONE) && !players[3].getPlayerType().equals(PlayerType.NONE)){
				message += "\n" + players[0].getName() + ":\t " + points[0];
				message += "\n" + players[1].getName() + ":\t " + points[1];
				message += "\n" + players[2].getName() + ":\t " + points[2];
				message += "\n" + players[3].getName() + ":\t " + points[3];
			}
		}
		if (gameController.getGame().getMode().equals(Mode.SOLITAIR) 
				|| gameController.getGame().getMode().equals(Mode.SOLITAIRBIG)) {
			
			int total = points[0] + points[1] + points[2] + points[3];
			message += "\n Total Score:\t " + total;
			double timeTotal = players[0].getTotalTime() + players[1].getTotalTime()
					+ players[2].getTotalTime() + players[3].getTotalTime();
			players[0].setTotalTime(timeTotal);
			gameController.getHighscoreController().addHighScore(total, players[0], gameController.getGame().getMode());
		}
		else//Hier hab ich was geändert
			if(players.length !=2 &&!players[1].getName().equals(players[3].getName())){
				for(int j = 0; j< players.length;j++){
					gameController.getHighscoreController().addHighScore(points[j], players[j], gameController.getGame().getMode());
				}
			}
			else{
				gameController.getHighscoreController().addHighScore(points[0]+points[2], players[0], gameController.getGame().getMode());
				gameController.getHighscoreController().addHighScore(points[1]+points[3], players[1], gameController.getGame().getMode());
			}
		int choosedMode = gameController.getGame().getMode().ordinal();
		lokusTheGame.highscoreScreen.choosedMode = choosedMode;
		ScreenUIPopup popup = new ScreenUIPopup("Game Over", message, lokusTheGame.highscoreScreen);
		popup.setListener(new GameOverListener(ScreenGame.this, popup, lokusTheGame.highscoreScreen));
		lokusTheGame.setScreen(popup);

//		gameOver = false;
		displayGOScreen = false;
	}
	
	public ArrayList<UIBrickGroup> getBrickGroups() {
		return brickgroups;
	}

	@Override
	public void resize(int newWidth, int newHeight) {
		viewport.update(newWidth, newHeight);
	}

	protected void playBgMusic(){
		if(backgroundMusic == null){
			switch (game.getMode()) {
			case STANDARD:
				backgroundMusic = Gdx.audio.newSound(Gdx.files.internal("res/sfx/ChibiNinjaMP3.mp3"));
				break;
			case BLOKUSDUO:
				backgroundMusic = Gdx.audio.newSound(Gdx.files.internal("res/sfx/ChibiNinjaMP3.mp3"));
				break;
			case SOLITAIR:
				backgroundMusic = Gdx.audio.newSound(Gdx.files.internal("res/sfx/solitairTheme.mp3"));
				break;
			case SOLITAIRBIG:
				backgroundMusic = Gdx.audio.newSound(Gdx.files.internal("res/sfx/solitairTheme.mp3"));
				break;
			case CUSTOM:
				backgroundMusic = Gdx.audio.newSound(Gdx.files.internal("res/sfx/fogOfWarTheme.mp3"));
				break;
			}
		}
		if(!bgMusicOn){
			switch (game.getMode()) {
			case STANDARD:
				backgroundMusic.loop(lokusTheGame.bgVolume);
				break;
			case BLOKUSDUO:
				backgroundMusic.loop(lokusTheGame.bgVolume);
				break;
			case SOLITAIR:
				backgroundMusic.loop(lokusTheGame.bgVolume*3);
				break;
			case SOLITAIRBIG:
				backgroundMusic.loop(lokusTheGame.bgVolume*3);
				break;
			case CUSTOM:
				backgroundMusic.loop(lokusTheGame.bgVolume);
				break;
			}
			
			isMuted = false;
			bgMusicOn = true;
		}
	}
	
	protected void pauseBgMusic(){
		backgroundMusic.pause();
		bgMusicOn = false;
	}
	protected void muteBgMusic(){
		backgroundMusic.pause();
		isMuted= true;
		bgMusicOn = false;
	}
	
	// unused methods
	@Override
	public void dispose() {
		backgroundMusic.stop();
		backgroundMusic.dispose();
	}

	@Override
	public void pause() {
		// nothing to do
	}

	@Override
	public void resume() {
		// nothing to do
	}

	@Override
	public void hide() {
	}

	private void setLabelTurnPlayerText(String newText) {
		labelTurnPlayer.setText(newText);
	}
	
	public void setLabelIncorrectTurnText(String newText) {
		labelIncorrectTurn.setText(newText);
	}

	private boolean isInField(float x, float y) {
		return x >= field.getX() && x <= field.getX() + field.getWidth()
				&& y >= field.getY() && y <= field.getY() + field.getHeight();
	}

	public void grabBrick(UIBrick brick) {
		if (this.grabbedBrick == null) {
			if (lokusTheGame.gameController.getGame().getMode().equals(Mode.SOLITAIRBIG)
					&& !isMaximalValueBrick(brick.getBrick()))
				return;
			brick.grabbed = true;
			if (brick.group != null) {
				brick.group.removeBrick(brick);
				this.stage.addActor(brick);
			}
			brick.setScale(field.getScaleX() * 0.95f);
			this.grabbedBrick = brick;
			this.canUngrabBrick = false;
			
			Timeline.createSequence().push(
					Tween.to(grabbedBrick, UIBrickTween.SCALE_XY, 0.5f)  //object <UIBrick>, Obj.Atrribut, duration
					.target(field.getScaleX() * 1.05f, field.getScaleX() * 1.05f) //target Wert
					.ease(TweenEquations.easeInOutSine)		//Transitionsmodus
			).repeatYoyo(Tween.INFINITY, 0)
			.start(manager);
			
			renderGrabbedBrick();
		}
	}
	
	private boolean isMaximalValueBrick(Brick brick) {
		List<Brick> playerBricks = lokusTheGame.gameController.getCurrentPlayer().getBricks();
		int currentValue = brick.getValue();
		for (Brick playerBrick : playerBricks) {
			if (currentValue < playerBrick.getValue())
				return false;
		}
		return true;
	}

	public void ungrabBrick() {
		if(grabbedBrick == null) return;
		
		manager.killTarget(grabbedBrick);
		
		grabbedBrick.grabbed = false;
		if(!(grabbedBrick.getParent() instanceof UIBrickGroup)) {
			grabbedBrick.getParent().removeActor(grabbedBrick);
		}
		grabbedBrick = null;
	}

	public Brick getGrabbedBrick() {
		return grabbedBrick.getBrick();
	}

	public boolean hasBrickGrabbed() {
		return grabbedBrick != null;
	}
	
    private class AIThread extends Thread {
    	private KIController controller;
    	
    	public AIThread(KIController controller) {
    		super();
    		this.controller = controller;
    	}
    	
    	@Override
    	public void run() {
    		long before = System.currentTimeMillis();
    		Turn calculatedTurn = controller.calculateTurn();
    		long after = System.currentTimeMillis();
    		long time = after - before;
    		
    		doTurnCallback(calculatedTurn);
    	}
    }
    
    private class NetworkThread extends Thread {
    	Turn lastTurn;
    	
    	@Override
    	public void run() {
    		try {
				lastTurn = lokusTheGame.gameController.parseTurn();
			} catch (GteGameNotRunningException | ParseTurnException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		networkTurnCallback(lastTurn);
    	}
    }

    private void networkTurnCallback(Turn t) {
    	networkTurn = t;
    }
    
	private class ScreenGameInputHandler extends InputListener {

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			if (button == Input.Buttons.LEFT)
				onLeftTouchDown(x, y);
			else if (button == Input.Buttons.RIGHT)
				onRightTouchDown();

			return false;
		}

		private void onLeftTouchDown(float x, float y) {
			System.out.println("buttonasdasd pressed");
			if (grabbedBrick != null) {
				if (!isInField(x, y)) {
					if (canUngrabBrick) {
						if (grabbedBrick.group != null) {
							grabbedBrick.group.addBrick(grabbedBrick);
							
							ungrabBrick();
						}
					} else {
						System.out
								.println("brick not ungrabbed, but i can do taht next time");
						canUngrabBrick = true;
					}
				}
			}
		}

		private void onRightTouchDown() {
			if (grabbedBrick != null) {
				grabbedBrick.rotateBrick();
			}
		}
		
		@Override
		public boolean scrolled(InputEvent event, float x, float y, int amount) {
			if (grabbedBrick != null) {
				grabbedBrick.mirrorBrick();
			}
			return false;
		}

		@Override
		public boolean keyDown(InputEvent event, int keycode) {
			if (grabbedBrick != null) {
				if (keycode == Keys.R || keycode == Keys.D) {
					grabbedBrick.rotateBrick();
				}
				if (keycode == Keys.M || keycode == Keys.S) {
					grabbedBrick.mirrorBrick();
				}
			}
			return false;
		}

		@Override
		public boolean keyUp(InputEvent event, int keycode) {
			if (keycode == Keys.ESCAPE) {
				lokusTheGame.setScreen(lokusTheGame.gameMenuScreen);
				return true;
			}
			return false;
		}
	}
}
