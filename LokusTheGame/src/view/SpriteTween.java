package view;

import com.badlogic.gdx.graphics.g2d.Sprite;
import aurelienribon.tweenengine.TweenAccessor;

/**
 * SpriteTween defines modification options on single Sprite object
 * @author sopr049
 *
 */

public class SpriteTween implements TweenAccessor<Sprite>{

	/* changes alpha value */
	public static final int ALPHA = 1;
	
	/**
     * getValues gets the tween animation values from the object
     * @param target as Sprite object
     * @param tweenType as modification option
     * @param returnValues as dict of object values
     * @return number of object values returned, else -1
     */
    @Override
	public int getValues(Sprite arg0, int arg1, float[] arg2) {
		switch(arg1) {
			case ALPHA:
				arg2[0] = arg0.getColor().a;
				return 1;
			default:
				return 0;
		}
	}

    /**
     * setValues sets the tween animation values to the object
     * @param target as UIBrick object
     * @param tweenType as modification option
     * @param newValues as dict of new values
     */
    @Override
	public void setValues(Sprite arg0, int arg1, float[] arg2) {
		// TODO Auto-generated method stub
		switch(arg1) {
		case ALPHA:
			arg0.setColor(1, 1, 1, arg2[0]);
			break;
		}
	}

}
