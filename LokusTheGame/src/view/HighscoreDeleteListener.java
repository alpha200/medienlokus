package view;

import java.util.LinkedList;

import model.Mode;
import model.Score;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class HighscoreDeleteListener extends InputListener{
	private DefaultScreen screen;
	private ScreenUIPopup popup;
	private DefaultScreen fromScreen;
	private Mode[] gameModes = { Mode.STANDARD, Mode.BLOKUSDUO, Mode.SOLITAIR,
			Mode.SOLITAIRBIG, Mode.CUSTOM };
	public HighscoreDeleteListener(DefaultScreen screen, ScreenUIPopup popup, DefaultScreen fromScreen){
		super();
		this.screen = screen;	
		this.popup = popup;
		this.fromScreen = fromScreen;
	}
	public boolean touchDown(InputEvent event, float posX, float posY, int pointer, int button){
		Gdx.app.log("ScreenUIPopup", "Notification closed.");
		switch(gameModes[screen.lokusTheGame.highscoreScreen.choosedMode]){
		case STANDARD : screen.lokusTheGame.gameController.getHighscoreController().setStandardScores(new LinkedList<Score>());
		case BLOKUSDUO :screen.lokusTheGame.gameController.getHighscoreController().setBlokusDuoScores(new LinkedList<Score>());
		case CUSTOM : screen.lokusTheGame.gameController.getHighscoreController().setCustomScores(new LinkedList<Score>());
		case SOLITAIR : screen.lokusTheGame.gameController.getHighscoreController().setSolitairScores(new LinkedList<Score>());
		case SOLITAIRBIG : screen.lokusTheGame.gameController.getHighscoreController().setSolitairBigScores(new LinkedList<Score>());
		}
		popup.dispose();
        screen.lokusTheGame.setScreen(fromScreen);
        return true;
	}
}