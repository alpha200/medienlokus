package view;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class SaveGameListener extends InputListener{
	private ScreenGame screen;
	private ScreenUIPopup popup;
	public SaveGameListener(ScreenGame screen, ScreenUIPopup popup){
		super();
		this.screen = screen;	
		this.popup = popup;
	}
	public boolean touchDown(InputEvent event, float posX, float posY, int pointer, int button){
		Gdx.app.log("ScreenUIPopup", "Notification closed.");
		try {
			screen.lokusTheGame.gameController.getiOController().saveGame(
					screen.lokusTheGame.gameController.getGame());
		} catch (NullPointerException e) {
			System.out.println("NP Exception, fehler beim game");
		} catch (IOException e) {
			System.out.println("IO Exception, fehler beim SPeichern");
		}
		popup.dispose();
        screen.lokusTheGame.setScreen(screen);
        return true;
	}
}
