package view;

import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
 
public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		DisplayMode mode = LwjglApplicationConfiguration.getDesktopDisplayMode();
		config.height = mode.height;
		config.width = mode.width;

		config.resizable = false;
		config.vSyncEnabled = true;
		config.fullscreen = true;
		if(arg.length > 0) {
			config.fullscreen = Boolean.parseBoolean(arg[0]);
			config.height = mode.height / 2;
			config.width = mode.width / 2;
		}
		
		config.title = "Lokus the Game";
		new LwjglApplication(new LokusTheGame(), config);
	}
}