package view;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

/**
 * Menü Screen das während des Spieles aufrufbar ist
 * 
 * @author sopr042
 *
 */
public class ScreenGameMenu extends DefaultScreen {

	Label lblContinue, lblSave, lblMute, lblExit;
	Skin buttonSkin;
	private TextButton btnContinue;
	private TextButton btnSave;
	private TextButtonStyle styleBtnSave;
	private TextButtonStyle styleBtnContinue;
	private TextButton btnMute;
	private TextButton btnExit;
	private TextButtonStyle styleBtnMute;
	private TextButtonStyle styleBtnExit;
	private TextButtonStyle styleBtnUnmute;

	public ScreenGameMenu(LokusTheGame game) {
		super(game);
	}

	@Override
	public void show() {
		buttonSkin = new Skin();
		buttonSkin.addRegions(Resources.getInstance().atlas);
		LabelStyle style = new LabelStyle();
		style.background = buttonSkin.getDrawable("menuButtonBackground");
		style.font = Resources.getInstance().fontNormalColor;

		styleBtnContinue = new TextButtonStyle(); // ** Button properties **//
		styleBtnContinue.up = buttonSkin.getDrawable("btnContinue");
		styleBtnContinue.font = Resources.getInstance().fontNormalBlue;

		styleBtnSave = new TextButtonStyle();
		styleBtnSave.up = buttonSkin.getDrawable("btnSave");
		styleBtnSave.font = Resources.getInstance().fontNormalBlue;

		styleBtnMute = new TextButtonStyle(); // ** Button properties **//
		styleBtnMute.up = buttonSkin.getDrawable("btnMute");
		styleBtnMute.font = Resources.getInstance().fontNormalBlue;
		
		styleBtnUnmute = new TextButtonStyle(); // ** Button properties **//
		styleBtnUnmute.up = buttonSkin.getDrawable("btnUnmute");
		styleBtnUnmute.font = Resources.getInstance().fontNormalBlue;


		styleBtnExit = new TextButtonStyle(); // ** Button properties **//
		styleBtnExit.up = buttonSkin.getDrawable("btnExit");
		styleBtnExit.font = Resources.getInstance().fontNormalBlue;

		lblContinue = new Label("Continue", style);
		lblContinue.setAlignment(Align.center);

		lblSave = new Label("Save", style);
		lblSave.setAlignment(Align.center);

		lblMute = new Label("Mute", style);
		lblMute.setAlignment(Align.center);

		lblExit = new Label("Exit", style);
		lblExit.setAlignment(Align.center);

		// Create Buttons with Style
		btnContinue = new TextButton("", styleBtnContinue);
		btnContinue.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return goBackToGame();
			}

		});

		btnSave = new TextButton("", styleBtnSave);
		btnSave.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
						ScreenUIPopup popup = new ScreenUIPopup("Save", "Do you really want to save the current game?.", lokusTheGame.screenGame );
						popup.setListener(new SaveGameListener(lokusTheGame.screenGame, popup));
						lokusTheGame.setScreen(popup);
						return true;
				}
		});
		if(lokusTheGame.screenGame.isMuted){
			lblMute = new Label("Unmute", style);
			lblMute.setAlignment(Align.center);
		    btnMute = new TextButton("", styleBtnUnmute);
		}
		else{
			lblMute = new Label("Mute", style);
			lblMute.setAlignment(Align.center);
			btnMute = new TextButton("", styleBtnMute);
		}
		btnMute.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(!lokusTheGame.screenGame.isMuted){
					btnMute.setStyle(styleBtnUnmute);
					lblMute.setText("Unmute");
					lokusTheGame.screenGame.muteBgMusic();
				}
				else{
					btnMute.setStyle(styleBtnMute);
					lblMute.setText("Mute");
					lokusTheGame.screenGame.playBgMusic();
				}
				return true;
			}

		});
		btnExit = new TextButton("", styleBtnExit);
		btnExit.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				lokusTheGame.setScreen(lokusTheGame.mainMenu);
				lokusTheGame.screenGame.muteBgMusic();
				lokusTheGame.screenGame.dispose();
				return true;
			}

		});

		// KeyListener
		stage.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if (keycode == Keys.ESCAPE) {
					goBackToGame();
				}
				return true;
			}
		});

		Table tb = new Table();
		tb.add(lblContinue).width(230).padRight(20);
		tb.add(lblSave).width(230).padLeft(20);
		tb.row();
		tb.add(btnContinue).width(230).padRight(20);
		tb.add(btnSave).width(230).padLeft(20);
		tb.row();
		tb.add(btnMute).width(230).padRight(20);
		tb.add(btnExit).width(230).padLeft(20);
		tb.row();
		tb.add(lblMute).width(230).padRight(20);
		tb.add(lblExit).width(230).padLeft(20);
		tb.setFillParent(true);
		stage.addActor(tb);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0.5f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		lblContinue.setVisible(true);
		lblSave.setVisible(true);
		lblMute.setVisible(true);
		lblExit.setVisible(true);
		stage.draw();
		Gdx.input.setInputProcessor(stage);

	}

	private boolean goBackToGame() {
		
		lokusTheGame.setScreen(lokusTheGame.screenGame);
		
		return true;
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void hide() {
		stage.getActors().removeRange(0,stage.getActors().size-1);;
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
	}
}
