package view;

import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import model.Block;
import model.Brick;
import model.Game;
import model.Mode;
import model.Player;
import model.PlayerType;

public class ScreenGameConfiguration extends DefaultScreen {
	Viewport viewport;
	Camera camera;
	Stage stage;

	SpriteBatch batch, bgBatch;
	Button backButton, forwardButton,backNetButton, forwardNetButton, backBlue, forwardBlue, backRed,
			forwardRed, backYellow, forwardYellow, backGreen, forwardGreen;
	Button play, back;
	Skin buttonSkin;
	Sprite logo, background;
	Label labelStandard, labelNetwork, labelBlue, labelRed, labelYellow, labelGreen, labelError;
	Label labelPlayerBlue, labelPlayerRed, labelPlayerYellow, labelPlayerGreen;
	TextField textBlue, textRed, textYellow, textGreen;

	private Mode[] gameModes = { Mode.STANDARD, Mode.BLOKUSDUO, Mode.SOLITAIR,
			Mode.SOLITAIRBIG, Mode.CUSTOM};
	private PlayerType[] playerTypes = { PlayerType.PLAYER, PlayerType.SVOICE,
			PlayerType.CORTANA, PlayerType.SIRI, PlayerType.GOOGLENOW,
			PlayerType.NETWORK_PLAYER, PlayerType.NONE };
	private String[] network = { "Network Enabled", "Network Disabled"};
	private String[] privatePlayersNames = {"Player Blue", "Player Yellow", "Player Red", "Player Green"};
	
	private int choosedMode = 0;
	private int choosedTypeBlue = 0;
	private int choosedTypeYellow = 0;
	private int choosedTypeGreen = 0;
	private int choosedTypeRed = 0;
	private int choosedNetwork = 0;
	ByteArrayOutputStream baos;

	public ScreenGameConfiguration(LokusTheGame game) {
		super(game);
	}

	@Override
	public void show() {
		lokusTheGame.playerNamesUpdated = true;
		// Initial configurations
		Resources.getInstance().init();

		bgBatch = new SpriteBatch();
		bgBatch.getProjectionMatrix().setToOrtho2D(0, 0,
				Resources.getInstance().background.getWidth(),
				Resources.getInstance().background.getHeight());

		camera = new OrthographicCamera();
		viewport = new FitViewport(1920, 1200, camera);
		camera.translate(viewport.getWorldWidth() / 2,
				viewport.getWorldHeight() / 2, 0);
		camera.update();
		stage = new Stage(viewport);
		batch = new SpriteBatch();

		background = Resources.getInstance().background;

		// Inserting the logo at the top ( will be drawn in render() )
		logo = Resources.getInstance().logoSmall;
		
		// Buttons and labels for choosing game type
		buttonSkin = new Skin();
		buttonSkin.addRegions(Resources.getInstance().atlas);
		LabelStyle style = new LabelStyle();
		style.background = buttonSkin.getDrawable("menuButtonBackground");
		style.font = Resources.getInstance().fontNormalColor;
		labelStandard = new Label(gameModes[choosedMode].toString(), style);

		labelStandard.setWidth(225f);
		labelStandard.setPosition(
				viewport.getWorldWidth() / 2f - labelStandard.getWidth() / 2f,
				870f - (50f + logo.getHeight()));
		labelStandard.setAlignment(Align.center, Align.center);

		ButtonStyle backStyle = new ButtonStyle();
		backStyle.up = buttonSkin.getDrawable("menuArrowLeft");
		backButton = new Button(backStyle);
		backButton.setPosition(
				viewport.getWorldWidth() / 2 - labelStandard.getWidth() / 2f
						- 100f, 890f - (50f + logo.getHeight()));
		backButton.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedMode--;
				if (choosedMode < 0)
					choosedMode = gameModes.length - 1;
				labelStandard.setText(gameModes[choosedMode].toString());
				return true;
			}
		});

		ButtonStyle forwardStyle = new ButtonStyle();
		forwardStyle.up = buttonSkin.getDrawable("menuArrowRight");
		forwardButton = new Button(forwardStyle);
		forwardButton.setPosition(
				viewport.getWorldWidth() / 2 + labelStandard.getWidth() / 2f
						+ 100 - forwardButton.getWidth(),
				890f - (50f + logo.getHeight()));
		forwardButton.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedMode++;
				if (choosedMode >= gameModes.length)
					choosedMode = 0;
				labelStandard.setText(gameModes[choosedMode].toString());
				return true;
			}
		});

		buttonSkin = new Skin();
		buttonSkin.addRegions(Resources.getInstance().atlas);

		if(lokusTheGame.gameController.exchanger.isNetworkEnabled())
			choosedNetwork = 0;
		else
			choosedNetwork = 1;
		labelNetwork = new Label(network[choosedNetwork], style);

		labelNetwork.setWidth(225f);
		labelNetwork.setPosition(200,875);
		labelNetwork.setAlignment(Align.center, Align.center);

		LabelStyle styleError = new LabelStyle();
		styleError.background = buttonSkin.getDrawable("menuButtonBackground");
		styleError.font = Resources.getInstance().fontNormalRed;
		
		labelError = new Label("", styleError);
		labelError.setWidth(225f);
		labelError.setPosition(1600, 875);
		labelError.setAlignment(Align.center, Align.center);
		
		backNetButton = new Button(backStyle);
		backNetButton.setPosition(100,900);
		backNetButton.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				lokusTheGame.switchNetworkEnabled(!lokusTheGame.gameController.exchanger.isNetworkEnabled());
				choosedNetwork--;
				if (choosedNetwork < 0)
					choosedNetwork = network.length - 1;
				labelNetwork.setText(network[choosedNetwork].toString());
				
				if (choosedNetwork == 1) {
					if (playerTypes[choosedTypeBlue]
							.equals(PlayerType.NETWORK_PLAYER)) {
						choosedTypeBlue = 0;
						labelPlayerBlue.setText(playerTypes[choosedTypeBlue]
								.toString());
					}
					if (playerTypes[choosedTypeRed]
							.equals(PlayerType.NETWORK_PLAYER)) {
						choosedTypeRed = 0;
						labelPlayerRed.setText(playerTypes[choosedTypeRed]
								.toString());
					}
					if (playerTypes[choosedTypeYellow]
							.equals(PlayerType.NETWORK_PLAYER)) {
						choosedTypeYellow = 0;
						labelPlayerYellow
								.setText(playerTypes[choosedTypeYellow]
										.toString());
					}
					if (playerTypes[choosedTypeGreen]
							.equals(PlayerType.NETWORK_PLAYER)) {
						choosedTypeGreen = 0;
						labelPlayerGreen.setText(playerTypes[choosedTypeGreen]
								.toString());
					}
				}
				return true;
			}
		});


		forwardNetButton = new Button(forwardStyle);
		forwardNetButton.setPosition(500,900);
		forwardNetButton.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				lokusTheGame.switchNetworkEnabled(!lokusTheGame.gameController.exchanger.isNetworkEnabled());
				choosedNetwork++;
				if (choosedNetwork >= network.length)
					choosedNetwork = 0;
				labelNetwork.setText(network[choosedNetwork].toString());
				
				if (choosedNetwork == 1) {
					if (playerTypes[choosedTypeBlue]
							.equals(PlayerType.NETWORK_PLAYER)) {
						choosedTypeBlue = 0;
						labelPlayerBlue.setText(playerTypes[choosedTypeBlue]
								.toString());
					}
					if (playerTypes[choosedTypeRed]
							.equals(PlayerType.NETWORK_PLAYER)) {
						choosedTypeRed = 0;
						labelPlayerRed.setText(playerTypes[choosedTypeRed]
								.toString());
					}
					if (playerTypes[choosedTypeYellow]
							.equals(PlayerType.NETWORK_PLAYER)) {
						choosedTypeYellow = 0;
						labelPlayerYellow
								.setText(playerTypes[choosedTypeYellow]
										.toString());
					}
					if (playerTypes[choosedTypeGreen]
							.equals(PlayerType.NETWORK_PLAYER)) {
						choosedTypeGreen = 0;
						labelPlayerGreen.setText(playerTypes[choosedTypeGreen]
								.toString());
					}
				}
				return true;
			}
		});
		// Player Colour labels
		LabelStyle styleBlue, styleRed, styleGreen, styleYellow;
		styleBlue = new LabelStyle();
		styleRed = new LabelStyle();
		styleYellow = new LabelStyle();
		styleGreen = new LabelStyle();
		styleBlue.font = Resources.getInstance().fontNormalBlue;
		styleRed.font = Resources.getInstance().fontNormalRed;
		styleYellow.font = Resources.getInstance().fontNormalYellow;
		styleGreen.font = Resources.getInstance().fontNormalGreen;
		labelBlue = new Label("Blue:", styleBlue);
		labelRed = new Label("Red:", styleRed);
		labelYellow = new Label("Yellow:", styleYellow);
		labelGreen = new Label("Green:", styleGreen);

		labelBlue.setPosition(150f, 610f);
		labelRed.setPosition(150f, 370f);
		labelYellow.setPosition(150f, 490f);
		labelGreen.setPosition(150f, 250f);

		// Textfields for Player names
		TextFieldStyle tStyle = new TextFieldStyle();
		tStyle.font = Resources.getInstance().fontNormalColor;
		tStyle.fontColor = Color.WHITE;
		tStyle.cursor = buttonSkin.getDrawable("cursor_input");
		textBlue = new TextField("Player Blue", tStyle);
		textBlue.setMaxLength(16);
		textBlue.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if(keycode == Keys.ENTER) {
					privatePlayersNames[0] = textBlue.getText();
				}
				return false;
			}			
		});
		textRed = new TextField("Player Red", tStyle);
		textRed.setMaxLength(16);
		textRed.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if(keycode == Keys.ENTER) {
					privatePlayersNames[2] = textRed.getText();
				}
				return false;
			}			
		});
		textYellow = new TextField("Player Yellow", tStyle);
		textYellow.setMaxLength(16);
		textYellow.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if(keycode == Keys.ENTER) {
					privatePlayersNames[1] = textYellow.getText();
				}
				return false;
			}			
		});
		textGreen = new TextField("Player Green", tStyle);
		textGreen.setMaxLength(16);
		textGreen.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if(keycode == Keys.ENTER) {
					privatePlayersNames[3] = textGreen.getText();
				}
				return false;
			}			
		});

		textBlue.setPosition(500f, 610f);
		textRed.setPosition(500f, 370f);
		textYellow.setPosition(500f, 490f);
		textGreen.setPosition(500f, 250f);
		textBlue.setWidth(500f);
		textRed.setWidth(500f);
		textYellow.setWidth(500f);
		textGreen.setWidth(500f);

		// Labels and buttons for choosing player types for blue player
		backBlue = new Button(backStyle);
		backBlue.setPosition(1200f, 630f);
		backBlue.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedTypeBlue--;
				if (choosedTypeBlue < 0)
					choosedTypeBlue = playerTypes.length - 2;
				if (choosedNetwork == 1 && playerTypes[choosedTypeBlue].equals(PlayerType.NETWORK_PLAYER))
					choosedTypeBlue--;
				labelPlayerBlue.setText(playerTypes[choosedTypeBlue].toString());
				return true;
			}
		});

		forwardBlue = new Button(forwardStyle);
		forwardBlue.setPosition(1600f, 630f);
		forwardBlue.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedTypeBlue++;
				if (choosedTypeBlue >= playerTypes.length - 1)
					choosedTypeBlue = 0;
				if (choosedNetwork == 1 && playerTypes[choosedTypeBlue].equals(PlayerType.NETWORK_PLAYER))
					choosedTypeBlue = 0;
				labelPlayerBlue.setText(playerTypes[choosedTypeBlue
						% playerTypes.length].toString());
				return true;
			}
		});

		// Labels and buttons for choosing player types for red player
		backRed = new Button(backStyle);
		backRed.setPosition(1200f, 390f);
		backRed.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedTypeRed--;
				if (choosedTypeRed < 0)
					choosedTypeRed = playerTypes.length - 2;
				if (choosedNetwork == 1 && playerTypes[choosedTypeRed].equals(PlayerType.NETWORK_PLAYER))
					choosedTypeRed--;
				labelPlayerRed.setText(playerTypes[choosedTypeRed].toString());
				return true;
			}
		});

		forwardRed = new Button(forwardStyle);
		forwardRed.setPosition(1600f, 390f);
		forwardRed.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedTypeRed++;
				if (choosedTypeRed >= playerTypes.length - 1)
					choosedTypeRed = 0;
				if (choosedNetwork == 1 && playerTypes[choosedTypeRed].equals(PlayerType.NETWORK_PLAYER))
					choosedTypeRed = 0;
				labelPlayerRed.setText(playerTypes[choosedTypeRed
						% playerTypes.length].toString());
				return true;
			}
		});

		// Labels and buttons for choosing player types for yellow player
		backYellow = new Button(backStyle);
		backYellow.setPosition(1200f, 510f);
		backYellow.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedTypeYellow--;
				if (choosedTypeYellow < 0)
					choosedTypeYellow = playerTypes.length - 1;
				if (choosedNetwork == 1 && playerTypes[choosedTypeYellow].equals(PlayerType.NETWORK_PLAYER))
					choosedTypeYellow--;
				labelPlayerYellow.setText(playerTypes[choosedTypeYellow]
						.toString());
				return true;
			}
		});

		forwardYellow = new Button(forwardStyle);
		forwardYellow.setPosition(1600f, 510f);
		forwardYellow.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedTypeYellow++;
				if (choosedTypeYellow >= playerTypes.length)
					choosedTypeYellow = 0;
				if (choosedNetwork == 1 && playerTypes[choosedTypeYellow].equals(PlayerType.NETWORK_PLAYER))
					choosedTypeYellow++;
				labelPlayerYellow.setText(playerTypes[choosedTypeYellow
						% playerTypes.length].toString());
				return true;
			}
		});

		// Labels and buttons for choosing player types for green player
		backGreen = new Button(backStyle);
		backGreen.setPosition(1200f, 270f);
		backGreen.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedTypeGreen--;
				if (choosedTypeGreen < 0)
					choosedTypeGreen = playerTypes.length - 1;
				if (choosedNetwork == 1 && playerTypes[choosedTypeGreen].equals(PlayerType.NETWORK_PLAYER))
					choosedTypeGreen--;
				labelPlayerGreen.setText(playerTypes[choosedTypeGreen]
						.toString());
				return true;
			}
		});

		forwardGreen = new Button(forwardStyle);
		forwardGreen.setPosition(1600f, 270f);
		forwardGreen.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				choosedTypeGreen++;
				if (choosedTypeGreen >= playerTypes.length)
					choosedTypeGreen = 0;
				if (choosedNetwork == 1 && playerTypes[choosedTypeGreen].equals(PlayerType.NETWORK_PLAYER))
					choosedTypeGreen++;
				labelPlayerGreen.setText(playerTypes[choosedTypeGreen
						% playerTypes.length].toString());
				return true;
			}
		});

		labelPlayerBlue = new Label(playerTypes[choosedTypeBlue].toString(),
				style);
		labelPlayerRed = new Label(playerTypes[choosedTypeRed].toString(),
				style);
		labelPlayerYellow = new Label(
				playerTypes[choosedTypeYellow].toString(), style);
		labelPlayerGreen = new Label(playerTypes[choosedTypeGreen].toString(),
				style);

		labelPlayerBlue.setPosition(1300f, 610f);
		labelPlayerBlue.setWidth(225f);
		labelPlayerBlue.setAlignment(Align.center, Align.center);
		labelPlayerRed.setPosition(1300f, 370f);
		labelPlayerRed.setWidth(225f);
		labelPlayerRed.setAlignment(Align.center, Align.center);
		labelPlayerYellow.setPosition(1300f, 490f);
		labelPlayerYellow.setWidth(225f);
		labelPlayerYellow.setAlignment(Align.center, Align.center);
		labelPlayerGreen.setPosition(1300f, 250f);
		labelPlayerGreen.setWidth(225f);
		labelPlayerGreen.setAlignment(Align.center, Align.center);

		// Buttons for navigating
		TextButtonStyle textStyle = new TextButtonStyle();
		textStyle.font = Resources.getInstance().fontBigColor;
		play = new TextButton("play >", textStyle);
		play.setPosition(viewport.getWorldWidth() - 100f - play.getWidth(),
				100f);
		play.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
					boolean error = false;
				Player playerBlue = null, playerRed = null, playerYellow = null, playerGreen = null;
				//bei solitair(big) mode alle 4 spieler mit dem Namen von Blau erstellen
				if (gameModes[choosedMode].equals(Mode.SOLITAIR)) {
					if(textBlue.getText() == null ||textBlue.getText().trim().equals("")){
						error = true;
					}
					else{
						playerBlue = new Player(Brick.generateMiniSolitairBricks(), false, Block.BLUE, textBlue.getText(), playerTypes[choosedTypeBlue]);
						playerYellow = new Player(Brick.generateMiniSolitairBricks(), false, Block.YELLOW, "Yellow", playerTypes[choosedTypeBlue]);
						playerRed = new Player(Brick.generateMiniSolitairBricks(), false, Block.RED, "Red", playerTypes[choosedTypeBlue]);
						playerGreen = new Player(Brick.generateMiniSolitairBricks(), false, Block.GREEN, "Green", playerTypes[choosedTypeBlue]);
				} 
				}else if(gameModes[choosedMode].equals(Mode.SOLITAIRBIG)) {
					if(textBlue.getText() == null ||textBlue.getText().trim().equals("")){
						error = true;
					}
					else{
						playerBlue = new Player(false, Block.BLUE, textBlue.getText(), playerTypes[choosedTypeBlue]);
						playerYellow = new Player(false, Block.YELLOW, "Yellow", playerTypes[choosedTypeBlue]);
						playerRed = new Player(false, Block.RED, "Red", playerTypes[choosedTypeBlue]);
						playerGreen = new Player(false, Block.GREEN, "Green", playerTypes[choosedTypeBlue]);
					} 
					}
				else if (!gameModes[choosedMode].equals(Mode.BLOKUSDUO) && playerTypes[choosedTypeYellow].equals(PlayerType.NONE) && playerTypes[choosedTypeGreen].equals(PlayerType.NONE)) {
					if(textBlue.getText() == null ||textBlue.getText().trim().equals("")||
					   textRed.getText() == null||textRed.getText().trim().equals("") || textBlue.getText().equals(textRed.getText())){
						error = true;
					}
					else{
					playerBlue = new Player(false, Block.BLUE, textBlue.getText(), playerTypes[choosedTypeBlue]);
					playerYellow = new Player(false, Block.YELLOW, textRed.getText(), playerTypes[choosedTypeRed]);
					playerRed = new Player(false, Block.RED, textBlue.getText(),playerTypes[choosedTypeBlue]);
					playerGreen = new Player(false, Block.GREEN, textRed.getText(), playerTypes[choosedTypeRed]);
					}
				}else {
					if(!gameModes[choosedMode].equals(Mode.BLOKUSDUO)){
						if(textBlue.getText() == null ||textBlue.getText().trim().equals("")||
								textYellow.getText() == null||textYellow.getText().trim().equals("")|| 
								textRed.getText() == null||textRed.getText().trim().equals("")||
								textGreen.getText() == null|| textGreen.getText().trim().equals("")||
								textBlue.getText().equals(textYellow.getText())|| textBlue.getText().equals(textRed.getText())|| textBlue.getText().equals(textGreen.getText())||
								textYellow.getText().equals(textRed.getText())|| textYellow.getText().equals(textGreen.getText())||
								textRed.getText().equals(textGreen.getText())){
								
								error = true;
						}
						else{
							playerBlue = new Player(false, Block.BLUE, textBlue.getText(), playerTypes[choosedTypeBlue]);
							playerYellow = new Player(false, Block.YELLOW, textYellow.getText(), playerTypes[choosedTypeYellow]);
							playerRed = new Player(false, Block.RED, textRed.getText(),playerTypes[choosedTypeRed]);
							playerGreen = new Player(false, Block.GREEN, textGreen.getText(), playerTypes[choosedTypeGreen]);
						}
					}
					else{
						if(textBlue.getText() == null ||textBlue.getText().trim().equals("")||
							textRed.getText() == null||textRed.getText().trim().equals("")||
							textBlue.getText().equals(textRed.getText())){
							
									error = true;
							}
						else{
							playerBlue = new Player(false, Block.BLUE, textBlue.getText(), playerTypes[choosedTypeBlue]);
							playerRed = new Player(false, Block.RED, textRed.getText(),playerTypes[choosedTypeRed]);
						}
					}
					}
				Player[] players;
				
				if(error){
					labelError.setText("Please enter names \nfor each player!");
				}
				else{
				switch (gameModes[choosedMode]) {	
				case BLOKUSDUO:
					players = new Player[2];
					players[0] = playerBlue;
					players[1] = playerRed;
					break;
				default:
					players = new Player[4];
					players[0] = playerBlue;
					players[1] = playerYellow;
					players[2] = playerRed;
					players[3] = playerGreen;
					break;
				}
				
				lokusTheGame.pauseMusic();
				lokusTheGame.screenGame = new ScreenGame(lokusTheGame,
						new Game(players, gameModes[choosedMode]));
				lokusTheGame.setScreen(lokusTheGame.screenGame);
				return true;
				}return false;
				}
		});
		back = new TextButton("< back", textStyle);
		back.setPosition(100f, 100f);
		back.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				lokusTheGame.setScreen(lokusTheGame.mainMenu);
				return true;
			}
		});

		// KeyListener
		stage.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if (keycode == Keys.ESCAPE) {
					lokusTheGame.setScreen(lokusTheGame.mainMenu);
				}
				return true;
			}
		});
		
		Gdx.input.setInputProcessor(stage);

		// Finally adding every visible component to the stage
		stage.addActor(backButton);
		stage.addActor(forwardButton);
		stage.addActor(labelStandard);
		stage.addActor(backNetButton);
		stage.addActor(forwardNetButton);
		stage.addActor(labelNetwork);
		stage.addActor(labelBlue);
		stage.addActor(labelRed);
		stage.addActor(labelYellow);
		stage.addActor(labelGreen);
		stage.addActor(textBlue);
		stage.addActor(textRed);
		stage.addActor(textYellow);
		stage.addActor(textGreen);
		stage.addActor(labelError);

		stage.addActor(backBlue);
		stage.addActor(forwardBlue);
		stage.addActor(labelPlayerBlue);
		stage.addActor(backRed);
		stage.addActor(forwardRed);
		stage.addActor(labelPlayerRed);
		stage.addActor(backYellow);
		stage.addActor(forwardYellow);
		stage.addActor(labelPlayerYellow);
		stage.addActor(backGreen);
		stage.addActor(forwardGreen);
		stage.addActor(labelPlayerGreen);
		stage.addActor(play);
		stage.addActor(back);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		

		boolean redEnabled = (!gameModes[choosedMode].equals(Mode.SOLITAIR) && !gameModes[choosedMode]
				.equals(Mode.SOLITAIRBIG));

		boolean greenYellowEnabled = !gameModes[choosedMode]
				.equals(Mode.BLOKUSDUO) && redEnabled;

		
		forwardBlue.setVisible(redEnabled);
		backBlue.setVisible(redEnabled);
		if (!redEnabled) {
			choosedTypeBlue = 0;
			labelPlayerBlue.setText(playerTypes[0].toString());
		}
		labelGreen.setVisible(greenYellowEnabled);
		textGreen.setVisible(greenYellowEnabled
				&& !playerTypes[choosedTypeGreen].equals(PlayerType.NONE));
		backGreen.setVisible(greenYellowEnabled);
		forwardGreen.setVisible(greenYellowEnabled);
		labelPlayerGreen.setVisible(greenYellowEnabled);
		labelYellow.setVisible(greenYellowEnabled);
		textYellow.setVisible(greenYellowEnabled
				&& !playerTypes[choosedTypeYellow].equals(PlayerType.NONE));
		backYellow.setVisible(greenYellowEnabled);
		forwardYellow.setVisible(greenYellowEnabled);
		labelPlayerYellow.setVisible(greenYellowEnabled);
		//TODO wäre es nicht gut bei einem standardspiel mit beliebigen farben zu spielen? so ist man auf min blau und rot festgelegt
		labelRed.setVisible(redEnabled);
		textRed.setVisible(redEnabled);
		backRed.setVisible(redEnabled);
		forwardRed.setVisible(redEnabled);
		labelPlayerRed.setVisible(redEnabled);

		if(lokusTheGame.playerNamesUpdated) {
			if(lokusTheGame.gameController.exchanger.isGameStarted()) {
				if(lokusTheGame.playerNames.length >= 1) {
					textBlue.setText(lokusTheGame.playerNames[0]);
					choosedTypeBlue = 5;
					if(lokusTheGame.playerNames[0].equals("")) {textBlue.setText("Empty"); choosedTypeBlue = 6;}
					
					try {
						if(lokusTheGame.playerNames[0].equals(InetAddress.getLocalHost().getHostName())) { choosedTypeBlue = 0;}
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					textBlue.setText("Empty");
					choosedTypeBlue = 6;
					textYellow.setText("Empty");
					choosedTypeYellow = 6;
					textRed.setText("Empty");
					choosedTypeRed = 6;
					textGreen.setText("Empty");
					choosedTypeGreen = 6;
				}
				if(lokusTheGame.playerNames.length >= 2) {
						textYellow.setText(lokusTheGame.playerNames[1]);
						choosedTypeYellow = 5;
						if(lokusTheGame.playerNames[1].equals("")) {textYellow.setText("Empty"); choosedTypeYellow = 6;}
						
						try {
							if(lokusTheGame.playerNames[1].equals(InetAddress.getLocalHost().getHostName())) { choosedTypeYellow = 0;}
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
				}
				else
				{
					textYellow.setText("Empty");
					choosedTypeYellow = 6;
					textRed.setText("Empty");
					choosedTypeRed = 6;
					textGreen.setText("Empty");
					choosedTypeGreen = 6;
				}
				if(lokusTheGame.playerNames.length >= 3) {
					//if(!lokusTheGame.playerNames[2].equals("Empty")) {
						textRed.setText(lokusTheGame.playerNames[2]);
						choosedTypeRed = 5;
						if(lokusTheGame.playerNames[2].equals("")) {textRed.setText("Empty");choosedTypeRed = 6;}
						try {
							if(lokusTheGame.playerNames[2].equals(InetAddress.getLocalHost().getHostName())) { choosedTypeRed = 0;}
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					//}
				}
				else
				{
					textRed.setText("Empty");
					choosedTypeRed = 6;
					textGreen.setText("Empty");
					choosedTypeGreen = 6;
				}
				if(lokusTheGame.playerNames.length >= 4) {
					textGreen.setText(lokusTheGame.playerNames[3]);
					choosedTypeGreen = 5;
					if(lokusTheGame.playerNames[3].equals("")) {textGreen.setText("Empty"); choosedTypeGreen = 6;}
					try {
						if(lokusTheGame.playerNames[3].equals(InetAddress.getLocalHost().getHostName())) { choosedTypeGreen = 0;}
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else
				{
					textGreen.setText("Empty");
					choosedTypeGreen = 6;
				}
				if(lokusTheGame.playerNames.length == 3 && lokusTheGame.playerNames[1].equals("Empty")) {
					textYellow.setText("Empty");
					choosedTypeYellow = 6;
				}
				
			}
			else {
				textBlue.setText(privatePlayersNames[0]);
				textYellow.setText(privatePlayersNames[1]);
				textRed.setText(privatePlayersNames[2]);
				textGreen.setText(privatePlayersNames[3]);
				choosedTypeBlue = 0;
				choosedTypeYellow = 0;
				choosedTypeRed = 0;
				choosedTypeGreen = 0;
			}
			labelPlayerBlue.setText(playerTypes[choosedTypeBlue].toString());
			labelPlayerYellow.setText(playerTypes[choosedTypeYellow].toString());
			labelPlayerRed.setText(playerTypes[choosedTypeRed].toString());
			labelPlayerGreen.setText(playerTypes[choosedTypeGreen].toString());
			lokusTheGame.playerNamesUpdated = false;
		}
		
		bgBatch.begin();
		background.draw(bgBatch);
		bgBatch.end();

		batch.begin();
		batch.draw(
				logo,
				(viewport.getWorldWidth() / 2 - logo.getWidth()/2) ,
				labelStandard.getY() + labelStandard.getHeight());
		
		batch.end();

		stage.act();
		stage.draw();
	}

	@Override
	public void dispose() {
		batch.dispose();
	}

	@Override
	public void hide() {
		this.dispose();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}
}