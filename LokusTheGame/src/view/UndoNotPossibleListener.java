package view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class UndoNotPossibleListener extends InputListener{
	private ScreenGame screen;
	private ScreenUIPopup popup;
	public UndoNotPossibleListener(ScreenGame screen, ScreenUIPopup popup){
		super();
		this.screen = screen;	
		this.popup = popup;
	}
	public boolean touchDown(InputEvent event, float posX, float posY, int pointer, int button){
		Gdx.app.log("ScreenUIPopup", "Notification closed.");
		popup.dispose();
        screen.lokusTheGame.setScreen(screen);
        return true;
	}
}