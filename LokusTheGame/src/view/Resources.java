package view;

import model.Block;

import java.util.Hashtable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * TextureResources presaves the ressources (fonts, textures)
 * @author sopr049
 *
 */
public class Resources {
	
	public static Resources instance;
	public TextureAtlas atlas, blocks, popup;
	public Sprite background, backgroundPopup, logo, logoSmall;
	public Sprite backgroundButtonBig, backgroundButton, backgroundButtonNormal, backgroundButtonHover, backgroundButtonSelected, backgroundButtonSave, cursorInput;
	public Image backgroundPopupText;
	public Hashtable<Block, Texture> blockTextures;
	public Hashtable<String, Texture> stringTextures;
	public Hashtable<String, Sound> sounds;
	
	public Animation walkUp, walkLeft, walkDown, walkRight;
	public BitmapFont fontSmallWhite, fontSmallColor, fontSmallGreen, fontSmallRed, fontSmallYellow, fontSmallBlue;
	public BitmapFont fontBigWhite, fontBigColor, fontBigGreen, fontBigRed, fontBigYellow, fontBigBlue;
	public BitmapFont fontNormalWhite, fontNormalColor, fontNormalGreen, fontNormalRed, fontNormalYellow, fontNormalBlue;
	
	public Sound place_block;
	public Sound incorrect_turn;
	private Sound reload;
	private Sound shoot;
	private Sound mainMenu_selectedExplosion;
	
	public static Resources getInstance()	{
		if(instance == null)	{
			instance = new Resources();
		}
		return instance;
	}
	
	/**
	 * default constructor with loading textures
	 */
	public Resources() {
		init();		
	}
	
	/**
	 * initialization of texture instances
	 */
	public void init() {
		dispose();
		//load textures first
		atlas = new TextureAtlas(Gdx.files.internal("res/packs/menu/menu.pack"));
		blocks = new TextureAtlas(Gdx.files.internal("res/packs/blocks/blocks.pack"));
		popup = new TextureAtlas(Gdx.files.internal("res/packs/popup/popup.pack"));
		
		blockTextures = new Hashtable<Block, Texture>();
		stringTextures = new Hashtable<String, Texture>();
		sounds = new Hashtable<String, Sound>();
		
		//initialize sprites
		initMenuPack();
		
		//initialize single textures
		initTableTexture();
		
		//initialize fonts
		initFontPack();		
		
		//initialize sounds
		initSounds();
	}
	
	/**
	 * help method for initializing menu texture pack
	 */
	private void initMenuPack() {
		background = atlas.createSprite("menuBackground");
		logo = atlas.createSprite("menuLogoBig");
		logoSmall = atlas.createSprite("menuLogoSmall");
		cursorInput = atlas.createSprite("cursor_input");
		background = atlas.createSprite("menuBackground");
		backgroundPopup = atlas.createSprite("popupBackground");
		logo = atlas.createSprite("menuLogoBig");
		logoSmall = atlas.createSprite("menuLogoSmall");
		backgroundButtonBig = atlas.createSprite("menuButtonBackgroundBig");
		backgroundButton = atlas.createSprite("menuButtonBackground");
		backgroundButtonNormal = atlas.createSprite("menuButtonBackgroundBigNormal");
		backgroundButtonHover = atlas.createSprite("menuButtonBackgroundBigHover");
		backgroundButtonSelected = atlas.createSprite("menuButtonBackgroundBigSelected");
		backgroundPopupText = new Image(new Texture(Gdx.files.internal("res/packs/popup-src/popupBackground.png")));
		backgroundButtonSave = atlas.createSprite("btnSave");
		walkUp = new Animation(0.2f, atlas.findRegion("menuArrowTop"));
		walkLeft = new Animation(0.2f, atlas.findRegions("menuArrowLeft"));
		walkDown = new Animation(0.2f, atlas.findRegion("menuArrowBottom"));
		walkRight = new Animation(0.2f, atlas.findRegions("menuArrowRight"));
	}
	
	/**
	 * help method for initializing table texture pack
	 */
	private void initTableTexture() {
		blockTextures.put(Block.BLUE, new Texture(Gdx.files.internal("res/packs/blocks-src/block_blue.png")));
		blockTextures.put(Block.GREEN, new Texture(Gdx.files.internal("res/packs/blocks-src/block_green.png")));
		blockTextures.put(Block.RED, new Texture(Gdx.files.internal("res/packs/blocks-src/block_red.png")));
		blockTextures.put(Block.YELLOW, new Texture(Gdx.files.internal("res/packs/blocks-src/block_yellow.png")));
		blockTextures.put(Block.EMPTY, new Texture(Gdx.files.internal("res/packs/blocks-src/block_empty.png")));
		//backgroundPopupText.setScale(2);
		stringTextures.put("field", new Texture(Gdx.files.internal("res/textures/fields/fieldBackground.png")));
		stringTextures.put("background", new Texture(Gdx.files.internal("res/textures/misc/background.png")));
		stringTextures.put("fow_1", new Texture(Gdx.files.internal("res/packs/fow-src/fow_1.png")));
		stringTextures.put("fow_2", new Texture(Gdx.files.internal("res/packs/fow-src/fow_2.png")));
		stringTextures.put("fow_3", new Texture(Gdx.files.internal("res/packs/fow-src/fow_3.png")));
		stringTextures.put("fow_4", new Texture(Gdx.files.internal("res/packs/fow-src/fow_4.png")));
	}
	
	/**
	 * help method for initializing font pack
	 */
	private void initFontPack() {
		fontSmallWhite = new BitmapFont(Gdx.files.internal("res/fonts/fontSmallWhite.fnt"),false);
		fontSmallColor = new BitmapFont(Gdx.files.internal("res/fonts/fontSmallColor.fnt"),false);
		fontSmallGreen = new BitmapFont(Gdx.files.internal("res/fonts/fontSmallGreen.fnt"),false);
		fontSmallRed = new BitmapFont(Gdx.files.internal("res/fonts/fontSmallRed.fnt"),false);
		fontSmallYellow = new BitmapFont(Gdx.files.internal("res/fonts/fontSmallYellow.fnt"),false);
		fontSmallBlue = new BitmapFont(Gdx.files.internal("res/fonts/fontSmallBlue.fnt"),false);
		fontBigWhite = new BitmapFont(Gdx.files.internal("res/fonts/fontBigWhite.fnt"),false);
		fontBigColor = new BitmapFont(Gdx.files.internal("res/fonts/fontBigColor.fnt"),false);
		fontBigGreen = new BitmapFont(Gdx.files.internal("res/fonts/fontBigGreen.fnt"),false);
		fontBigRed = new BitmapFont(Gdx.files.internal("res/fonts/fontBigRed.fnt"),false);
		fontBigYellow = new BitmapFont(Gdx.files.internal("res/fonts/fontBigYellow.fnt"),false);
		fontBigBlue = new BitmapFont(Gdx.files.internal("res/fonts/fontBigBlue.fnt"),false);
		fontNormalWhite = new BitmapFont(Gdx.files.internal("res/fonts/fontNormalWhite.fnt"),false);
		fontNormalColor = new BitmapFont(Gdx.files.internal("res/fonts/fontNormalColor.fnt"),false);
		fontNormalGreen = new BitmapFont(Gdx.files.internal("res/fonts/fontNormalGreen.fnt"),false);
		fontNormalRed = new BitmapFont(Gdx.files.internal("res/fonts/fontNormalRed.fnt"),false);
		fontNormalYellow = new BitmapFont(Gdx.files.internal("res/fonts/fontNormalYellow.fnt"),false);
		fontNormalBlue = new BitmapFont(Gdx.files.internal("res/fonts/fontNormalBlue.fnt"),false);
	}
	
	private void initSounds() {
		place_block = Gdx.audio.newSound(Gdx.files.internal("res/sfx/place_block.wav"));
		incorrect_turn = Gdx.audio.newSound(Gdx.files.internal("res/sfx/incorrect_turn.wav"));
		reload = Gdx.audio.newSound(Gdx.files.internal("res/sfx/reload.wav"));
		shoot = Gdx.audio.newSound(Gdx.files.internal("res/sfx/shoot.wav"));
		mainMenu_selectedExplosion = Gdx.audio.newSound(Gdx.files.internal("res/sfx/mainMenu_selectedExplosion.wav"));
		
		sounds.put("place_block", place_block);
		sounds.put("incorrect_turn", incorrect_turn);
		sounds.put("reload", reload);
		sounds.put("shoot", shoot);
		sounds.put("mainMenu_selectedExplosion",mainMenu_selectedExplosion);
	}
	
	/**
	 * deletes texture atlas from memory
	 */
	public void dispose()	{
		if(atlas != null) {
			atlas.dispose();
		}
		if(blocks != null)	{
			blocks.dispose();
		}
		if(popup != null) {
			popup.dispose();
		}
	}
}
