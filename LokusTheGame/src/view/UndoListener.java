package view;

import java.util.ArrayList;
import java.util.List;

import model.Block;
import model.Brick;
import model.Mode;
import model.Turn;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import control.FirstRoundException;

public class UndoListener extends InputListener{
	private ScreenGame screen ;
	private ScreenUIPopup popup;
	public UndoListener(ScreenGame screen, ScreenUIPopup popup){
		super();
		this.popup = popup;
		this.screen = screen;		
	}
	public boolean touchDown(InputEvent event, float posX, float posY, int pointer, int button){
		try {
			LokusTheGame lokusTheGame = screen.lokusTheGame;
			ArrayList<UIBrickGroup> brickgroups = screen.getBrickGroups();
			List<Turn> turns = lokusTheGame.gameController.getGame()
					.getTurns();

			if (lokusTheGame.gameController.getGame().getMode() != Mode.SOLITAIR
					&& lokusTheGame.gameController.getGame().getMode() != Mode.SOLITAIRBIG) {
				// not solitair
				if (turns.size() >= 4) {// lokusTheGame.gameController.getGame().getPlayers().length)
										// {
					int offset = 0;

					for (int i = turns.size() - 1; i > turns.size() - 5; i--) {
						Turn turn = turns.get(i);

						Brick brick = turn.getBrick();
						Block playerColor = turn.getPlayer().getColor();

						if (brick != null) {
							int playerIndex = (lokusTheGame.gameController
									.getGame().getTurns().size() - 1 - offset)
									% lokusTheGame.gameController
											.getGame().getPlayers().length;
							brickgroups.get(playerIndex).addBrick(
									new UIBrick(screen, brick,
											playerColor, brickgroups
													.get(playerIndex),
											0, 0, 1f));
							brickgroups.get(playerIndex).adjust();
						}
						offset++;
					}
				}

			} 
			else {
				// solitair
				while (turns.size() > 0) {
					Turn turn = turns.get(turns.size() - 1);

					Brick brick = turn.getBrick();
					Block playerColor = turn.getPlayer().getColor();

					if (brick != null) {
						int playerIndex = (lokusTheGame.gameController
								.getGame().getTurns().size() - 1)
								% lokusTheGame.gameController.getGame()
										.getPlayers().length;
						brickgroups.get(playerIndex).addBrick(
								new UIBrick(screen, brick,
										playerColor, brickgroups
												.get(playerIndex), 0,
										0, 1f));
						brickgroups.get(playerIndex).adjust();
						break;
					}
					else {
						lokusTheGame.gameController.getTurnController().undoTurn();
					}		
				}
				lokusTheGame.gameController.getGame().getPlayers()[0].setCheaterFlag(true);
				lokusTheGame.gameController.getGame().getPlayers()[1].setCheaterFlag(true);
				lokusTheGame.gameController.getGame().getPlayers()[2].setCheaterFlag(true);
				lokusTheGame.gameController.getGame().getPlayers()[3].setCheaterFlag(true);
			}
			screen.removeHintBrick();
			lokusTheGame.gameController.getTurnController().undoTurn();
			lokusTheGame.gameController.getCurrentPlayer().setCheaterFlag(true);
			screen.getField().updateField();

		} catch (FirstRoundException e) {
		}
		popup.dispose();
        screen.lokusTheGame.setScreen(screen);
		return true;
	}
}
