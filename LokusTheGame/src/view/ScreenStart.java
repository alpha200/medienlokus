package view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquation;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;
import model.Block;
import model.Brick;

/**
 * Splash screen on start of the game
 * @author sopr049
 *
 */
public class ScreenStart extends DefaultScreen {

	/* Batches for drawing scaled background and logo */
	private SpriteBatch batch, bgBatch;
	/* Tween manager for animation */
	private TweenManager manager;
	/* instances of background and logo */
	private Sprite background, logo;
	/* instance of skin for button */
	private Skin buttonSkin;
	/* single button */
	private Button button;
	/* instances of rotating blocks */
	private UIBrick brick, brick2;
	/* text button style */
	private TextButtonStyle style;
	
	/**
	 * ScreenStart default constructor, sets up the first screen of the game
	 * @param game
	 */
	public ScreenStart(LokusTheGame game) {
		super(game);
		
	}

	/**
	 * show method
	 */
	@Override
	public void show() {
		
		lokusTheGame.startMusic();
		batch = new SpriteBatch();
		bgBatch = new SpriteBatch();
		manager = new TweenManager(); 
		background = Resources.getInstance().background;
		logo = Resources.getInstance().logo;
		buttonSkin = new Skin();
		style = new TextButtonStyle(); 
        style.font = Resources.getInstance().fontNormalColor;
		button = new TextButton("please click", style);
        brick = new UIBrick(this, Brick.getStandardBrick(3), Block.BLUE, 250, 250, 1f);
        brick2 = new UIBrick(this, Brick.getStandardBrick(16), Block.RED, lokusTheGame.DISPLAY_WIDTH - 350, 250, 1f);
        
        Tween.registerAccessor(Sprite.class, new SpriteTween());
        Tween.registerAccessor(TextButton.class, new TextButtonTween());
        Tween.registerAccessor(UIBrick.class, new UIBrickTween());
		
		batch = new SpriteBatch();
		bgBatch = new SpriteBatch();
		batch.setProjectionMatrix(stage.getBatch().getProjectionMatrix());
		batch.setTransformMatrix(stage.getBatch().getTransformMatrix());
		bgBatch.setProjectionMatrix(stage.getBatch().getProjectionMatrix());
		bgBatch.setTransformMatrix(stage.getBatch().getTransformMatrix());
		
		batch.getProjectionMatrix().setToOrtho2D(0, 0, Resources.getInstance().background.getWidth(), Resources.getInstance().background.getHeight());
		bgBatch.getProjectionMatrix().setToOrtho2D(0, 0, Resources.getInstance().background.getWidth(), Resources.getInstance().background.getHeight());

		// TODO Auto-generated method stub
		logo.setColor(1,1,1,0);
		logo.setX((background.getWidth() / 2) - logo.getWidth() / 2);
		logo.setY(background.getHeight()/2);
		
		buttonSkin.addRegions(Resources.getInstance().atlas);
		style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
        style.down = buttonSkin.getDrawable("menuButtonBackgroundBigSelected");
        style.over = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
       
        //button = new TextButton("press enter", style); //** Button text and style **//
        button.setPosition((background.getWidth() / 2) - Resources.getInstance().backgroundButton.getWidth() / 2, background.getHeight()/2 - Resources.getInstance().backgroundButton.getHeight() - 50); //** Button location **//
        button.setHeight(Resources.getInstance().backgroundButton.getHeight()); //** Button Height **//
        button.setWidth(Resources.getInstance().backgroundButton.getWidth()); //** Button Width **//
        button.setColor(1,1,1,0.5f);
        
        stage.addActor(brick);
        stage.addActor(brick2);
        
		TweenCallback tweenCallback = new TweenCallback() {
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				Timeline.createSequence()
					.push(Tween
						.to(button, SpriteTween.ALPHA, 0.5f)
						.target(1f)
						.ease(TweenEquations.easeInQuad))
					.push(Tween.to(button, SpriteTween.ALPHA, 0.5f)
						.target(0.4f)
						.ease(TweenEquations.easeOutQuad))
					.repeatYoyo(Tween.INFINITY, 0.0f)
					.start(manager);
				
				/**
				Timeline.createSequence()
					.push(Tween
						.to(brick, UIBrickTween.ROTATION, 2f)
						.target(360)
						.ease(TweenEquations.easeInOutCirc))
					.push(Tween
						.to(brick, UIBrickTween.ROTATION, 2f)
						.target(0)
						.ease(TweenEquations.easeInOutCirc))
					.repeatYoyo(Tween.INFINITY, 0.0f)
					.start(manager);
				
				Timeline gg = Timeline.createSequence()
					.push(Tween
						.to(brick2, UIBrickTween.ROTATION, 2f)
						.target(360)
						.ease(TweenEquations.easeInOutCirc))
					.push(Tween
						.to(brick2, UIBrickTween.ROTATION, 2f)
						.target(0)
						.ease(TweenEquations.easeInOutCirc))
					.repeatYoyo(Tween.INFINITY, 0.0f)
					.start(manager);
				**/
				
				Timeline.createSequence().push(Tween
					.to(brick, UIBrickTween.POS_XY, 0.3f)
					.target(brick.getX()+50, brick.getY())
					.ease(TweenEquations.easeInExpo)
				).push(Tween
					.to(brick, UIBrickTween.POS_XY, 0.6f)
					.target(brick.getX()-150, brick.getY())
					.ease(TweenEquations.easeInExpo)						
				).repeatYoyo(Tween.INFINITY, 0.0f)
				.start(manager);
				
				Timeline.createSequence().push(Tween
						.to(brick2, UIBrickTween.POS_XY, 0.7f)
						.target(brick2.getX()-150, brick2.getY())
						.ease(TweenEquations.easeInExpo)
					).push(Tween
						.to(brick2, UIBrickTween.POS_XY, 0.5f)
						.target(brick2.getX()+50, brick2.getY())
						.ease(TweenEquations.easeInExpo)						
					).repeatYoyo(Tween.INFINITY, 0.0f)
					.start(manager);
				
			}
		};
		
		BaseTween tt = Tween.to(logo,SpriteTween.ALPHA, 0.5f)
			.target(1)
			.ease(TweenEquations.easeInQuad)
			.setCallback(tweenCallback)
			.setCallbackTriggers(TweenCallback.COMPLETE)
			.start(manager);
	
		TweenManager.setAutoRemove(tt, true);
	}
	
	/**
	 * render function
	 */
	@Override
	public void render(float delta) {
		
		clearScreen();
		if(Gdx.input.isTouched() || Gdx.input.isKeyJustPressed(Keys.ENTER))	{
			Gdx.app.log("ScreenStart", "Start screen closed.");
			lokusTheGame.setScreen(new ScreenMainMenu(lokusTheGame));
		}
		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			Gdx.app.log("ScreenStart", "Game closed properly.");
			lokusTheGame.dispose();
			System.exit(1);
		}
		
		manager.update(delta);
		bgBatch.begin();
		background.draw(bgBatch);
		bgBatch.end();
		
		batch.begin();
		logo.draw(batch);
		button.draw(batch, 1);
		batch.end();
		
		stage.draw();		
	}

	@Override
	public void hide() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		batch.dispose();
	}

}
