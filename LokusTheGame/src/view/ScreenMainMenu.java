package view;
import java.io.IOException;

import model.Game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

/**
 * 
 * @author sopr049
 *
 */
public class ScreenMainMenu extends DefaultScreen {

	private SpriteBatch batch, bgBatch;
	private Sprite background, logo;
	private Skin buttonSkin;
	private Button buttonSingle, buttonMulti, buttonQuit, buttonLoad;
	private Table table;
	private TextButtonStyle style;
	private String[] options = {"play", "load", "score", "quit"};
	private int selectedOption = 0;
	private boolean bgMusicOn = false;
	private TextButtonStyle styleBtnMute;
	private TextButtonStyle styleBtnUnmute;
	private Label lblMute;
	private TextButton btnMute;
	private Label labelStandard;
	LabelStyle labelStyle = new LabelStyle();
	private Button backButton;
	private Button forwardButton;
	private int volume = 2;
	
	
	public ScreenMainMenu(LokusTheGame game) {
		super(game);
		
	}
	
	@Override
	public void show() {
		Resources.getInstance().init();	
		lokusTheGame.startMusic();
		batch = new SpriteBatch();
		bgBatch = new SpriteBatch();
		background = Resources.getInstance().background;
		logo = Resources.getInstance().logo;
		table = new Table();
		buttonSkin = new Skin();
		style = new TextButtonStyle(); 
		style.font = Resources.getInstance().fontBigColor;
		buttonSingle = new TextButton("play", style);
		
		batch.setProjectionMatrix(stage.getBatch().getProjectionMatrix());
		batch.setTransformMatrix(stage.getBatch().getTransformMatrix());
		bgBatch.setProjectionMatrix(stage.getBatch().getProjectionMatrix());
		bgBatch.setTransformMatrix(stage.getBatch().getTransformMatrix());
		
		batch.getProjectionMatrix().setToOrtho2D(0, 0, Resources.getInstance().background.getWidth(), Resources.getInstance().background.getHeight());
		bgBatch.getProjectionMatrix().setToOrtho2D(0, 0, Resources.getInstance().background.getWidth(), Resources.getInstance().background.getHeight());
		
		logo.setColor(1,1,1,1);
		logo.setX((background.getWidth() / 2) - logo.getWidth() / 2);
		logo.setY(background.getHeight()/2);
		
		buttonSkin.addRegions(Resources.getInstance().atlas);
		//** Button properties **//
		style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
        style.down = buttonSkin.getDrawable("menuButtonBackgroundBigSelected");
        style.over = buttonSkin.getDrawable("menuButtonBackgroundBigHover");  
        
        //Sound Componenten Button/set Volume
        
        
        
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.background = buttonSkin.getDrawable("menuButtonBackground");
        labelStyle.font = Resources.getInstance().fontNormalColor;
        
        styleBtnMute = new TextButtonStyle(); // ** Button properties **//
		styleBtnMute.up = buttonSkin.getDrawable("btnMute");
		styleBtnMute.font = Resources.getInstance().fontNormalBlue;
		
		styleBtnUnmute = new TextButtonStyle(); // ** Button properties **//
		styleBtnUnmute.up = buttonSkin.getDrawable("btnUnmute");
		styleBtnUnmute.font = Resources.getInstance().fontNormalBlue;
		
		lblMute = new Label("Mute", labelStyle);
		lblMute.setAlignment(Align.center);
		
		if(lokusTheGame.isMuted){
			lblMute = new Label("Unmute", labelStyle);
			lblMute.setAlignment(Align.center);
		    btnMute = new TextButton("", styleBtnUnmute);
		}
		else{
			lblMute = new Label("Mute", labelStyle);
			lblMute.setAlignment(Align.center);
			btnMute = new TextButton("", styleBtnMute);
		}
		btnMute.setPosition(100f, 180f);
		btnMute.setWidth(120f);
		btnMute.setHeight(120f);
		lblMute.setPosition(btnMute.getX(), btnMute.getY()-lblMute.getHeight());
		lblMute.setVisible(false);
		btnMute.setVisible(true);
		
		// Buttons and labels for choosing Volume Type
        labelStyle.background = buttonSkin.getDrawable("menuButtonBackground");
		labelStyle.font = Resources.getInstance().fontNormalColor;
        labelStandard = new Label(""+volume, labelStyle);
        
 		labelStandard.setWidth(225f);
 		Float choosePosX = btnMute.getX()-btnMute.getWidth()/2;
 		Float choosePosY = btnMute.getY() - labelStandard.getHeight();
 		labelStandard.setPosition(choosePosX,choosePosY);
 		labelStandard.setAlignment(Align.center, Align.center);

 		ButtonStyle backStyle = new ButtonStyle();
 		backStyle.up = buttonSkin.getDrawable("menuArrowLeft");
 		backButton = new Button(backStyle);
 		backButton.setPosition(
 				labelStandard.getX()-backButton.getWidth(),labelStandard.getY()+20);
 		backButton.addListener(new InputListener() {
 			@Override
 			public boolean touchDown(InputEvent event, float x, float y,
 					int pointer, int button) {
 				volume--;
 				lokusTheGame.setBgVolume(volume);
 				if (volume <= 0)
 					volume= 10;
 					lokusTheGame.setBgVolume(volume);
 				labelStandard.setText(""+volume);
 				lokusTheGame.pauseMusic();
 				lokusTheGame.startMusic();
 				return true;
 			}
 		});

 		ButtonStyle forwardStyle = new ButtonStyle();
 		forwardStyle.up = buttonSkin.getDrawable("menuArrowRight");
 		forwardButton = new Button(forwardStyle);
 		forwardButton.setPosition(labelStandard.getX()+labelStandard.getWidth(),labelStandard.getY()+20);
 		forwardButton.addListener(new InputListener() {
 			@Override
 			public boolean touchDown(InputEvent event, float x, float y,
 					int pointer, int button) {
 				volume++;
 				lokusTheGame.setBgVolume(volume);
 				if (volume > 10)
 					volume=1;
 					lokusTheGame.setBgVolume(volume);
 				labelStandard.setText(""+volume);
 				lokusTheGame.pauseMusic();
 				lokusTheGame.startMusic();
 				return true;
 			}
 		});
		
		btnMute.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(!lokusTheGame.isMuted){
					btnMute.setStyle(styleBtnUnmute);
					lblMute.setText("Unmute");
					lokusTheGame.muteMusic();;
				}
				else{
					btnMute.setStyle(styleBtnMute);
					lblMute.setText("Mute");
					lokusTheGame.unMuteMusic();
				}
				return true;
			}

		});

        buttonSingle.setHeight(Resources.getInstance().backgroundButton.getHeight()); 
        buttonSingle.setWidth(Resources.getInstance().backgroundButton.getWidth());
        buttonSingle.addListener(new InputListener() {
        	@Override
            public boolean touchDown (InputEvent event, float posX, float posY, int pointer, int button) {
            	Gdx.app.log("ScreenMainMenu", "Play game."); 
                lokusTheGame.setScreen(new ScreenGameConfiguration(lokusTheGame));
                return true;
            }
        });
        
        buttonMulti = new TextButton("score", style); //** Button text and style **//
        buttonMulti.setHeight(Resources.getInstance().backgroundButton.getHeight()); //** Button Height **//
        buttonMulti.setWidth(Resources.getInstance().backgroundButton.getWidth()); //** Button Width **//
        buttonMulti.addListener(new InputListener() {
        	@Override
            public boolean touchDown (InputEvent event, float posX, float posY, int pointer, int button) {
            	Gdx.app.log("ScreenMainMenu", "Highscore screen loaded."); 
                lokusTheGame.setScreen(lokusTheGame.highscoreScreen);
                return true;
            }
        });
        
        buttonLoad = new TextButton("load", style); //** Button text and style **//
        buttonLoad.setHeight(Resources.getInstance().backgroundButton.getHeight()); //** Button Height **//
        buttonLoad.setWidth(Resources.getInstance().backgroundButton.getWidth()); //** Button Width **//
        buttonLoad.addListener(new InputListener() {
        	@Override
            public boolean touchDown (InputEvent event, float posX, float posY, int pointer, int button) {
            	Gdx.app.log("ScreenMainMenu", "Game loaded."); 
                Game loadedGame;
				try {
					loadedGame = lokusTheGame.gameController.getiOController().loadGame();
					lokusTheGame.screenGame = new ScreenGame(lokusTheGame, loadedGame);
					lokusTheGame.pauseMusic();
					lokusTheGame.setScreen(lokusTheGame.screenGame);
				} catch (NullPointerException e) {
					System.out.println("NPE");
				} catch (ClassNotFoundException e) {
					System.out.println("CNF");
				} catch (IOException e) {
					System.out.println("IOE");
				}
                return true;
            }
        });
        
        buttonQuit = new TextButton("quit", style);
        buttonQuit.setHeight(Resources.getInstance().backgroundButton.getHeight());
        buttonQuit.setWidth(Resources.getInstance().backgroundButton.getWidth());
        buttonQuit.addListener(new InputListener() {
        	@Override
            public boolean touchDown (InputEvent event, float posX, float posY, int pointer, int button) {
                    Gdx.app.log("ScreenMainMenu", "Game closed."); 
                    dispose();
                    System.exit(1);
                    return true;
            }
        });
        
        table.setBounds((viewport.getWorldWidth()/2) - 200, viewport.getWorldHeight()/2 - 350, 400, 200);
        table.add(buttonSingle).width(400).align(Align.center);
        table.add(buttonLoad).width(400).align(Align.center);
        table.row();
        table.add(buttonMulti).width(400).align(Align.center);
        table.add(buttonQuit).width(400).align(Align.center);
        table.row();
        
        backButton.setVisible(true);
        forwardButton.setVisible(true);
        labelStandard.setVisible(true);
        
        stage.addActor(table);
        stage.addActor(btnMute);
        stage.addActor(lblMute);
        stage.addActor(backButton);
        stage.addActor(forwardButton);
        stage.addActor(labelStandard);
        
    	
        Gdx.input.setInputProcessor(stage);		
	}
	
	@Override
	public void render(float delta) {
		clearScreen();
				
		bgBatch.begin();
		background.draw(bgBatch);
		bgBatch.end();
		
		batch.begin();
		logo.draw(batch);
		batch.end();
		
		stage.act(delta);        
        stage.draw();
		
		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			dispose();
			System.exit(1);
		}
		
		if(Gdx.input.isKeyPressed(Keys.ENTER))	{			
			switch(options[selectedOption]) {
			case "play": 
				lokusTheGame.setScreen(new ScreenGameConfiguration(lokusTheGame));
				break;
			case "load":
				lokusTheGame.setScreen(lokusTheGame.screenGame);
			case "score":
				lokusTheGame.setScreen(lokusTheGame.highscoreScreen);
				break;
			case "quit":
				dispose();
				System.exit(1);
				break;
			}
		}
		
		if(Gdx.input.isKeyPressed(Keys.DOWN)) {	
			//Altes button deaktivieren
			if("play".equals(options[selectedOption])) {
				ButtonStyle style = buttonSingle.getStyle();
				style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
				//buttonSingle.setStyle(style);
				
				selectedOption = (selectedOption + 1) % 3;
				
				style = buttonLoad.getStyle();
				//style.up = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
				//buttonLoad.setStyle(style);
				
			}
			else if("load".equals(options[selectedOption])) {
				ButtonStyle style = buttonLoad.getStyle();
				style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
				//buttonLoad.setStyle(style);
				
				selectedOption = (selectedOption + 1) % 3;
				
				style = buttonMulti.getStyle();
				//style.up = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
				//buttonMulti.setStyle(style);
				
			}
			else if("score".equals(options[selectedOption])) {
				ButtonStyle style = buttonMulti.getStyle();
				style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
				//buttonMulti.setStyle(style);
				
				selectedOption = (selectedOption + 1) % 3;
				
				style = buttonQuit.getStyle();
				//style.up = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
				//buttonQuit.setStyle(style);
			}
			else if("quit".equals(options[selectedOption])) {
				ButtonStyle style = buttonQuit.getStyle();
				style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
				//buttonQuit.setStyle(style);
				
				selectedOption = (selectedOption + 1) % 3;
				
				style = buttonSingle.getStyle();
				//style.up = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
				//buttonSingle.setStyle(style);
			}
		}
		if(Gdx.input.isKeyPressed(Keys.UP)) {
			//Altes button deaktivieren
			if("play".equals(options[selectedOption])) {
				ButtonStyle style = buttonSingle.getStyle();
				style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
				//buttonSingle.setStyle(style);
				
				if(selectedOption == 0) { selectedOption = 3;}
				selectedOption = (selectedOption -1) % 3;
				
				style = buttonQuit.getStyle();
				//style.up = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
				//buttonQuit.setStyle(style);
				
			}
			else if("load".equals(options[selectedOption])) {
				ButtonStyle style = buttonLoad.getStyle();
				style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
				//buttonLoad.setStyle(style);
				
				if(selectedOption == 0) { selectedOption = 3;}
				selectedOption = (selectedOption -1) % 3;
				
				style = buttonSingle.getStyle();
				//style.up = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
				//buttonSingle.setStyle(style);
				
			}
			else if("score".equals(options[selectedOption])) {
				ButtonStyle style = buttonMulti.getStyle();
				style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
				//buttonMulti.setStyle(style);
				
				if(selectedOption == 0) { selectedOption = 3;}
				selectedOption = (selectedOption -1) % 3;
				
				style = buttonLoad.getStyle();
				//style.up = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
				//buttonLoad.setStyle(style);
			}
			else if("quit".equals(options[selectedOption])) {
				ButtonStyle style = buttonQuit.getStyle();
				style.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
				//buttonQuit.setStyle(style);
				
				if(selectedOption == 0) { selectedOption = 3;}
				selectedOption = (selectedOption -1) % 3;
				
				style = buttonMulti.getStyle();
				//style.up = buttonSkin.getDrawable("menuButtonBackgroundBigHover");
				//buttonMulti.setStyle(style);
			}
		}
	}
	
	
	@Override
	public void hide() {
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {}
	@Override
	public void dispose() {
		batch.dispose();
		stage.dispose();
		lokusTheGame.dispose();
	}

}
