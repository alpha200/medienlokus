package view;

import model.Block;
import model.Brick;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class UIBrick extends Group {	
	Texture blockTexture;
	
	float opacity = 1f;
	
	public UIBrickGroup group;
	
	public boolean grabbed = false;
	private boolean grabbedSound = true;
	private Brick brick;
	private float scale;
	
	public UIBrick(DefaultScreen screen, Brick brick, Block color, int xPos, int yPos, float scale) {
		this(screen, brick, color, null, xPos, yPos, scale);
	}
	
	public UIBrick(final DefaultScreen screen, Brick brick, Block color, UIBrickGroup group, int xPos, int yPos, float scale) {
		this.group = group;
		this.brick = brick;
		this.scale = scale;
		
		this.setPosition(xPos, yPos);

		blockTexture = Resources.getInstance().blockTextures.get(color);
		
		float width, height;
		width = blockTexture.getWidth() * brick.getCoord().length;
		height = blockTexture.getHeight() * brick.getCoord()[0].length;
		
		updateBrick();
		this.setSize(width, height);
		
		this.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int buttons) {
				if(buttons == Input.Buttons.LEFT && screen instanceof ScreenGame) {
					ScreenGame screenGame = (ScreenGame) screen;
					screenGame.grabBrick(UIBrick.this);
					switch(screen.lokusTheGame.gameController.getGame().getMode()){
						case CUSTOM:
							if(grabbedSound){
								Resources.getInstance().sounds.get("reload").play(screenGame.lokusTheGame.soundVolume);
								grabbedSound = false;
							}
							else{
								Resources.getInstance().sounds.get("place_block").play(screenGame.lokusTheGame.soundVolume);
								grabbedSound = true;
							}
							break;
						default:
							Resources.getInstance().sounds.get("place_block").play(screenGame.lokusTheGame.soundVolume);
							break;
					}
					
					return true;
				}
				//wenn wir nicht im game screen sind, wird jeder input ignoriert
				return false;
			}
		});
	}

	private void updateBrick() {
		this.clearChildren();
		for (int x = 0; x < brick.getCoord().length; x++) {
			for (int y = 0; y < brick.getCoord()[0].length; y++) {
				if (brick.getCoord()[x][brick.getCoord()[0].length - 1 - y]) {
					this.addActor(new UIBlock(this,
							x * (blockTexture.getWidth() * scale),
							y * (blockTexture.getHeight() * scale),
							scale));
				}
			}
		}
	}
	
	public void rotateBrick() {
		brick.rotateBrick();
		updateBrick();
	}
	
	public void mirrorBrick() {
		brick.mirrorBrick();
		updateBrick();
	}
	
	public Brick getBrick() {
		return this.brick;
	}
	
	@Override
	public void setScale(float scaleXY) {
		super.setScale(scaleXY);
	}

	private class UIBlock extends Actor {
		
		public UIBlock(UIBrick pbrick, float x, float y, float scale) {
			super();
			setScale(scale, scale);
			setBounds(x, y, blockTexture.getWidth(),
					blockTexture.getHeight());
			
		}

		public void draw(Batch batch, float alpha) {
			batch.setColor(1, 1, 1, opacity);
			batch.draw(blockTexture, getX(), getY(),
					blockTexture.getWidth() * this.getScaleX(),
					blockTexture.getHeight() * this.getScaleY());
		}
	}
	
	@Override
	public void setColor(Color color) {
		opacity = color.a;
	}
}