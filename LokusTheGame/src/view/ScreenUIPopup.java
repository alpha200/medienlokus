package view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Align;

/**
 * Popup object shown on top of actual screen
 * @author sopr049
 *
 */
public class ScreenUIPopup extends DefaultScreen {

	private Image background;
	private Skin buttonSkin;
	private LabelStyle style;
	private String title, message;	
	private DefaultScreen fromScreen;
	private TextButton buttonOK, buttonCancel;
	private Label labelTitle, labelMessage;
	
	
	/**
	 * ScreenUIPopup constructor sets up the popup and shows up on the display
	 * @param title as title text
	 * @param message as message text
	 * @param fromScreen as return screen to response on ok button
	 */
	public ScreenUIPopup(String title, String message, final DefaultScreen fromScreen) {
		//Set up the object
		super(fromScreen.lokusTheGame);
		this.title = title;
		this.message = message;
		this.fromScreen = fromScreen;
		
		//defines the drawing options
		background = Resources.getInstance().backgroundPopupText;
		buttonSkin = new Skin();
		style = new LabelStyle();	
		background.setHeight((float)450);
		background.setBounds((viewport.getWorldWidth() / 2) - (background.getWidth() / 2) , (viewport.getWorldHeight() / 2) - (background.getHeight() / 2), background.getWidth(), background.getHeight());		
		
		buttonSkin.addRegions(Resources.getInstance().atlas);
		style.background = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");		
		style.font = Resources.getInstance().fontNormalRed;
		
		labelTitle = new Label(this.title, style);
		labelTitle.setPosition((viewport.getWorldWidth() / 2) -  (labelTitle.getWidth() / 2), (viewport.getWorldHeight() / 2) + (background.getHeight() / 2)  - 100);
		labelTitle.setAlignment(Align.center);
		
		style.font = Resources.getInstance().fontSmallColor;
		labelMessage = new Label(this.message, style);
		labelMessage.setWidth(background.getWidth()-90);
		labelMessage.setHeight(150);
		labelMessage.setPosition(background.getX() + (background.getWidth() / 2) - labelMessage.getWidth()/2,
				background.getY() + (background.getHeight() / 2) - labelMessage.getHeight()/2);
		labelMessage.setAlignment(Align.center, Align.center);
		
		TextButtonStyle styleBtn = new TextButtonStyle();
		styleBtn.up = buttonSkin.getDrawable("menuButtonBackgroundBigNormal");
		styleBtn.font = Resources.getInstance().fontNormalColor;
       
        buttonOK = new TextButton("ok", styleBtn); //** Button text and style **//
        buttonOK.setWidth(150f);
        buttonOK.setPosition((viewport.getWorldWidth() / 2) -  (background.getWidth() / 3), (viewport.getWorldHeight() / 2) - (background.getHeight() / 2)  +40);
//        buttonOK.addListener(listener);
        
        buttonCancel = new TextButton("cancel", styleBtn); //** Button text and style **//
        buttonCancel.setWidth(150f);
        buttonCancel.setPosition((viewport.getWorldWidth() / 2), (viewport.getWorldHeight() / 2) - (background.getHeight() / 2)  + 40);
        buttonCancel.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float posX, float posY, int pointer, int button) {
                    Gdx.app.log("ScreenUIPopup", "Notification closed.");
                    lokusTheGame.setScreen(fromScreen);
        			dispose();
                    return true;
            }
        });
	}

	public void setListener(InputListener listener){
		buttonOK.addListener(listener);
	}
	/**
	 * creates labels and button for the popup
	 */
	@Override
	public void show() {
		

		stage.addActor(background);
        stage.addActor(labelTitle);
        stage.addActor(labelMessage);
        stage.addActor(buttonOK);
        stage.addActor(buttonCancel);
        
        Gdx.input.setInputProcessor(stage);
	}

	/**
	 * makes an transparent background
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 0);
		Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFuncSeparate(
        		GL20.GL_DST_COLOR, 
        		GL20.GL_SRC_COLOR,
        		GL20.GL_ONE,
        		GL20.GL_ONE);
		
		//on escape, leave the message
		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {	
			Gdx.app.log("ScreenUIPopup", "Notification closed.");
			lokusTheGame.setScreen(fromScreen);
			dispose();
		}		
		stage.draw();
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		stage.dispose();
	}	
}