package view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class GameOverListener extends InputListener{
	private ScreenGame screen;
	private ScreenUIPopup popup;
	private DefaultScreen fromScreen;
	private Sound backgroundMusic;
	public GameOverListener(ScreenGame screen, ScreenUIPopup popup, DefaultScreen fromScreen){
		super();
		screen.pauseBgMusic();
		backgroundMusic = Gdx.audio.newSound(Gdx.files.internal("res/sfx/odeToJoy.mp3"));
		backgroundMusic.play(screen.lokusTheGame.bgVolume*1.45f);
		this.screen = screen;	
		this.popup = popup;
		this.fromScreen = fromScreen;
	}
	public boolean touchDown(InputEvent event, float posX, float posY, int pointer, int button){
		Gdx.app.log("ScreenUIPopup", "Notification closed.");
		backgroundMusic.dispose();
		popup.dispose();
        screen.lokusTheGame.setScreen(fromScreen);
        return true;
	}
}
