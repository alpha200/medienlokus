package view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * DefaultScreen has declarations for all screen used in game implementation per screen classes
 * @author sopr049
 */

public abstract class DefaultScreen implements Screen{

	/* lokusTheGame has the instance of the game */
	public LokusTheGame lokusTheGame;
	/* camera has the drawing focus on rendering objects */
	protected Camera camera;
	/* mode deliveres native resolution of client monitor */
	protected DisplayMode mode;
	/* viewport has the camera and let the user show small viewport area only */
	protected Viewport viewport;
	/* stage is a container for drawing options */
	protected Stage stage;
	
	/**
	 * DefaultScreen constructor requires the instance of the game and sets the screen up
	 * @param game instance of the game
	 */
	public DefaultScreen(LokusTheGame game)	{
		
		this.lokusTheGame = game;
		camera = new OrthographicCamera();
		mode = LwjglApplicationConfiguration.getDesktopDisplayMode();
		viewport = new FitViewport(game.DISPLAY_WIDTH, game.DISPLAY_HEIGHT, camera);
		stage = new Stage(viewport);
	}
	
	/**
	 * resize changes viewport area with required width and height
	 * @param width, height of the new viewport area
	 */
	public void resize(int width, int height)	{
		stage.getViewport().update(width, height);
	}
	
	/**
	 * clearScreen deletes all objects on stage and sets black background with opacity 1
	 */
	public void clearScreen()	{
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}
	
	/**
	 * hide stops rendering the background
	 */
	public void hide() {
		Gdx.gl.glClearColor(0,0,0,0);
	}
	
	/**
	 * pause implementation is custom
	 */
	public abstract void pause();
	
	/**
	 * resume implementation is custom
	 */
	public abstract void resume();
	
	/**
	 * dispose deletes the instances of internal objects
	 */
	public abstract void dispose();
	
}
