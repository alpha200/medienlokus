package view;

import aurelienribon.tweenengine.TweenAccessor;
import com.badlogic.gdx.graphics.Color;

/**
 * UIBrickTween defines modification options on single UIBrick object
 * @author sopr049
 *
 */

public class UIBrickTween implements TweenAccessor<UIBrick>{
	
	/* changes absolute position */
	public static final int POS_XY = 1;
	/* changes relative position */
    public static final int CPOS_XY = 2;
    /* changes scaling (both x and y coordinate) */
    public static final int SCALE_XY = 3;
    /* changes rotation */
    public static final int ROTATION = 4;
    /* sets alpha value of object */
    public static final int OPACITY = 5;
    /* sets tint value */
    public static final int TINT = 6;

    /**
     * getValues gets the tween animation values from the object
     * @param target as UIBrick object
     * @param tweenType as modification option
     * @param returnValues as dict of object values
     * @return number of object values returned, else -1
     */
    @Override
    public int getValues(UIBrick target, int tweenType, float[] returnValues) {
    		// changes applied only to selected modification option
            switch (tweenType) {
                    case POS_XY:
                            returnValues[0] = target.getX();
                            returnValues[1] = target.getY();
                            return 2;

                    case CPOS_XY:
                            returnValues[0] = target.getX() + target.getWidth()/2;
                            returnValues[1] = target.getY() + target.getHeight()/2;
                            return 2;

                    case SCALE_XY:
                            returnValues[0] = target.getScaleX();
                            returnValues[1] = target.getScaleY();
                            return 2;

                    case ROTATION: returnValues[0] = target.getRotation(); return 1;
                    case OPACITY: returnValues[0] = target.getColor().a; return 1;

                    case TINT:
                            returnValues[0] = target.getColor().r;
                            returnValues[1] = target.getColor().g;
                            returnValues[2] = target.getColor().b;
                            return 3;

                    default: assert false; return -1;
            }
    }

    /**
     * setValues sets the tween animation values to the object
     * @param target as UIBrick object
     * @param tweenType as modification option
     * @param newValues as dict of new values
     */
    @Override
    public void setValues(UIBrick target, int tweenType, float[] newValues) {
    		// changes applied only to selected modification option
            switch (tweenType) {
                    case POS_XY: target.setPosition(newValues[0], newValues[1]); break;
                    case CPOS_XY: target.setPosition(newValues[0] - target.getWidth()/2, newValues[1] - target.getHeight()/2); break;
                    case SCALE_XY: target.setScale(newValues[0], newValues[1]); break;
                    case ROTATION: target.setRotation(newValues[0]); break;

                    case OPACITY:
                            Color color = target.getColor();
                            color.set(color.r, color.g, color.b, newValues[0]);
                            target.setColor(color);
                            break;

                    case TINT:
                    		color = target.getColor();
                    		color.set(newValues[0], newValues[1], newValues[2], color.a);
                            target.setColor(color);
                            break;

                    default: assert false;
            }
    }
}
