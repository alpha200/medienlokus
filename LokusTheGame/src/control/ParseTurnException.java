package control;

public class ParseTurnException extends Exception {
	private static final long serialVersionUID = 1L;

	public ParseTurnException() {
	}

	public ParseTurnException(String message) {
		super(message);
	}
}
