package control;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;

import model.Game;

public class IOController {

	private GameController gameController;
	
	public IOController(GameController gamecontroller){
		this.gameController = gamecontroller;
	}
	
	/**
     * @param currentSession 	The game that should be serialized.
     * @return boolean			True if successfully.
     * @throws IOException 		If the configuration file could not be created/loaded/closed.
     */
	public boolean saveGame(final Game currentSession) throws NullPointerException, IOException {
        if (currentSession == null) {
            throw new NullPointerException("Error : The parameters are not initialized!");
        }
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final ObjectOutput outputObj = new ObjectOutputStream(outputStream);
        byte[] exportArray;
        new File("output").mkdir();
        try {
            outputObj.writeObject(currentSession);
            exportArray = outputStream.toByteArray();
            final FileOutputStream fileStream = new FileOutputStream("output/config.ser");
            fileStream.write(exportArray);
            fileStream.close();
        } catch (IOException exception) {
            throw new IOException("Error : The configuration file could not be created/loaded!");
        } finally {
            try {
                outputObj.close();
                outputStream.close();
            } catch (IOException exception) {
                throw new IOException("Error : The configuration file could not be closed!");
            }
        }

        return true;
	}
	
	/**
     * @return Game
     * @throws IOException            If the configuration file could not be created/loaded/closed.
     * @throws ClassNotFoundException If the objects implement no serializeable interface.
     */
	public Game loadGame() throws NullPointerException, IOException, ClassNotFoundException {
        final byte[] inputBuff;
        try {
            //noinspection ResultOfMethodCallIgnored
            inputBuff = Files.readAllBytes(FileSystems.getDefault().getPath("output/config.ser"));
        } catch (IOException exception) {
            throw new IOException("Error : The configuration file could not be loaded!");
        }
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputBuff);
        ObjectInput inputObj = null;
        Game game = null;
        try {
            inputObj = new ObjectInputStream(inputStream);
            game = (Game) inputObj.readObject();
        } catch (ClassNotFoundException exception) {
            throw new ClassNotFoundException("Error : This object implements no serializeable interface!");
        } catch (IOException exception) {
            throw new IOException("Error : The content of the configuration file could not be loaded!");
        } finally {
            try {
                inputStream.close();
                if (inputObj != null) {
                    inputObj.close();
                }
            } catch (IOException exception) {
                throw new IOException("Error : The configuration file could not be closed!");
            }
        }
        gameController.setGame(game);
        return game;
	}
}
