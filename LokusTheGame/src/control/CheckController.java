package control;

import java.awt.Point;

import model.Block;
import model.Brick;
import model.Player;
import model.Turn;

public class CheckController {

	public CheckController() {

	}

	/**
	 * Überprüft ob der übergebene Zug gültig ist und gibt true zurück, wenn der
	 * Zug gültig ist, sonst false.
	 * 
	 * @param turn
	 *            Der Zug der ausgeführt werden soll.
	 * @param field
	 *            Das aktuelle Spielfeld.
	 * @return true wenn der Spielzug gültig ist.
	 */
	public boolean checkTurn(Turn turn, Block[][] field) {
		Player player = turn.getPlayer();
		Block ownColor = player.getColor();
		Point coord = turn.getCoord();
		Brick brick = turn.getBrick();
		boolean[][] brickCoord = brick.getCoord();

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (brickCoord[i][j] == false) {
					continue;
				} else {
					int fieldX = (int) (coord.getX() + (i - 2));
					int fieldY = (int) (coord.getY() + (j - 2));
					Point pField = new Point(fieldX, fieldY);
					try {
						// Feld bereits belegt?
						if (field[fieldX][fieldY] != Block.EMPTY) {
							if (field[fieldX][fieldY] != null) {
								return false;
							}
						}
						// angrenzende Felder von eigener Farbe?
						if (isNextToOwnColor(field, ownColor, pField))
							return false;

					} catch (IndexOutOfBoundsException e) {
						throw new IndexOutOfBoundsException(
								"Block liegt außerhalb des Spielfeldes!");
					}
				}
			}

		}
		return isPossiblePositon(field, turn);
	}

	/**
	 * Returns whether the Block position is Diagonal to own Color or if its a
	 * Start Positon of the color.
	 * 
	 * @param field
	 *            The Game-Board
	 * @param turn
	 *            the requested Turn
	 * 
	 * @return true if requested Brick is diagonal to block of own color
	 * @return startPos if requested Brick is not diagonal to a Block of own
	 *         color
	 */
	private boolean isPossiblePositon(Block[][] field, Turn turn) {
		Player player = turn.getPlayer();
		Block ownColor = player.getColor();
		Point coord = turn.getCoord();
		Brick brick = turn.getBrick();

		boolean startPos = false;
		for (Point cornerBlock : brick.getCornerBlocks()) {
			int fieldX = (int) (coord.getX() + (cornerBlock.getX() - 2));
			int fieldY = (int) (coord.getY() + (cornerBlock.getY() - 2));
			Point pField = new Point(fieldX, fieldY);
			// liegt eine Ecke Diagonal zu einem eigenen Block?
			if (isDiagonalToOwnColor(field, ownColor, pField))
				return true;
			int xMax = field.length - 1;
			int yMax = field[0].length - 1;
			// liegt eine Ecke auf der StartPostition?
			//oben Links
			boolean startPosBlue = fieldX == 0 && fieldY == 0
					&& ownColor == Block.RED;
			//oben Rechts
			boolean startposYellow = fieldX == xMax && fieldY == 0
					&& ownColor == Block.GREEN;
			//unten Rechts
			boolean startPosRed = fieldX == xMax && fieldY == yMax
					&& ownColor == Block.BLUE;
			//unten Links
			boolean startPosGreen = fieldX == 0 && fieldY == yMax
					&& ownColor == Block.YELLOW;
			if (startPosBlue || startposYellow || startPosRed || startPosGreen)
				startPos = true;
		}
		return startPos;
	}

	/**
	 * Checks if there is a block of own color diagonal to the requested block
	 * 
	 * @param field
	 *            The game-board
	 * @param ownColor
	 *            color of the player which wants to set a brick
	 * @param fieldX
	 *            X-coordinate of the field where the player wants to place a
	 *            brick
	 * @param fieldY
	 *            y-coordinate of the field where the player wants to place a
	 *            brick
	 * 
	 * @return true if there is a diagonal block of own color
	 */
	private boolean isDiagonalToOwnColor(Block[][] field, Block ownColor,
			Point pField) {
		int fieldX = (int) pField.getX();
		int fieldY = (int) pField.getY();
		boolean isDiagonal = false;
		try {
			if (field[fieldX + 1][fieldY + 1] == ownColor) {
				isDiagonal = true;
			}
		} catch (IndexOutOfBoundsException ex) {

		}
		try {
			if (field[fieldX - 1][fieldY - 1] == ownColor) {
				isDiagonal = true;
			}
		} catch (IndexOutOfBoundsException ex) {

		}
		try {
			if (field[fieldX + 1][fieldY - 1] == ownColor) {
				isDiagonal = true;
			}
		} catch (IndexOutOfBoundsException ex) {

		}
		try {
			if (field[fieldX - 1][fieldY + 1] == ownColor) {
				isDiagonal = true;
			}
		}

		catch (IndexOutOfBoundsException ex) {

		}
		return isDiagonal;
	}

	/**
	 * Checks whether there is a block of own color next to requested brick
	 * 
	 * @param field
	 *            The game-board
	 * @param ownColor
	 *            color of the player which wants to set a brick
	 * @param fieldX
	 *            X-coordinate of the field where the player wants to place a
	 *            brick
	 * @param fieldY
	 *            y-coordinate of the field where the player wants to place a
	 *            brick
	 * 
	 * @return true if there is a block of own color next to it
	 */
	private boolean isNextToOwnColor(Block[][] field, Block ownColor,
			Point pField) {
		int fieldX = (int) pField.getX();
		int fieldY = (int) pField.getY();
		boolean isNext = false;
		try {
			if (field[fieldX + 1][fieldY] == ownColor) {
				isNext = true;
			}
		} catch (IndexOutOfBoundsException ex) {

		}
		try {
			if (field[fieldX - 1][fieldY] == ownColor) {
				isNext = true;
			}
		} catch (IndexOutOfBoundsException ex) {

		}
		try {
			if (field[fieldX][fieldY + 1] == ownColor) {
				isNext = true;
			}
		} catch (IndexOutOfBoundsException ex) {

		}
		try {
			if (field[fieldX][fieldY - 1] == ownColor) {
				isNext = true;
			}
		} catch (IndexOutOfBoundsException ex) {

		}
		return isNext;

	}
}