package control;

import java.awt.Point;

import model.AIValueCalculator;
import model.Block;
import model.Brick;
import model.CalculationHelpers;
import model.FieldNode;
import model.FirstAIValueCalculator;
import model.Game;
import model.GameState;
import model.Player;
import model.ShrinkedBrick;
import model.ShrinkedPlayer;
import model.ShrinkedTurn;
import model.Turn;

public class CortanaKIController extends KIController {

	

	private Player player;
	private AIValueCalculator aiValueCalculator;
	private int pointsEarned;
	private int ownNo;
	
	
	/**
	 * Konstruktor
	 * @param game
	 * @param kiPlayer
	 */
	public CortanaKIController(Game game, Player kiPlayer) {
		super(game, kiPlayer);
		ShrinkedPlayer[] players = new ShrinkedPlayer[game.getPlayers().length];
		for(int i=0; i< game.getPlayers().length;i++){
			ShrinkedPlayer play = new ShrinkedPlayer(game.getPlayers()[i].getColor());
			players[i]= play;

		} 
		ShrinkedPlayer aiPlayer = new ShrinkedPlayer(kiPlayer.getColor());

		player = new Player(aiPlayer);
		aiValueCalculator = new FirstAIValueCalculator();
		pointsEarned = 0;
		if(player.getColor()==Block.BLUE){
			ownNo = 0;
		}
		if(player.getColor()==Block.YELLOW){
			ownNo = 1;
		}
		if(player.getColor()==Block.RED){
			if(game.getPlayers().length==2){
				ownNo=1;
			}
			else{
				ownNo = 2;
			}
		}
		if(player.getColor()==Block.GREEN){
			ownNo = 3;
		}
		
	}
	
	
	public int getPointsEarned() {
		return pointsEarned;
	}


	public AIValueCalculator getAiValueCalculator() {
		return aiValueCalculator;
	}



	/**
	 * Berechnet den aktuell besten Zug. Bewertet wird die Größe und die Position des gelegten Steins. 
	 * @param actualGame
	 * @return ShrinkedTurn
	 */
	private Turn giveBestTurn(){

		Player clonePlayer = new Player(game.getPlayers()[ownNo]);
		player = new Player(clonePlayer);
		Player[] players = new Player[game.getPlayers().length];
		for(int i=0; i< game.getPlayers().length;i++){
			Player play = new Player(game.getPlayers()[i]);
			players[i]= play;

		} 

		Turn bestTurn;

		Turn[] allTurns = KIController.calculatePossibleTurns(game.getField(), player);
		for(int i=0;i< allTurns.length;i++){

			allTurns[i].setAiValue(aiValueCalculator.ratePossibleTurn(allTurns[i], game.getField()));
		}

		bestTurn = giveMaxTurn(allTurns);
		pointsEarned += bestTurn.getBrick().getValue();
		return bestTurn;

		}
	
	private Turn giveMaxTurn(Turn[] turn){
		double max = turn[0].getAiValue();
		Turn maxTurn = turn[0];
		for(int i=1;i<turn.length;i++){
			if(turn[i].getAiValue()>max){
				max=turn[i].getAiValue();
				maxTurn = turn[i];
			}
		}
		return maxTurn;
	}

	@Override
	public Turn calculateTurn() {
		Turn best;
		Turn bestShrinked = giveBestTurn();
		best = new Turn(bestShrinked);
		return best;
	}
	/**
	 * DummyMethode
	 */
	@Override
	public void transmitTurn(Turn turnDone) {
		
	}
}
