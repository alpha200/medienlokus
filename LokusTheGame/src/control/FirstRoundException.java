package control;

public class FirstRoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public FirstRoundException(String string) {
		 super(string);
	}

}
