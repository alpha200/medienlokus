package control;

import java.awt.Point;
import java.util.List;

import edu.udo.cs.sopra.tools.gameturnexchanger.client.GteGameNotRunningException;
import model.Block;
import model.Brick;
import model.Game;
import model.Mode;
import model.Player;
import model.Turn;

public class TurnController {

	private GameController gameController;
	private CheckController checkController;

	public TurnController(GameController gameController) {
		this.gameController = gameController;
		checkController = new CheckController();
	}

	/**
	 * Macht eine ganze Spielrunde rückgägngig. Legt die jeweils letzten
	 * gelegten Blöcke auf die Hand der SPieler zurück und entfernt sie vom
	 * Spielfeld.
	 * 
	 * @throws FirstRoundException
	 *             In der ersten Spielrunde ist es nicht möglich etwas
	 *             rueckgängig zu machen.
	 */
	public void undoTurn() throws FirstRoundException {

		Game currentGame = gameController.getGame();
		List<Turn> turnsList = currentGame.getTurns();
		int playerCount = 0;
		if(gameController.getGame().getMode() == Mode.SOLITAIR || gameController.getGame().getMode() == Mode.SOLITAIRBIG){
			playerCount = 1;
		}
		else{
			playerCount = currentGame.getPlayers().length;
		}
		Turn lastTurn;
		if (turnsList.size() < playerCount) {
			throw new FirstRoundException(
					"Rückgängig machen ist in der ersten Runde nicht möglich!");
		}
		while (playerCount > 0) {
			lastTurn = turnsList.get(turnsList.size() - 1);
			if (lastTurn.getBrick() != null) {
				lastTurn.getPlayer().getBricks().add(lastTurn.getBrick());
				removeBrickFromField(lastTurn);
			}
			turnsList.remove(lastTurn);
			playerCount--;
		}

	}

	/**
	 * Does the requested Turn if possible
	 * 
	 * @param turn
	 *            the requested turn
	 * @param field
	 *            the game-board
	 * 
	 */
	public boolean doTurn(Turn turn, Block[][] field, boolean sendTurn){
			try {
				if(turn.getBrick() == null){
					Game current = gameController.getGame();
					current.getTurns().add(turn);
					
					if(sendTurn)
						gameController.formatTurn(turn);
					
					return true;
				}
				
				if (checkController.checkTurn(turn, field)) {
					Game current = gameController.getGame();
					current.getTurns().add(turn);

					Player player = turn.getPlayer();
					Point coord = turn.getCoord();
					Brick brick = turn.getBrick();
					int pointX = (int) coord.getX();
					int pointY = (int) coord.getY();
					// TODO Brick auf liste?
					for (int i = 0; i < 5; i++) {
						for (int j = 0; j < 5; j++) {
							if (brick.getCoord()[i][j]) {
								Block color = player.getColor();
								field[pointX + (i - 2)][pointY + (j - 2)] = color;
							}
						}

					}
					player.getBricks().remove(brick);
					if(sendTurn)
					{
						try {
							gameController.formatTurn(turn);
						} 
						catch (GteGameNotRunningException e) {
							
						}
					}
					for(KIController kiController : gameController.getkIController()) {
						if (kiController != null) {
							kiController.transmitTurn(turn);
						}
					}
					gameController.getLokusTheGame().screenGame.updateTimeForNextPlayer(true,turn.getPlayer());
					return true;
					
				}
				return false;
			
			} catch (IndexOutOfBoundsException e) {
				return false;
			}
			catch (GteGameNotRunningException e)
			{
				return false;
			}
	}

	/**
	 * Entfernt einen Block vom Spielfeld.
	 * 
	 * @param turn
	 *            Beinhaltet die Position des Blockes.
	 */
	protected void removeBrickFromField(Turn turn) {
		Game currentGame = gameController.getGame();
		Block[][] field = currentGame.getField();
		Point coord = turn.getCoord();
		Brick brick = turn.getBrick();
		boolean[][] brickCoord = brick.getCoord();

		int pointX = (int) coord.getX();
		int pointY = (int) coord.getY();

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (brickCoord[i][j] == false)
					continue;
				else {
					int currx = pointX + (i - 2);
					int curry = pointY + (j - 2);
					field[currx][curry] = Block.EMPTY;
				}
			}
		}
	}
	//TODO flag fürs cheaten (undo oder hint)
	/**
	 * chooses which parameter for score calculation should be used 
	 * @return	scores for single or multiplayer
	 */
	public int[] gameScore() {
		Mode gameMode = gameController.getGame().getMode();
		if (gameMode.equals(Mode.SOLITAIR) || gameMode.equals(Mode.SOLITAIRBIG)) {
			return gameScoreSolo();
		}
		return gameScoreMultiplayer();
	}
	/**
	 * calculates the score for singleplayer mode
	 * @return	scores	the scores for each color
	 */
	private int[] gameScoreSolo() {
		int minScore = -(5*4); // oder Anzahl der Bloecke aller 4er-Steine einer Farbe
		if (gameController.getGame().getMode().equals(Mode.SOLITAIRBIG))
			minScore = -(1 + 2 + 2*3 + 5*4 + 12*5); // oder Anzahl der Bloecke aller Standard-Steine
		int[] scores = {minScore, minScore, minScore, minScore};
		Block[][] field = gameController.getGame().getField();
		scores = addScores(scores, field);
		return scores;
	}

	/**
	 * calculates the score for multiplayer mode
	 * @return	scores	the scores for each color
	 */
	private int[] gameScoreMultiplayer() {
		int minScore = -(1 + 2 + 2*3 + 5*4 + 12*5); // oder Anzahl der Bloecke aller Standard-Steine
		int[] scores = {minScore, minScore, minScore, minScore};
		Block[][] field = gameController.getGame().getField();
		scores = addScores(scores, field);
		for(int player = 0; player < 4; player++) {
			if (scores[player] == 0) {
				scores[player] = 15;		// score == 0 -> Spieler ist alle Steine losgeworden (15 Bonuspunkte)
				List<Turn> turns = gameController.getGame().getTurns();
				for (int i = turns.size() - 1; i >= 0; i--) {
					if (turns.get(i).getBrick() != null && turns.get(i).getPlayer().getColor().equals(blockAtIndex(player))) {
						int count = 0;
						for (boolean[] line : turns.get(i).getBrick().getCoord()) {
							for (boolean isBlock : line) {
								if (isBlock) {
									count++;
								}
							}
						}
						if (count == 1)		// Letzter gesetzter Stein hatte einen Block: 20 Bonuspunkte
							scores[player] += 20;
						break;
					}
				}
			}
		}
		return scores;
	}
	
	/**
	 * iterates over the field and adds +1 for every block to the score of a color
	 * @param scores	the base scores array
	 * @param field		the game-board	
	 * @return	scores	the modified scores array
	 */
	private int[] addScores(int[] scores, Block[][] field) {
		for (Block[] line : field) {
			for (Block block : line) {
				switch (block) {
				case BLUE:
					scores[0] += 1;
					break;
				case YELLOW:
					scores[1] += 1;
					break;
				case RED:
					scores[2] += 1;
					break;
				case GREEN:
					scores[3] += 1;
					break;
				default:
				}
			}
		}
		if (gameController.getGame().getMode().equals(Mode.BLOKUSDUO))
			scores[1] = scores[2];
		return scores;
	}
	
	
	private Block blockAtIndex(int index) {
		switch (index) {
		case 0: return Block.BLUE;
		case 1: return Block.YELLOW;
		case 2: return Block.RED;
		case 3: return Block.GREEN;
		default: return Block.EMPTY;
		}
	}
}
