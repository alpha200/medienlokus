package control;

import model.Game;
import model.Player;
import model.Turn;

import java.awt.Point;
import java.util.Random;

public class SVoiceKIController extends KIController {

    public SVoiceKIController(Game game, Player kiPlayer) {
        super(game, kiPlayer);
    }

    @Override
	public Turn calculateTurn() {
        Turn[] possibleTurns = calculatePossibleTurns(game.getField(), kiPlayer);

        if (possibleTurns.length == 0)
        	return new Turn(kiPlayer, null, new Point(0, 0));
        
        int randomTurnNumber = new Random().nextInt(possibleTurns.length);
        return possibleTurns[randomTurnNumber];
	}

    @Override
    public void transmitTurn(Turn turnDone) {

    }
}
