package control;

import model.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class GoogleNowKIController extends KIController {

	private Turn currentBestTurn;
	private Player kiPlayer;
	private final Semaphore available = new Semaphore(1, true);
    private Socket socket;
    private ObjectOutputStream objectOutputStream;
    private boolean fuckTheKI = false;

    CheckController ck = new CheckController();

    private KIController fallBackKI;

	public GoogleNowKIController(Game game, Player kiPlayer) {
		super(game, kiPlayer);

        if(kiPlayer.getColor() == Block.RED || kiPlayer.getColor() == Block.GREEN) {
            fuckTheKI = true;
        }

        fallBackKI = new CortanaKIController(game, kiPlayer);

		this.kiPlayer = kiPlayer;
		this.currentBestTurn = null;
		try {
			available.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

        try {
            if(!fuckTheKI) {
                socket = new Socket("plutonium.cs.tu-dortmund.de", 13742);
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                Thread thread = new Thread(new TurnWaiter(new ObjectInputStream(socket.getInputStream())));
                thread.start();
                objectOutputStream.writeObject(new ShrinkedPlayer(kiPlayer));
            }
            else {
                socket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	@Override
	public Turn calculateTurn() {

        try {
			if(!fuckTheKI && available.tryAcquire(9400, TimeUnit.MILLISECONDS)) {
                if(ck.checkTurn(currentBestTurn, game.getField())) {
                    return currentBestTurn;
                }
                else {
                    fuckTheKI = true;
                    System.err.println("Invalid turn!");

                    CalculationHelpers.printBrick(currentBestTurn.getBrick().getCoord());
                    System.out.println("coords: " + currentBestTurn.getCoord());

                    return fallBackKI.calculateTurn();
                }
            }
            else {
                System.err.println("KI not responding -> Using fallback!");
                fuckTheKI = true;
                return fallBackKI.calculateTurn();
            }
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
	}

    @Override
	public void transmitTurn(Turn turnDone) {
        try {
            if(!fuckTheKI) {
                objectOutputStream.writeObject(new ShrinkedTurn(turnDone));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class TurnWaiter implements Runnable {
        ObjectInputStream objectInputStream;

        public TurnWaiter(ObjectInputStream objectInputStream) {
            this.objectInputStream = objectInputStream;
        }

        @Override
        public void run() {
            while(true) {
                try {
                    Object inputObject = objectInputStream.readObject();

                    if(inputObject instanceof ShrinkedTurn) {
                        Turn transmittedTurn = new Turn((ShrinkedTurn)inputObject);
                        if(kiPlayer.equals(transmittedTurn.getPlayer())) {
                            currentBestTurn = transmittedTurn;
                            available.release();
                        }
                    }
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }
}
