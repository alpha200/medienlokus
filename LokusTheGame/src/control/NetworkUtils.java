package control;


import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Vector;

import edu.udo.cs.sopra.tools.gameturnexchanger.client.GameTurnExchangerClient;
import edu.udo.cs.sopra.tools.gameturnexchanger.client.GteGameNotRunningException;

public class NetworkUtils extends Thread{

	public boolean isNetworkEnabled;
	private boolean isGameStarted;
	private Vector<String> msgQueue;
	private GameTurnExchangerClient networkModule;
	private boolean isDebug = false;
	private int portTCP = 22222;
	private int portUDP = 22223;
	private String name;
	
	public NetworkUtils() {
		super();
		isNetworkEnabled = false;
		isGameStarted = false;
		try {
			name = InetAddress.getLocalHost().getHostName() + (Math.random() % 30);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		msgQueue = new Vector<String>();
	}
	
	@Override 
	public void run() {
		//consume messages
		String msgNetwork = null;
		
		while(isNetworkEnabled) {
			do {
				if(!isNetworkEnabled) {
					return;
				}
				msgNetwork = networkModule.pollGameTurn();
				try {
					Thread.sleep(10);
				} catch(InterruptedException ex) {}
			} while(msgNetwork == null);
			
			msgQueue.addElement(msgNetwork);
		}
	}
	
	public boolean isNetworkEnabled() {
		return isNetworkEnabled;
	}

	public boolean isGameStarted() {
		return isGameStarted;
	}
	
	public void sendMessage(String msg) {
		if(isNetworkEnabled) {
			try {
				networkModule.sendGameTurn(msg);
			} catch (NullPointerException | GteGameNotRunningException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void sendGameOver() {
		if(isNetworkEnabled && networkModule.isGameRunning()) {
			try {
				networkModule.sendGameOver();
			} catch (NullPointerException | GteGameNotRunningException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public String getMessage(){
		while(msgQueue.isEmpty()) {
			try {
				Thread.sleep(10);
			} catch(InterruptedException e) {}
		}
		
		String tmpMsg = msgQueue.firstElement();
		msgQueue.removeElementAt(0);
		return tmpMsg;
	}
	
	public void close() {
		isNetworkEnabled = false;
		networkModule.dispose();
	}
	
	public void open() {
		if(isDebug) {
			GameTurnExchangerClient.enableDevelopmentMode();
			networkModule = GameTurnExchangerClient.createTestClient(portTCP, portUDP, name);
		}
		else {
			networkModule = new GameTurnExchangerClient();
		
		}
		isNetworkEnabled = true;
		
		this.start();
	}
}
