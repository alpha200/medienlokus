package control;

public class EmptyStringException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmptyStringException() {

    }

    public EmptyStringException(String string) {
        super(string);
    }
}
