package control;

import java.awt.Point;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import view.LokusTheGame;
import model.Brick;
import model.Game;
import model.Mode;
import model.Player;
import model.Turn;
import edu.udo.cs.sopra.tools.gameturnexchanger.client.GameTurnExchangerClient;
import edu.udo.cs.sopra.tools.gameturnexchanger.client.GteGameNotRunningException;


public class GameController {
	
		public final int DEBUG_PORT_TCP = 37648;
		public final int DEBUG_PORT_UDP = 47329;
		
        private TurnController turnController;

        private IOController iOController;

        private KIController[] kIController;
       
        private CheckController checkController;
       
        private HighScoreController highscoreController;

        private Game game;

        public NetworkUtils exchanger;
		private LokusTheGame lokusTheGame;
       
        public GameController(LokusTheGame loki) {
                turnController = new TurnController(this);
                iOController = new IOController(this);
                kIController = new KIController[4];
                checkController = new CheckController();
                highscoreController = new HighScoreController();
                lokusTheGame = loki;
                exchanger = new NetworkUtils();
        }


		public GameController() {
            turnController = new TurnController(this);
            iOController = new IOController(this);
            kIController = new KIController[4];
            checkController = new CheckController();
            highscoreController = new HighScoreController();
            exchanger = new NetworkUtils();
		}

        /**
         * Startet ein neues Spiel.
         * @param players Spieler die mitspielen sollen.
         * @param gameMode GameMode in dem gespielt werden soll.
         */
        public void startNewGame(Player[] players, Mode gameMode) {
                game = new Game(players, gameMode);
        }

        /**
         * Parses a turn from GameTurnExchanger and converts it to a Turn value for local use.
         *
         * @return the parsed and converted Turn from GameTurnExchanger, or null if no turn is avaiable.
         * @throws GteGameNotRunningException
         * @throws ParseTurnException
         */
        public Turn parseTurn() throws GteGameNotRunningException, ParseTurnException {
        		if (exchanger == null || exchanger.isGameStarted())
                        throw new GteGameNotRunningException();
                Turn parsedTurn = null;
                
                String input = exchanger.getMessage();
				
                if (input != null) {                	
                    String[] tokens = input.split("[ |]");
                       
                    Player player = parsePlayer(tokens);
                       
                    if (tokens[1].equals("passe")) {
                    	return new Turn(player, null, new Point(0, 0));
                    }
                       
                    Point[] coords = parsePoints(tokens);
                      
                    Point min = getMinimum(coords);
                       
                    Brick brick = getBrickFrom(coords, min);
                    //TODO: KEINE AHNUNG OB DAS STIMMT
                    parsedTurn = new Turn(player, brick, new Point((int) min.getX()+1, (int) min.getY()+1));

                }
                return parsedTurn;
        }
       
        /**
         * Parses the Player from the input tokens, using the colour the player has at the current Game.
         *
         * @param tokens - Parsing input from parseTurn()
         * @return the Player that corresponds to the Turn
         * @throws ParseTurnException
         */
        private Player parsePlayer(String[] tokens) throws ParseTurnException {
                Player player;
                switch(tokens[0]) {
                case "blau":
                		//if(game.getPlayers().length < 1) { throw new ParseTurnException("Invalid Player colour " + tokens[0]); 	}
                		player = game.getPlayers()[0];
                        break;
                case "gelb":
                		//if(game.getPlayers().length < 3) { throw new ParseTurnException("Invalid Player colour " + tokens[0]); }
                        player = game.getPlayers()[1];
                        break;
                case "rot":
                		//if(game.getPlayers().length < 3) { throw new ParseTurnException("Invalid Player colour " + tokens[0]); }   
                		if(game.getPlayers().length == 2) {
                			player = game.getPlayers()[1];
                		}
                		else {
                			player = game.getPlayers()[2];
                		}
                        break;
                case "gruen":
                		//if(game.getPlayers().length < 4) { throw new ParseTurnException("Invalid Player colour " + tokens[0]); }
                        player = game.getPlayers()[3];
                        break;
                default:
                        throw new ParseTurnException("Invalid Player colour " + tokens[0]);
                }
                return player;
        }
       
        /**
         * Parses the field points of the Brick used in the parsed Turn.
         *
         * @param tokens - Parsing input from parseTurn()
         * @return Point[5] with Brick coordinates in the playground (not needed trailing elements are null)
         */
        private Point[] parsePoints(String[] tokens) {
                Point[] coords = new Point[5];
                int xCoord = -1;
                int yCoord = -1;
                int coordIndex = 0;
                for (String str : tokens) {
                        if (str.equals(""))
                                continue;
                        try {
                                if (xCoord == -1)
                                        xCoord = Integer.parseInt(str);
                                else
                                        yCoord = Integer.parseInt(str);
                        } catch (NumberFormatException e) {
                                continue;
                        }
                        if (yCoord != -1) {
                                coords[coordIndex] = new Point(xCoord, yCoord);
                                xCoord = yCoord = -1;
                                coordIndex++;
                                if (coordIndex >= coords.length)
                                        break;
                        }
                }
                return coords;
        }

        /**
         * Calculates the minimum x- and y-value contained in all the points given to the method.
         *
         * @param coords - Array of Points
         * @return One Point containing the minimum x- and y-value
         */
        private Point getMinimum(Point[] coords) {
                int minX = Integer.MAX_VALUE;
                int minY = Integer.MAX_VALUE;
                for (Point point : coords) {
                        if (point == null)
                                break;
                        if (minX > (int) point.getX())
                                minX = (int) point.getX();
                        if (minY > (int) point.getY())
                                minY = (int) point.getY();
                }
                return new Point(minX, minY);
        }

        /**
         * Generates a Brick from the parsing input from parseTurn()
         *
         * @param coords - Array of Points the Brick uses
         * @param min - Point with upper left coordinates the Brick uses
         * @return a Brick specified by its coordinates and relative position
         */
        private Brick getBrickFrom(Point[] coords, Point min) {
                boolean[][] brick = new boolean[5][5];
                for(Point point : coords) {
                        if (point == null)
                                break;
                        brick[((int) point.getX()) - (int) min.getX()][((int) point.getY()) - (int) min.getY()] = true;
                }
                return new Brick(brick);
        }

        /**
         * Sends a Turn to the GameTurnExchanger by formatting it into a String the GameTurnExchanger requires.
         *
         * @param turn - Turn to be sent to the GameTurnExchanger
         * @throws GteGameNotRunningException if the Game is not connected to a GameTurnExchanger
         * @throws NullPointerException if turn is null
         */
        public void formatTurn(Turn turn) throws GteGameNotRunningException {
        		if (turn == null)
                        throw new NullPointerException();
                if (exchanger == null)
                        throw new GteGameNotRunningException();
                String formattedTurn = "";
                switch (turn.getPlayer().getColor()) {
                case BLUE:
                	formattedTurn += "blau";
                	break;
                case RED:
                	formattedTurn += "rot";
                	break;
                case YELLOW:
                	formattedTurn += "gelb";
                	break;
                case GREEN:
                	formattedTurn += "gruen";
                	break;
            	default:
            		formattedTurn += "Fehler!";
            		return;
                }
                if (turn.getBrick() == null) {
                        formattedTurn += " passe";
                        exchanger.sendMessage(formattedTurn);
                        return;
                }
                for (int i = 0; i < 5; i++) {
                        for (int j = 0; j < 5; j++) {
                                if (turn.getBrick().getCoord()[i][j]) {
                                        formattedTurn += " | " + ((int) turn.getCoord().getX() + i-1) + " " + ((int) turn.getCoord().getY() + j-1);
                                }
                        }
                }
                if(lokusTheGame != null)	{
                	lokusTheGame.screenGame.getField().updateField();
                }
                exchanger.sendMessage(formattedTurn);
                
        }

        /**
         * This method calculates the current players and return this player
         *
         * @return Player
         */
        public Player getCurrentPlayer() {
                Player[] players = game.getPlayers();
                List<Turn> turns = game.getTurns();
                return players[turns.size() % players.length];
        }
        public void setGame(Game game){
                this.game = game;
        }
        public Game getGame() {
                return game;
        }
        
        
        public LokusTheGame getLokusTheGame() {
			return lokusTheGame;
		}
		
		
		
		/**
		 * Disposes the client interface because we lost the connection and 
		 * are going to create a new one.
		 */
		public void disposeClientInterface() {
			if (exchanger == null) {
				throw new IllegalStateException("exchanger == null");
			}
		}

        public TurnController getTurnController() {
                return turnController;
        }

        public IOController getiOController() {
                return iOController;
        }

        public KIController[] getkIController() {
                return kIController;
        }

        public CheckController getCheckController() {
                return checkController;
        }

        public HighScoreController getHighscoreController() {
                return highscoreController;
        }
        
        public void dispose() {
        	disposeClientInterface();
        }

}
  