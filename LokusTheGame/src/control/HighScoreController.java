package control;

import model.Mode;
import model.Player;
import model.PlayerType;
import model.Score;

import java.awt.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class HighScoreController {

	private List<Score> standardScores = new LinkedList<Score>();
	
	private List<Score> blokusDuoScores = new LinkedList<Score>();
	
	private List<Score> solitairScores = new LinkedList<Score>();

	private List<Score> solitairBigScores = new LinkedList<Score>();
	
	private List<Score> customScores = new LinkedList<Score>();
	
	private List<List<Score>> scores;
	
	public HighScoreController(){
		//SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy");
		//testwerte
//		solitairBigScores.add(new Score(42,"Dieter","11.09.15"));
//		
//		solitairScores.add(new Score(42,"Dieter","11.09.15"));
//		solitairScores.add(new Score(21,"Otto","11.09.15"));
//		
//		blockusDuoScores.add(new Score(1,"Dieter","11.09.15"));
//		blockusDuoScores.add(new Score(42,"Johann","11.09.15"));
//		blockusDuoScores.add(new Score(30,"Joseph","11.09.15"));
//		
//		standardScores.add(new Score(35,"Hermann",formatter.format(new Date()),12));
//		standardScores.add(new Score(35,"Fridolin","11.09.15",13));
//		standardScores.add(new Score(35,"Hugo","11.09.15",14));
//		standardScores.add(new Score(35,"Fred","11.09.15",12));
//		standardScores.add(new Score(20,"Markus","11.09.15",8));
//		standardScores.add(new Score(40,"Alwin","11.09.15",9));
//		standardScores.add(new Score(42,"Rottraud","11.09.15"));
//		standardScores.add(new Score(92,"Torge","11.09.15"));
//		standardScores.add(new Score(82,"Gisela","11.09.15"));
//		standardScores.add(new Score(34,"Henriette","11.09.15"));
//		standardScores.add(new Score(55,"Waltraud","11.09.15"));
//		standardScores.add(new Score(66,"Friederike","11.09.15"));
//		standardScores.add(new Score(77,"Kunigunde","11.09.15"));
//		standardScores.add(new Score(88,"Albert","11.09.15"));
	}

	/**
	 * calculates the position where the score has to be add
	 * @param scores
	 * @param score
	 * @param low
	 * @param high
	 * @return the position of the score in the list
	 */
	private int posToAddScore(List<Score> scores, Score score, Point borders){
		if(scores.size() == 0) return 0;
		int low = (int)borders.getX();
		int high = (int)borders.getY();
		if(low == high){
			return low;
		}
		if(score.getPoints()>scores.get((high+low)/2).getPoints()){
			return posToAddScore(scores, score, new Point(low,(high+low)/2));
		}
		else{
			return posToAddScore(scores, score, new Point(((high+low)/2+1), high));
		}
	}
	
	/**
	 * A new Score will be created and added to the current highscore list
	 * 
	 * @param points
	 * @param player
	 * @throws IllegalArgumentException
	 * @throws EmptyStringException
	 * @throws NullPointerException
	 */
	public void addHighScore(int points, Player player, Mode gameMode) {
		String emptyString ="";
		//TODO Negativpunkte sollten erlaubt sein (anleitung)
		//if(points < 0) throw new IllegalArgumentException("The points are incorrect!");
		if(player == null) throw new NullPointerException("Please enter a player name!");
		if(player.getName() == emptyString) throw new EmptyStringException("The player name field is empty!");
		if(player.getCheaterFlag())
		{
			return;
		}
		if(!player.getPlayerType().equals(PlayerType.PLAYER))
		{
			return;
		}
		//define Date
		//TODO Zeit tatsächlich notwendig? sieht komisch aus
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd - HH:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy");
		Score score = new Score(points, player.getName(), formatter.format(new Date()), player.getTotalTime());
		switch(gameMode)
		{
		case STANDARD:
			standardScores.add(posToAddScore(standardScores, score, new Point(0, standardScores.size())), score);
			break;
		case SOLITAIR:
			solitairScores.add(posToAddScore(solitairScores, score, new Point(0, solitairScores.size())), score);
			break;
		case SOLITAIRBIG:
			solitairBigScores.add(posToAddScore(solitairBigScores, score, new Point(0, solitairBigScores.size())), score);
			break;
		case BLOKUSDUO:
			blokusDuoScores.add(posToAddScore(blokusDuoScores, score, new Point(0, blokusDuoScores.size())), score);
			break;
		case CUSTOM:
			customScores.add(posToAddScore(customScores, score, new Point(0, customScores.size())), score);
			break;
		}
	}

	@SuppressWarnings("unchecked")
	/**
	 * This method loads the highscore list
	 * @throws NullPointerException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void loadHighscore() throws NullPointerException, IOException, ClassNotFoundException {
        final byte[] inputBuff;
        try {
            //noinspection ResultOfMethodCallIgnored
            inputBuff = Files.readAllBytes(FileSystems.getDefault().getPath("output/score.ser"));
        } catch (IOException exception) {
            throw new IOException("Error : The configuration file could not be loaded!");
        }
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(inputBuff);
        ObjectInput inputObj = null;
        List<List<Score>> scores = null;
        try {
            inputObj = new ObjectInputStream(inputStream);
            scores = (List<List<Score>>) inputObj.readObject();
        } catch (ClassNotFoundException exception) {
            throw new ClassNotFoundException("Error : This object implements no serializeable interface!");
        } catch (IOException exception) {
            throw new IOException("Error : The content of the configuration file could not be loaded!");
        } finally {
            try {
                inputStream.close();
                if (inputObj != null) {
                    inputObj.close();
                }
            } catch (IOException exception) {
                throw new IOException("Error : The configuration file could not be closed!");
            }
        }
        //this.scores = scores;
        this.standardScores.addAll(scores.get(0));
        this.blokusDuoScores.addAll(scores.get(1));
        this.solitairScores.addAll(scores.get(2));
        this.solitairBigScores.addAll(scores.get(3));
        this.customScores.addAll(scores.get(4));
	}

	/**
	 * This method saves the highscore list to open it after a restart of the game
	 * @param scores
	 * @throws NullPointerException
	 * @throws IOException
	 */
	public void saveHighScore() throws NullPointerException, IOException {
		scores = new LinkedList<List<Score>>();
		scores.add(standardScores);
		scores.add(blokusDuoScores);
		scores.add(solitairScores);
		scores.add(solitairBigScores);
		scores.add(customScores);
        if (scores == null) {
            throw new NullPointerException("Error : The parameters are not initialized!");
        }
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final ObjectOutput outputObj = new ObjectOutputStream(outputStream);
        byte[] exportArray;
        new File("output").mkdir();
        try {
            outputObj.writeObject(scores);
            exportArray = outputStream.toByteArray();
            final FileOutputStream fileStream = new FileOutputStream("output/score.ser");
            fileStream.write(exportArray);
            fileStream.close();
        } catch (IOException exception) {
            throw new IOException("Error : The configuration file could not be created/loaded!");
        } finally {
            try {
                outputObj.close();
                outputStream.close();
            } catch (IOException exception) {
                throw new IOException("Error : The configuration file could not be closed!");
            }
        }
	}
	
	public List<Score> getHighscore(Mode gameMode){
		List<Score> listToReturn = null;
		switch(gameMode)
		{
		case BLOKUSDUO:
			listToReturn =  blokusDuoScores;
			break;
		case CUSTOM:
			listToReturn =  customScores;
			break;
		case SOLITAIR:
			listToReturn =  solitairScores;
			break;
		case SOLITAIRBIG:
			listToReturn =  solitairBigScores;
			break;
		case STANDARD:
			listToReturn =  standardScores;
			break;
		}
		
		return listToReturn;
	}
	
	public void setStandardScores(List<Score> standardScores) {
		this.standardScores = standardScores;
	}

	public void setBlokusDuoScores(List<Score> blokusDuoScores) {
		this.blokusDuoScores = blokusDuoScores;
	}

	public void setSolitairScores(List<Score> solitairScores) {
		this.solitairScores = solitairScores;
	}

	public void setSolitairBigScores(List<Score> solitairBigScores) {
		this.solitairBigScores = solitairBigScores;
	}

	public void setCustomScores(List<Score> customScores) {
		this.customScores = customScores;
	}
}