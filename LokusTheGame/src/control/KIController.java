package control;

import model.*;

import java.awt.Point;
import java.util.*;

public abstract class KIController {

    public abstract Turn calculateTurn();
    public abstract void transmitTurn(Turn turnDone);

    protected final Game game;
    protected final Player kiPlayer;

    /**
     * Calculates all corners blocks, where you could lay a brick
     * @param field the current field
     * @param blockColor the players color
     * @return list of all corners, where you could lay bricks
     */
    public static List<Point> calculateCorners(Block[][] field, Block blockColor) {
        List<Point> corners = new ArrayList<>();

        if(field[0][0] != blockColor
                && field[0][field[0].length-1] != blockColor
                && field[field.length-1][0] != blockColor
                && field[field.length-1][field[field.length-1].length-1] != blockColor) {

        	switch (blockColor) {
        	case GREEN:
                corners.add(new Point(field.length -1, 0));
                break;
        	case RED:
                corners.add(new Point(0, 0));
                break;
        	case YELLOW:
                corners.add(new Point(0, field[0].length-1));
                break;
        	case BLUE: default:
                corners.add(new Point(field.length -1, field[field.length-1].length-1));
                break;
            }
        }
        else {
            for (int i = 0; i < field.length; i++) {
                for (int j = 0; j < field[i].length; j++) {
                    if (field[i][j] == Block.EMPTY) {
                        // Check if there is no collision with own color at top, bottom, left, right
                        if (    (i - 1 < 0 || field[i - 1][j] != blockColor)
                                && (i + 1 >= field.length || field[i + 1][j] != blockColor)
                                && (j - 1 < 0 || field[i][j - 1] != blockColor)
                                && (j + 1 >= field[i].length || field[i][j + 1] != blockColor)) {
                            // Check if there is at least one own color diagonal
                            if ((i - 1 >= 0 && j - 1 >= 0 && field[i - 1][j - 1] == blockColor)
                                    || (i - 1 >= 0 && j + 1 < field[i - 1].length && field[i - 1][j + 1] == blockColor)
                                    || (i + 1 < field.length && j - 1 >= 0 && field[i + 1][j - 1] == blockColor)
                                    || (i + 1 < field.length && j + 1 < field[i + 1].length && field[i + 1][j + 1] == blockColor)) {
                                corners.add(new Point(i, j));
                            }
                        }
                    }
                }
            }
        }

        return corners;
    }

    /**
     * @param field the current field
     * @param player the current player
     * @return possible turns
     */
    public static Turn[] calculatePossibleTurns(Block[][] field, Player player) {
        Block color = player.getColor();
        List<Turn> possibleTurns= new ArrayList<>();
        List<Point> corners = calculateCorners(field, color);
        List<Brick> brickSet;
       
        brickSet = player.getBricks();

        for (Brick currentBrick : brickSet) {
            Set<Brick> brickForms = getBrickInAllForms(currentBrick);

            for (Brick currentForm : brickForms) {
                for (Point currentCorner : corners) {
                    for (Point currentCornerBlock : currentForm.getCornerBlocks()) {
                    	Point currentPoint = new Point(currentCorner.x - currentCornerBlock.x + 2, currentCorner.y - currentCornerBlock.y + 2);
                    	Turn turn = new Turn(player, currentForm, currentPoint);
                        if(simpleCheckTurn(field, turn)) {
                        	possibleTurns.add(turn);
                        }
                    }
                }
            }
        }

        return possibleTurns.toArray(new Turn[possibleTurns.size()]);
    }
    
    /**
     * Checks if the given turn is a valid turn
     * @param field the field
     * @param turn the turn
     * @return true if the turn is valid
     */

    private static boolean simpleCheckTurn(Block[][] field, Turn turn) {

        boolean[][] brickCoord = turn.getBrick().getCoord();
        Block playerColor = turn.getPlayer().getColor();

        for (int i = 0; i < brickCoord.length; i++) {
            for (int j = 0; j < brickCoord[i].length; j++) {
                // Position nur relevant, falls dort ein Block ist
                if(brickCoord[i][j]) {
                    int calculatedXCoord = turn.getCoord().x - 2 + i;
                    int calculatedYCoord = turn.getCoord().y - 2 + j;

                    // Falls Block des aktuellen Stein außerhalb des Spielfelds liegen wuerde false returnen
                    if(calculatedXCoord < 0 || calculatedYCoord < 0 || calculatedXCoord >= field.length || calculatedYCoord >= field[calculatedXCoord].length) {
                        return false;
                    }

                    // Falls auf dem Feld an der Stelle schon ein Block vorhanden ist abbrechen
                    if(field[calculatedXCoord][calculatedYCoord] != Block.EMPTY) {
                        return false;
                    }

                    // Überprüfen, ob nicht bereits ein eigener Stein direkt angrenzt
                    if((calculatedXCoord - 1 >= 0 && field[calculatedXCoord-1][calculatedYCoord] == playerColor)
                            || (calculatedXCoord + 1 < field.length && field[calculatedXCoord+1][calculatedYCoord] == playerColor)
                            || (calculatedYCoord -1 >= 0 && field[calculatedXCoord][calculatedYCoord-1] == playerColor)
                            || (calculatedYCoord +1 < field[calculatedXCoord].length && field[calculatedXCoord][calculatedYCoord+1] == playerColor)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Rotates and mirrors Brick in all directions
     * @param currentBrick the brick
     * @return list of all possible forms
     */
    protected static Set<Brick> getBrickInAllForms(Brick currentBrick) {
        Brick alreadyClonedBrick = new Brick(currentBrick);

        Set<Brick> brickForms = new HashSet<>();
        for (int i = 0; i < 4; i++) {
            brickForms.add(new Brick(alreadyClonedBrick));
            brickForms.add(new Brick(alreadyClonedBrick).mirrorBrick());
            alreadyClonedBrick.rotateBrick();
        }

        return brickForms;
    }
    
    public Turn transmitEnemyTurn(){
    	return null;
    }

    /**
     * Default constructor
     * @param game a reference to the current game
     * @param kiPlayer the player to control
     */
    public KIController(Game game, Player kiPlayer) {
        this.game = game;
        this.kiPlayer = kiPlayer;
    }
}
