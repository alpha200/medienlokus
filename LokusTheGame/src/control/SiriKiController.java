package control;

import java.util.Arrays;

import model.AIValueCalculator;
import model.Block;
import model.Game;
import model.GameState;
import model.Player;
import model.ShrinkedPlayer;
import model.ShrinkedTurn;
import model.SiriAIValueCalculator;
import model.Turn;

public class SiriKiController extends KIController {

	private Player player;

	private Turn currentBest;
	private int numberOfPlayers;
	private int turnCount;
	private AIValueCalculator aiValueCalculator;
	private int ownNo;
	
	/**
	 * Konstruktor
	 * @param game
	 * @param kiPlayer
	 */
	public SiriKiController(Game game, Player kiPlayer) {
		super(game, kiPlayer);
		player = new Player(kiPlayer);
		turnCount = 0;
		aiValueCalculator = new SiriAIValueCalculator();
		numberOfPlayers = game.getPlayers().length;
		ShrinkedPlayer[] players = new ShrinkedPlayer[numberOfPlayers];
		for(int i=0; i< game.getPlayers().length;i++){
			ShrinkedPlayer play = new ShrinkedPlayer(game.getPlayers()[i].getColor());
			players[i]= play;

		}
		if(player.getColor()==Block.BLUE){
			ownNo = 0;
		}
		if(player.getColor()==Block.YELLOW){
			ownNo = 1;
		}
		if(player.getColor()==Block.RED){
			if(game.getPlayers().length==2){
				ownNo=1;
			}
			else{
				ownNo = 2;
			}
		}
		if(player.getColor()==Block.GREEN){
			ownNo = 3;
		}
		
	}

	/**
	 * Methode um den besten Zug in einer Ebene zu finden.
	 * @param game
	 * @param playerNo
	 * @return
	 */
	private Turn findBestTurn(Game game ,int playerNo){
		//Variablen
		int maxValue;
		Turn actualTurn;
		Game actualGame = new Game(game);
		
		ShrinkedPlayer player;
		ShrinkedPlayer[] players = new ShrinkedPlayer[numberOfPlayers];
		Turn[] allPossibleTurns= KIController.calculatePossibleTurns(actualGame.getField(), actualGame.getPlayers()[playerNo]);
		for(int i=0; i<game.getPlayers().length;i++){
			player = new ShrinkedPlayer(game.getPlayers()[i]);
			players[i] = new ShrinkedPlayer(player);
		}
		
		for(int i=0;i<allPossibleTurns.length;i++){
			allPossibleTurns[i].setAiValue(aiValueCalculator.ratePossibleTurn(allPossibleTurns[i], actualGame.getField()));
		}
		//Besten Zug finden
		actualTurn = giveMaxTurn(allPossibleTurns);

		//Um doTurn zu Simulieren
//		GameState dummyGame = new GameState(actualGame.getField(), players);
//		GameState currentGame = new GameState(dummyGame);
		maxValue = (int) aiValueCalculator.ratePossibleTurn(actualTurn, actualGame.getField());
		actualTurn.setAiValue(maxValue);
		return actualTurn;
	}
	
	/**
	 * Rechnet ein Spiel zu ende um die mögliche Punktzahl zu ermitteln wenn als
	 * erstes der i-beste Zug genommen wird gefolgt von den 
	 * besten von allen Spielern.
	 * @param field
	 * @param turn
	 * @param playerNumber
	 * @return value wird der Wert des Zuges berechnet anhand von verschieden Faktoren.
	 */
//	private int calculateGame(Block[][] field, Turn turn, int playerNumber){
//		//PseudoSpielfeld für Zugsimulation
//		ShrinkedPlayer player;
//		ShrinkedTurn currentTurn = new ShrinkedTurn(turn);
//		ShrinkedPlayer[] players = new ShrinkedPlayer[numberOfPlayers];
//		for(int i=0; i<numberOfPlayers;i++){
//			player = new ShrinkedPlayer(game.getPlayers()[i]);
//			players[i] = new ShrinkedPlayer(player);
//		}
//		GameState dummyGame = new GameState(field, players);
//		GameState currentGame = new GameState(dummyGame);
//		currentGame.doTurn(currentTurn);
//		Player nextPlayer = new Player(players[(playerNumber+1)%numberOfPlayers]);
//		// Simulation zuende
//		Turn[] turns = KIController.calculatePossibleTurns(currentGame.getField(), nextPlayer);
//		if(turns.length!=0){
//			for(Turn xs : turns){
//				xs.setAiValue(aiValueCalculator.rateMidGame(xs, currentGame.getField()));
//			}
//			Turn newTurn = giveMaxTurn(turns);
//			if(playerNumber ==ownNo){
//				return turn.getBrick().getValue()+calculateGame(currentGame.getField(), newTurn, (playerNumber+1)%numberOfPlayers);
//			}
//			else{
//				return 0+ calculateGame(currentGame.getField(), newTurn, (playerNumber+1)%numberOfPlayers);
//			}
//		}
//		else{
//			if(playerNumber == ownNo){
//				return 0;
//			}
//			else{
//				return 0+calculateGame(field, turn, (playerNumber+1)%numberOfPlayers);
//			}
//		}
//	}
	
	private Turn giveMaxTurn(Turn[] turn){
		double max = turn[0].getAiValue();
		Turn maxTurn = turn[0];
		for(int i=1;i<turn.length;i++){
			if(turn[i].getAiValue()>max){
				max=turn[i].getAiValue();
				maxTurn = turn[i];
			}
		}
		return maxTurn;
	}
	
	/**
	 * Methode für den GameController um einen Zug anzufordern.
	 */
	@Override
	public Turn calculateTurn() {		
		int i=1; //Zähler für allpossibles
		long usedTime = System.currentTimeMillis();
		Game actual =  new Game(game);
		Turn[] allpossibles = KIController.calculatePossibleTurns(game.getField(), kiPlayer);
		Turn bestCurrent = findBestTurn(actual, ownNo);
		Turn possibleTurn;
		
		//Damit der 1er Stein als Letztes gelegt wird.
		if(bestCurrent.getBrick().getStandardbricknr()==1&& allpossibles.length>1){
			bestCurrent.setAiValue(0);
		}
		//5er Eckblock ist als erstes sehr sinnvoll
		if(bestCurrent.getBrick().getStandardbricknr()==14 && aiValueCalculator.ratePossibleTurn(bestCurrent, game.getField())==1){
			return bestCurrent;
		}
		
		turnCount = game.getTurns().size();
		Arrays.sort(allpossibles);
		while(System.currentTimeMillis()-usedTime < 8000 && i<allpossibles.length){
				//Da Arrays.sort das Feld aufsteigend Sortiert wird am ende angefangen
				possibleTurn = allpossibles[allpossibles.length-1-(i)];
				//Um sicher zu gehen das der 1er Block nicht gelegt wird
				if(possibleTurn.getBrick().getStandardbricknr()==1 && allpossibles.length>1){
					i++;
				}
				else{
					//2. Runde
					if((turnCount>=4) &&turnCount<8&& possibleTurn.getBrick().getValue()==5){
						if(aiValueCalculator.ratePossibleTurn(possibleTurn, game.getField())>=4){
							return possibleTurn;
						}
					}
					//3.Runde
					if((turnCount>=8) &&turnCount<12&& possibleTurn.getBrick().getValue()==5){
						if(aiValueCalculator.ratePossibleTurn(possibleTurn, game.getField())==7){
							return possibleTurn;
						}
					}
					//4.Runde
					if((turnCount>=12) &&turnCount<16&& possibleTurn.getBrick().getValue()==5){
						if(aiValueCalculator.ratePossibleTurn(possibleTurn, game.getField())==10){
							return possibleTurn;
						}
					}
					//5.Runde
					if((turnCount>=16)&&turnCount<20&&possibleTurn.getBrick().getValue()==5 ){
						if(aiValueCalculator.ratePossibleTurn(possibleTurn, game.getField())==10){
							return possibleTurn;
						}
					}
					//Ab Runde 6
					if(turnCount >16){
						CortanaKIController cortana = new CortanaKIController(actual, player);
						return cortana.calculateTurn();
					}
				}
				i++;
				
		}
		currentBest = new Turn(bestCurrent);
		return currentBest;
	}
	/** Dummy-Methode
	 * 
	 */
	@Override
	public void transmitTurn(Turn turnDone) {

	}

}
